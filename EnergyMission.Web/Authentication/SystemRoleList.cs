﻿using System.Collections.Generic;
using EnergyMission.Common.Security;

namespace EnergyMission.Web.Authentication
{
    /// <summary>
    ///     Class containing all the roles within the system
    /// </summary>
    public static class SystemRoleList
    {
        #region Public Members

        public static Dictionary<SystemRoleKindEnum, ApplicationRole> SystemRoles
        {
            get
            {
                Dictionary<SystemRoleKindEnum, ApplicationRole> result = new Dictionary<SystemRoleKindEnum, ApplicationRole>
                {
                    {
                        SystemRoleKindEnum.Administrator, new ApplicationRole()
                        {
                            Name = "Administrator",
                            Description = "Administrator"
                        }
                    },
                    {
                        SystemRoleKindEnum.Client, new ApplicationRole()
                        {
                            Name = "Client",
                            Description = "Client"
                        }
                    },
                    {
                        SystemRoleKindEnum.None, new ApplicationRole()
                        {
                            Name = "None",
                            Description = "None"
                        }
                    },
                    {
                        SystemRoleKindEnum.SuperAdministrator, new ApplicationRole()
                        {
                            Name = "Super Administrator",
                            Description = "Super Administrator"
                        }
                    },
                    {
                        SystemRoleKindEnum.TeamManager, new ApplicationRole()
                        {
                            Name = "Team Manager",
                            Description = "Team Manager"
                        }
                    },
                    {
                        SystemRoleKindEnum.TeamMember, new ApplicationRole()
                        {
                            Name = "Team Member",
                            Description = "Team Member"
                        }
                    }
                };

                return result;
            }
        }

        #endregion
    }
}