﻿using System.Web;
using EnergyMission.Web.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EnergyMission.Web.Authentication
{
    /// <summary>
    ///     Helper class that can be used within views to access the Identity framework
    ///     for security information.
    /// </summary>
    public static class RequestSecurityContextHelper
    {
        #region Public Members

        public static ApplicationRoleManager AppRoleManager
        {
            get
            {
                return HttpContext.Current
                                  .GetOwinContext()
                                  .Get<ApplicationRoleManager>();
            }
        }

        public static ApplicationUserManager AppUserManager
        {
            get
            {
                return HttpContext.Current
                                  .GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>();
            }
        }


        /// <summary>
        /// Determines whether the user is authenticated
        /// </summary>
        /// <returns>Returns TRUE should the user be logged in.</returns>
        public static bool UserAuthenticated()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        #endregion
    }
}