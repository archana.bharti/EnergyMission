﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace EnergyMission.Web.Authentication
{
    /// <summary>
    /// Concrete implementation of the roles recognised within the system
    /// </summary>
    public class ApplicationRole : IdentityRole
    {
        #region Public Members

        public ApplicationRole() : base()
        {
        }

        public ApplicationRole(string aName) : base(aName)
        {
        }

        public string Description { get; set; }

        #endregion
    }
}