﻿using System.Security.Claims;
using System.Threading.Tasks;
using EnergyMission.Domain.Entities;
using Microsoft.AspNet.Identity;
using EnergyMission.Data;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations.Schema;
namespace EnergyMission.Web.Authentication
{
    /// <summary>
    ///     Encapsulation of all the users in the system.
    /// </summary>
    /// <remarks>
    ///     See http://go.microsoft.com/fwlink/?LinkID=317594 about adding additional properties for more detail
    /// </remarks>
    public class ApplicationUser : IdentityUser
    {
        #region Public Members

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> aManager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            ClaimsIdentity userIdentity = await aManager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        //Contains all the profile information specific to the users in the application
        public long? UserProfileInformation_Id { get; set; }
        [ForeignKey("UserProfileInformation_Id")]
        public virtual UserProfileInformationEntity  UserProfileInformations { get; set; }
       
        #endregion
    }
}