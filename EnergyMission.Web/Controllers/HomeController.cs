﻿using System.Web.Mvc;
using EnergyMission.Web._Framework;
using System;

namespace EnergyMission.Web.Controllers
{
    public class HomeController : EmControllerBase
    {
        /// <summary>
        ///     Main index page for the site
        /// </summary>
        /// <returns>home</returns>
        public ActionResult Index()
        {
            ViewBag.PageTitle = "Dashboard";
            string value = Convert.ToString(TempData["chk"]);
            if (value =="1")
                return RedirectToPrevious();
            else
            return View();
        }


    
    
    
    

    }
}