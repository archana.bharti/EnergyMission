﻿using System;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using Abp.Domain.Uow;
using EnergyMission.Client;
using EnergyMission.Client.Dtos;
using EnergyMission.ClientAdministrator;
using EnergyMission.Common.Security;
using EnergyMission.Domain.Entities;
using EnergyMission.Person;
using EnergyMission.Person.Dtos;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels.SystemAdministration;
using EnergyMission.Web._Framework;
using Microsoft.AspNet.Identity;

namespace EnergyMission.Web.Controllers
{
    public class SystemAdministrationController : EmControllerBase
    {
        private readonly IClientAdministratorAppService _ClientAdministratorAppService;
        private readonly IClientAppService _ClientAppService;
        private readonly IPersonAppService _PersonAppService;

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientAppService"></param>
        /// <param name="aClientAdministratorAppService"></param>
        /// <param name="aPersonAppService"></param>
        public SystemAdministrationController(IClientAppService aClientAppService, IClientAdministratorAppService aClientAdministratorAppService, IPersonAppService aPersonAppService)
        {
            PageTitle = "System Administration";

            _ClientAppService = aClientAppService;
            _ClientAdministratorAppService = aClientAdministratorAppService;
            _PersonAppService = aPersonAppService;
        }

        /// <summary>
        ///     Returns a view to allow the system administrator to setup a new client.
        /// </summary>
        /// <returns></returns>
        public ActionResult ClientSetup()
        {
            PageTitle = "Client Setup";

            return View();
        }

        /// <summary>
        ///     Displays a form to allow the user to setup the client and administrator
        ///     person for the client
        /// </summary>
        /// <returns>Returns a view that allows for a new client to be creted</returns>
        public ActionResult CreateNewClient()
        {
            PageTitle = "Add New Client";

            CreateNewClientViewModel viewModel = new CreateNewClientViewModel();

            return View(viewModel);
        }

        /// <summary>
        ///     Adds a new client to the system and sets the administrator.
        /// </summary>
        /// <param name="aViewModel">Information to add to the database</param>
        /// <returns>Returns the user either to the client setup page or redisplays the form on error.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateNewClient(CreateNewClientViewModel aViewModel)
        {
            if (ModelState.IsValid)
            {
                IUnitOfWorkManager unitOfWorkManager = IocManager.Instance.Resolve<IUnitOfWorkManager>();

                try
                {
                    GetPersonOutput person = _PersonAppService.GetPerson(new GetPersonInput {Id = aViewModel.ClientAdminPersonId});

                    using (IUnitOfWorkCompleteHandle unitOfWork = unitOfWorkManager.Begin())
                    {
                        CreateNewClientWithAdministratorInput paramCreateNewClient = new CreateNewClientWithAdministratorInput
                        {
                            ClientAdminPersonId = aViewModel.ClientAdminPersonId,
                            ClientName = aViewModel.ClientName
                        };

                        CreateNewClientWithAdministratorOutput createNewClientResult = (_ClientAppService.CreateNewClientWithAdminstrator(paramCreateNewClient));
                        if (createNewClientResult.Success)
                        {
                            //Add the login record should we not already have one for this person
                            ApplicationUser personAppUser = AppUserManager.Users
                                                                          .FirstOrDefault(r => r.UserProfileInformation.PersonId == aViewModel.ClientAdminPersonId);

                            //See if we already have this user in the system
                            if (personAppUser == null)
                            {
                                //User is not in the system so add them
                                ApplicationUser appUser = new ApplicationUser
                                {
                                    Email = person.ContactDetail.EmailAddress,
                                    UserName = person.ContactDetail.EmailAddress,
                                    UserProfileInformation = new UserProfileInformationEntity {ClientId = createNewClientResult.NewClientId, PersonId = aViewModel.ClientAdminPersonId}
                                };

                                IdentityResult createUserResult = AppUserManager.Create(appUser, aViewModel.ClientAdminPassword);
                                if (!createUserResult.Succeeded)
                                {
                                    string errors = string.Empty;
                                    foreach (string error in createUserResult.Errors)
                                    {
                                        errors += error + "\n";
                                    }
                                    throw new Exception(errors);
                                }

                                IdentityResult addToRoleResult = AppUserManager.AddToRole(appUser.Id, SystemRoleList.SystemRoles.First(r => r.Key == SystemRoleKindEnum.Client)
                                                                                                                    .Value.Name);
                                if (!addToRoleResult.Succeeded)
                                {
                                    string errors = string.Empty;
                                    foreach (string error in addToRoleResult.Errors)
                                    {
                                        errors += error + "\n";
                                    }
                                    throw new Exception(errors);
                                }
                            }
                            else
                            {
                                //User is already in the system so:
                                //- Associate them with this client
                                //- Ensure they have the client role
                                personAppUser.UserProfileInformation.ClientId = createNewClientResult.NewClientId;
                                personAppUser.Roles.Clear();
                                AppUserManager.Update(personAppUser);

                                IdentityResult addToRoleResult = AppUserManager.AddToRole(personAppUser.Id, SystemRoleList.SystemRoles.First(r => r.Key == SystemRoleKindEnum.Client)
                                                                                                                          .Value.Name);
                                if (!addToRoleResult.Succeeded)
                                {
                                    string errors = string.Empty;
                                    foreach (string error in addToRoleResult.Errors)
                                    {
                                        errors += error + "\n";
                                    }
                                    throw new Exception(errors);
                                }
                            }
                        }
                        else
                        {
                            string errors = string.Empty;
                            foreach (string error in createNewClientResult.Errors)
                            {
                                errors += error + "\n";
                            }
                            throw new Exception(errors);
                        }

                        unitOfWork.Complete();

                        StatusMessageSuccess("Successfully added new client administrator");

                        return RedirectToPrevious();
                    }
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("CreateNewClient", aViewModel);
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}