﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Client;
using EnergyMission.Client.Dtos;
using EnergyMission.Domain.Entities;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using EnergyMission.Data.Repositories;
using Newtonsoft.Json;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels.Client;
using EnergyMission.Web._Framework;
using System.Linq;
namespace EnergyMission.Web.Controllers
{
    public class organizationsController : EmControllerBase
    {
        #region Private Members

        private readonly IRepository<Data.Organization> _organizationAppService;
   
        /// <summary>
        ///     Helper method to retrieve all the clients.
        /// </summary>
        /// <returns>Returns a list of clients</returns>
        private List<OrganizationEntity> RetrieveAllClients()
        {





            //if(loggedInUser.UserProfileInformation.ClientId!=null)


            //    return Mapper.Map<List<ClientEntity>>(_ClientAppService.GetClients(loggedInUser.UserProfileInformation.ClientId));
            //else

            ApplicationUser loggedInUser= AppUserManager.GetLoggedInUser();
         var Id = loggedInUser.UserProfileInformations.ClientId.Value;
           var value= _organizationAppService.Where(tt => tt.ClientId == Id && tt.IsDeleted == false);
            List<OrganizationEntity> org = new List<OrganizationEntity>();
            if (value == null)
                return org;
            else
                return Mapper.Map<List<OrganizationEntity>>(value);



            //GetClientsInput getClientsInput = new GetClientsInput();
            //return _ClientAppService.GetClients(getClientsInput)
            //                        .Clients;
        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientAppService">Application service for clients</param>
        public organizationsController()
        {

            ViewBag.PageTitle = "Clients";

            _organizationAppService = new Repository<Data.Organization>();
        
        }

        /// <summary>
        ///     Called when we want to CREATE a client in the database.
        /// </summary>
        /// <param name="aClient">Information about the client to create.</param>
        /// <returns>Returns gridview on success, create client view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Createev(OrganizationEntity aClient)
        {
            if (ModelState.IsValid)
            {
                try
                {
                  
                    if(aClient.Id==null)
                    {



                    }
                    _organizationAppService.Insert(Mapper.Map<Data.Organization>(aClient));



                  
                  
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("error",e.Message);
                }
            }
         
            return View("Index", aClient);
        }

        /// <summary>
        ///     Called when we want to DELETE the client.
        /// </summary>
        /// <param name="Id">Unique identifier for the client record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public  ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                    _organizationAppService.Delete(Id);
        
                }
                catch (Exception e)
                {

                    ModelState.AddModelError("error", e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>


        public JsonResult getclients(string Prefix)
        {
      ApplicationUser loggedInUser= AppUserManager.GetLoggedInUser();
         var Id = loggedInUser.UserProfileInformations.ClientId.Value;
       var value= _organizationAppService.Where(tt => tt.ClientId == Id && tt.Name.Contains(Prefix)).OrderBy(tt=>tt.SuperAdminEmail).Select(tt => tt.SuperAdminEmail);

       return Json(value, JsonRequestBehavior.AllowGet);




        }

        public JsonResult  Edits(long Id)
        {
            ViewBag.PageTitle = "Edit Client";
            IQueryable<Data.Organization> SSB = _organizationAppService.SelectAll1();

            var res = (from sub in SSB
                       where sub.Id == Id
                       select new
                       {
                           id = sub.Id,
                           stateid = sub.StateId,
                           name = sub.Name,
                           noe=sub.NOE,
                           postcodeid=sub.PostCodeId,
                           city=sub.City,
                           address=sub.Address,
                           clientid=sub.ClientId,
                           SuperAdminEmail=sub.SuperAdminEmail,
                           Geo_location_Latitude=sub.Geo_location_Latitude,
                           Geo_location_Longditude = sub.Geo_location_Longditude

                       }
                                       );



   //var x = from i in _ClientAppService.GetClientById(Id)
   //                 select new { result.StateId, result.Id, result.Name, result.NOE, result.SuperAdminEmail, result.PostCodeId, result.City, result.Address, result.Geo_location_Latitude, result.Geo_location_Longditude};
            return Json(res, JsonRequestBehavior.AllowGet);
            //return View("Edit", _ClientAppService.GetClientById(Id));
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public  ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllClients());
        }

        /// <summary>
        ///     Main view to display all the clients in the system.
        /// </summary>
        /// <returns>Returns the view for all the clients in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {

            if (TempData["msg"] != null)
            {

                ViewBag.msg = TempData["msg"];
            }
            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var Id = loggedInUser.UserProfileInformations.ClientId.Value;
            ViewBag.client = Id;
            return View(RetrieveAllClients());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new client to be added.</returns>
        /// <summary>
        ///     Called when a country needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aClient">Information about the client to be updated.</param>
        /// <returns>Returns the list of clients on success, error on failure.</returns>
        ///   [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create()
        {
            return View();
        }
       [HttpPost]
        public ActionResult Create(OrganizationEntity aClient)
        {
     

            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                      var value = loggedInUser.UserProfileInformations.ClientId.Value;
                    if (aClient.Id == null)
                    {
                      
                        aClient.ClientId = value;
                        aClient.CreationTime = DateTime.Now;
                        aClient.CreatorUserId = value;
                        aClient.IsDeleted = false;
                        _organizationAppService.Insert(Mapper.Map<Data.Organization>(aClient));
                        TempData["msg"] = "Organization added successfully";
                    }
                    else
                    { 

                          Data.Organization org =new Organization();
                        org=_organizationAppService.SelectByID(aClient.Id);
                        org.SuperAdminEmail = aClient.SuperAdminEmail;
                        org.LastModificationTime=DateTime.Now;
                        org.LastModifierUserId=value;
                        org.StateId = aClient.StateId;
                        org.PostCodeId = aClient.PostCodeId;
                        org.Name = aClient.Name;
                        org.NOE = aClient.NOE;
                        org.Address = aClient.Address;
                        org.City = aClient.City;
                        org.Geo_location_Latitude = aClient.Geo_location_Latitude;
                        org.Geo_location_Longditude = aClient.Geo_location_Longditude;

                        _organizationAppService.Update(org);
                        TempData["msg"] = "Organization updated successfully";
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {

                    ModelState.AddModelError("error", e.Message);
                }
            }


            return View("Index");
        }

        #endregion
    }
}