﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.Identity;
using EnergyMission.Web.ViewModels.Manage;
using EnergyMission.Web._Framework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace EnergyMission.Web.Controllers
{
    [Authorize]
    public class ManageController : EmControllerBase
    {
        #region Private Members

        private ApplicationUserManager _UserManager;

        #endregion

        #region Public Members

        public ManageController()
        {
            ViewBag.PageTitle = "Manage";
        }

        public ManageController(ApplicationUserManager aUserManager)
        {
            UserManager = aUserManager;
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel aModel)
        {
            if (!ModelState.IsValid)
            {
                return View(aModel);
            }
            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), aModel.OldPassword, aModel.NewPassword);
            if (result.Succeeded)
            {
                ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, aIsPersistent: false);
                }
                return RedirectToAction("Index", new {Message = ManageMessageId.ChangePasswordSuccess});
            }
            AddErrors(result);
            return View(aModel);
        }

        [HttpPost]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, aIsPersistent: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        [HttpPost]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, aIsPersistent: false);
            }
            return RedirectToAction("Index", "Manage");
        }

        public async Task<ActionResult> Index(ManageMessageId? aMessage)
        {
            ViewBag.StatusMessage =
                aMessage == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                    : aMessage == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                        : aMessage == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                            : aMessage == ManageMessageId.Error ? "An error has occurred."
                                : aMessage == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                                    : aMessage == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                                        : "";

            IndexViewModel model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(User.Identity.GetUserId()),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(User.Identity.GetUserId()),
                Logins = await UserManager.GetLoginsAsync(User.Identity.GetUserId()),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(User.Identity.GetUserId())
            };
            return View(model);
        }

        //
        // GET: /Manage/SetPassword

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string aProvider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(aProvider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            ExternalLoginInfo loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new {Message = ManageMessageId.Error});
            }
            IdentityResult result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new {Message = ManageMessageId.Error});
        }

        public async Task<ActionResult> ManageLogins(ManageMessageId? aMessage)
        {
            ViewBag.StatusMessage =
                aMessage == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                    : aMessage == ManageMessageId.Error ? "An error has occurred."
                        : "";
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return View("Error");
            }
            IList<UserLoginInfo> userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
            List<AuthenticationDescription> otherLogins = AuthenticationManager.GetExternalAuthenticationTypes()
                                                                               .Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider))
                                                                               .ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        public ActionResult RemoveLogin()
        {
            IList<UserLoginInfo> linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return View(linkedAccounts);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string aLoginProvider, string aProviderKey)
        {
            ManageMessageId? message;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(aLoginProvider, aProviderKey));
            if (result.Succeeded)
            {
                ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, aIsPersistent: false);
                }
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("ManageLogins", new {Message = message});
        }

        public async Task<ActionResult> RemovePhoneNumber()
        {
            IdentityResult result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
            if (!result.Succeeded)
            {
                return RedirectToAction("Index", new {Message = ManageMessageId.Error});
            }
            ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user != null)
            {
                await SignInAsync(user, aIsPersistent: false);
            }
            return RedirectToAction("Index", new {Message = ManageMessageId.RemovePhoneSuccess});
        }

        public ActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel aModel)
        {
            if (ModelState.IsValid)
            {
                IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), aModel.NewPassword);
                if (result.Succeeded)
                {
                    ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                    if (user != null)
                    {
                        await SignInAsync(user, aIsPersistent: false);
                    }
                    return RedirectToAction("Index", new {Message = ManageMessageId.SetPasswordSuccess});
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(aModel);
        }

        public async Task<ActionResult> VerifyPhoneNumber(string aPhoneNumber)
        {
            string code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), aPhoneNumber);
            // Send an SMS through the SMS provider to verify the phone number
            return aPhoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel {PhoneNumber = aPhoneNumber});
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel aModel)
        {
            if (!ModelState.IsValid)
            {
                return View(aModel);
            }
            IdentityResult result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), aModel.PhoneNumber, aModel.Code);
            if (result.Succeeded)
            {
                ApplicationUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInAsync(user, aIsPersistent: false);
                }
                return RedirectToAction("Index", new {Message = ManageMessageId.AddPhoneSuccess});
            }
            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "Failed to verify phone");
            return View(aModel);
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _UserManager ?? HttpContext.GetOwinContext()
                                                  .GetUserManager<ApplicationUserManager>();
            }
            private set { _UserManager = value; }
        }

        #endregion

        #region Helpers

        // Used for XSRF protection when adding external logins

        #region Private Members

        private const string XsrfKey = "XsrfId";

        private void AddErrors(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private bool HasPhoneNumber()
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PhoneNumber != null;
            }
            return false;
        }

        private async Task SignInAsync(ApplicationUser aUser, bool aIsPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie, DefaultAuthenticationTypes.TwoFactorCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties {IsPersistent = aIsPersistent}, await aUser.GenerateUserIdentityAsync(UserManager));
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext()
                                  .Authentication;
            }
        }

        #endregion

        #region Public Members

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion

        #endregion
    }
}