﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Common.Security;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.ViewModels.StructureDivision;
using Microsoft.AspNet.Identity;
using EnergyMission.Web._Framework;
using System.Linq;

namespace EnergyMission.Web.Controllers
{
    public class StructureDivisionsController : EmControllerBase
    {
        #region Private Members

        private readonly IRepository<Data.StructureDivision> _StructureDivisionAppService;
        private readonly IRepository<Data.Structure> _StructureSDAppService;
        private readonly IRepository<Data.Site> _SiteSDDivisionAppService;

        public StructureDivisionsController()
        {
            ViewBag.PageTitle = "Structure";
            _StructureDivisionAppService = new EnergyMission.Data.Repositories.Repository<Data.StructureDivision>();
            _StructureSDAppService = new EnergyMission.Data.Repositories.Repository<Data.Structure>();
            _SiteSDDivisionAppService = new EnergyMission.Data.Repositories.Repository<Data.Site>();


        }


        private bool AlreadyExists(StructureDivisionEntity aViewModel)
        {
            Data.StructureDivision count = _StructureDivisionAppService.Wheres(tt => tt.Name == aViewModel.Name && tt.IsDeleted == false && tt.StructureId == aViewModel.StructureId);
            bool value = false;
            if (count != null)
            {

                value = true;
            }


            return value;

        }
        /// <summary>
        ///     Helper method to retrieve all the structureDivision.
        /// </summary>
        /// <returns>Returns a list of structureDivision</returns>
        private List<StructureDivisionEntity> RetrieveAllStructureDivisions()
        {


            try
            {
                return Mapper.Map<List<StructureDivisionEntity>>(_StructureDivisionAppService.Where(t => t.IsDeleted == false));
            }
            catch (Exception)
            {

                List<StructureDivisionEntity> OBJ = new List<StructureDivisionEntity>();
                return OBJ;


            }

            //return Mapper.Map<List<StructureDivisionEntity>>(_StructureDivisionAppService.SelectAll());  
            //StructureDivision getStructureDivisionsInput = new StructureDivision();
            //return _StructureDivisionAppService.GetStructureDivisionsWithStructures(getStructureDivisionsInput)
            //                                   .StructureDivisions;
        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStructureDivisionAppService">Application service for structureDivision</param>
        //public StructureDivisionsController(IStructureDivisionAppService aStructureDivisionAppService)
        //{
        //    ViewBag.PageTitle = "Structure Divisions";
        //    _StructureDivisionAppService = aStructureDivisionAppService;
        //}

        /// <summary>
        ///     Called when we want to CREATE a structureDivision in the database.
        /// </summary>
        /// <param name="aStructureDivision">Information about the structureDivision to create.</param>
        /// <returns>Returns gridview on success, create structureDivision view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create(StructureDivisionEntity aStructureDivision)
        {
            if (ModelState.IsValid)
            {
                try
                {


                    if (!AlreadyExists(aStructureDivision))
                      {

                          ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                          aStructureDivision.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                          if (aStructureDivision.Id == null)
                          {
                              aStructureDivision.IsDeleted = false;
                              aStructureDivision.CreationTime = DateTime.Now;
                              _StructureDivisionAppService.Insert(Mapper.Map<StructureDivision>(aStructureDivision));
                              ModelState.Clear();
                              TempData["msg"] = "StructureDivision Saved Succesfully";
                              return RedirectToAction("Index");

                          }
                          else
                          {
                              Data.StructureDivision count = _StructureDivisionAppService.SelectByID(aStructureDivision.Id);
                              count.LastModificationTime = DateTime.Now;
                              count.Name = aStructureDivision.Name;
                              count.StructureId = aStructureDivision.StructureId;
                              count.LastModifierUserId = aStructureDivision.CreatorUserId;
                              _StructureDivisionAppService.Update(count);
                              ModelState.Clear();
                              TempData["msg"] = "StructureDivision Updated Succesfully";
                              return RedirectToAction("Index");

                          }

                      }
                      else
                      {

                          TempData["msg"] = "There is already a StructureDivision for this combination";
                      }
                        
                    
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            return RedirectToAction("Index");
           // return View("Create", aStructureDivision);
        }

        /// <summary>
        ///     Called when we want to DELETE the structureDivision.
        /// </summary>
        /// <param name="Id">Unique identifier for the structureDivision record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                    _StructureDivisionAppService.Delete(Id);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit Structure Division";
            //StructureDivisionAddEditViewModel viewModel = new StructureDivisionAddEditViewModel
            //{
            //    StructureDivision = _StructureDivisionAppService.GetStructureDivisionWithStructure(new GetStructureDivisionInput { Id = Id })
            //};

            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                //viewModel.RestrictToClient(appUser.UserProfileInformation.ClientId.Value);
            }

            return View();
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllStructureDivisions());
        }

        /// <summary>
        ///     Main view to display all the structureDivision in the system.
        /// </summary>
        /// <returns>Returns the view for all the structureDivision in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            if ((TempData["msg"] != string.Empty) || (TempData["msg"] != null))

                ViewBag.msg = TempData["msg"];
            return View(RetrieveAllStructureDivisions());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new structureDivision to be added.</returns>
        public ActionResult New()
        {
            ViewBag.PageTitle = "New Structure Division";
            //StructureDivisionAddEditViewModel viewModel = new StructureDivisionAddEditViewModel
            //{
            //    StructureDivision = _StructureDivisionAppService.GetNewStructureDivision(new GetNewStructureDivisionInput())
            //};

            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                //viewModel.RestrictToClient(appUser.UserProfileInformation.ClientId.Value);
            }

            return View("Create");
        }

        /// <summary>
        ///     Called when a structureDivision needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aStructureDivision">Information about the structureDivision to be updated.</param>
        /// <returns>Returns the list of structureDivision on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(StructureDivisionAddEditViewModel aStructureDivision)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _StructureDivisionAppService.Update(Mapper.Map<StructureDivision>(aStructureDivision.StructureDivision));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aStructureDivision);
        }


        public JsonResult SDEdits(long Id)
        {

            IQueryable<Data.StructureDivision> result = _StructureDivisionAppService.Where(tt => tt.Id == Id);

            var value = from t in result
                        select new { t.Id, t.Name, t.StructureId, t.Structure.SiteId,t.Structure.Site.LocationId };

            return Json(value, JsonRequestBehavior.AllowGet);
        }


        public JsonResult OnchangeOfLocationchangesiteEntry(long deptId)
        {

            IQueryable<Data.Site> result = _SiteSDDivisionAppService.Where(tt => tt.LocationId == deptId);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public JsonResult OnChangeOfSiteChangeStructureEntry(long SDDdeptId)
        {

            IQueryable<Data.Structure> result = _StructureSDAppService.Where(tt => tt.SiteId == SDDdeptId);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult OnchangeOfStructureStructureDivisonEntry(long StructureId)
        {

            IQueryable<Data.StructureDivision> result = _StructureDivisionAppService.Where(tt => tt.StructureId == StructureId);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }

}