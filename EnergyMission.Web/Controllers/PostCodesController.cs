﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Domain.Entities;
using EnergyMission.PostCode;
using EnergyMission.PostCode.Dtos;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data.Repositories;

namespace EnergyMission.Web.Controllers
{
    public class PostCodesController : EmControllerGridView<GetNewPostCodeOutput, GetPostCodeOutput>
    {
        #region Private Members

        private readonly IPostCodesRepository  _PostCodeAppService;

        /// <summary>
        ///     Helper method to retrieve all the postCodes.
        /// </summary>
        /// <returns>Returns a list of postCodes</returns>
        private List<PostCodeEntity> RetrieveAllPostCodes()
        {

            return Mapper.Map<List<PostCodeEntity>>(_PostCodeAppService.GetPostCodes());             ////GetPostCodesInput getPostCodesInput = new GetPostCodesInput();
            //return _PostCodeAppService.GetPostCodes(getPostCodesInput)
            //                          .PostCodes;
        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aPostCodeAppService">Application service for postCodes</param>
        public PostCodesController()
        {
            ViewBag.PageTitle = "Post Codes";
            _PostCodeAppService = new PostCodesRepository();
            //_PostCodeAppService = aPostCodeAppService;
        }

        /// <summary>
        ///     Called when we want to CREATE a postCode in the database.
        /// </summary>
        /// <param name="aPostCode">Information about the postCode to create.</param>
        /// <returns>Returns gridview on success, create postCode view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Create(GetNewPostCodeOutput aPostCode)
        {
            


            if (ModelState.IsValid)
            {
                try
                {

                    if (aPostCode.Id == 0)
                    {
                        var value = _PostCodeAppService.GetPostcodeBypostnumber(aPostCode.PostCodeNumber);
                          if (value == null)
                          {
                              aPostCode.IsDeleted = false;
                              aPostCode.CreationTime = DateTime.Now;
                              _PostCodeAppService.InsertPostcode(Mapper.Map<Data.PostCode>(aPostCode));
                              TempData["msg"] = "Postcode added successfully!";
                          }
                    
                               else
                                          
                         throw new Exception("Postcode allready exits!");
                    }
                    else
                    {
                          var value = _PostCodeAppService.GetPostcodeBypostnumber(aPostCode.PostCodeNumber);
                          if (value == null)
                          {
                              Data.PostCode count = _PostCodeAppService.GetPostcodeBypostnumber(aPostCode.PostCodeNumber);

                              count.LastModificationTime = DateTime.Now;
                              count.PostCodeNumber = aPostCode.NameConfirm;
                              count.PostName = count.PostName;
                              _PostCodeAppService.UpdatePostcode(count);
                          }
                          else

                              throw new Exception("Postcode allready exits!");

                    }
                    //_PostCodeAppService.Insert(Mapper.Map<Data.PostCode>(aPostCode));



                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
       
                    StatusMessageError(e.Message);
                    TempData["msg"] = e.Message;
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            ViewBag.msg = TempData["msg"];
            return View("Index");
        }

        /// <summary>
        ///     Called when we want to DELETE the postCode.
        /// </summary>
        /// <param name="Id">Unique identifier for the postCode record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public override ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {

                try
                {

                    //Data.PostCode count = _PostCodeAppService.SelectByID(Id);
                    //count.LastModificationTime = DateTime.Now;

                    //count.IsDeleted = true;
                    _PostCodeAppService.DeletePostcode(Id);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
                //try
                //{
                     
                //    _PostCodeAppService.Delete(Id);
                //}
                //catch (Exception e)
                //{
                //    StatusMessageError(e.Message);
                //}
            }
            return RedirectToAction("GridViewPartial");
        }

        public JsonResult Edits(long Id)
        {
       
            var result = _PostCodeAppService.GetPostcodeByIds(Id);


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public override ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit PostCode";
            return View("Edit", Mapper.Map<PostCodeEntity>(_PostCodeAppService.GetPostcodeById(Id)));
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public override ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllPostCodes());
        }

        /// <summary>
        ///     Main view to display all the postCodes in the system.
        /// </summary>
        /// <returns>Returns the view for all the postCodes in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            if (TempData["msg"] != null)
            {
                ViewBag.msg = TempData["msg"];
            }
            return View(RetrieveAllPostCodes());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new postCode to be added.</returns>
        public override ActionResult New()
        {
            ViewBag.PageTitle = "New PostCode";
            //return View("Create", _PostCodeAppService.GetNewPostCode(new GetNewPostCodeInput()));
            return View("Create");

        
        }

        /// <summary>
        ///     Called when a postCode needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aPostCode">Information about the postCode to be updated.</param>
        /// <returns>Returns the list of postCodes on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Update(GetPostCodeOutput aPostCode)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _PostCodeAppService.UpdatePostcode(Mapper.Map<Data.PostCode>(aPostCode));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aPostCode);
        }

        #endregion
    }
}