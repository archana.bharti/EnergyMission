using System.Web.Mvc;

using DevExpress.Web.Mvc;
using EnergyMission.Web._Framework;

namespace EnergyMission.Web.Controllers
{
    /// <summary>
    ///     All CRUD controllers managing a grid view and then the separate views for edit and insert should descend.
    ///     from this controller. All virtual public methods have sample implementations that serve as an example for
    ///     descendants.
    /// </summary>
    /// <typeparam name="TCreateDataFromViewDto">
    ///     Class to use for DTO containing the data from the view for ADDING the new
    ///     record.
    /// </typeparam>
    /// <typeparam name="TUpdateDataFromViewDto">
    ///     Class to use for DTO containing the data from the view for UPDATING the record
    ///     record.
    /// </typeparam>
    public class EmControllerGridView<TCreateDataFromViewDto, TUpdateDataFromViewDto> : EmControllerBase, IEmControllerGridviewOperations, IEmControllerGridViewCrud<TCreateDataFromViewDto, TUpdateDataFromViewDto>
    {
        #region Public Members

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Create(TCreateDataFromViewDto aDataFromView)
        {
            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        _CountryAppService.InsertCounty(Mapper.Map<InsertCountryInput>(aCountry));
            //        return RedirectToAction("Index");
            //    }
            //    catch (Exception e)
            //    {
            //        ViewData["EditError"] = e.Message;
            //    }
            //}
            //else
            //    ViewData["EditError"] = "Please, correct all errors.";

            //return View("Create", aCountry);
            return HttpNotFound();
        }

        [AcceptVerbs(HttpVerbs.Delete)]
        public virtual ActionResult Delete(long Id)
        {
            //if (Id >= 0)
            //{
            //    try
            //    {
            //        _CountryAppService.DeleteCounty(new DeleteCountryInput { Id = Id });
            //    }
            //    catch (Exception e)
            //    {
            //        ViewData["EditError"] = e.Message;
            //    }
            //}
            //return RedirectToAction("GridViewPartial");
            return HttpNotFound();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult Edit(long Id)
        {
            //return View("Edit", _CountryAppService.GetCountry(new GetCountryInput { Id = Id }));
            return HttpNotFound();
        }

        [AcceptVerbs("Post")]
        public virtual ActionResult GridViewNew()
        {
            return RedirectToAction("New");
        }

        [ValidateInput(false)]
        public virtual ActionResult GridViewPartial()
        {
            //return PartialView("_GridViewPartial", RetrieveAllCountries());
            return HttpNotFound();
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public virtual ActionResult New()
        {
            //_CountryAppService.GetNewCountry(new GetNewCountryInput());
            //return View("Create", _CountryAppService.GetNewCountry(new GetNewCountryInput()));

            return HttpNotFound();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public virtual ActionResult Update(TUpdateDataFromViewDto aCountry)
        {
            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        _CountryAppService.UpdateCounty(Mapper.Map<UpdateCountryInput>(aCountry));
            //        return RedirectToAction("Index");
            //    }
            //    catch (Exception e)
            //    {
            //        ViewData["EditError"] = e.Message;
            //    }
            //}
            //else
            //    ViewData["EditError"] = "Please, correct all errors.";

            //return View("Edit", aCountry);
            return HttpNotFound();
        }

        #endregion
    }
}