﻿using System;
using System.Linq;
using System.Web.Mvc;
using EnergyMission.Common.Security;
using EnergyMission.Domain.Entities;
using EnergyMission.User.Dtos;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels.User;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;
using EnergyMission.Data;
using EnergyMission.Domain.EntitiesDto;
using AutoMapper;
namespace EnergyMission.Web.Controllers
{
    public class UsersController : EmControllerGridView<GetNewUserOutput, GetUserOutput>
    {
        /// <summary>
        ///     Class constructor
        /// </summary>
        public UsersController()
        {
            ViewBag.PageTitle = "Users";
        }

        /// <summary>
        ///     Called when we want to CREATE a User in the database.
        /// </summary>
        /// <param name="aUser">Information about the User to create.</param>
        /// <returns>Returns gridview on success, create User view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreateEx(UserAddEditViewModel aUser)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Ensure we don't have a user with that username already
                    if (AppUserManager.FindByName(aUser.EmailAddress) != null)
                    {
                        throw new Exception("User with that email address already exists.");
                    }

                    //Now add the user
                    ApplicationUser appUser = new ApplicationUser
                    { 

                        UserName =aUser.EmailAddress,
                        Email = aUser.EmailAddress,
                        UserProfileInformations = new  UserProfileInformationEntity { ClientId = aUser.ClientId }
                    };
                    IdentityResult result = await AppUserManager.CreateAsync(appUser, aUser.Password);
                    //AppUserManager.Create(appUser, aUser.Password);
                    if (result.Succeeded)
                    {
                        ApplicationUser apps = AppUserManager.FindByName(aUser.EmailAddress);
                        AppUserManager.AddToRole(apps.Id, SystemRoleList.SystemRoles.First(r => r.Key == aUser.SystemRole)
                                                                         .Value.Name);


                        return RedirectToAction("Index");
                    }

                    StatusMessageError(result.Errors.ToString());

                }   

                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Create", aUser);
        }

        /// <summary>
        ///     Called when we want to DELETE the User.
        /// </summary>
        /// <param name="Id">Unique identifier for the User record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public ActionResult DeleteEx(string Id)
        {
            if (!string.IsNullOrWhiteSpace(Id))
            {
                try
                {
                    ApplicationUser appUser = AppUserManager.FindById(Id);
                    if (appUser == null)
                        throw new Exception("Could not find the user.");

                    AppUserManager.Delete(appUser);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public ActionResult EditEx(string Id)
        {
            ViewBag.PageTitle = "Edit User";

            return View("Edit", UserGet(Id));
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public override ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllUsers()
                .Users);
        }

        /// <summary>
        ///     Main view to display all the User in the system.
        /// </summary>
        /// <returns>Returns the view for all the User in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            return View(RetrieveAllUsers()
                .Users);
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new User to be added.</returns>
        public override ActionResult New()
        {
            ViewBag.PageTitle = "New User";

            UserAddEditViewModel viewModel = new UserAddEditViewModel
            {
                SystemRole = SystemRoleKindEnum.None
            };

            return View("Create", viewModel);
        }

        /// <summary>
        ///     Called when a User needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aUserViewModel">Information about the User to be updated.</param>
        /// <returns>Returns the list of User on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateEx(UserAddEditViewModel aUserViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ApplicationUser appUser = AppUserManager.FindById(aUserViewModel.Id);
                    if (appUser == null)
                        throw new Exception("Could not find the user.");

                    //Should the email address have changed then we need to check to see if we already have a username with that
                    //email address and if not then we can add a new user
                    if (aUserViewModel.EmailAddress != appUser.UserName)
                    {
                        //Email address has changed
                        ApplicationUser conflictingUser = AppUserManager.FindByName(aUserViewModel.EmailAddress);
                        if (conflictingUser != null)
                        {
                            throw new Exception(string.Format("A username {0} already exists within the system.", aUserViewModel.EmailAddress));
                        }
                    }

                    //Update the user
                    AddEditViewModelToApplicationUser(aUserViewModel, appUser);
                    AppUserManager.Update(appUser);

                    //Ensure the roles are correct
                    foreach (IdentityUserRole role in appUser.Roles)
                    {
                        AppUserManager.RemoveFromRole(aUserViewModel.Id, role.RoleId);
                    }
                    AppUserManager.AddToRole(appUser.Id, SystemRoleList.SystemRoles.First(r => r.Key == aUserViewModel.SystemRole)
                                                                       .Value.Name);

                    //Should a password have been supplied then update the user password
                    if (!string.IsNullOrWhiteSpace(aUserViewModel.Password) && (aUserViewModel.Password == aUserViewModel.PasswordConfirm))
                    {
                        IdentityResult resetPasswordResult = AppUserManager.ResetPassword(aUserViewModel.Id, AppUserManager.GeneratePasswordResetToken(aUserViewModel.Id), aUserViewModel.Password);
                        if (!resetPasswordResult.Succeeded)
                        {
                            string passwordResetErrors = String.Empty;
                            foreach (string error in resetPasswordResult.Errors)
                            {
                                passwordResetErrors += error + "\n";
                                throw new Exception("There was a problem resetting the password: \n\n" + passwordResetErrors);
                            }
                        }
                    }

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aUserViewModel);
        }

        /// <summary>
        ///     Internal helper method that transfers the email address, client id to a found
        ///     application user.
        /// </summary>
        /// <param name="aViewModel">View model to transfer into an ApplicationUser</param>
        /// <param name="aAppUser">Application user to populate</param>
        /// <returns>Returns the application user partially populated if found, null otherwise.</returns>
        private void AddEditViewModelToApplicationUser(UserAddEditViewModel aViewModel, ApplicationUser aAppUser)
        {
            aAppUser.Email = aViewModel.EmailAddress;
            aAppUser.UserName = aViewModel.EmailAddress;
            if (aAppUser.UserProfileInformations == null)
            {
                aAppUser.UserProfileInformations = new UserProfileInformationEntity();
            }
            aAppUser.UserProfileInformations.ClientId = aViewModel.ClientId.Value;

        }
        /// <summary>
        ///     Helper method to retrieve all the User.
        /// </summary>
        /// <returns>Returns a list of User</returns>
        private UserBrowseViewModel RetrieveAllUsers()
        {
            UserBrowseViewModel result = new UserBrowseViewModel();

            foreach (ApplicationUser appUser in AppUserManager.Users)
            {
                UserDto userDto = new UserDto
                {
                    Id = appUser.Id,
                    UserName = appUser.UserName,
                    UserProfileInformation = Mapper.Map<UserProfileInformationEntity>(appUser.UserProfileInformations)
                 
                };
                result.Users.Add(userDto);
            }

            return result;
        }

        /// <summary>
        ///     Internal helper method to populate the user Add/Edit view model with the user
        ///     information from the MS Identity subsystem.
        /// </summary>
        /// <param name="aAppUserId">Unique identifier for the user within MS Identity</param>
        /// <returns>Returns the populated user view model if the user is found, raises an exception otherwise</returns>
        private UserAddEditViewModel UserGet(string aAppUserId)
        {
            ApplicationUser appUser = AppUserManager.FindById(aAppUserId);
            if (appUser == null)
                throw new Exception("Could not find the user.");

            UserAddEditViewModel viewModel = new UserAddEditViewModel
            {
                Id = aAppUserId,
                ClientId = appUser.UserProfileInformations == null ? 0 : appUser.UserProfileInformations.ClientId,
                EmailAddress = appUser.Email,
                SystemRole = SystemRoleKindEnum.None //Default  
            };

            IdentityUserRole roleInfo = appUser.Roles.FirstOrDefault();
            if (roleInfo != null)
                viewModel.SystemRole = AppRoleManager.GetRoleKindByRoleId(roleInfo.RoleId);

            return viewModel;
        }
    }
}