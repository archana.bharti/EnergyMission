﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Domain.Entities;
using EnergyMission.State.Dtos;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
namespace EnergyMission.Web.Controllers
{
    public class StatesController : EmControllerGridView<GetNewStateOutput, GetStateOutput>
    {
        #region Private Members

        private readonly IRepository<Data.State> _StateAppService;

        /// <summary>
        ///     Helper method to retrieve all the states.
        /// </summary>
        /// <returns>Returns a list of states</returns>
        private List<StateEntity> RetrieveAllStates()
        {
            return Mapper.Map<List<StateEntity>>(_StateAppService.Where(tt=>tt.IsDeleted==false));  
            //GetStatesInput getStatesInput = new GetStatesInput();
            //return _StateAppService.GetStates(getStatesInput)
            //                       .States;
        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStateAppService">Application service for states</param>
        public StatesController()
        {
            ViewBag.PageTitle = "States";
            _StateAppService =  new EnergyMission.Data.Repositories.Repository<Data.State>();
        }

        /// <summary>
        ///     Called when we want to CREATE a state in the database.
        /// </summary>
        /// <param name="aState">Information about the state to create.</param>
        /// <returns>Returns gridview on success, create state view on failure.</returns>
  //      [AcceptVerbs(HttpVerbs.Post)]
  //      public override ActionResult Create(InsertStateInput  aState)
  //      {
  //          if (ModelState.IsValid)
  //          {
  //              try
  //              {
  ////aState.IsDeleted = false;
  //                      //aState.CreationTime = DateTime.Now;
  //                      _StateAppService.Insert(Mapper.Map<Data.State>(aState));
  //                  //}
  //                  //else
  //                  //{
  //                  //    Data.State count = _StateAppService.SelectByID(aState.Id);

  //                  //    count.LastModificationTime = DateTime.Now;
  //                  //    count.Name = aState.NameConfirm;
  //                  //    _StateAppService.Update(count);


  //                  //}
  //                  //_StateAppService.Insert(Mapper.Map<Data.State>(aState));
  //                  return RedirectToAction("Index");
  //              }
  //              catch (Exception e)
  //              {
  //                  StatusMessageError(e.Message);
  //              }
  //          }
  //          else
  //              StatusMessageErrorsModelState(ModelState);

  //          return View("Create", aState);
  //      }


        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Creates(StateEntity aState)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    if (aState.Id == 0)
                    {
                        var value = _StateAppService.Wheres(tt => tt.Name == aState.Name && tt.IsDeleted == false);
                        if (value == null)
                        {
                            aState.IsDeleted = false;
                            aState.CreationTime = DateTime.Now;
                            _StateAppService.Insert(Mapper.Map<Data.State>(aState));
                            TempData["msg"] = "State added sucessfully!";
                        }
                        else

                            throw new Exception("State allready exits!");
                    }
                    else
                    {
                          var value = _StateAppService.Wheres(tt => tt.Name == aState.Name && tt.IsDeleted == false);
                          if (value == null)
                          {
                              Data.State count = _StateAppService.Wheres(tt => tt.Name == aState.Name && tt.IsDeleted == false);

                              aState.LastModificationTime = DateTime.Now;
                              aState.Name = aState.NameConfirm;
                              aState.AbbreviationCode = count.AbbreviationCode;
                              _StateAppService.Update(count);
                              TempData["msg"] = "State added sucessfully!";
                          }
                          else
                              throw new Exception("State allready exits!");



                    }
                  
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                    TempData["msg"] = e.Message;
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            ViewBag.msg = TempData["msg"];
                   return View("Index");
        }
        /// <summary>
        ///     Called when we want to DELETE the state.
        /// </summary>
        /// <param name="Id">Unique identifier for the state record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public override ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {


                try
                {

                    Data.State count = _StateAppService.SelectByID(Id);
                    count.LastModificationTime = DateTime.Now;

                    count.IsDeleted = true;
                    _StateAppService.Update(count);
                    //_StateAppService.Delete(Id);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public override ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit State";
            return View("Edit", _StateAppService.SelectByID(Id));
        }

        public JsonResult Edits(long Id)
        {

            //var result = _StateAppService.SelectByID(Id);


            return Json(_StateAppService.SelectByID(Id).Name, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public override ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllStates());
        }

        /// <summary>
        ///     Main view to display all the states in the system.
        /// </summary>
        /// <returns>Returns the view for all the states in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            if (TempData["msg"] != null)
                ViewBag.msg = TempData["msg"];

            return View(RetrieveAllStates());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new state to be added.</returns>
        public override ActionResult New()
        {
            ViewBag.PageTitle = "New State";
            //_StateAppService.GetNewState(new GetNewStateInput());
            return View("Create");
        }

        /// <summary>
        ///     Called when a country needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aState">Information about the state to be updated.</param>
        /// <returns>Returns the list of states on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Update(GetStateOutput aState)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _StateAppService.Update(Mapper.Map<Data.State>(aState));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aState);
        }

        #endregion
    }
}