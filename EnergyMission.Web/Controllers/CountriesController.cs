﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Country;
using EnergyMission.Country.Dtos;
using EnergyMission.Domain.Entities;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using System.Linq;
namespace EnergyMission.Web.Controllers
{
    public class CountriesController : EmControllerGridView<GetNewCountryOutput, GetCountryOutput>
    {
        #region Private Members

        private readonly IRepository<Data.Country> _CountryAppService;

        /// <summary>
        ///     Helper method to retrieve all the countries.
        /// </summary>
        /// <returns>Returns a list of countries</returns>
        private List<CountryEntity> RetrieveAllCountries()
        {
            return Mapper.Map<List<CountryEntity>>(_CountryAppService.Where(t=>t.IsDeleted==false).OrderBy(tt=>tt.Name)); 
            //GetCountriesInput getCountriesInput = new GetCountriesInput();
            //return _CountryAppService.GetCountries(getCountriesInput)
            //                         .Countries;
        }

        #endregion

        #region Public Members


        /// <summary>
        ///     Class constructor
        /// </summary>
        ///// <param name="aCountryAppService">Application service for countries</param>
        public CountriesController()
        {
            ViewBag.PageTitle = "Countries";
            _CountryAppService = new EnergyMission.Data.Repositories.Repository<Data.Country>();
        }

        /// <summary>
        ///     Called when we want to CREATE a country in the database.
        /// </summary>
        /// <param name="aCountry">Information about the country to create.</param>
        /// <returns>Returns gridview on success, create country view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Create(GetNewCountryOutput aCountry)
        {
            if (ModelState.IsValid)
            {
                try
                {
                   

                    _CountryAppService.Insert(Mapper.Map<Data.Country>(aCountry));
                

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Create", aCountry);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Creates(CountryEntity aCountry)
        {

        
            if (ModelState.IsValid)
            {
                try
                {

                 
                    if (aCountry.Id == 0)
                    {
                     var value=   _CountryAppService.Wheres(tt => tt.Name == aCountry.Name && tt.IsDeleted==false);
                     if (value == null)
                     {
                         aCountry.IsDeleted = false;
                         aCountry.CreationTime = DateTime.Now;
                         _CountryAppService.Insert(Mapper.Map<Data.Country>(aCountry));
                         TempData["msg"] = "Country added sucessfully";
                     }
                     else
                                          
                         throw new Exception("Country allready exits!");
                         
                
                    }
                    else
                    {


                        var value = _CountryAppService.Wheres(tt => tt.Name == aCountry.Name && tt.IsDeleted == false);

                        if (value == null)
                        {
                            Data.Country count = _CountryAppService.SelectByID(aCountry.Id);

                            count.LastModificationTime = DateTime.Now;
                            count.Name = aCountry.NameConfirm;
                            _CountryAppService.Update(count);
                            TempData["msg"] = "Country updated sucessfully";
                        }
                        throw new Exception("Country allready exits!");

                    }
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                    TempData["msg"] = e.Message;
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            ViewBag.msg = TempData["msg"];
            return View("Index");
        }
        /// <summary>
        ///     Called when we want to DELETE the country.
        /// </summary>
        /// <param name="Id">Unique identifier for the country record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public override ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {

                 Data.Country count=  _CountryAppService.SelectByID(Id);
                 count.LastModificationTime = DateTime.Now;

                 count.IsDeleted = true;
                    _CountryAppService.Update(count);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public override ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit Country";
            return View("Edit", _CountryAppService.SelectByID(Id));
        }
        public  JsonResult Edits(long Id)
        {
            //var result = _CountryAppService.SelectByID(Id);
            IQueryable<Data.Country> SSB = _CountryAppService.Where(tt => tt.Id == Id);

            var res = (from sub in SSB
                       where sub.Id == Id
                       select new
                       {
                           id = sub.Id,
                           name = sub.Name

                       }
                                       );

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public override ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllCountries());
        }

        /// <summary>
        ///     Main view to display all the countries in the system.
        /// </summary>
        /// <returns>Returns the view for all the countries in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {

            if (TempData["msg"] != null)
            {
                ViewBag.msg = TempData["msg"];
            }
            return View(RetrieveAllCountries());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new country to be added.</returns>
        public override ActionResult New()
        {
            ViewBag.PageTitle = "New Country";
            //return View("Create", _CountryAppService.GetNewCountry(new GetNewCountryInput()));

            return View("Create");
        }

        /// <summary>
        ///     Called when a country needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aCountry">Information about the country to be updated.</param>
        /// <returns>Returns the list of countries on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Update(GetCountryOutput aCountry)
        {
            if (ModelState.IsValid)
            {
                try
                {
                   
                    _CountryAppService.Update(Mapper.Map<Data.Country>(aCountry));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aCountry);
        }

        #endregion
    }
}