﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using Abp.Domain.Uow;
using AutoMapper;
using EnergyMission.Common.Enums;
using EnergyMission.Common.Security;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.Identity;
using EnergyMission.Web.ViewModels.ClientTeam;
using Microsoft.AspNet.Identity;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Person.Dtos;
using EnergyMission.Web._Framework;
using EnergyMission.Data.Repositories;

namespace EnergyMission.Web.Controllers
{
    public class ClientTeamsController : EmControllerBase
    {
        private readonly IRepository<Data.ClientTeam> _ClientTeamAppService;
        private readonly IRepository<Data.Sensor> _SensorAppService;
        private readonly EnergyMission.Data.IRepostitories.IStructureDivisionSensorRepository _StructureDivisionSensorRepository;

        private readonly ITeamSensormappingRepository _ITeamSensorMappingRepository;

        public ClientTeamsController()
        {
           
            _ClientTeamAppService = new EnergyMission.Data.Repositories.Repository<Data.ClientTeam>();
            _SensorAppService = new EnergyMission.Data.Repositories.Repository<Data.Sensor>();
            _StructureDivisionSensorRepository = new StructureDivisionSensorRepositoryMain();
            _ITeamSensorMappingRepository = new TeamSensorMappingRepostory();
        }
      

        #region UserDefineMethod
        private bool AlreadyExists(ClientTeamEntity objClientTeamEntity)
        {
            Data.ClientTeam count = _ClientTeamAppService.Wheres(tt => tt.Name == objClientTeamEntity.Name && tt.IsDeleted == false);
            bool value = true;
            if (count == null)
            {

                value = false;
            }


            return value;

        }
        [HttpGet]
        public ActionResult DeleteTeam(long id)
        {
            _ClientTeamAppService.Delete(id);
            return PartialView("_GridViewPartial", RetrieveAllClients());
        }
        [HttpGet]
        public ActionResult DeleteTeamSensorMappling(Int32 id, Int32 TeamId)
        {
            _ITeamSensorMappingRepository.RemoveTeamSensorMapping(id);

            List<SensorEntity> objList = null;
            objList = Mapper.Map<List<SensorEntity>>(_ITeamSensorMappingRepository.getSensorByTeamID(TeamId));
            return PartialView("_TeamSensorPartialView", objList);

        }
       
        #endregion

        #region PublicMethod
          [HttpGet]
        public ActionResult OnchangeOfStructureDivisonId(long StructureDivisionId)
        {
            List<SensorEntity> objList = Mapper.Map<List<SensorEntity>>(_ITeamSensorMappingRepository.GetSensorListByStructureDivisionId(StructureDivisionId));
            var value = from t in objList
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        private List<ClientTeamEntity> RetrieveAllClients()
        {
            try
            {
                ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                   var LoggedInUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                   List<ClientTeamEntity> objList = Mapper.Map<List<ClientTeamEntity>>(_ClientTeamAppService.Where(t => t.IsDeleted == false && t.CreatorUserId == LoggedInUserId));
                return objList;
            }
            catch (Exception)
            {

                List<ClientTeamEntity> OBJ = new List<ClientTeamEntity>();
                return OBJ;


            }
            //GetLocationMappingsInput getLocationMappingsInput = new GetLocationMappingsInput();
            // return Mapper.Map<List<LocationMappingEntity>>(_LocationMappingAppService.SelectAll()); 
            //return _LocationMappingAppService.SelectAll();

        }



        [HttpGet]
        public ActionResult GetSensoreByClientTeamId(long TeamId)
        {
            try
            {
                List<SensorEntity> objList = null;
               objList = Mapper.Map<List<SensorEntity>>(_ITeamSensorMappingRepository.getSensorByTeamID(TeamId));
               return PartialView("_TeamSensorPartialView", objList);
            }
            catch (Exception)
            {

                List<SensorEntity> OBJ = new List<SensorEntity>();
                return Json("error occured");


            }
        }
        /// <summary>
        /// Get  Method Used To Create Team
        /// </summary>
        /// <param name="objClientTeamEntity"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllClients());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index(int type = 0, long TeamId = 0)
        {
            if (type == 1)
            {
                ViewBag.turn = 2;
                ViewBag.TeamId = TeamId;
            }
            else
            {
                ViewBag.turn = 1;
                ViewBag.TeamId = 0;
            }

            return View(RetrieveAllClients());

        }



        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CreateTeam()
        {
            ViewBag.msg = "";
            return View();

        }
        /// <summary>
        /// Post Method To Create Team
        /// </summary>
        /// <param name="objClientTeamEntity"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateTeam(ClientTeamEntity objClientTeamEntity)
        {
            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();

         
            if (ModelState.IsValid)
            {
                try
                {
                    if (AlreadyExists(objClientTeamEntity))
                    {
                        ModelState.AddModelError("Caution:", "Team Name Already Exist");
                        ViewBag.msg = "Team Name Already Exist";
                    }
                    else
                    {
                     

                        var LoggedInUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                        objClientTeamEntity.ClientId = Convert.ToInt64(loggedInUser.UserProfileInformations.ClientId);
                        objClientTeamEntity.PersonTeamManagerId = Convert.ToInt64(loggedInUser.UserProfileInformations.PersonId);
                        objClientTeamEntity.IsDeleted = false;
                        objClientTeamEntity.CreationTime = DateTime.Now;
                        objClientTeamEntity.CreatorUserId = LoggedInUserId;
                        _ClientTeamAppService.Insert(Mapper.Map<Data.ClientTeam>(objClientTeamEntity));
                        return RedirectToAction("Index"); 
                      
                    }

                }
                catch (Exception e)
                {
                    ViewBag.msg = "Some Error occured please try later...";
                    StatusMessageError(e.Message);
                }
               
            }
            return View(objClientTeamEntity);
        }


        [ValidateInput(false)]
        public ActionResult TeamSensorPartialView()
        {
            return PartialView("_TeamSensorPartialView", new List<SensorEntity>());
        }
 

        [HttpGet]
        public ActionResult EditTeamSensorPartialView(Int16 Id)
        {

            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            long LoggedInUserId = loggedInUser.UserProfileInformations.PersonId.Value;
            ClientTeamSensorMapping objClientTeamSensorMapping = new ClientTeamSensorMapping();
            ClientTeamEntity objClientTeamEntity = Mapper.Map<ClientTeamEntity>(_ClientTeamAppService.SelectByID(Id));
            objClientTeamSensorMapping.TeamName = objClientTeamEntity.Name;
            objClientTeamSensorMapping.SensorEntity = Mapper.Map<List<SensorEntity>>(_ITeamSensorMappingRepository.getNotMappedSensorListOnTeamID(Id, LoggedInUserId));
            objClientTeamSensorMapping.TeamId = Id;
           
            return PartialView("_EditTeamSensorPartialView", objClientTeamSensorMapping);
        }


        public ActionResult StructureDivisionSensors(int SensorId, int StructureDivisionId, int TeamId)
        {
            long Return = 0;
            try 
            {
                ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var LoggedInUserId = loggedInUser.UserProfileInformations.PersonId.Value;
            StructureDivisionSensorEntity objStructureDivisionSensorEntity = new StructureDivisionSensorEntity();
            objStructureDivisionSensorEntity.SensorId = SensorId;
            objStructureDivisionSensorEntity.StructureDivisionId = StructureDivisionId;
            objStructureDivisionSensorEntity.CreatorUserId = LoggedInUserId;
              
        Return = _StructureDivisionSensorRepository.StructureDivisionSensors(objStructureDivisionSensorEntity,TeamId);
            }
            catch
            {

            }
            if (Return!=-0)
            return Json("mapped Sucessfully...",JsonRequestBehavior.AllowGet);
            else
                return Json("Already mapped ...", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TeamSensorMapping(ClientTeamSensorMapping objClientTeamSensorMapping)
        {
            try
            {
                if (objClientTeamSensorMapping.TeamName!=null)
                {
                    Data.ClientTeam objClientTeam = _ClientTeamAppService.Wheres(t => t.Id == objClientTeamSensorMapping.TeamId);
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    long LoggedInUserId = loggedInUser.UserProfileInformations.PersonId.Value;


                    objClientTeam.Name = objClientTeamSensorMapping.TeamName;
                    objClientTeam.LastModifierUserId = LoggedInUserId;
                    objClientTeam.LastModificationTime = DateTime.Now;
                    _ClientTeamAppService.Update(Mapper.Map<Data.ClientTeam>(objClientTeam));

                    _ITeamSensorMappingRepository.MapTeamSensor(LoggedInUserId, objClientTeamSensorMapping.TeamId);
                    return RedirectToAction("Index", new { type = 1, TeamId = objClientTeamSensorMapping.TeamId });
                }
                else
                {
                    return RedirectToAction("Index", new { type = 1, TeamId = objClientTeamSensorMapping.TeamId });
                }
            }
            catch
            {

            }
            //TempData["AlertMessage"] = "Mapped Sucessfully...";

            return RedirectToAction("Index", new { type = 1, TeamId = objClientTeamSensorMapping.TeamId });
            
            //return RedirectToAction("Index");
        }
        #endregion


       
    }
}