﻿using System.Web.Mvc;
using EnergyMission.Common.Security;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels.Dashboard;
using EnergyMission.Web._Framework;

namespace EnergyMission.Web.Controllers
{
    public class DashboardController : EmControllerBase
    {
        #region Public Members

        /// <summary>
        /// Displays the user dashboard based on the role they are in.
        /// </summary>
        /// <returns>Returns the view for the user depending on the role they are in.</returns>
        public ActionResult LoggedInUserDashboard()
        {
            switch (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager))
            {
                case SystemRoleKindEnum.Administrator:
                    return PartialView("_AdministratorDashboard");
                case SystemRoleKindEnum.Client:
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    DashboardClientViewModel viewModel = new DashboardClientViewModel
                    {
                        ClientId = loggedInUser.UserProfileInformations.ClientId.Value
                    };
                    return PartialView("_ClientDashboard", viewModel);
                case SystemRoleKindEnum.SuperAdministrator:
                    return PartialView("_SystemAdministratorDashboard");
                case SystemRoleKindEnum.TeamManager:
                    return PartialView("_TeamManagerDashboard");
                default:
                    return PartialView("_NoneDashboard");
            }
        }

        #endregion
    }
}