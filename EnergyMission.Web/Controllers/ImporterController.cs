﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using EnergyMission.DataImport;
using EnergyMission.DataImport.Importers;
using DevExpress.Office.Utils;
using EnergyMission.Client;
using EnergyMission.Domain.Entities;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using EnergyMission.Data.Repositories;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.Text;
using EnergyMission.Web.Authentication;


namespace EnergyMission.Web.Controllers
{
    public class ImporterController : Controller
    {

        private readonly IDataPointImportProcessor _IDataPointImportProcessor;

        List<DataPointMeasurementItem> list = new List<DataPointMeasurementItem>();



        private readonly IRepository<Data.StructureDivision> _StructureDivisionAppService;
        private readonly IRepository<Data.Structure> _StructureSDAppService;
        private readonly IRepository<Data.Site> _SiteSDDivisionAppService;



        public ImporterController()
        {
            _IDataPointImportProcessor = new AnuOptergyDataImporter();
            _StructureDivisionAppService = new EnergyMission.Data.Repositories.Repository<Data.StructureDivision>();
            _StructureSDAppService = new EnergyMission.Data.Repositories.Repository<Data.Structure>();
            _SiteSDDivisionAppService = new EnergyMission.Data.Repositories.Repository<Data.Site>();

        }
        [HttpPost]
        public ActionResult Save(ImporterEntity IImporterEntity, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {


                if (file != null && file.ContentLength > 0)
                {

                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/uploads"), fileName);
                    file.SaveAs(path);
                    _IDataPointImportProcessor.ReadCSV(path);
                    _IDataPointImportProcessor.CreateXML(list);
                    _IDataPointImportProcessor.Save(IImporterEntity);
                    TempData["notice"] = "CSV File Uploaded sucessfully";                 
                    return RedirectToAction("Index");
                }           
                else
                {
                    TempData["notice"] = "CSV File Uploaded sucessfully";                 
                    return RedirectToAction("Index");

                }

            }

            else
            {
                return RedirectToAction("Index");

            }
          

        }





        public ActionResult Index()
        {
            if ((TempData["notice"] != string.Empty) || (TempData["notice"] != null))

                ViewBag.msg = TempData["notice"];
                return View();
            
           
        }


        public JsonResult OnchangeOfLocationchangesiteEntry(long deptId)
        {

            IQueryable<Data.Site> result = _SiteSDDivisionAppService.Where(tt => tt.LocationId == deptId && tt.IsDeleted == false);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }


        public JsonResult OnChangeOfSiteChangeStructureEntry(long SDDdeptId)
        {

            IQueryable<Data.Structure> result = _StructureSDAppService.Where(tt => tt.SiteId == SDDdeptId && tt.IsDeleted == false);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }



        public JsonResult OnChangeOfStructureChangeStructureDivisionEntry(long RDDdeptId)
        {

            IQueryable<Data.StructureDivision> result = _StructureDivisionAppService.Where(tt => tt.StructureId == RDDdeptId && tt.IsDeleted == false);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }



        public JsonResult KKKKK(long StructureDivisonRRR)
        {
            EnergyMission.Web.Common.Common cs = new EnergyMission.Web.Common.Common();
            var value = cs.StructureDivisionSensors(StructureDivisonRRR);
            return Json(value, JsonRequestBehavior.AllowGet);

        }


    }
}