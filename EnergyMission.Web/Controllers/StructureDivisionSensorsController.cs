﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Common.Security;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels;
using Microsoft.AspNet.Identity;

namespace EnergyMission.Web.Controllers
{
    public class StructureDivisionSensorsController : EmControllerGridView<StructureDivisionSensorAddEditViewModel, StructureDivisionSensorAddEditViewModel>
    {
        #region Private Members

        private readonly IRepository<Data.StructureDivisionSensor> _StructureDivisionSensorAppService;

        /// <summary>
        ///     Helper method to retrieve all the structureDivisionSensorInput.
        /// </summary>
        /// <returns>Returns a list of structureDivisionSensorInput</returns>
        private List<StructureDivisionSensorEntity> RetrieveAllStructureDivisionSensors()
        {
            //GetStructureDivisionSensorsInput getStructureDivisionSensorsInput = new GetStructureDivisionSensorsInput();
            return Mapper.Map<List<StructureDivisionSensorEntity>>(_StructureDivisionSensorAppService.SelectAll());
            
            //return _StructureDivisionSensorAppService.SelectAll();
        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStructureDivisionSensorInputAppService">Application service for structureDivisionSensorInput</param>
        //public StructureDivisionSensorsController(IStructureDivisionSensorAppService aStructureDivisionSensorInputAppService)
        //{
        //    ViewBag.PageTitle = "Structure Division Sensors";
        //    _StructureDivisionSensorAppService = aStructureDivisionSensorInputAppService;
        //}

        /// <summary>
        ///     Called when we want to CREATE a structureDivisionSensorInput in the database.
        /// </summary>
        /// <param name="aStructureDivisionSensorInput">Information about the structureDivisionSensorInput to create.</param>
        /// <returns>Returns gridview on success, create structureDivisionSensorInput view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Create(StructureDivisionSensorAddEditViewModel aStructureDivisionSensorInput)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _StructureDivisionSensorAppService.Insert(Mapper.Map<StructureDivisionSensor>(aStructureDivisionSensorInput.StructureDivisionSensor));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Create", aStructureDivisionSensorInput);
        }

        /// <summary>
        ///     Called when we want to DELETE the structureDivisionSensorInput.
        /// </summary>
        /// <param name="Id">Unique identifier for the structureDivisionSensorInput record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public override ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                    _StructureDivisionSensorAppService.Delete(Id);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public override ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit Structure Division Sensor";
            //StructureDivisionSensorAddEditViewModel viewModel = new StructureDivisionSensorAddEditViewModel
            //{
            //    StructureDivisionSensor = _StructureDivisionSensorAppService.GetStructureDivisionSensorWithInclude(new GetStructureDivisionSensorInput {Id = Id})
            //};

            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                //viewModel.RestrictToClient(appUser.UserProfileInformation.ClientId.Value);
            }

            return View("Edit");
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public override ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllStructureDivisionSensors());
        }

        /// <summary>
        ///     Main view to display all the structureDivisionSensorInput in the system.
        /// </summary>
        /// <returns>Returns the view for all the structureDivisionSensorInput in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            return View(RetrieveAllStructureDivisionSensors());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new structureDivisionSensorInput to be added.</returns>
        public override ActionResult New()
        {
            ViewBag.PageTitle = "New Structure Division Sensor";

            //StructureDivisionSensorAddEditViewModel viewModel = new StructureDivisionSensorAddEditViewModel
            //{
            //    StructureDivisionSensor = _StructureDivisionSensorAppService.GetNewStructureDivisionSensor(new GetNewStructureDivisionSensorInput())
            //};


            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                //viewModel.RestrictToClient(appUser.UserProfileInformation.ClientId.Value);
            }

            return View("Create");
        }

        /// <summary>
        ///     Called when a structureDivisionSensorInput needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aStructureDivisionSensorInput">Information about the structureDivisionSensorInput to be updated.</param>
        /// <returns>Returns the list of structureDivisionSensorInput on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Update(StructureDivisionSensorAddEditViewModel aStructureDivisionSensorInput)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _StructureDivisionSensorAppService.Update(Mapper.Map<StructureDivisionSensor>(aStructureDivisionSensorInput.StructureDivisionSensor));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aStructureDivisionSensorInput);
        }

        #endregion
    }
}