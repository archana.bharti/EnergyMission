﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Common.Security;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Web.ViewModels.Structure;
using Microsoft.AspNet.Identity;
using EnergyMission.Web._Framework;
using System.Linq;

namespace EnergyMission.Web.Controllers
{

    [HandleError]
    public class StructuresController : EmControllerBase
    {
        #region Private Members

        private readonly IRepository<Data.Structure> _StructureAppService;
        private readonly IRepository<Data.Site> _SiteTAppService;


        public StructuresController()
        {
            ViewBag.PageTitle = "Structure";
            _StructureAppService = new EnergyMission.Data.Repositories.Repository<Data.Structure>();
            _SiteTAppService = new EnergyMission.Data.Repositories.Repository<Data.Site>();

        }



        private bool AlreadyExists(StructureEntity aViewModel)
        {
            Data.Structure count = _StructureAppService.Wheres(tt => tt.Name == aViewModel.Name && tt.IsDeleted == false && tt.SiteId == aViewModel.SiteId);
            bool value = false;
            if (count != null)
            {

                value = true;
            }


            return value;

        }

        /// <summary>
        ///     Helper method to retrieve all the structures.
        /// </summary>
        /// <returns>Returns a list of structures</returns>
        private IEnumerable<StructureEntity> RetrieveAllStructures()
        {


            try
            {
                return Mapper.Map<List<StructureEntity>>(_StructureAppService.Where(t => t.IsDeleted == false));
            }
            catch (Exception)
            {

                List<StructureEntity> OBJ = new List<StructureEntity>();
                return OBJ;


            }

            //return Mapper.Map<List<StructureEntity>>(_StructureAppService.SelectAll());  

        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStructureAppService">Application service for structures</param>
        //public StructuresController(StructureAppService aStructureAppService)
        //{
        //    ViewBag.PageTitle = "Structures";
        //    _StructureAppService = aStructureAppService;
        //}

        /// <summary>
        ///     Called when we want to CREATE a structure in the database.
        /// </summary>
        /// <param name="aViewModel">Information about the structure to create.</param>
        /// <returns>Returns gridview on success, create structure view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Createstru(StructureEntity aViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!AlreadyExists(aViewModel))
                    {

                        ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                        aViewModel.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                        if (aViewModel.Id == null)
                        {
                          
                            aViewModel.IsDeleted = false;
                            aViewModel.CreationTime = DateTime.Now;
                            _StructureAppService.Insert(Mapper.Map<Data.Structure>(aViewModel));
                            ModelState.Clear();
                            TempData["msg"] = "Structure Saved Succesfully";
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            Data.Structure count = _StructureAppService.SelectByID(aViewModel.Id);
                            count.LastModificationTime = DateTime.Now;
                            count.Name = aViewModel.Name;                           
                            count.SiteId = aViewModel.SiteId;
                            count.LastModifierUserId = aViewModel.CreatorUserId;
                            _StructureAppService.Update(count);
                            ModelState.Clear();
                            TempData["msg"] = "Structure Updated Succesfully";
                            return RedirectToAction("Index");

                        }
                    }
                    else
                    {

                        TempData["msg"] = "There is already a Site for this combination";
                    }




                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            return RedirectToAction("Index");
           // return View("Create", aViewModel);
        }

        /// <summary>
        ///     Called when we want to DELETE the structure.
        /// </summary>
        /// <param name="Id">Unique identifier for the structure record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                    _StructureAppService.Delete(Id);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit Structure";
            Structure st = new Structure();
            st = _StructureAppService.SelectByID(Id);

            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                //viewModel.RestrictToClient(appUser.UserProfileInformation.ClientId.Value);
            }

            return View();
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllStructures());
        }

        /// <summary>
        ///     Main view to display all the structures in the system.
        /// </summary>
        /// <returns>Returns the view for all the structures in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {

            if ((TempData["msg"] != string.Empty) || (TempData["msg"] != null))

                ViewBag.msg = TempData["msg"];
            return View(RetrieveAllStructures());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new structure to be added.</returns>
        public ActionResult New()
        {
            PageTitle = "New Structure";
            //StructureAddEditViewModel viewModel = new StructureAddEditViewModel {Structure = _StructureAppService.SelectByID(new GetNewStructureInput())};

            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                //viewModel.RestrictToClient(appUser.UserProfileInformation.ClientId.Value);
            }

            return View("Create");
        }

        /// <summary>
        ///     Called when a country needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aViewModel">Information about the structure to be updated.</param>
        /// <returns>Returns the list of structures on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Update(StructureAddEditViewModel aViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _StructureAppService.Update(Mapper.Map<Structure>(aViewModel.Structure));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aViewModel);
        }






        public JsonResult Edits(long Id)
        {

            IQueryable<Data.Structure> result = _StructureAppService.Where(tt => tt.Id == Id);

            var value = from t in result
                        select new { t.Id, t.Name, t.SiteId, t.Site.LocationId };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Ddits(long deptId)
        {

            IQueryable<Data.Site> result = _SiteTAppService.Where(tt => tt.LocationId == deptId);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}