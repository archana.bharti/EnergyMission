﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Domain.ClientAdministrator;
using EnergyMission.Web.ViewModels.ClientAdministrator;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNet.Identity;
using EnergyMission.Web._Framework;
using Microsoft.AspNet.Identity.EntityFramework;
namespace EnergyMission.Web.Controllers
{
    public class TeamManagersController :EmControllerBase
    {
        //private readonly IClientAdministratorAppService _ClientAdministratorAppService;
        private readonly IRepository<Data.ClientTeamManager> _ClientManagerAppService;
        private readonly IRepository<Data.ClientAdministrator> _ClientAdminstrator;
        private readonly IRepository<Data.Person> _PesonAppService;
        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientAdministratorAppService">Application service for ClientAdministrator</param>
        public TeamManagersController()
        {
            ViewBag.PageTitle = "Client Administrators";
            _ClientManagerAppService = new EnergyMission.Data.Repositories.Repository<Data.ClientTeamManager>();
            _PesonAppService = new EnergyMission.Data.Repositories.Repository<Data.Person>();
            _ClientAdminstrator = new EnergyMission.Data.Repositories.Repository<Data.ClientAdministrator>();
        }

        /// <summary>
        ///     Called when we want to CREATE a ClientAdministrator in the database.
        /// </summary>
        /// <param name="aClientAdministrator">Information about the ClientAdministrator to create.</param>
        /// <returns>Returns gridview on success, create ClientAdministrator view on failure.</returns>
        //[AcceptVerbs(HttpVerbs.Post)]
        //public override ActionResult Create(ClientAdministratorAddEditViewModel aClientAdministrator)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _ClientAdministratorAppService.InsertClientAdministrator(Mapper.Map<InsertClientAdministratorInput>(aClientAdministrator.ClientAdministrator));
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception e)
        //        {
        //            StatusMessageError(e.Message);
        //        }
        //    }
        //    else
        //        StatusMessageErrorsModelState(ModelState);

        //    return View("Create", aClientAdministrator);
        //}



        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Creates(ClientTeamManagerEntity aClientAdministrator)
        {
            if (ModelState.IsValid)
            {
                try

                {
                     ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();


                     var value = _ClientManagerAppService.Wheres(tt => tt.PersonId == aClientAdministrator.PersonId);
                    if (aClientAdministrator.Id == 0)
                    {


           
                           if (value == null)
                           {
                               aClientAdministrator.ClientId = loggedInUser.UserProfileInformations.ClientId.Value;
                               aClientAdministrator.CreationTime = DateTime.Now;
                               aClientAdministrator.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                               _ClientManagerAppService.Insert(Mapper.Map<Data.ClientTeamManager>(aClientAdministrator));
                               Data.Person pres = _PesonAppService.SelectByID(aClientAdministrator.PersonId);
                               ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);

                               AppUserManager.RemoveFromRole(apps.Id, "Team Manager");
                               AppUserManager.AddToRole(apps.Id, "Team Manager");
                               //Data.Person pres = _PesonAppService.SelectByID(aClientAdministrator.PersonId);
                               //ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);
                               //AppUserManager.AddToRole(apps.Id, "Administrator");
                           }

                        
 
                           else
                           {

                               TempData["msg"] = "Allready Assigned";

                           }
                    }
                    else

                    {
                      
                        if (value == null)
                        {


                            ClientTeamManagerEntity clientadminstrator = new ClientTeamManagerEntity();
                            clientadminstrator = Mapper.Map<ClientTeamManagerEntity>(_ClientManagerAppService.SelectByID(aClientAdministrator.Id));

                            clientadminstrator.ClientId = loggedInUser.UserProfileInformations.ClientId.Value;


                            clientadminstrator.LastModificationTime = DateTime.Now;
                            clientadminstrator.LastModifierUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                            _ClientManagerAppService.Update(Mapper.Map<Data.ClientTeamManager>(clientadminstrator));
                            Data.Person pres = _PesonAppService.SelectByID(clientadminstrator.PersonId);
                            ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);

                            AppUserManager.RemoveFromRole(apps.Id, "Team Manager");
                            AppUserManager.AddToRole(apps.Id, "Team Manager");
                        }

                        else

                        {

                            TempData["msg"] = "Allready Assigned";

                        }
                    
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Index");
        }
        /// <summary>
        ///     Called when we want to DELETE the ClientAdministrator.
        /// </summary>
        /// <param name="Id">Unique identifier for the ClientAdministrator record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public  ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    Data.ClientTeamManager clientadminstrator = new Data.ClientTeamManager();
                 clientadminstrator = _ClientManagerAppService.SelectByID(Id);
                 clientadminstrator.IsDeleted = true;
                 clientadminstrator.LastModificationTime = DateTime.Now;
                 clientadminstrator.LastModifierUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                 _ClientManagerAppService.Update(clientadminstrator);
                Data.Person pres = _PesonAppService.SelectByID(clientadminstrator.PersonId);
                ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);
                AppUserManager.RemoveFromRole(apps.Id, "Team Manager");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        //public override ActionResult Edit(long Id)
        //{
        //    ViewBag.PageTitle = "Edit Client Administrator";

        //    ClientAdministratorAddEditViewModel viewModel = new ClientAdministratorAddEditViewModel
        //    {
        //        ClientAdministrator = _ClientAdministratorAppService.GetClientAdministratorWithInclude(new GetClientAdministratorInput {Id = Id})
        //    };

        //    return View("Edit", viewModel);
        //}
          public  JsonResult Edits(long Id)
        {

            IQueryable<Data.ClientTeamManager> SSB = _ClientManagerAppService.Where(tt => tt.Id == Id);

            var res = (from sub in SSB
                      
                       select new
                       {
                           id = sub.Id,
                           personid = sub.PersonId
                       }
                                       );



            return Json(res, JsonRequestBehavior.AllowGet);

   //             var result = _ClientAdministratorAppService.SelectByID(Id);
   ////var x = from i in _ClientAppService.GetClientById(Id)
   ////                 select new { result.StateId, result.Id, result.Name, result.NOE, result.SuperAdminEmail, result.PostCodeId, result.City, result.Address, result.Geo_location_Latitude, result.Geo_location_Longditude};
   //         return Json(result, JsonRequestBehavior.AllowGet);


        }
        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public  ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllClientAdministrators());
        }

        /// <summary>
        ///     Main view to display all the ClientAdministrator in the system.
        /// </summary>
        /// <returns>Returns the view for all the ClientAdministrator in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {

            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var Id = loggedInUser.UserProfileInformations.ClientId.Value;

            var ids = new HashSet<long>(_ClientAdminstrator.Where(tt=>tt.IsDeleted==false).Select(x => x.PersonId));
            var lists =_ClientAdminstrator.Where(tt=>tt.IsDeleted==false);
            var value = _PesonAppService.Where(tt => tt.IsDeleted == false && tt.CreatorUserId == Id && !ids.Contains(tt.Id)).Select(tt => new { PersonId=tt.Id, Name = tt.FirstName+" "+ tt.LastName});
            ViewBag.pesonal = new SelectList(value, "PersonId", "Name");
            var empid = _PesonAppService.Where(tt => tt.IsDeleted == false && tt.CreatorUserId == Id && !ids.Contains(tt.Id)).Select(tt => new { PersonId = tt.Id, emps = tt.EmployeeID }).ToList();
            ViewBag.emp = new SelectList(empid, "PersonId", "emps");

            if (TempData["msg"] != null)
            {
                ViewBag.msg = TempData["msg"];
                  
            
            }


            return View(RetrieveAllClientAdministrators());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new ClientAdministrator to be added.</returns>
        //public override ActionResult New()
        //{
        //    ViewBag.PageTitle = "New Client Team";

        //    ClientAdministratorAddEditViewModel viewModel = new ClientAdministratorAddEditViewModel
        //    {
        //        ClientAdministrator = _ClientAdministratorAppService.GetNewClientAdministrator(new GetNewClientAdministratorInput())
        //    };

        //    return View("Create", viewModel);
        //}

        /// <summary>
        ///     Called when a ClientAdministrator needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aClientAdministrator">Information about the ClientAdministrator to be updated.</param>
        /// <returns>Returns the list of ClientAdministrator on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Update(ClientAdministratorAddEditViewModel aClientAdministrator)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _ClientManagerAppService.Update(Mapper.Map<Data.ClientTeamManager>(aClientAdministrator.ClientAdministrator));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aClientAdministrator);
        }

        /// <summary>
        ///     Helper method to retrieve all the ClientAdministrator.
        /// </summary>
        /// <returns>Returns a list of ClientAdministrator</returns>
        private List<ClientTeamManagerEntity> RetrieveAllClientAdministrators()
        {
                       ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                       return Mapper.Map<List<ClientTeamManagerEntity>>(_ClientManagerAppService.Where(tt => tt.IsDeleted == false && tt.CreatorUserId == loggedInUser.UserProfileInformations.PersonId.Value));

            //GetClientAdministratorsInput getClientAdministratorsInput = new GetClientAdministratorsInput();
            //return _ClientAdministratorAppService.GetClientAdministrators(getClientAdministratorsInput)
            //                                     .ClientAdministrators;
        }
    }
}