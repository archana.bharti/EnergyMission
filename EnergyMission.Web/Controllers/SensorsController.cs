﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Domain.Entities;
using EnergyMission.Sensor;
using EnergyMission.Sensor.Dtos;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using EnergyMission.Web.Authentication;
using EnergyMission.Web._Framework;
using System.Linq;


namespace EnergyMission.Web.Controllers
{
    public class SensorsController : EmControllerBase
    {
        #region Private Members

        private readonly IRepository<Data.Sensor> _SensorAppService;
     

        #endregion

        #region Public Members

        public SensorsController()
        {

            _SensorAppService = new EnergyMission.Data.Repositories.Repository<Data.Sensor>();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            List<SensorEntity> objList = Mapper.Map<List<SensorEntity>>(_SensorAppService.Where(t => t.IsDeleted == false));
            return View(objList);
        }

        public ActionResult GridViewPartial()
        {
            List<SensorEntity> objList = Mapper.Map<List<SensorEntity>>(_SensorAppService.Where(t => t.IsDeleted == false));
            return PartialView("_GridViewPartial", objList);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult  Create()
        {
            ViewBag.msg = "";
            return View(); 
        }

        public JsonResult Edits(long Id)
        {



            // viewModel = new PersonAddEditViewModel
            //{
            //    Person = _PersonAppService.SelectByID(Id)
            IQueryable<Data.Sensor> ExistingSensor = _SensorAppService.Where(tt => tt.Id == Id);

            var res = (from sub in ExistingSensor
                       where sub.Id == Id
                       select new
                       {
                           id = sub.Id,
                           Name = sub.Name,
                           SensorType = sub.SensorType,
                           DataFeed = sub.DataFeed,
                           CreatedBy = sub.CreatorUserId,
                           CreatedOn = sub.CreationTime,



                       }
                                       );

            return Json(res, JsonRequestBehavior.AllowGet);
        }



          [AcceptVerbs(HttpVerbs.Post)]

        public ActionResult Create(SensorEntity objSensorEntity)
        {
            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            if (ModelState.IsValid)
            {
                try
                {
                    var LoggedInUserId = loggedInUser.UserProfileInformations.ClientId.Value;
                    if (objSensorEntity.Id == 0)
                    {
                     

                        objSensorEntity.IsDeleted = false;
                        objSensorEntity.CreationTime = DateTime.Now;
                        objSensorEntity.CreatorUserId = LoggedInUserId;
                        _SensorAppService.Insert(Mapper.Map<Data.Sensor>(objSensorEntity));
                        return RedirectToAction("Index");
                    }
                    else
                    {

                        objSensorEntity.LastModificationTime = objSensorEntity.LastModificationTime;
                        objSensorEntity.LastModifierUserId = LoggedInUserId;
                        _SensorAppService.Update(Mapper.Map<Data.Sensor>(objSensorEntity));
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                    ViewBag.msg = "Some error occured please try later..";
                }
            }
            else
            {
                ModelState.AddModelError("Caution", "Please fill required fields..");
            }


            return View(objSensorEntity);
        }



        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Delete(SensorEntity objobjSensorEntity)
        {
            return View();
        }


        public ActionResult Deletesensore(long Id)
        {
           
                try
                {
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    var Ids = loggedInUser.UserProfileInformations.ClientId.Value;
                    Data.Sensor objSensore = _SensorAppService.SelectByID((Id));
                    objSensore.LastModifierUserId = Ids;
                    objSensore.LastModificationTime = DateTime.Now;
                    objSensore.IsDeleted = true;


                    _SensorAppService.Update(Mapper.Map<Data.Sensor>(objSensore));
                    return RedirectToAction("Index");

                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                   
                }
           
            return RedirectToAction("GridViewPartial");
        }
        ///// <summary>
        /////     Called when we want to DELETE the sensor.
        ///// </summary>
        ///// <param name="Id">Unique identifier for the sensor record to delete.</param>
        ///// <returns>Returns the grid view on success, error otherwise.</returns>
        //public override ActionResult Delete(long Id)
        //{
        //    if (Id >= 0)
        //    {
        //        try
        //        {
        //            _SensorAppService.Delete(Id);
        //        }
        //        catch (Exception e)
        //        {
        //            StatusMessageError(e.Message);
        //        }
        //    }
        //    return RedirectToAction("GridViewPartial");
        //}

        ///// <summary>
        /////     Called when Edit is clicked in the GridView.
        ///// </summary>
        ///// <param name="Id">Unique identifier for the record to edit.</param>
        ///// <returns>Returns the edit view on view on success, error on failure.</returns>
        //public override ActionResult Edit(long Id)
        //{
        //    ViewBag.PageTitle = "Edit Sensor";
        //    return View("Edit", _SensorAppService.SelectByID(new Data.Sensor {Id = Id}));
        //}

        ///// <summary>
        /////     Displays the view that contains only the grid view.
        ///// </summary>
        ///// <returns>Returns the view containing the grid view.</returns>
        //[ValidateInput(false)]
        //public override ActionResult GridViewPartial()
        //{
        //    return PartialView("_GridViewPartial", RetrieveAllSensors());
        //}

        ///// <summary>
        /////     Main view to display all the sensor in the system.
        ///// </summary>
        ///// <returns>Returns the view for all the sensor in the system.</returns>
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Index()
        //{
        //    return View(RetrieveAllSensors());
        //}

        ///// <summary>
        /////     Called when New is clicked in the gridview.
        ///// </summary>
        ///// <returns>Returns the form that allows for a new sensor to be added.</returns>
        //public override ActionResult New()
        //{
        //    ViewBag.PageTitle = "New Sensor";
        //    //return View("Create", _SensorAppService.GetNewSensor(new GetNewSensorInput()));

        //    return View("Create");
        //}

        ///// <summary>
        /////     Called when a sensor needs to be UPDATED to the database.
        ///// </summary>
        ///// <param name="aSensor">Information about the sensor to be updated.</param>
        ///// <returns>Returns the list of sensor on success, error on failure.</returns>
        //[AcceptVerbs(HttpVerbs.Post)]
        //public override ActionResult Update(GetSensorOutput aSensor)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _SensorAppService.Update(Mapper.Map<Data.Sensor>(aSensor));
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception e)
        //        {
        //            StatusMessageError(e.Message);
        //        }
        //    }
        //    else
        //        StatusMessageErrorsModelState(ModelState);

        //    return View("Edit", aSensor);
        //}

        #endregion
    }
}