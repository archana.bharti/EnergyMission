﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Domain.Entities;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Suburb.Dtos;
using EnergyMission.Data.Repositories;
using System.Linq;

namespace EnergyMission.Web.Controllers
{
    public class SuburbsController : EmControllerGridView<GetNewSuburbOutput, GetSuburbOutput>
    {
        #region Private Members

        private readonly IRepository<Data.Suburb> _SuburbAppService;

        /// <summary>
        ///     Helper method to retrieve all the suburbs.
        /// </summary>
        /// <returns>Returns a list of suburbs</returns>
        private List<SuburbEntity> RetrieveAllSuburbs()
        {
            //GetSuburbsInput getSuburbsInput = new GetSuburbsInput();
            return Mapper.Map<List<SuburbEntity>>(_SuburbAppService.Where(tt=>tt.IsDeleted==false));
                                 
        }

        #endregion

        #region Public Members


        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aSuburbAppService">Application service for suburbs</param>
        public SuburbsController()
        {
            ViewBag.PageTitle = "Suburbs";
            _SuburbAppService = new  Repository < Data.Suburb > ();
        }

        /// <summary>
        ///     Called when we want to CREATE a suburb in the database.
        /// </summary>
        /// <param name="aSuburb">Information about the suburb to create.</param>
        /// <returns>Returns gridview on success, create suburb view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Create(GetNewSuburbOutput aSuburb)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    if (aSuburb.Id == 0)
                    {
                        var value = _SuburbAppService.Wheres(tt => tt.Name == aSuburb.Name  && tt.StateId==aSuburb.StateId && tt.IsDeleted == false);
                          if (value == null)
                          {
                              aSuburb.CreationTime = DateTime.Now;

                              _SuburbAppService.Insert(Mapper.Map<Data.Suburb>(aSuburb));
                              TempData["msg"] = "Subrub added successfully!";
                          }
               
                              else

                              throw new Exception("Suburb allready exits!");
                    }
                    else
                    {

                         var value = _SuburbAppService.Wheres(tt => tt.Name == aSuburb.Name  && tt.StateId==aSuburb.StateId && tt.IsDeleted == false);
                         if (value == null)
                         {
                             Data.Suburb Sub = _SuburbAppService.SelectByID(aSuburb.Id);
                             Sub.Name = aSuburb.Name;
                             Sub.StateId = aSuburb.StateId;
                             Sub.LastModificationTime = DateTime.Now;

                             _SuburbAppService.Update(Sub);
                         }

                         else

                             throw new Exception("Suburb allready exits!");
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                    TempData["msg"] = e.Message;
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            ViewBag.msg = TempData["msg"];
            return View("Index");
        }

        /// <summary>
        ///     Called when we want to DELETE the suburb.
        /// </summary>
        /// <param name="Id">Unique identifier for the suburb record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public override ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {

            Data.Suburb sur=    _SuburbAppService.SelectByID(Id);
            sur.IsDeleted = true;
            sur.LastModificationTime = DateTime.Now;
                    _SuburbAppService.Update(sur);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public override ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit Suburb";
            return View("Edit", _SuburbAppService.SelectByID(new GetSuburbInput { Id = Id }));
        }

        public  JsonResult  Edits(long Id)
        {

            IQueryable<Data.Suburb> SSB = _SuburbAppService.Where(tt=>tt.Id==Id);

 var res = (from sub in SSB
                        where sub.Id==Id
                         select new
                         {
                             id = sub.Id,
                             stateid = sub.StateId,
                             name = sub.Name
                           
                         }
                            );



 return Json(res, JsonRequestBehavior.AllowGet);
        
        }
        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public override ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllSuburbs());
        }

        /// <summary>
        ///     Main view to display all the suburbs in the system.
        /// </summary>
        /// <returns>Returns the view for all the suburbs in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            if (TempData["msg"] != null)
            {
                ViewBag.msg = TempData["msg"];
            
            }
            return View(RetrieveAllSuburbs());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new suburb to be added.</returns>
        public override ActionResult New()
        {
            ViewBag.PageTitle = "New Suburb";
            return View("Create");
        }

        /// <summary>
        ///     Called when a suburb needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aSuburb">Information about the suburb to be updated.</param>
        /// <returns>Returns the list of suburbs on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Update(GetSuburbOutput aSuburb)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _SuburbAppService.Update(Mapper.Map<Data.Suburb>(aSuburb));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aSuburb);
        }

        #endregion
    }

}