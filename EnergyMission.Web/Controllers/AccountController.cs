﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EnergyMission.Common.Security;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.Identity;
using EnergyMission.Web.ViewModels.Account;
using EnergyMission.Web._Framework;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace EnergyMission.Web.Controllers
{
    [Authorize]
    public class AccountController : EmControllerBase
    {
        #region Private Members

        private ApplicationSignInManager _SignInManager;
        private ApplicationUserManager _UserManager;

        #endregion

        #region Public Members

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager aUserManager, ApplicationSignInManager aSignInManager)
        {
            UserManager = aUserManager;
            SignInManager = aSignInManager;
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string aUserId, string aCode)
        {
            if (aUserId == null || aCode == null)
            {
                return View("Error");
            }
            IdentityResult result = await UserManager.ConfirmEmailAsync(aUserId, aCode);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string aProvider, string aReturnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(aProvider, Url.Action("ExternalLoginCallback", "Account", new {ReturnUrl = aReturnUrl}));
        }

        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string aReturnUrl)
        {
            ExternalLoginInfo loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            SignInStatus result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(aReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new {ReturnUrl = aReturnUrl, RememberMe = false});
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = aReturnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel {Email = loginInfo.Email});
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel aModel, string aReturnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                ExternalLoginInfo info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                ApplicationUser user = new ApplicationUser {UserName = aModel.Email, Email = aModel.Email};
                IdentityResult result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(aReturnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = aReturnUrl;
            return View(aModel);
        }

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel aModel)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = await UserManager.FindByNameAsync(aModel.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
               
                

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                   string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                   var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                   await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                   return RedirectToAction("ForgotPasswordConfirmation", "Account");
                    }
            }

            // If we got this far, something failed, redisplay form
            return View(aModel);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            TempData["chk"] = 0;
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Login(string aReturnUrl)
        {

            ViewBag.ReturnUrl = aReturnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel aModel, string aReturnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(aModel);
            }


            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            SignInStatus result = await SignInManager.PasswordSignInAsync(aModel.Email, aModel.Password, aModel.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    TempData["chk"] = 0;

                    TempData.Keep("chk");
                    return  RedirectToLocal ("/home/Index");

                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new {ReturnUrl = aReturnUrl, RememberMe = aModel.RememberMe});
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Unknown User Name or Password.");
                    return View(aModel);
            }
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel aModel)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser {UserName = aModel.Email, Email = aModel.Email};
                IdentityResult result = await UserManager.CreateAsync(user, aModel.Password);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return RedirectToAction("Index", "Home");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(aModel);
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code,string userid)
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();
            if (code != null)
            {
                model.Code = code;
                model.userId = userid;
            }
            
            return  View(model);

        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel aModel)
        {
            if (!ModelState.IsValid)
            {
                return View(aModel);
            }
            ApplicationUser user = UserManager.FindById(aModel.userId);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            IdentityResult result = await UserManager.ResetPasswordAsync(user.Id, aModel.Code, aModel.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string aReturnUrl, bool aRememberMe)
        {
            string userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            IList<string> userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            List<SelectListItem> factorOptions = userFactors.Select(purpose => new SelectListItem {Text = purpose, Value = purpose})
                                                            .ToList();
            return View(new SendCodeViewModel {Providers = factorOptions, ReturnUrl = aReturnUrl, RememberMe = aRememberMe});
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel aModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(aModel.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new {Provider = aModel.SelectedProvider, ReturnUrl = aModel.ReturnUrl, RememberMe = aModel.RememberMe});
        }

        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string aProvider, string aReturnUrl, bool aRememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            ApplicationUser user = await UserManager.FindByIdAsync(await SignInManager.GetVerifiedUserIdAsync());
            if (user != null)
            {
                string code = await UserManager.GenerateTwoFactorTokenAsync(user.Id, aProvider);
            }
            return View(new VerifyCodeViewModel {Provider = aProvider, ReturnUrl = aReturnUrl, RememberMe = aRememberMe});
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel aModel)
        {
            if (!ModelState.IsValid)
            {
                return View(aModel);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            SignInStatus result = await SignInManager.TwoFactorSignInAsync(aModel.Provider, aModel.Code, isPersistent: aModel.RememberMe, rememberBrowser: aModel.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(aModel.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(aModel);
            }
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _SignInManager ?? HttpContext.GetOwinContext()
                                                    .Get<ApplicationSignInManager>();
            }
            private set { _SignInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _UserManager ?? HttpContext.GetOwinContext()
                                                  .GetUserManager<ApplicationUserManager>();
            }
            private set { _UserManager = value; }
        }

        #endregion

        #region Helpers

        // Used for XSRF protection when adding external logins
        internal class ChallengeResult : HttpUnauthorizedResult
        {
            #region Public Members

            public ChallengeResult(string aProvider, string aRedirectUri)
                : this(aProvider, aRedirectUri, null)
            {
            }

            public ChallengeResult(string aProvider, string aRedirectUri, string aUserId)
            {
                LoginProvider = aProvider;
                RedirectUri = aRedirectUri;
                UserId = aUserId;
            }

            public override void ExecuteResult(ControllerContext aContext)
            {
                AuthenticationProperties properties = new AuthenticationProperties {RedirectUri = RedirectUri};
                if (UserId != null)
                {
                    properties.Dictionary[XSRF_KEY] = UserId;
                }
                aContext.HttpContext.GetOwinContext()
                        .Authentication.Challenge(properties, LoginProvider);
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            #endregion
        }

        #region Private Members

        private const string XSRF_KEY = "XsrfId";

        private void AddErrors(IdentityResult aResult)
        {
            foreach (string error in aResult.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string aReturnUrl)
        {
            if (Url.IsLocalUrl(aReturnUrl))
            {
                return Redirect(aReturnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext()
                                  .Authentication;
            }
        }

        #endregion

        #endregion

        //
        // GET: /Account/ExternalLoginCallback
    }
}