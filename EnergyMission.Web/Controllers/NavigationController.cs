﻿using System;
using System.Web.Mvc;
using EnergyMission.Common.Security;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels.Navigation;
using EnergyMission.Web._Framework;

namespace EnergyMission.Web.Controllers
{
    public class NavigationController : EmControllerBase
    {
        /// <summary>
        ///     Returns a view that represents the top navigation for the logged in user
        /// </summary>
        /// <returns></returns>
        public ActionResult Top()
        {
            switch (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager))
            {
                case SystemRoleKindEnum.Administrator:
                    return PartialView("AdministratorTopNav");
                case SystemRoleKindEnum.Client:
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    TopNavClientViewModel viewModel = new TopNavClientViewModel
                    {
                        ClientId = loggedInUser.UserProfileInformations.ClientId.Value
                    };

                    return PartialView("ClientTopNav", viewModel);
                case SystemRoleKindEnum.TeamManager:
                    return PartialView("TeamManagerTopNav");
                case SystemRoleKindEnum.TeamMember:
                    return PartialView("TeamMemberTopNav");
                case SystemRoleKindEnum.SuperAdministrator:
                    return PartialView("SuperAdministratorTopNav");
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}