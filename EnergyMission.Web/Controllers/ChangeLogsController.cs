﻿using System.Web.Mvc;
using EnergyMission.ChangeLog;
using EnergyMission.Web._Framework;
using Natiki.Web.ChangeLog;

namespace EnergyMission.Web.Controllers
{
    public class ChangeLogsController : EmControllerBase
    {
        #region Private Members

        private readonly IChangeLogAppService _ChangeLogAppService;

        #endregion

        #region Public Members

        public ChangeLogsController(IChangeLogAppService aChangeLogAppService)
        {
            ViewBag.PageTitle = "Change Log";
            _ChangeLogAppService = aChangeLogAppService;
        }

        // GET: ChangeLogs
        public ActionResult Index()
        {
            ViewBag.ChangeLogTableHtml = _ChangeLogAppService.ChangeLogTableAsHtml();
            return View();
        }

        #endregion
    }
}