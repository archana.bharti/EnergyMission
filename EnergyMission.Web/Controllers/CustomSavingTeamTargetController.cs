﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data.Repositories;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using EnergyMission.Web._Framework;

namespace EnergyMission.Web.Controllers
{
    public class CustomSavingTeamTargetController : EmControllerBase
    {
        private readonly ICustomSavingTeamTargetRepositoy _ICustomSavingTeamTargetRepositoy;
        // GET: CustomSavingTarget
        public ActionResult Index(CustomSavingTeamTargetEntity CCustomSavingTeamTargetEntity)
        {

            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var value = loggedInUser.UserProfileInformations.PersonId.Value;
            var ClientId = loggedInUser.UserProfileInformations.ClientId.Value;
            CCustomSavingTeamTargetEntity.CreatorUserId = value;
            CCustomSavingTeamTargetEntity.ClientId = ClientId;
            if ((TempData["msg"] != string.Empty) || (TempData["msg"] != null))
                ViewBag.msg = TempData["msg"];
            return View(_ICustomSavingTeamTargetRepositoy.getByID(CCustomSavingTeamTargetEntity));

            //return View();
    }
        public CustomSavingTeamTargetController()
        {
            _ICustomSavingTeamTargetRepositoy = new CustomSavingTeamTargetRepositoy();

        }

        [HttpPost]
        public ActionResult CreatSetPricce(CustomSavingTeamTargetEntity CCustomSavingTeamTargetEntity, string button)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                CCustomSavingTeamTargetEntity.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                var ClientId = loggedInUser.UserProfileInformations.ClientId.Value;
                CCustomSavingTeamTargetEntity.ClientId = ClientId;
                if (button == "Save Changes")
                {

                    _ICustomSavingTeamTargetRepositoy.InsertClient(CCustomSavingTeamTargetEntity);
                    TempData["msg"] = "Record Saved Succesfully";

                }
                if (button == "Restore Current Values")
                {


                    _ICustomSavingTeamTargetRepositoy.UpdateClient(CCustomSavingTeamTargetEntity);
                    TempData["msg"] = "Restore Current Values Saved Succesfully";

                }
                if (button == "Done")
                {
                    return RedirectToAction("Index","Home");
                   
                }


                var values = _ICustomSavingTeamTargetRepositoy.getByID(CCustomSavingTeamTargetEntity);

                return RedirectToAction("Index");

            }
            else
            {


            }
            //StatusMessageErrorsModelState(ModelState);

            return View("Index");
        }




    }
}