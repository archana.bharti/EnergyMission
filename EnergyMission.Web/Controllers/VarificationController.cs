﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using EnergyMission.Data.Repositories;
namespace EnergyMission.Web.Controllers
{
    public class VarificationController : Controller
    {
        VarificationRepository varfication = new VarificationRepository();
     
        // GET: Varification
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult person(string Id)
        {
            string resultString = Regex.Match(Id, @"\d+").Value;


            varfication.varifiedPerson(Convert.ToInt64(resultString));
            ViewBag.msg = "Successfully varified !"; 

            return View();
        }
    }
}