﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Client;
using EnergyMission.Web._Framework;
using EnergyMission.Common.Security;
using EnergyMission.Client.Dtos;
using EnergyMission.Domain.Entities;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using EnergyMission.Data.Repositories;
using Newtonsoft.Json;
using System.Threading.Tasks;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels.Client;
using EnergyMission.Web.ViewModels.User;
using Microsoft.AspNet.Identity;
using EnergyMission.User.Dtos;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnergyMission.Web.Controllers
{
    public class ClientsController : EmControllerBase
    {
        #region Private Members

        private readonly IClientRepositoy  _ClientAppService;
        private readonly IRepository<Data.Organization> OrganizationAppService;
        /// <summary>
        ///     Helper method to retrieve all the clients.
        /// </summary>
        /// <returns>Returns a list of clients</returns>
        private List<ClientEntity> RetrieveAllClients()
        {





            //if(loggedInUser.UserProfileInformation.ClientId!=null)


            //    return Mapper.Map<List<ClientEntity>>(_ClientAppService.GetClients(loggedInUser.UserProfileInformation.ClientId));
            //else

                return Mapper.Map<List<ClientEntity>>(_ClientAppService.GetClients(0));



            //GetClientsInput getClientsInput = new GetClientsInput();
            //return _ClientAppService.GetClients(getClientsInput)
            //                        .Clients;
        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientAppService">Application service for clients</param>
        public ClientsController()
        {

            ViewBag.PageTitle = "Clients";
         
            _ClientAppService = new ClientRepositoy() ;
                OrganizationAppService = new Repository<Data.Organization>();
        }

        /// <summary>
        ///     Called when we want to CREATE a client in the database.
        /// </summary>
        /// <param name="aClient">Information about the client to create.</param>
        /// <returns>Returns gridview on success, create client view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Createev(OrganizationEntity aClient)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    if (aClient.Id != null)
                    {
                        var client = OrganizationAppService.SelectByID(aClient.Id);
                        client.NOE = aClient.NOE;
                        client.Name = aClient.Name;
                        client.SuperAdminEmail = aClient.SuperAdminEmail;
                        _ClientAppService.UpdateClient(client);
                    }
                    else

                    
                         _ClientAppService.UpdateClient(Mapper.Map<Data.Organization>(aClient));



                  
                  
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    ViewBag.msg = "Kindly edit client then add orginization details";
                    return View("Index");
                }
            }

            ViewBag.msg = "Kindly edit client then add orginization details";
            return View("Index");
        }

        /// <summary>
        ///     Called when we want to DELETE the client.
        /// </summary>
        /// <param name="Id">Unique identifier for the client record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public  ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try

                {
                    _ClientAppService.DeleteClient(Id);
                    _ClientAppService.Save();
            var value=        _ClientAppService.GetUserID(Id);

            ApplicationUser appUser = AppUserManager.FindById(value);
            if (appUser != null)
                    AppUserManager.Delete(appUser);
                


                }
                catch (Exception e)
                {

                    ModelState.AddModelError("", e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
       
        public JsonResult  Edits(long Id)
        {
            ViewBag.PageTitle = "Edit Client";
            var result = _ClientAppService.GetClientByIds(Id);
   //var x = from i in _ClientAppService.GetClientById(Id)
   //                 select new { result.StateId, result.Id, result.Name, result.NOE, result.SuperAdminEmail, result.PostCodeId, result.City, result.Address, result.Geo_location_Latitude, result.Geo_location_Longditude};
            return Json(result, JsonRequestBehavior.AllowGet);
            //return View("Edit", _ClientAppService.GetClientById(Id));
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public  ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllClients());
        }

        /// <summary>
        ///     Main view to display all the clients in the system.
        /// </summary>
        /// <returns>Returns the view for all the clients in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            ViewBag.msg = "";
            return View(RetrieveAllClients());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new client to be added.</returns>
        /// <summary>
        ///     Called when a country needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aClient">Information about the client to be updated.</param>
        /// <returns>Returns the list of clients on success, error on failure.</returns>
        ///   [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Create()
        {
            return View();
        }
       [HttpPost]
        public async Task<ActionResult> Create(ClientEntity aClient)
        {
     

            if (ModelState.IsValid)
            {
                try

                     
                {
                    if (AppUserManager.FindByName(aClient.EmailAddress) != null)
                    {


                        throw new Exception("User with that email address already exists.");
                        //throw new Exception("User with that email address already exists.");
                    }





                    if (_ClientAppService.GetClientbyname(aClient.Name))
                    {
                        aClient.Status = 3;



                        long value = _ClientAppService.InsertClient(Mapper.Map<Data.Client>(aClient));

                        //Now add the user
                        ApplicationUser appUser = new ApplicationUser
                        {

                            UserName = aClient.EmailAddress,
                            Email = aClient.EmailAddress,
                            UserProfileInformations = new UserProfileInformationEntity { ClientId = value, CreationTime = DateTime.Now }
                        };
                        IdentityResult result = await AppUserManager.CreateAsync(appUser, aClient.Password);
                        if (result.Succeeded)
                        {
                            ApplicationUser apps = AppUserManager.FindByName(aClient.EmailAddress);


                            //string role_key=  _ClientAppService.RoleKey("Client");

                            AppUserManager.AddToRole(apps.Id, "Client");
                            ModelState.Clear();
                            ViewBag.msg = "Client added successfully!";

                            return View();
                        }
                    }

                    else
                    {

                        throw new Exception("Client allready exists!.");
                    
                    }
             
                }
                catch (Exception e)
                {

                    ModelState.AddModelError("", e.Message);
                    ViewBag.msg = e.Message;
                }
            }
            

            return View("Create");
        }

        #endregion
    }
}