﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data.Repositories;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using EnergyMission.Web._Framework;

namespace EnergyMission.Web.Controllers
{
    public class CustomSavingTargetController : EmControllerBase
    {

        private readonly ICustomSavingTargetRepositoy _ICustomSavingTargetRepositoy;
        // GET: CustomSavingTarget
        public ActionResult Index(CustomSavingTargetEntity CCustomSavingTargetEntity)
        {

            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var value = loggedInUser.UserProfileInformations.PersonId.Value;
            CCustomSavingTargetEntity.CreatorUserId = value;
            if ((TempData["msg"] != string.Empty) || (TempData["msg"] != null))

                ViewBag.msg = TempData["msg"];
            return View(_ICustomSavingTargetRepositoy.getByID(CCustomSavingTargetEntity));

            //return View();
        }
        public CustomSavingTargetController()
        {
            _ICustomSavingTargetRepositoy = new CustomSavingTargetRepositoy();

        }

        [HttpPost]
        public ActionResult CreatSetPricce(CustomSavingTargetEntity CCustomSavingTargetEntity, string button)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                CCustomSavingTargetEntity.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                if (button == "Save Changes")
                {

                    _ICustomSavingTargetRepositoy.InsertClient(CCustomSavingTargetEntity);
                    TempData["msg"] = "Record Saved Succesfully";

                }
                if (button == "Restore Current Values")
                {


                    _ICustomSavingTargetRepositoy.UpdateClient(CCustomSavingTargetEntity);
                    TempData["msg"] = "Restore Current Values Saved Succesfully";

                }



                var values = _ICustomSavingTargetRepositoy.getByID(CCustomSavingTargetEntity);

                return RedirectToAction("Index");

            }
            else
            {


            }
            //StatusMessageErrorsModelState(ModelState);

            return View("Index");
        }




    }
}