﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data.Repositories;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using EnergyMission.Web._Framework;
namespace EnergyMission.Web.Controllers
{
    public class SetResourcePriceController : EmControllerBase
    {

        private readonly ISetResourcePriceRepositoy _ISetResourcePriceRepositoy;
        // GET: SetResourcePrice
        public ActionResult Index(SetResourcePriceEntity SSetResourcePriceEntity)
        {      


            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var value = loggedInUser.UserProfileInformations.PersonId.Value;
            SSetResourcePriceEntity.CreatorUserId = value;
            if ((TempData["msg"] != string.Empty) || (TempData["msg"] != null))          

                ViewBag.msg = TempData["msg"];
            return View(_ISetResourcePriceRepositoy.getByID(SSetResourcePriceEntity));
        }
        public SetResourcePriceController()
        {
            _ISetResourcePriceRepositoy = new SetResourcePriceRepositoy();

        }       


        [HttpPost]
        public ActionResult CreatSetPricce(SetResourcePriceEntity SSetResourcePriceEntity, string button)
        {
            if (ModelState.IsValid)
            {
                ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                SSetResourcePriceEntity.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                if (button == "Save Changes")
                {
                
                    _ISetResourcePriceRepositoy.InsertClient(SSetResourcePriceEntity);
                    TempData["msg"] = "Record Saved Succesfully";
                  
                }
                if (button == "Restore Current Values")
                {
                 
 
                    _ISetResourcePriceRepositoy.UpdateClient(SSetResourcePriceEntity);
                    TempData["msg"] = "Restore Current Values Saved Succesfully";
                  
                }



                var values = _ISetResourcePriceRepositoy.getByID(SSetResourcePriceEntity);

               return RedirectToAction("Index");

            }
            else
            {


            }
            //StatusMessageErrorsModelState(ModelState);

            return View("Index");
        }

       





    }
}