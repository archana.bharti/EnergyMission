﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Abp.Dependency;
using EnergyMission.DataImport;
using EnergyMission.DataImport.Importers;
using EnergyMission.DataPoint;

namespace EnergyMission.Web.Controllers
{
    /// <summary>
    /// Controller used to test various aspects of the system
    /// </summary>
    public class TestingController : Controller
    {
        // GET: Testing
        public ActionResult TestOpetergyImport()
        {
            DataImportProcessorManager dataImportProcessorManager = new DataImportProcessorManager();
            AnuOptergyDataImporter dataImporter = new AnuOptergyDataImporter(HttpContext.Server.MapPath("~/TestImport.csv"));
            var dataPointAppService = IocManager.Instance.Resolve<IDataPointAppService>();

            string errorMessage;
            ViewBag.ImportResult = dataImportProcessorManager.Process(1, dataImporter, dataPointAppService, out errorMessage);
            ViewBag.ImportErrorMessage = errorMessage;
            
            return View();
        }

        public ActionResult TestReadOfSiteMap()
        {
            XmlDocument xmlSiteMap = new ConfigXmlDocument();
            xmlSiteMap.Load(Server.MapPath("~/Test.sitemap"));
            return null;
        }
    }
}