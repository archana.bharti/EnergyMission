﻿using EnergyMission.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnergyMission.Domain.Entities;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using EnergyMission.Data.Repositories;
namespace EnergyMission.Web.Controllers
{
    public class VisualizationController : Controller
    {

        private EmDbContext db = new EmDbContext();
        // GET: Visualization
        private readonly IVisualizatin _ClientAppService;
        private readonly IRepository<Data.LocationMapping> _LocationAppService = new EnergyMission.Data.Repositories.Repository<Data.LocationMapping>();

        public VisualizationController()
        {
            _ClientAppService = new VisualilzatiionRepository();
        }


        [HttpGet]
        public ActionResult Index(Boolean Electricitys = false, Boolean Water = false, Boolean Gas = false, Boolean IsDay=false, Boolean IsNight=false,Boolean Label=false,Boolean DD=false,Boolean MM=false,Boolean YY=false)
        {
            VisulationSearchEntity objVisulationSearchEntity = new VisulationSearchEntity();
            objVisulationSearchEntity.Electricitys = Electricitys;
            objVisulationSearchEntity.Water = Water;
            objVisulationSearchEntity.Gas = Gas;
            objVisulationSearchEntity.IsDay = IsDay;
            objVisulationSearchEntity.IsNight = IsNight;
            objVisulationSearchEntity.Label = Label;

            long SiteId = 0, StructureId = 0, StructureDivisionId = 0, SensorId = 0, TeamId = 0, LocationId = 0;
            object values = null;

            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                SearchComman.searchConditions = values as Dictionary<string, object>;
                SiteId = (long)SearchComman.GetSearchConditionValue(SearchComman.searchConditions, "SiteId");
                StructureId = (long)SearchComman.GetSearchConditionValue(SearchComman.searchConditions, "StructureId");
                StructureDivisionId = (long)SearchComman.GetSearchConditionValue(SearchComman.searchConditions, "StructureDivisionId");
                SensorId = (long)SearchComman.GetSearchConditionValue(SearchComman.searchConditions, "SensorId");
                TeamId = (long)SearchComman.GetSearchConditionValue(SearchComman.searchConditions, "TeamId");
                LocationId = (long)SearchComman.GetSearchConditionValue(SearchComman.searchConditions, "LocationId");
            }
        
            ChartSeriesBindingDemoOptions options = new ChartSeriesBindingDemoOptions();
            string category = string.IsNullOrEmpty(options.Category) ? ChartSeriesBindingDemoOptions.DefaultCategory : options.Category;
            double valueofDay = 0;
            double valueofNight = 0;
            double OverFlowUnderFlow = 0;
            double TodayOverFlowUnderFlow = 0;
            double MonthOverFlowUnderFlow = 0;
            double YearOverFlowUnderFlow = 0;



            objVisulationSearchEntity.ObjList = _ClientAppService.Getvaisualizaton(SiteId, StructureId, StructureDivisionId, TeamId, Electricitys, Gas, Water, 0, 0, 0, IsNight, IsDay, DateTime.Parse("12/01/2013"), DateTime.Parse("12/19/2013"), LocationId, SensorId);
            for (int i = 0; i < objVisulationSearchEntity.ObjList.Count(); i++)
            {
                double Less10 = (objVisulationSearchEntity.ObjList[i].value - objVisulationSearchEntity.ObjList[i].prvValue) * 100;

                double Target = (Less10 / objVisulationSearchEntity.ObjList[i].prvValue);
                string targetString = String.Format("{0:0.00}", Target);
                objVisulationSearchEntity.ObjList[i].Target = Convert.ToDouble(targetString);

                valueofDay = valueofDay + objVisulationSearchEntity.ObjList[i].Day;
                valueofNight = valueofNight + objVisulationSearchEntity.ObjList[i].Night;

                OverFlowUnderFlow = OverFlowUnderFlow + objVisulationSearchEntity.ObjList[i].Target;


            }

            int CurrentYear = DateTime.Now.Year;
            ViewBag.PreviousYear = CurrentYear - 1;
            ViewBag.Percent = "- 10%";


            if (OverFlowUnderFlow > -3 && OverFlowUnderFlow <= 3)
            {
                ViewBag.OverFlowUnderFlow = "Ok";
            }

            if (OverFlowUnderFlow < -3)
            {
                ViewBag.OverFlowUnderFlow = "SAVING";
            }

            if (OverFlowUnderFlow > 3)
            {
                ViewBag.OverFlowUnderFlow = "EXCESS";
            }


            ViewBag.LocationName = "All";
            if (valueofDay != 0 && valueofNight != 0)
            {
                @ViewBag.ChartType = "Stack";
            }
            else
            {
                @ViewBag.ChartType = "Normal";
            }

            if (valueofDay != 0 & valueofNight == 0)
            {
                @ViewBag.DayNight = "Day";
            }
          
            if (valueofDay == 0 & valueofNight != 0)
            {
                @ViewBag.DayNight = "Night";
            }
            if (objVisulationSearchEntity.ObjList.Count > 0)
                @ViewBag.type = objVisulationSearchEntity.ObjList[0].type;
            else
                @ViewBag.type = "Days";

            ViewBag.IsDefault = "Yes";
            ViewData["OptionsKey"] = options;


            ///////////////////////////// Default Functionality Region////////////

            # region today

            var Today = _ClientAppService.Getvaisualizaton(SiteId, StructureId, StructureDivisionId, TeamId, Electricitys, Gas, Water,2013, 12, 8, IsNight, IsDay, null, null, LocationId, SensorId);
           
            Double TotalTodayConsumedValue = 0;
            for (int i = 0; i < Today.Count(); i++)
            {
                double Less10 = (Today[i].value - Today[i].prvValue) * 100;

                double Target = (Less10 / Today[i].prvValue);
                string targetString = String.Format("{0:0.00}", Target);
                Today[i].Target = Convert.ToDouble(targetString == "Infinity" || targetString == "NaN" ? 0 : Convert.ToDouble(targetString));

                valueofDay = valueofDay + Today[i].Day;
                valueofNight = valueofNight + Today[i].Night;


                string CovertedTargaet = (Today[i].Target).ToString();

                TodayOverFlowUnderFlow = TodayOverFlowUnderFlow + (CovertedTargaet == "Infinity" || CovertedTargaet == "NaN" ? 0 : Convert.ToDouble(CovertedTargaet));
                TotalTodayConsumedValue = TotalTodayConsumedValue + Today[i].value;

            }

            ViewBag.TotalTodayConsumedValue = TotalTodayConsumedValue;

            ViewBag.TodayPercent = TodayOverFlowUnderFlow + "%";


            if (TodayOverFlowUnderFlow > -3 && TodayOverFlowUnderFlow <= 3)
            {
                ViewBag.TodayOverFlowUnderFlow = "Ok";
            }
            if (TodayOverFlowUnderFlow < -3)
            {
                ViewBag.TodayOverFlowUnderFlow = "SAVING";
            }

            if (TodayOverFlowUnderFlow > 3)
            {
                ViewBag.TodayOverFlowUnderFlow = "EXCESS";
            }


            if (valueofDay != 0 && valueofNight != 0)
            {
                @ViewBag.TodayChartType = "Stack";
            }
            else
            {
                @ViewBag.TodayChartType = "Normal";
            }
            if (Today.Count > 0)
                @ViewBag.Todaytype = Today[0].type;
            else
                @ViewBag.Todaytype = "Days";

            ViewBag.TodayData = Today.ToList();
            #endregion


            # region Year


            var Year = _ClientAppService.Getvaisualizaton(SiteId, StructureId, StructureDivisionId, TeamId, Electricitys, Gas, Water, 2013, 0, 0, IsNight, IsDay, null, null, LocationId, SensorId);
           
            Double TotalYearConsumedValue = 0;
            for (int i = 0; i < Year.Count(); i++)
            {
                double Less10 = (Year[i].value - Year[i].prvValue) * 100;

                double Target = (Less10 / Year[i].prvValue);
                string targetString = String.Format("{0:0.00}", Target);
                Year[i].Target = Convert.ToDouble(targetString == "Infinity" || targetString == "NaN" ? 0 : Convert.ToDouble(targetString));

                valueofDay = valueofDay + Year[i].Day;
                valueofNight = valueofNight + Year[i].Night;

                string CovertedTargaet = (Year[i].Target).ToString();


                YearOverFlowUnderFlow = YearOverFlowUnderFlow + (CovertedTargaet == "Infinity" || CovertedTargaet == "NaN" ? 0 : Convert.ToDouble(CovertedTargaet));
                TotalYearConsumedValue = TotalYearConsumedValue + Year[i].value;

            }
            ViewBag.TotalYearConsumedValue = TotalYearConsumedValue;
            ViewBag.YearPercent = YearOverFlowUnderFlow + "%";

            if (YearOverFlowUnderFlow > -3 && YearOverFlowUnderFlow <= 3)
            {
                ViewBag.YearOverFlowUnderFlow = "Ok";
            }
            if (YearOverFlowUnderFlow < -3)
            {
                ViewBag.YearOverFlowUnderFlow = "SAVING";
            }

            if (YearOverFlowUnderFlow > 3)
            {
                ViewBag.YearOverFlowUnderFlow = "EXCESS";
            }

            if (valueofDay != 0 && valueofNight != 0)
            {
                @ViewBag.YearChartType = "Stack";
            }
            else
            {
                @ViewBag.YearChartType = "Normal";
            }
            if (Year.Count > 0)
                @ViewBag.Yeartype = Year[0].type;
            else
                @ViewBag.Yeartype = "Days";

            ViewBag.YearData = Year.ToList();

            #endregion


            # region Month


            var Month = _ClientAppService.Getvaisualizaton(SiteId, StructureId, StructureDivisionId, TeamId, Electricitys, Gas, Water, 2013, 12, 0, IsNight, IsDay, null, null, LocationId, SensorId);
          
            Double TotalMonthConsumedValue = 0;
            for (int i = 0; i < Month.Count(); i++)
            {
                double Less10 = (Month[i].value - Month[i].prvValue) * 100;

                double Target = (Less10 / Month[i].prvValue);
                string targetString = String.Format("{0:0.00}", Target);
                Month[i].Target = Convert.ToDouble(targetString == "Infinity" || targetString == "NaN" ? 0 : Convert.ToDouble(targetString));

                valueofDay = valueofDay + Month[i].Day;
                valueofNight = valueofNight + Month[i].Night;

                string CovertedTargaet = (Month[i].Target).ToString();

                MonthOverFlowUnderFlow = MonthOverFlowUnderFlow + (CovertedTargaet == "Infinity" || CovertedTargaet == "NaN" ? 0 : Convert.ToDouble(CovertedTargaet));
                TotalMonthConsumedValue = TotalMonthConsumedValue + Month[i].value;

            }
            ViewBag.TotalMonthConsumedValue = TotalMonthConsumedValue;
            ViewBag.MonthPercent = MonthOverFlowUnderFlow + "%";



            if (MonthOverFlowUnderFlow > -3 && MonthOverFlowUnderFlow <= 3)
            {
                ViewBag.MonthOverFlowUnderFlow = "Ok";
            }
            if (MonthOverFlowUnderFlow < -3)
            {
                ViewBag.MonthOverFlowUnderFlow = "SAVING";
            }

            if (MonthOverFlowUnderFlow > 3)
            {
                ViewBag.MonthOverFlowUnderFlow = "EXCESS";
            }



            ViewBag.LocationName = "All";
            if (valueofDay != 0 && valueofNight != 0)
            {
                @ViewBag.MonthChartType = "Stack";
            }
            else
            {
                @ViewBag.MonthChartType = "Normal";
            }
            if (Month.Count > 0)
                @ViewBag.Monthtype = Month[0].type;
            else
                @ViewBag.Yeartype = "Days";

            ViewBag.MonthData = Month.ToList();

            #endregion

            if (Label == true)
            {
                ViewBag.HideLable = "true";
            }
            else
            {
                ViewBag.HideLable = "false";
            }
            //////////////////////////////////////////////////////////////////////


            return View(objVisulationSearchEntity);
        }






        [HttpPost]
        public ActionResult Index(VisulationSearchEntity objVisulationSearchEntity ,int Year = 0, int Day = 0, int Month = 0)
        {
            objVisulationSearchEntity.Start_Date = objVisulationSearchEntity.Start_Date == Convert.ToDateTime("1/1/0001 12:00:00 AM") ? null : objVisulationSearchEntity.Start_Date;

            objVisulationSearchEntity.End_Date = objVisulationSearchEntity.End_Date == Convert.ToDateTime("1/1/0001 12:00:00 AM") ? null : objVisulationSearchEntity.End_Date;

            objVisulationSearchEntity.SiteId = objVisulationSearchEntity.SiteId ?? 0;
            objVisulationSearchEntity.LocationId = objVisulationSearchEntity.LocationId ?? 0;
            objVisulationSearchEntity.SensorId = objVisulationSearchEntity.SensorId ?? 0;
            objVisulationSearchEntity.StructureDivisionId = objVisulationSearchEntity.StructureDivisionId ?? 0;
            objVisulationSearchEntity.StructureId = objVisulationSearchEntity.StructureId ?? 0;
            objVisulationSearchEntity.TeamId = objVisulationSearchEntity.TeamId ?? 0;

            ///////

            object values = null;
            if (this.TempData.TryGetValue("SearchConditions", out values))
            {
                SearchComman.searchConditions = values as Dictionary<string, object>;
            }
            SearchComman.SetSearchConditionValue(SearchComman.searchConditions, "SiteId", objVisulationSearchEntity.SiteId);
            SearchComman.SetSearchConditionValue(SearchComman.searchConditions, "LocationId", objVisulationSearchEntity.LocationId);
            SearchComman.SetSearchConditionValue(SearchComman.searchConditions, "SensorId", objVisulationSearchEntity.SensorId);
            SearchComman.SetSearchConditionValue(SearchComman.searchConditions, "StructureId", objVisulationSearchEntity.StructureId);
            SearchComman.SetSearchConditionValue(SearchComman.searchConditions, "StructureDivisionId", objVisulationSearchEntity.StructureDivisionId);
            SearchComman.SetSearchConditionValue(SearchComman.searchConditions, "TeamId", objVisulationSearchEntity.TeamId);

            TempData["SearchConditions"] = SearchComman.searchConditions;

            ChartSeriesBindingDemoOptions options = new ChartSeriesBindingDemoOptions();

            objVisulationSearchEntity.ObjList = _ClientAppService.Getvaisualizaton((long)objVisulationSearchEntity.SiteId, (long)objVisulationSearchEntity.StructureId, (long)objVisulationSearchEntity.StructureDivisionId, (long)objVisulationSearchEntity.TeamId, objVisulationSearchEntity.Electricitys, objVisulationSearchEntity.Gas, objVisulationSearchEntity.Water, Year, Month, Day, objVisulationSearchEntity.IsNight, objVisulationSearchEntity.IsDay, objVisulationSearchEntity.Start_Date, objVisulationSearchEntity.End_Date, (long)objVisulationSearchEntity.LocationId, (long)objVisulationSearchEntity.SensorId);
          
            if (objVisulationSearchEntity.DD == false)
            {
                double valueofDay = 0;
                double valueofNight = 0;
                double OverFlowUnderFlow = 0;
                double TodayOverFlowUnderFlow = 0;
                double MonthOverFlowUnderFlow = 0;
                double YearOverFlowUnderFlow = 0;

                ViewBag.LocationName = "All";
                ViewBag.IsDefault = "Yes";
                ViewData["OptionsKey"] = options;
                # region today
                var Today = _ClientAppService.Getvaisualizaton((long)objVisulationSearchEntity.SiteId, (long)objVisulationSearchEntity.StructureId, (long)objVisulationSearchEntity.StructureDivisionId, (long)objVisulationSearchEntity.TeamId, objVisulationSearchEntity.Electricitys, objVisulationSearchEntity.Gas, objVisulationSearchEntity.Water, 2013, 12, 8, objVisulationSearchEntity.IsNight, objVisulationSearchEntity.IsDay, objVisulationSearchEntity.Start_Date, objVisulationSearchEntity.End_Date, (long)objVisulationSearchEntity.LocationId, (long)objVisulationSearchEntity.SensorId);
                //var Today = _ClientAppService.Getvaisualizaton(0, 0, 0, 0, false, false, false, 2013, 12, 8, false, false, null, null, 0, 0);
                Double TotalTodayConsumedValue = 0;
                for (int i = 0; i < Today.Count(); i++)
                {
                    double Less10 = (Today[i].value - Today[i].prvValue) * 100;

                    double Target = (Less10 / Today[i].prvValue);
                    string targetString = String.Format("{0:0.00}", Target);
                    Today[i].Target = Convert.ToDouble(targetString == "Infinity" || targetString == "NaN" ? 0 : Convert.ToDouble(targetString));

                    valueofDay = valueofDay + Today[i].Day;
                    valueofNight = valueofNight + Today[i].Night;


                    string CovertedTargaet = (Today[i].Target).ToString();

                    TodayOverFlowUnderFlow = TodayOverFlowUnderFlow + (CovertedTargaet == "Infinity" || CovertedTargaet == "NaN" ? 0 : Convert.ToDouble(CovertedTargaet));
                    TotalTodayConsumedValue = TotalTodayConsumedValue + Today[i].value;

                }

                ViewBag.TotalTodayConsumedValue = TotalTodayConsumedValue;

                ViewBag.TodayPercent = TodayOverFlowUnderFlow + "%";



                if (TodayOverFlowUnderFlow > -3 && TodayOverFlowUnderFlow <= 3)
                {
                    ViewBag.TodayOverFlowUnderFlow = "Ok";
                }
                if (TodayOverFlowUnderFlow < -3)
                {
                    ViewBag.TodayOverFlowUnderFlow = "SAVING";
                }

                if (TodayOverFlowUnderFlow > 3)
                {
                    ViewBag.TodayOverFlowUnderFlow = "EXCESS";
                }

                if (valueofDay != 0 && valueofNight != 0)
                {
                    @ViewBag.TodayChartType = "Stack";
                }
                else
                {
                    @ViewBag.TodayChartType = "Normal";
                }
                if (Today.Count > 0)
                    @ViewBag.Todaytype = Today[0].type;
                else
                    @ViewBag.Todaytype = "Days";

                ViewBag.TodayData = Today.ToList();
                #endregion


                # region Year
                var Years = _ClientAppService.Getvaisualizaton((long)objVisulationSearchEntity.SiteId, (long)objVisulationSearchEntity.StructureId, (long)objVisulationSearchEntity.StructureDivisionId, (long)objVisulationSearchEntity.TeamId, objVisulationSearchEntity.Electricitys, objVisulationSearchEntity.Gas, objVisulationSearchEntity.Water, 2013, 0, 0, objVisulationSearchEntity.IsNight, objVisulationSearchEntity.IsDay, objVisulationSearchEntity.Start_Date, objVisulationSearchEntity.End_Date, (long)objVisulationSearchEntity.LocationId, (long)objVisulationSearchEntity.SensorId);
                //var Years = _ClientAppService.Getvaisualizaton(0, 0, 0, 0, false, false, false, 2013, 0, 0, false, false, null, null, 0, 0);
                Double TotalYearConsumedValue = 0;
                for (int i = 0; i < Years.Count(); i++)
                {
                    double Less10 = (Years[i].value - Years[i].prvValue) * 100;

                    double Target = (Less10 / Years[i].prvValue);
                    string targetString = String.Format("{0:0.00}", Target);
                    Years[i].Target = Convert.ToDouble(targetString == "Infinity" || targetString == "NaN" ? 0 : Convert.ToDouble(targetString));

                    valueofDay = valueofDay + Years[i].Day;
                    valueofNight = valueofNight + Years[i].Night;

                    string CovertedTargaet = (Years[i].Target).ToString();


                    YearOverFlowUnderFlow = YearOverFlowUnderFlow + (CovertedTargaet == "Infinity" || CovertedTargaet == "NaN" ? 0 : Convert.ToDouble(CovertedTargaet));
                    TotalYearConsumedValue = TotalYearConsumedValue + Years[i].value;

                }
                ViewBag.TotalYearConsumedValue = TotalYearConsumedValue;
                ViewBag.YearPercent = YearOverFlowUnderFlow + "%";

                if (YearOverFlowUnderFlow > -3 && YearOverFlowUnderFlow <= 3)
                {
                    ViewBag.YearOverFlowUnderFlow = "Ok";
                }
                if (YearOverFlowUnderFlow < -3)
                {
                    ViewBag.YearOverFlowUnderFlow = "SAVING";
                }

                if (YearOverFlowUnderFlow > 3)
                {
                    ViewBag.YearOverFlowUnderFlow = "EXCESS";
                }



                if (valueofDay != 0 && valueofNight != 0)
                {
                    @ViewBag.YearChartType = "Stack";
                }
                else
                {
                    @ViewBag.YearChartType = "Normal";
                }
                if (Years.Count > 0)
                    @ViewBag.Yeartype = Years[0].type;
                else
                    @ViewBag.Yeartype = "Days";

                ViewBag.YearData = Years.ToList();

                #endregion


                # region Months
                var Months = _ClientAppService.Getvaisualizaton((long)objVisulationSearchEntity.SiteId, (long)objVisulationSearchEntity.StructureId, (long)objVisulationSearchEntity.StructureDivisionId, (long)objVisulationSearchEntity.TeamId, objVisulationSearchEntity.Electricitys, objVisulationSearchEntity.Gas, objVisulationSearchEntity.Water, 2013, 12, 0, objVisulationSearchEntity.IsNight, objVisulationSearchEntity.IsDay, objVisulationSearchEntity.Start_Date, objVisulationSearchEntity.End_Date, (long)objVisulationSearchEntity.LocationId, (long)objVisulationSearchEntity.SensorId);
                //var Months = _ClientAppService.Getvaisualizaton(0, 0, 0, 0, false, false, false, 2013, 12, 0, false, false, null, null, 0, 0);
                Double TotalMonthConsumedValue = 0;
                for (int i = 0; i < Months.Count(); i++)
                {
                    double Less10 = (Months[i].value - Months[i].prvValue) * 100;

                    double Target = (Less10 / Months[i].prvValue);
                    string targetString = String.Format("{0:0.00}", Target);
                    Months[i].Target = Convert.ToDouble(targetString == "Infinity" || targetString == "NaN" ? 0 : Convert.ToDouble(targetString));

                    valueofDay = valueofDay + Months[i].Day;
                    valueofNight = valueofNight + Months[i].Night;

                    string CovertedTargaet = (Months[i].Target).ToString();

                    MonthOverFlowUnderFlow = MonthOverFlowUnderFlow + (CovertedTargaet == "Infinity" || CovertedTargaet == "NaN" ? 0 : Convert.ToDouble(CovertedTargaet));
                    TotalMonthConsumedValue = TotalMonthConsumedValue + Months[i].value;

                }
                ViewBag.TotalMonthConsumedValue = TotalMonthConsumedValue;
                ViewBag.MonthPercent = MonthOverFlowUnderFlow + "%";


                if (MonthOverFlowUnderFlow > -3 && MonthOverFlowUnderFlow <= 3)
                {
                    ViewBag.MonthOverFlowUnderFlow = "Ok";
                }
                if (MonthOverFlowUnderFlow < -3)
                {
                    ViewBag.MonthOverFlowUnderFlow = "SAVING";
                }

                if (MonthOverFlowUnderFlow > 3)
                {
                    ViewBag.MonthOverFlowUnderFlow = "EXCESS";
                }


                ViewBag.LocationName = "All";
                if (valueofDay != 0 && valueofNight != 0)
                {
                    @ViewBag.MonthChartType = "Stack";
                }
                else
                {
                    @ViewBag.MonthChartType = "Normal";
                }
                if (Months.Count > 0)
                    @ViewBag.Monthtype = Months[0].type;
                else
                    @ViewBag.Yeartype = "Days";

                ViewBag.MonthData = Months.ToList();

                #endregion


                @ViewBag.ChartType = "Stack";

            }
            //else
            //{
            //    if (LabelShow == false && label == true)
            //        ViewBag.HideLable = "true";
            //    else
            //        ViewBag.HideLable = "false";

            //    ViewData["OptionsKey"] = options;
            //    if (objVisulationSearchEntity.ObjList.Count > 0)
            //        @ViewBag.type = objVisulationSearchEntity.ObjList[0].type;
            //    else
            //        @ViewBag.type = "Days";
            //    double valueofDay = 0;
            //    double valueofNight = 0;
            //    double OverFlowUnderFlow = 0;
            //    double TotalValue = 0;
            //    for (int i = 0; i < objVisulationSearchEntity.ObjList.Count(); i++)
            //    {

            //        double Less10 = (objVisulationSearchEntity.ObjList[i].value - objVisulationSearchEntity.ObjList[i].prvValue) * 100;

            //        double Target = (Less10 / objVisulationSearchEntity.ObjList[i].prvValue);
            //        string targetString = String.Format("{0:0.00}", Target);
            //        objVisulationSearchEntity.ObjList[i].Target = Convert.ToDouble(targetString == "Infinity" || targetString == "NaN" ? 0 : Convert.ToDouble(targetString));
            //        valueofDay = valueofDay + objVisulationSearchEntity.ObjList[i].Day;
            //        valueofNight = valueofNight + objVisulationSearchEntity.ObjList[i].Night;

            //        TotalValue = TotalValue + objVisulationSearchEntity.ObjList[i].value;


            //        string CovertedTargaet = (objVisulationSearchEntity.ObjList[i].Target).ToString();



            //        OverFlowUnderFlow = OverFlowUnderFlow + (CovertedTargaet == "Infinity" || CovertedTargaet == "NaN" ? 0 : Convert.ToDouble(CovertedTargaet));

            //    }

            //    ViewBag.TotalValue = TotalValue;
            //    ViewBag.Percent = OverFlowUnderFlow + "%";
            //    if (LocationId == 0)
            //        ViewBag.LocationName = "All";
            //    else
            //    {
            //        string LocationName = _LocationAppService.Where(x => x.Id == LocationId).Select(x => x.Locationname).FirstOrDefault();
            //        ViewBag.LocationName = LocationName;
            //    }
            //    if (OverFlowUnderFlow > -3 && OverFlowUnderFlow <= 3)
            //    {
            //        ViewBag.OverFlowUnderFlow = "Ok";
            //    }

            //    if (OverFlowUnderFlow < -3)
            //    {
            //        ViewBag.OverFlowUnderFlow = "SAVING";
            //    }

            //    if (OverFlowUnderFlow > 3)
            //    {
            //        ViewBag.OverFlowUnderFlow = "EXCESS";
            //    }

            //    if (OverFlowUnderFlow > 3)
            //    {
            //        ViewBag.OverFlowUnderFlow = "EXCESS";
            //    }

            //    if (valueofDay != 0 && valueofNight != 0)
            //    {
            //        @ViewBag.ChartType = "Stack";
            //    }
            //    else
            //    {
            //        @ViewBag.ChartType = "Normal";
            //    }

            //    int CurrentYear = DateTime.Now.Year;
            //    ViewBag.PreviousYear = CurrentYear - 1;
            //    ViewBag.IsDefault = "No";

            //    if (Gas == true)
            //        ViewBag.Gas = "true";
            //    if (Water == true)
            //        ViewBag.Water = "true";
            //    if (Electricitys == true)
            //        ViewBag.Electricitys = "true";
            //    if (Gas == true && Water == true && Electricitys == true)
            //    {
            //        ViewBag.GEW = "true";
            //    }
            //    else if (Gas != true && Water != true && Electricitys != true)
            //    {
            //        ViewBag.GEW = "true";
            //    }


            //}
            return View(objVisulationSearchEntity);
        }



    

     


        public JsonResult AutoCompleteSite(string term)
        {
            List<SiteEntity> objList = _ClientAppService.SiteAutoComplete(term);

            var result = (from r in objList
                          select new { Name = r.Name, Id = r.Id }).Distinct();

            return Json(result, JsonRequestBehavior.AllowGet);



        }



    }
}
