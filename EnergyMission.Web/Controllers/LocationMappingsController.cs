﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Domain.Entities;
using EnergyMission.LocationMapping;
using EnergyMission.LocationMapping.Dtos;
using EnergyMission.Web.ViewModels;
using System.Linq;
using EnergyMission.Web._Framework;
using EnergyMission.Web.Authentication;

namespace EnergyMission.Web.Controllers
{
    public class LocationMappingsController : EmControllerBase
    {


        private readonly IRepository<Data.LocationMapping> _LocationMappingAppService;
        private readonly IRepository<Data.State> _StateAppService;
        private readonly IRepository<Data.Suburb> _SuburbAppService;
        private readonly IRepository<Data.PostCode> _PostCode;




        public LocationMappingsController()
        {
            ViewBag.PageTitle = "LocationMappings";
            _LocationMappingAppService = new EnergyMission.Data.Repositories.Repository<Data.LocationMapping>();
            _StateAppService = new EnergyMission.Data.Repositories.Repository<Data.State>();
            _SuburbAppService = new EnergyMission.Data.Repositories.Repository<Data.Suburb>();
            _PostCode = new EnergyMission.Data.Repositories.Repository<Data.PostCode>();
        }

        private bool AlreadyExists(LocationMappingEntity aLocationMapping)
        {
            Data.LocationMapping count = _LocationMappingAppService.Wheres(tt => tt.Locationname == aLocationMapping.Locationname && tt.SuburbId==aLocationMapping.SuburbId && tt.IsDeleted == false);
            bool value = false;
            if (count != null)
            {

                value = true;
            }


            return value;

        }

        private List<LocationMappingEntity> RetrieveAllLocationMappings()
        {
            try
            {
                return Mapper.Map<List<LocationMappingEntity>>(_LocationMappingAppService.Where(t => t.IsDeleted == false));
            }
            catch (Exception)
            {

                List<LocationMappingEntity> OBJ = new List<LocationMappingEntity>();
                return OBJ;


            }

        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateEx(LocationMappingEntity aLocationMapping)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!AlreadyExists(aLocationMapping))
                    {
                        ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                        aLocationMapping.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;

                        if (aLocationMapping.Id == null)
                        {
                            aLocationMapping.IsDeleted = false;
                            aLocationMapping.CreationTime = DateTime.Now;
                            _LocationMappingAppService.Insert(Mapper.Map<Data.LocationMapping>(aLocationMapping));
                            TempData["msg"] = "Record Saved Succesfully";
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            Data.LocationMapping count = _LocationMappingAppService.SelectByID(aLocationMapping.Id);
                            count.LastModificationTime = DateTime.Now;
                            count.Locationname = aLocationMapping.Locationname;
                            count.CountryId = aLocationMapping.CountryId;
                            count.StateId = aLocationMapping.StateId;
                            count.SuburbId = aLocationMapping.SuburbId;
                            count.PostCodeId = aLocationMapping.PostCodeId;
                            count.LastModifierUserId = aLocationMapping.CreatorUserId;
                            _LocationMappingAppService.Update(count);
                            TempData["msg"] = "Record Updated Succesfully";
                            return RedirectToAction("Index");

                        }


                    }
                    else
                    {

                        TempData["msg"] = "There is already a location mapping for this combination";
                       // StatusMessageError("There is already a location mapping for this combination.");
                    }
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            return RedirectToAction("Index");
            // return View("Create", aLocationMapping);
        }

        //public override ActionResult DeleteEx(long Id)
        //{
        //    if (Id >= 0)
        //    {
        //        try
        //        {

        //            Data.LocationMapping count = _LocationMappingAppService.SelectByID(Id);
        //            count.LastModificationTime = DateTime.Now;

        //            count.IsDeleted = true;
        //            _LocationMappingAppService.Update(count);
        //        }
        //        catch (Exception e)
        //        {
        //            StatusMessageError(e.Message);
        //        }
        //    }
        //    return RedirectToAction("GridViewPartial");
        //}




        [AcceptVerbs(HttpVerbs.Delete)]
        public ActionResult DeleteEx(long Id)
        {
            if (Id >= 0)
            {
                try
                {

                    Data.LocationMapping count = _LocationMappingAppService.SelectByID(Id);
                    count.LastModificationTime = DateTime.Now;

                    count.IsDeleted = true;
                  
                    _LocationMappingAppService.Update(count);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }


        public ActionResult EditEx(long Id)
        {

            ViewBag.PageTitle = "Edit Location";
            return View("Edit", _LocationMappingAppService.SelectByID(Id));


        }
        public JsonResult Edits(long Id)
        {

            IQueryable<Data.LocationMapping> result = _LocationMappingAppService.Where(tt => tt.Id == Id);

            var value = from t in result
                        select new { t.Id, t.Locationname, t.StateId, t.SuburbId, t.PostCodeId, t.CountryId };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllLocationMappings());
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {

            if (TempData["msg"] != "" || TempData["msg"] != null)

                ViewBag.msg = TempData["msg"];

            return View(RetrieveAllLocationMappings());
        }


        public ActionResult New()
        {
            ViewBag.PageTitle = "New Location Mapping";

            return View("Create");
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateEx(LocationMappingEntity aLocationMapping)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Data.LocationMapping updateInput = Mapper.Map<Data.LocationMapping>(aLocationMapping);

                    _LocationMappingAppService.Update(updateInput);
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aLocationMapping);
        }



        public JsonResult Ddits(long deptId)
        {

            IQueryable<Data.State> result = _StateAppService.Where(tt => tt.countryId == deptId);

            var value = from t in result
                        select new { t.Id,t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }


        //public JsonResult Subdits(long StateIDSu)
        //{

        //    IQueryable<Data.Suburb> result = _SuburbAppService.Where(tt => tt.StateId == StateIDSu);

        //    var value = from t in result
        //                select new { t.Id, t.Name };

        //    return Json(value, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult Subdits(long StateIDSu)
        {

            IQueryable<Data.PostCode> result = _PostCode.Where(tt => tt.StateId == StateIDSu && tt.IsDeleted == false);

            var value = from t in result
                        select new { t.Id, t.PostCodeNumber };

            return Json(value, JsonRequestBehavior.AllowGet);
        }





        public JsonResult OnChangePostCode(long StateIDSu)
        {

            IQueryable<Data.Suburb> result = _SuburbAppService.Where(tt => tt.PostcodeId == StateIDSu && tt.IsDeleted == false);

            var value = from t in result
                        select new { t.Id, t.Name };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

    }
}