﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using Abp.Domain.Uow;
using AutoMapper;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Common.Enums;
using EnergyMission.Common.Security;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.Identity;
using EnergyMission.Web.ViewModels.ClientTeamMember;
using Microsoft.AspNet.Identity;
using EnergyMission.Web._Framework;
using System.Web;
using System.IO;
using EnergyMission.Domain.Mail;
using System.Threading.Tasks;
using EnergyMission.Data.Repositories;
using System.Data.SqlClient;

namespace EnergyMission.Web.Controllers
{
    public class ClientTeamMembersController : EmControllerBase
    {
        private readonly IRepository<Data.ClientTeamMember> _ClientTeamMemberAppService;
        private readonly IRepository<Data.ClientTeam>  _ClientTeamAppService;
        private readonly IRepository<Data.ClientTeamsSensor> _ClientTeamSensorAppService;
        private readonly IRepository<Data.Person> _PersonAppService;
        private readonly IClientRepositoy _ClientAppService;
        Common.Common comm;

        private EmDbContext db = new EmDbContext();

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientTeamAppService">Application service for Client Teams</param>
        /// <param name="aClientTeamMemberAppService">Application service for Client Team Members</param>
        public ClientTeamMembersController()
        {

            _ClientTeamMemberAppService = new EnergyMission.Data.Repositories.Repository<Data.ClientTeamMember>();
            _ClientTeamAppService = new EnergyMission.Data.Repositories.Repository<Data.ClientTeam>();
            _PersonAppService = new EnergyMission.Data.Repositories.Repository<Data.Person>();
            _ClientTeamSensorAppService = new EnergyMission.Data.Repositories.Repository<Data.ClientTeamsSensor>();
         comm=   new Common.Common();
            _ClientAppService = new ClientRepositoy();
            //_ClientTeamAppService = aClientTeamAppService;
            //_ClientTeamMemberAppService = aClientTeamMemberAppService;
            //_PersonAppService = aPersonAppService;
        }

        /// <summary>
        ///     Called when we want to CREATE a clientTeamMember in the database.
        /// </summary>
        /// <param name="aClientTeamMember">Information about the clientTeamMember to create.</param>
        /// <returns>Returns gridview on success, create clientTeamMember view on failure.</returns>
        /// 

        public ActionResult Create()
        {

            return View();
        
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Create(ClientTeamMemberEntity aClientTeamMember)
        {
            if (ModelState.IsValid)
            {
                try
                {
                     ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();

                     if (aClientTeamMember.Id == 0)
                     {

                         var value = _ClientTeamMemberAppService.Wheres(tt => tt.PersonId == aClientTeamMember.PersonId && tt.IsDeleted == false && tt.ClientTeamId == aClientTeamMember.ClientTeamId);
                         if (value == null)
                         {
                             aClientTeamMember.CreationTime = DateTime.Now;
                             aClientTeamMember.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
        
                             Data.Person pres = _PersonAppService.SelectByID(aClientTeamMember.PersonId);
                             ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);
                             if (apps != null)
                             {
                                 _ClientTeamMemberAppService.Insert(Mapper.Map<Data.ClientTeamMember>(aClientTeamMember));
                                 AppUserManager.RemoveFromRole(apps.Id, "Team Member");
                                 AppUserManager.AddToRole(apps.Id, "Team Member");
                                 TempData["msg"] = "Member  assigned successfully";
                             }
                             //Data.Person pres = _PesonAppService.SelectByID(aClientAdministrator.PersonId);
                             //ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);
                             //AppUserManager.AddToRole(apps.Id, "Administrator");
                         }

                         else
                         {

                             TempData["msg"] = "Allready Assigned";

                         }


                         //_ClientTeamMemberAppService.Insert(Mapper.Map<Data.ClientTeamMember>(aClientTeamMember.ClientTeamMember));
                         return RedirectToAction("Index");
                     }
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);

                    TempData["msg"] = e.Message;
                }
            }
           

            return View("Create", aClientTeamMember);
        }

        /// <summary>
        ///     Called when we want to DELETE the clientTeamMember.
        /// </summary>
        /// <param name="ClientTeamId">Unique identifier for the clientTeamMember record to delete.</param>
        /// <param name="PersonId"></param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
    

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="ClientTeamId"></param>
        /// <param name="PersonId"></param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult EditEx(long ClientTeamId, long PersonId)
        {
            ViewBag.PageTitle = "Edit Client Team Member";

            //ClientTeamMemberAddEditViewModel viewModel = new ClientTeamMemberAddEditViewModel
            //{
            //    ClientTeamMember = _ClientTeamMemberAppService.SelectByID(new GetClientTeamMemberInput {ClientTeamId = ClientTeamId, PersonId = PersonId}),
            //    OriginalClientTeamId = ClientTeamId,
            //    OriginalPersonId = PersonId
            //};

            return View("Edit");
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public  ActionResult GridViewPartial()
        {

            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var Ids = loggedInUser.UserProfileInformations.PersonId.Value;
            return PartialView("_GridViewPartial", RetrieveAllClientTeamMembers(Ids));
        }


        //public JsonResult getpersons(string Prefix)
        //{
        //    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
        //    var Id = loggedInUser.UserProfileInformations.PersonId.Value;
        //    var value = null;
        //    //var value = comm.GetPersons(Id, Prefix);
        //    //var value = _PersonAppService.Where(tt => tt.IsDeleted == false && tt.CreatorUserId == Id && tt.FirstName.Contains(Prefix)).OrderBy(tt => tt.FirstName).Select(tt => tt.FirstName);

        //    //// List<PersonEntity> obj1 = db.Database.SqlQuery<PersonEntity>("Select * from Persons where FirstName like '%" + Prefix + "%'").ToList();
        //    return Json(value, JsonRequestBehavior.AllowGet);




        //}
        //public JsonResult GetTeam(string Prefix)
        //{
        //    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
        //    var Id = loggedInUser.UserProfileInformations.PersonId.Value;
        //    var value = comm.GetTeams(Id, Prefix);

        //    return Json(value, JsonRequestBehavior.AllowGet);




        //}

        public ActionResult Test()
        {

            return View();


        }

        public ActionResult ForTeamGridViewPartial(long TeamId)
        {
            SesnsorEntitymain main = new SesnsorEntitymain();
            IEnumerable<SensorEntity> value = _ClientAppService.GetSensorByTeam(TeamId);
          
   
           main.sensor = value;
           main.TeamName = comm.GetTeamId(TeamId);


            return PartialView("_GridViewPartial1", main);
        }

        /// <summary>
        ///     Main view to display all the clientTeamMember in the system.
        /// </summary>
        /// <returns>Returns the view for all the clientTeamMember in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {
            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var Ids = loggedInUser.UserProfileInformations.PersonId.Value;
            ViewBag.user = Ids;
            ViewBag.person = new SelectList(_PersonAppService.Where(tt => tt.IsDeleted == false && tt.CreatorUserId == Ids).Select(tt => new { Name = tt.FirstName+" "+tt.LastName, PersonId = tt.Id }), "PersonId", "Name");
            if (TempData["msg"] != null)
            {
                ViewBag.msg = TempData["msg"];
            
            
            }

            return View(RetrieveAllClientTeamMembers(Ids));
        }


        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public ActionResult Delete(long Id)
        {
            if (Id != 0  )
            {
                try
                {
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    Data.ClientTeamMember clientadminstrator = new Data.ClientTeamMember();
                    clientadminstrator = _ClientTeamMemberAppService.SelectByID(Id);
                    clientadminstrator.IsDeleted = true;
                    clientadminstrator.LastModificationTime = DateTime.Now;
                    clientadminstrator.LastModifierUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                    _ClientTeamMemberAppService.Update(clientadminstrator);
                    Data.Person pres = _PersonAppService.SelectByID(clientadminstrator.CreatorUserId);
                    ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);
                    AppUserManager.RemoveFromRole(apps.Id, "Team Member");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        /// Returns view to display all the team members for the specified team
        /// </summary>
        /// <param name="aTeamId">Team we are displaying the members for</param>
        /// <returns></returns>
        //public ActionResult ForTeam(long aTeamId)
        //{
        //    //GetClientTeamOutput clientTeam = _ClientTeamAppService.GetClientTeam(new GetClientTeamInput {Id = aTeamId});            
        //    ViewBag.PageTitle = "Team Members For Team - " + clientTeam.Name;

        //    ClientTeamMembersForTeamViewModel viewModel = new ClientTeamMembersForTeamViewModel {ClientTeamId = aTeamId};

        //    return View(viewModel);
        //}

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new clientTeamMember to be added.</returns>
    

        /// <summary>
        ///     Called when a clientTeamMember needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aClientTeamMember">Information about the clientTeamMember to be updated.</param>
        /// <returns>Returns the list of clientTeamMember on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Update(ClientTeamMemberAddEditViewModel aClientTeamMember)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Data.ClientTeamMember viewModel = Mapper.Map<Data.ClientTeamMember>(aClientTeamMember.ClientTeamMember);
                    //viewModel.OriginalClientTeamId = aClientTeamMember.OriginalClientTeamId;
                    //viewModel.OriginalPersonId = aClientTeamMember.OriginalPersonId;

                    _ClientTeamMemberAppService.Update(viewModel);
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aClientTeamMember);
        }

        /// <summary>
        ///     Helper method to retrieve all the clientTeamMember.
        /// </summary>
        /// <returns>Returns a list of clientTeamMember</returns>
        private List<ClientTeamMemberEntity> RetrieveAllClientTeamMembers(long Id )
        {

            return Mapper.Map<List<ClientTeamMemberEntity>>(_ClientTeamMemberAppService.Where(tt => tt.IsDeleted == false &&  tt.CreatorUserId==Id).OrderBy(tt => tt.PersonId)); 

      
                                              
        }

        /// <summary>
        /// Displays the view to allow for a new team member to be added
        /// </summary>
        /// <param name="aClientTeamId"></param>
        /// <returns></returns>
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult NewTeamMemberPerson(long aClientTeamId)
        //{
        //    //ClientTeamMemberForTeamPersonViewModel viewModel = new ClientTeamMemberForTeamPersonViewModel 
        //    //{
        //    //    ClientId = AppUserManager.GetLoggedInUser()
        //    //                             .UserProfileInformation.ClientId.GetValueOrDefault(),
        //    //    ClientTeamId = aClientTeamId
        //    //};
        //    return View(viewModel);
        //}

        /// <summary>
        /// Displays the view to allow for a new team member to be added
        /// </summary>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> NewPerson(PersonEntity aPerson, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                try
                {
                ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    var Id = loggedInUser.UserProfileInformations.ClientId.Value;
                    var Ids = loggedInUser.UserProfileInformations.PersonId.Value;

                    string name = "";
                    if (aPerson.Id == 0)
                    {
                      

                            if (AppUserManager.FindByName(aPerson.EmailAddress) != null)
                            {


                                throw new Exception("User with that email address already exists.");
                                //throw new Exception("User with that email address already exists.");
                            }
                            aPerson.Status = 2;


                            if (file != null && file.ContentLength > 0)
                            {

                                string ext = Path.GetExtension(file.FileName);
                                var path = Path.Combine(Server.MapPath("~/CVS/"), Guid.NewGuid() + ext);
                                name = Guid.NewGuid() + ext;


                            }
                            Data.Person persons = new Data.Person();

                            aPerson.CreationTime = DateTime.Now;
                            aPerson.CreatorUserId = Ids;
                            aPerson.CVUrl = name;

                            Mailhelper.SendVarificationEmailToPerson(aPerson, "Amber@gmail.com");
                            persons = _PersonAppService.Inserts(Mapper.Map<Data.Person>(aPerson));







                            //Now add the user
                            ApplicationUser appUser = new ApplicationUser
                            {

                                UserName = aPerson.EmailAddress,
                                Email = aPerson.EmailAddress,
                                UserProfileInformations = new UserProfileInformationEntity { ClientId = Id, CreationTime = DateTime.Now, PersonId = persons.Id }
                            };
                            IdentityResult result = await AppUserManager.CreateAsync(appUser, aPerson.Password);
                            ModelState.Clear();
                            ViewBag.msg = "People added successfully!";
                          
                            return View("Create");
                    }
                }
                catch (Exception ex)
                {
                    StatusMessageError(ex.Message);
                    ViewBag.msg = ex.Message;
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Create", aPerson);
        }
    }
}