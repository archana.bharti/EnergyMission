﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Domain.ClientAdministrator;
using EnergyMission.Web.ViewModels.ClientAdministrator;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNet.Identity;
using EnergyMission.Web._Framework;
using Microsoft.AspNet.Identity.EntityFramework;
using DevExpress.Web.Mvc;
namespace EnergyMission.Web.Controllers
{
    public class ClientAdministratorsController :EmControllerBase
    {
        //private readonly IClientAdministratorAppService _ClientAdministratorAppService;
        private readonly IRepository<Data.ClientAdministrator> _ClientAdministratorAppService;
        private readonly IRepository<Data.Person> _PesonAppService;
        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientAdministratorAppService">Application service for ClientAdministrator</param>
        public ClientAdministratorsController()
        {
            ViewBag.PageTitle = "Client Administrators";
            _ClientAdministratorAppService = new EnergyMission.Data.Repositories.Repository<Data.ClientAdministrator>();
            _PesonAppService = new EnergyMission.Data.Repositories.Repository<Data.Person>();
        }

        /// <summary>
        ///     Called when we want to CREATE a ClientAdministrator in the database.
        /// </summary>
        /// <param name="aClientAdministrator">Information about the ClientAdministrator to create.</param>
        /// <returns>Returns gridview on success, create ClientAdministrator view on failure.</returns>
        //[AcceptVerbs(HttpVerbs.Post)]
        //public override ActionResult Create(ClientAdministratorAddEditViewModel aClientAdministrator)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _ClientAdministratorAppService.InsertClientAdministrator(Mapper.Map<InsertClientAdministratorInput>(aClientAdministrator.ClientAdministrator));
        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception e)
        //        {
        //            StatusMessageError(e.Message);
        //        }
        //    }
        //    else
        //        StatusMessageErrorsModelState(ModelState);

        //    return View("Create", aClientAdministrator);
        //}



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Creates([ModelBinder(typeof(DevExpressEditorsBinder))] ClientAdministratorEntity aClientAdministrator)
        {
            if (ModelState.IsValid)
            {
                try

                {
                     ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                
                    if (aClientAdministrator.Id == 0)
                    {

                           var value = _ClientAdministratorAppService.Wheres(tt=>tt.PersonId==aClientAdministrator.PersonId && tt.IsDeleted==false);
                           if (value == null)
                           {
                               aClientAdministrator.ClientId = loggedInUser.UserProfileInformations.ClientId.Value;
                               aClientAdministrator.CreationTime = DateTime.Now;
                               aClientAdministrator.CreatorUserId = loggedInUser.UserProfileInformations.ClientId.Value;
                               _ClientAdministratorAppService.Insert(Mapper.Map<Data.ClientAdministrator>(aClientAdministrator));
                               Data.Person pres = _PesonAppService.SelectByID(aClientAdministrator.PersonId);
                               ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);
                               AppUserManager.AddToRole(apps.Id, "Administrator");
                               TempData["msg"] = "People Assigned successfully!";
                           }

                           else
                           {

                               TempData["msg"] = "Allready Assigned";

                           }
                    }
                    else

                    {
           
                        var value = _ClientAdministratorAppService.Wheres(tt=>tt.PersonId==aClientAdministrator.PersonId && tt.IsDeleted==false);
                        if (value == null)
                        {


                            ClientAdministratorEntity clientadminstrator = new ClientAdministratorEntity();
                            clientadminstrator = Mapper.Map<ClientAdministratorEntity>(_ClientAdministratorAppService.SelectByID(aClientAdministrator.Id));

                            clientadminstrator.ClientId = loggedInUser.UserProfileInformations.ClientId.Value;


                            clientadminstrator.LastModificationTime = DateTime.Now;
                            clientadminstrator.LastModifierUserId = loggedInUser.UserProfileInformations.ClientId.Value;
                            _ClientAdministratorAppService.Update(Mapper.Map<Data.ClientAdministrator>(clientadminstrator));
                            Data.Person pres = _PesonAppService.SelectByID(clientadminstrator.PersonId);
                            ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);

                            AppUserManager.RemoveFromRole(apps.Id, "Administrator");
                            AppUserManager.AddToRole(apps.Id, "Administrator");
                            TempData["msg"] = "People Re-Assigned successfully!";
                        }

                        else

                        {

                            TempData["msg"] = "Allready Assigned";

                        }
                    
                    }
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);

                    TempData["msg"] = e.Message;
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Index");
        }
        /// <summary>
        ///     Called when we want to DELETE the ClientAdministrator.
        /// </summary>
        /// <param name="Id">Unique identifier for the ClientAdministrator record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public  ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                 Data.ClientAdministrator clientadminstrator = new Data.ClientAdministrator();
                 clientadminstrator=   _ClientAdministratorAppService.SelectByID(Id);
                 clientadminstrator.IsDeleted = true;
                 clientadminstrator.LastModificationTime = DateTime.Now;
                 clientadminstrator.LastModifierUserId = loggedInUser.UserProfileInformations.ClientId.Value;
                _ClientAdministratorAppService.Update(clientadminstrator);
                Data.Person pres = _PesonAppService.SelectByID(clientadminstrator.PersonId);
                ApplicationUser apps = AppUserManager.FindByName(pres.EmailAddress);
                AppUserManager.RemoveFromRole(apps.Id, "Administrator");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        //public override ActionResult Edit(long Id)
        //{
        //    ViewBag.PageTitle = "Edit Client Administrator";

        //    ClientAdministratorAddEditViewModel viewModel = new ClientAdministratorAddEditViewModel
        //    {
        //        ClientAdministrator = _ClientAdministratorAppService.GetClientAdministratorWithInclude(new GetClientAdministratorInput {Id = Id})
        //    };

        //    return View("Edit", viewModel);
        //}
          public  JsonResult Edits(long Id)
        {

            IQueryable<Data.ClientAdministrator> SSB = _ClientAdministratorAppService.Where(tt=>tt.Id==Id);

            var res = (from sub in SSB
                      
                       select new
                       {
                           id = sub.Id,
                           personid = sub.PersonId,
                           name = sub.Person.FirstName
                       }
                                       );



            return Json(res, JsonRequestBehavior.AllowGet);

   //             var result = _ClientAdministratorAppService.SelectByID(Id);
   ////var x = from i in _ClientAppService.GetClientById(Id)
   ////                 select new { result.StateId, result.Id, result.Name, result.NOE, result.SuperAdminEmail, result.PostCodeId, result.City, result.Address, result.Geo_location_Latitude, result.Geo_location_Longditude};
   //         return Json(result, JsonRequestBehavior.AllowGet);


        }
        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public  ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllClientAdministrators());
        }

        /// <summary>
        ///     Main view to display all the ClientAdministrator in the system.
        /// </summary>
        /// <returns>Returns the view for all the ClientAdministrator in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {

            ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
            var Id = loggedInUser.UserProfileInformations.ClientId.Value;
            //var value = _PesonAppService.Where(tt => tt.IsDeleted == false && tt.CreatorUserId == Id).Select(tt => new { PersonId=tt.Id, Name = tt.FirstName }).ToList();
            ViewBag.pesonal = Id;

        if(TempData["msg"]!=null)
        {
            ViewBag.msg = TempData["msg"];

        }
            return View(RetrieveAllClientAdministrators());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new ClientAdministrator to be added.</returns>
        //public override ActionResult New()
        //{
        //    ViewBag.PageTitle = "New Client Team";

        //    ClientAdministratorAddEditViewModel viewModel = new ClientAdministratorAddEditViewModel
        //    {
        //        ClientAdministrator = _ClientAdministratorAppService.GetNewClientAdministrator(new GetNewClientAdministratorInput())
        //    };

        //    return View("Create", viewModel);
        //}

        /// <summary>
        ///     Called when a ClientAdministrator needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aClientAdministrator">Information about the ClientAdministrator to be updated.</param>
        /// <returns>Returns the list of ClientAdministrator on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Update(ClientAdministratorAddEditViewModel aClientAdministrator)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _ClientAdministratorAppService.Update(Mapper.Map<Data.ClientAdministrator>(aClientAdministrator.ClientAdministrator));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aClientAdministrator);
        }

        /// <summary>
        ///     Helper method to retrieve all the ClientAdministrator.
        /// </summary>
        /// <returns>Returns a list of ClientAdministrator</returns>
        private List<ClientAdministratorEntity> RetrieveAllClientAdministrators()
        {
                       ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                       return Mapper.Map<List<ClientAdministratorEntity>>(_ClientAdministratorAppService.Where(tt => tt.IsDeleted == false && tt.CreatorUserId == loggedInUser.UserProfileInformations.ClientId.Value));

            //GetClientAdministratorsInput getClientAdministratorsInput = new GetClientAdministratorsInput();
            //return _ClientAdministratorAppService.GetClientAdministrators(getClientAdministratorsInput)
            //                                     .ClientAdministrators;
        }
    }
}