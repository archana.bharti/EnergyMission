﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Common.Security;
using EnergyMission.Domain.Entities;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using EnergyMission.Site;
using EnergyMission.Site.Dtos;
using EnergyMission.Web.Authentication;
using EnergyMission.Web.ViewModels.Site;
using Microsoft.AspNet.Identity;
using System.Linq;

namespace EnergyMission.Web.Controllers
{


    [HandleError(View = "")]
    public class SitesController : EmControllerGridView<SiteAddEditViewModel, SiteAddEditViewModel>
    {
        private readonly IRepository<Data.Site> _SiteAppService;

        public SitesController()
        {
            ViewBag.PageTitle = "Sites";
            _SiteAppService = new EnergyMission.Data.Repositories.Repository<Data.Site>();
        }

        private bool AlreadyExists(SiteEntity aViewModel)
        {
            Data.Site count = _SiteAppService.Wheres(tt => tt.Name == aViewModel.Name && tt.IsDeleted == false && tt.LocationId == aViewModel.LocationId);
            bool value = false;
            if (count != null)
            {

                value = true;
            }


            return value;

        }

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aSiteAppService">Application service for sites</param>
        //public SitesController(SiteAppService aSiteAppService)
        //{
        //    ViewBag.PageTitle = "Sites";
        //    _SiteAppService = aSiteAppService;
        //}

        /// <summary>
        ///     Called when we want to CREATE a site in the database.
        /// </summary>
        /// <param name="aViewModel">Information about the site to create.</param>
        /// <returns>Returns gridview on success, create site view on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Creates(SiteEntity aViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (!AlreadyExists(aViewModel))
                    {
                        ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                        aViewModel.CreatorUserId = loggedInUser.UserProfileInformations.PersonId.Value;
                        aViewModel.ClientId = loggedInUser.UserProfileInformations.ClientId.Value;
                        if (aViewModel.Id == null)
                        {                      

                            aViewModel.IsDeleted = false;
                            aViewModel.CreationTime = DateTime.Now;                           
                            _SiteAppService.Insert(Mapper.Map<Data.Site>(aViewModel));
                            ModelState.Clear();
                            TempData["msg"] = "Site Saved Succesfully";
                            return RedirectToAction("Index");

                        }
                        else
                        {
                            Data.Site count = _SiteAppService.SelectByID(aViewModel.Id);
                            count.LastModificationTime = DateTime.Now;
                            count.Name = aViewModel.Name;
                            count.LocationId =Convert.ToInt32(aViewModel.LocationId);
                            count.LastModifierUserId = aViewModel.CreatorUserId;
                            _SiteAppService.Update(count);
                            ModelState.Clear();
                            TempData["msg"] = "Site Updated Succesfully";
                            return RedirectToAction("Index");

                        }
                    }
                   else
                    {

                        TempData["msg"] = "There is already a Site for this combination";
                    }



                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);
            return RedirectToAction("Index");
            //return View("Index", aViewModel);
        }

        /// <summary>
        ///     Called when we want to DELETE the site.
        /// </summary>
        /// <param name="Id">Unique identifier for the site record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public override ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                    //TempData["msg"] = "Site Deleted Succesfully";
                    _SiteAppService.Delete(Id);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public override ActionResult Edit(long Id)
        {
            PageTitle = "Edit Site";
            SiteAddEditViewModel viewModel = new SiteAddEditViewModel();
            GetSiteInput siteInput = new GetSiteInput { Id = Id };
            viewModel.Site = Mapper.Map<SiteEntity>(_SiteAppService.SelectByID(Id));

            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                viewModel.RestrictToClient(appUser.UserProfileInformations.ClientId.Value);
            }

            return View("Edit", viewModel);
        }

        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public override ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllSites());
        }

        /// <summary>
        ///     Main view to display all the sites in the system.
        /// </summary>
        /// <returns>Returns the view for all the sites in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index()
        {


            if ((TempData["msg"] != string.Empty) || (TempData["msg"] != null))

            ViewBag.msg = TempData["msg"];
            return View(RetrieveAllSites());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new site to be added.</returns>
        public override ActionResult New()
        {
            PageTitle = "New Site";

            //SiteAddEditViewModel viewModel = new SiteAddEditViewModel {Site = _SiteAppService.SelectByID(new GetNewSiteInput())};

            //Now should we be anything other than a SuperAdmin we need to filter this based on the client
            //the logged in user belongs to
            if (AppRoleManager.GetLoggedInUserRoleKind(AppUserManager) != SystemRoleKindEnum.SuperAdministrator)
            {
                ApplicationUser appUser = AppUserManager.FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());
                //viewModel.RestrictToClient(appUser.UserProfileInformation.ClientId.Value);
            }

            return View("Create");
        }

        /// <summary>
        ///     Called when a country needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aSite">Information about the site to be updated.</param>
        /// <returns>Returns the list of sites on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public override ActionResult Update(SiteAddEditViewModel aSite)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _SiteAppService.Update(Mapper.Map<Data.Site>(aSite.Site));
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aSite);
        }

        /// <summary>
        ///     Helper method to retrieve all the sites.
        /// </summary>
        /// <returns>Returns a list of sites</returns>
        private List<SiteEntity> RetrieveAllSites()
        {

            try
            {
                return Mapper.Map<List<SiteEntity>>(_SiteAppService.Where(t => t.IsDeleted == false));
            }
            catch (Exception)
            {

                List<SiteEntity> OBJ = new List<SiteEntity>();
                return OBJ;


            }
        }

        public JsonResult Edits(long Id)
        {

            IQueryable<Data.Site> result = _SiteAppService.Where(tt => tt.Id == Id);

            var value = from t in result
                        select new { t.Id, t.Name, t.LocationId };

            return Json(value, JsonRequestBehavior.AllowGet);
        }

    }
}