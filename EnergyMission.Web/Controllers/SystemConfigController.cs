﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EnergyMission.Common.Security;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.Authentication;
using EnergyMission.Web._Framework;
using Microsoft.AspNet.Identity;
using EnergyMission.Data;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnergyMission.Web.Controllers
{
    public class SystemConfigController : EmControllerBase
    {
        #region Public Members

        /// <summary>
        ///     Adds the roles recognised by the system
        /// </summary>
        /// <returns></returns>        
        public ActionResult InitRoles()
        {
            foreach (KeyValuePair<SystemRoleKindEnum, ApplicationRole> applicationRole in SystemRoleList.SystemRoles)
            {
                if (!AppRoleManager.RoleExists(applicationRole.Value.Name))
                {
                    AppRoleManager.Create(applicationRole.Value);
                }
            }
            return RedirectToAction("Index", "Manage");
        }

        /// <summary>
        ///     Adds the owner and developer user to the system
        /// </summary>
        /// <returns></returns>
        public ActionResult InitUsers()
        {
            //Add owner and developer users should they not already exist 
            if (AppUserManager.FindByName("donovan@natiki.com.au") == null)
            {
                ApplicationUser appUser = new ApplicationUser
                {
                    Email = "donovan@natiki.com.au",
                    UserName = "donovan@natiki.com.au",
                    UserProfileInformations = new   UserProfileInformationEntity { ClientId = -2 }
                };

                AppUserManager.Create(appUser, "Acdc2u!");
                AppUserManager.AddToRole(appUser.Id, SystemRoleList.SystemRoles.First(r => r.Key == SystemRoleKindEnum.SuperAdministrator)
                                                                   .Value.Name);
            }

            //Add owner and developer users should they not already exist 
            if (AppUserManager.FindByName("amber@a-positive.com.au") == null)
            {
                ApplicationUser appUser = new ApplicationUser
                {
                    Email = "amber@a-positive.com.au",
                    UserName = "amber@a-positive.com.au"
                };
                AppUserManager.Create(appUser, "Pr@ncingP0ny");
                AppUserManager.AddToRole(appUser.Id, SystemRoleList.SystemRoles.First(r => r.Key == SystemRoleKindEnum.SuperAdministrator)
                                                                   .Value.Name);
            }

            return RedirectToAction("Index", "Manage");
        }

        /// <summary>
        ///     Ensures that the base users in the system are assigned to the system administrator role
        /// </summary>
        /// <returns></returns>
        public ActionResult InittedUsersToRoles()
        {
            Action<string> ensureUserInRole = (aUserName) =>
            {
                //Find the Super User role id
                string superAdministratorUserId = AppRoleManager.GetRoleIdByRoleKind(SystemRoleKindEnum.SuperAdministrator);
                bool userInSuperAdminRole = false;

                ApplicationUser appUserDev = AppUserManager.FindByName(aUserName);
                if (appUserDev != null)
                {
                    foreach (IdentityUserRole userRole in appUserDev.Roles)
                    {
                        if (userRole.RoleId == superAdministratorUserId)
                        {
                            userInSuperAdminRole = true;
                            break;
                        }
                    }
                    if (!userInSuperAdminRole)
                    {
                        AppUserManager.AddToRole(appUserDev.Id, AppRoleManager.GetRoleNameByRoleKind(SystemRoleKindEnum.SuperAdministrator));
                    }
                }
            };

            ensureUserInRole("donovan@natiki.com.au");
            ensureUserInRole("amber@a-positive.com.au");

            return RedirectToAction("Index", "Manage");
        }

        #endregion
    }
}