﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using EnergyMission.Domain.Entities;
using EnergyMission.Web.ViewModels.Person;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data.Repositories;
using System.Web;
using System.IO;
using EnergyMission.Domain.Mail;
using System.Threading.Tasks;
using EnergyMission.Web._Framework;
using EnergyMission.Web.Authentication;
using Microsoft.AspNet.Identity;
using System.Linq;

namespace EnergyMission.Web.Controllers
{
    public class PersonsController : EmControllerBase
    {
        private readonly IRepository<Data.Person> _PersonAppService;
        private readonly IRepository<Data.UserProfileInformation> _UserProfile;
        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aPersonAppService">Application service for person</param>
        public PersonsController()
        {
            ViewBag.PageTitle = "Persons";
            _PersonAppService = new  EnergyMission.Data.Repositories.Repository<Data.Person>() ;
            _UserProfile = new EnergyMission.Data.Repositories.Repository<Data.UserProfileInformation>();
        }

        /// <summary>
        ///     Called when we want to CREATE a person in the database.
        /// </summary>
        /// <param name="aPerson">Information about the person to create.</param>
        /// <returns>Returns gridview on success, create person view on failure.</returns>
        //[AcceptVerbs(HttpVerbs.Post)]
        //public override ActionResult Create(PersonAddEditViewModel aPerson)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        //GetNewPersonOutput
        //        try
        //        {
              
        //            //string path = "~/CVS/"+Guid.NewGuid()+file.ContentType;
                  



        //            aPerson.Person.CreationTime = DateTime.Now;          
        //            _PersonAppService.Insert(Mapper.Map<Data.Person>(aPerson.Person));


        //            return RedirectToAction("Index");
        //        }
        //        catch (Exception e)
        //        {
        //            StatusMessageError(e.Message);
        //        }
        //    }
        //    else
        //        StatusMessageErrorsModelState(ModelState);

        //    return View("Create", aPerson);
        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> Creates(PersonEntity aPerson, HttpPostedFileBase file,string button)
        {
            if (ModelState.IsValid)
            {
                //GetNewPersonOutput
                try
                {
                  
                    ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    var Id = loggedInUser.UserProfileInformations.ClientId.Value;
                    string name = "";
                    if (aPerson.Id == 0)
                    {
                        if (button == "New")
                        {

                            if (AppUserManager.FindByName(aPerson.EmailAddress) != null)
                            {


                                throw new Exception("User with that email address already exists.");
                                //throw new Exception("User with that email address already exists.");
                            }
                            aPerson.Status = 2;


                            if (file != null && file.ContentLength > 0)
                            {

                                string ext = Path.GetExtension(file.FileName);
                                var path = Path.Combine(Server.MapPath("~/CVS/"), Guid.NewGuid() + ext);
                                name = Guid.NewGuid() + ext;


                            }
                            Data.Person persons = new Data.Person();

                            aPerson.CreationTime = DateTime.Now;
                            aPerson.CreatorUserId = Id;
                            aPerson.CVUrl = name;

                            Mailhelper.SendVarificationEmailToPerson(aPerson, "Amber@gmail.com");
                            persons = _PersonAppService.Inserts(Mapper.Map<Data.Person>(aPerson));







                            //Now add the user
                            ApplicationUser appUser = new ApplicationUser
                            {

                                UserName = aPerson.EmailAddress,
                                Email = aPerson.EmailAddress,
                                UserProfileInformations = new UserProfileInformationEntity { ClientId = Id, CreationTime = DateTime.Now, PersonId = persons.Id }
                            };
                            IdentityResult result = await AppUserManager.CreateAsync(appUser, aPerson.Password);

                            TempData["msg"] = "People added successfully!";

                        }
                    }
                    //if (aPerson.Id == 0)
                    //{
                    //    aPerson.CreationTime = DateTime.Now;
                    //    aPerson.CVUrl = name;

                    //    Mailhelper.SendVarificationEmailToPerson(aPerson, "Amber@gmail.com");
                    //    _PersonAppService.Insert(Mapper.Map<Data.Person>(aPerson));

                    //}
                    else
                    {
                        Data.Person person = _PersonAppService.SelectByID(aPerson.Id);
                        UserProfileInformationEntity prfile = Mapper.Map<UserProfileInformationEntity>(_UserProfile.Wheres(tt => tt.PersonId == person.Id && tt.ClientId == Id));
                        ApplicationUser appUser = new ApplicationUser
                        {

                            UserName = aPerson.EmailAddress,
                            Email = aPerson.EmailAddress,
                            PasswordHash = aPerson.PasswordConfirm,
                            UserProfileInformations = prfile
                        };


                        IdentityResult result = AppUserManager.Update(appUser);
                        person.LastModificationTime = DateTime.Now;
                        person.LastModifierUserId = Id;

                        person.FirstName = aPerson.FirstName;
                        person.LastName = aPerson.LastName;
                        person.EmployeeID = aPerson.EmployeeID;
                        person.EmailAddress = aPerson.EmailAddress;
                   _PersonAppService.Update(Mapper.Map<Data.Person>(person));
                        TempData["msg"] = "People updated successfully!";

                        //if (result.Succeeded)
                        //{
                        //    Mailhelper.SendVarificationEmailToPerson(aPerson, "Amber@gmail.com");
                       
                        //    if (name == "")
                        //        person.CVUrl = aPerson.CVUrl;
                         
                        //}




                    }


        




                    
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                         
                    StatusMessageError(e.Message);
                    ViewBag.msg = e.Message;
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("index");
        }


        /// <summary>
        ///     Called when we want to DELETE the person.
        /// </summary>
        /// <param name="Id">Unique identifier for the person record to delete.</param>
        /// <returns>Returns the grid view on success, error otherwise.</returns>
        public  ActionResult Delete(long Id)
        {
            if (Id >= 0)
            {
                try
                {
                      ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    var Ids = loggedInUser.UserProfileInformations.ClientId.Value;
Data.Person per=                    _PersonAppService.SelectByID(Id);
per.LastModifierUserId = Ids;
per.LastModificationTime = DateTime.Now;
per.IsDeleted = true;
_PersonAppService.Update(per);
ApplicationUser apps = AppUserManager.FindByEmail(per.EmailAddress);
IdentityResult result = AppUserManager.Delete(apps);
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            return RedirectToAction("GridViewPartial");
        }

        /// <summary>
        ///     Called when Edit is clicked in the GridView.
        /// </summary>
        /// <param name="Id">Unique identifier for the record to edit.</param>
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public  ActionResult Edit(long Id)
        {
            ViewBag.PageTitle = "Edit Person";

            // viewModel = new PersonAddEditViewModel
            //{
            //    Person = _PersonAppService.SelectByID(Id)
            //};

            return View("Edit");
        }
        /// <returns>Returns the edit view on view on success, error on failure.</returns>
        public JsonResult Edits(long Id)
        {



            // viewModel = new PersonAddEditViewModel
            //{
            //    Person = _PersonAppService.SelectByID(Id)
            IQueryable<Data.Person> SSB = _PersonAppService.Where(tt => tt.Id == Id);

            var res = (from sub in SSB
                       where sub.Id == Id
                       select new
                       {
                           id = sub.Id,
                           firstname = sub.FirstName,
                           lastname = sub.LastName,
                           email=sub.EmailAddress,
                           empid=sub.EmployeeID,
                           cvurl=sub.CVUrl



                       }
                                       );
            //};
            //var result = _PersonAppService.Where(Id);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        ///     Displays the view that contains only the grid view.
        /// </summary>
        /// <returns>Returns the view containing the grid view.</returns>
        [ValidateInput(false)]
        public  ActionResult GridViewPartial()
        {
            return PartialView("_GridViewPartial", RetrieveAllPersons());
        }

        /// <summary>
        ///     Main view to display all the person in the system.
        /// </summary>
        /// <returns>Returns the view for all the person in the system.</returns>
        [AcceptVerbs(HttpVerbs.Get)]
  
        public ActionResult Index()
        {

            if (TempData["msg"] != null)
            {
                ViewBag.msg = TempData["msg"];
            }

            return View(RetrieveAllPersons());
        }

        /// <summary>
        ///     Called when New is clicked in the gridview.
        /// </summary>
        /// <returns>Returns the form that allows for a new person to be added.</returns>
        public  ActionResult New()
        {
            ViewBag.PageTitle = "New Person";

            //PersonAddEditViewModel viewModel = new PersonAddEditViewModel
            //{
            //    Person = _PersonAppService.Insert(new GetNewPersonInput())
            //};
            return View("Create");
        }

        /// <summary>
        ///     Called when a person needs to be UPDATED to the database.
        /// </summary>
        /// <param name="aPerson">Information about the person to be updated.</param>
        /// <returns>Returns the list of person on success, error on failure.</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public  ActionResult Update(PersonAddEditViewModel aPerson)
        {
            //GetPersonOutput
            if (ModelState.IsValid)
            {
                try
                {
                    _PersonAppService.Update(Mapper.Map<Data.Person>(aPerson.Person));
                    //return RedirectToAction("Index");
                    return RedirectToPrevious();
                }
                catch (Exception e)
                {
                    StatusMessageError(e.Message);
                }
            }
            else
                StatusMessageErrorsModelState(ModelState);

            return View("Edit", aPerson);
        }

        /// <summary>
        ///     Helper method to retrieve all the person.
        /// </summary>
        /// <returns>Returns a list of person</returns>
        private List<PersonEntity> RetrieveAllPersons()
        {
               ApplicationUser loggedInUser = AppUserManager.GetLoggedInUser();
                    var Ids = loggedInUser.UserProfileInformations.ClientId.Value;


            return Mapper.Map<List<PersonEntity>>(_PersonAppService.Where(tt=>tt.IsDeleted==false && tt.CreatorUserId==Ids)); 
            //GetPersonsInput getPersonsInput = new GetPersonsInput();
            //return _PersonAppService.GetPersons(getPersonsInput)
            //                        .Persons;
        }
    }
}