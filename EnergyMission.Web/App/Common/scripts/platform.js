﻿/* 
===================================================================================================
Purpose: Performs all the application wide Javascript functionality required by the application.
===================================================================================================
*/


var Platform = function () {

    // == Private functions & variables ===========================================================
    //Hook up the various JS libraries to extend the HTML controls
    function NatikiConfigureControls() {
        //Select 2
        $('.natiki-ctrl-select2').select2();
    }

    // == /Private functions & variables ==========================================================

    // == Public functions ========================================================================
    return {
        //Helper to navigate to the index of a specific controller
        NavigateToControllerIndex: function (aControllerName) {
            window.location.href = window.location.protocol + '//' + window.location.host + '/' + aControllerName;
        },

        //Helper to navigate to the index of a specific controller and its action
        NavigateToControllerAction: function (aControllerName, aActionName) {
            window.location.href = window.location.protocol + '//' + window.location.host + '/' + aControllerName + '/' + aActionName;
        },


        //Performs all the platform JS initialisation
        Init: function () {
            NatikiConfigureControls();
        }
    };

    
    // == /Public functions =======================================================================

}();
