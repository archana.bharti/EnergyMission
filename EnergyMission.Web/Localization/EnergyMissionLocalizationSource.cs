﻿using System.Web;
using Abp.Localization.Sources.Xml;

namespace EnergyMission.Web.Localization
{
    public class EnergyMissionLocalizationSource : XmlLocalizationSource
    {
        #region Public Members

        public EnergyMissionLocalizationSource()
            : base("EnergyMission", HttpContext.Current.Server.MapPath("~/Localization/EnergyMission"))
        {
        }

        #endregion
    }
}