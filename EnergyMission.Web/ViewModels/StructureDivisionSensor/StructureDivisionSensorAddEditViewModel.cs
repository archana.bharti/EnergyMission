﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using EnergyMission.Domain.Entities;
using EnergyMission.Data;
namespace EnergyMission.Web.ViewModels
{
    public class StructureDivisionSensorAddEditViewModel 
    {
        private long _RestrictedClientId;

        /// <summary>
        ///     Class constructor
        /// </summary>
        public StructureDivisionSensorAddEditViewModel()
        {
            FillLookups();
        }

        /// <summary>
        ///     Returns TRUE should we have been restricted by client
        /// </summary>
        public bool RestrictedToClient
        {
            get { return _RestrictedClientId != -1; }
        }

        public List<SelectListItem> SensorsLookup { get; set; }
        public StructureDivisionSensorEntity StructureDivisionSensor { get; set; }
        public List<SelectListItem> StructureDivisionsLookup { get; set; }

        /// <summary>
        ///     Restricts the sites displayed to those belonging to the client.
        /// </summary>
        /// <param name="aClientId">Client to restrict by</param>
        public void RestrictToClient(long aClientId)
        {
            _RestrictedClientId = aClientId;
            EmDbContext db = new EmDbContext();
            //IStructureDivisionAppService structureDivisionAppService = IocManager.Instance.Resolve<IStructureDivisionAppService>();
            List<Data.StructureDivision> structureDivisions = db.StructureDivisions.Where(tt=>tt.StructureId==aClientId).ToList();

            StructureDivisionsLookup.Clear();
            foreach (Data.StructureDivision structureDivision in structureDivisions)
            {
                StructureDivisionsLookup.Add(new SelectListItem
                {
                    Text = structureDivision.Name,
                    Value = Convert.ToString(structureDivision.Id)
                });
            }
        }

        /// <summary>
        ///     Lookups needed to support Add/Edit
        /// </summary>
        private void FillLookups()
        {

            EmDbContext db = new EmDbContext();
            //IStructureDivisionAppService structureDivisionAppService = IocManager.Instance.Resolve<IStructureDivisionAppService>();
            List<Data.StructureDivision> structureDivisions = db.StructureDivisions.ToList();

            StructureDivisionsLookup.Clear();
            foreach (Data.StructureDivision structureDivision in structureDivisions)
            {
                StructureDivisionsLookup.Add(new SelectListItem
                {
                    Text = structureDivision.Name,
                    Value = Convert.ToString(structureDivision.Id)
                });
            }
            //IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

            //Sensors

            List<Data.Sensor> Sensor = db.Sensors.ToList();


            SensorsLookup.Clear();
            foreach (Data.Sensor Sensors in Sensor)
            {
                SensorsLookup.Add(new SelectListItem
                {
                    Text = Sensors.Name,
                    Value = Convert.ToString(Sensors.Id)
                });
            }
            //SensorsLookup = listAppService.GetSensors()
            //                              .ToList();
            //StructureDivisions
            //StructureDivisionsLookup = listAppService.GetStructureDivisions()
            //                                         .ToList();
        }
    }
}