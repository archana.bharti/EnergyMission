﻿using System.ComponentModel.DataAnnotations;

namespace EnergyMission.Web.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress(ErrorMessage="Please Enter Valid Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
