﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using EnergyMission.Data;
using System;

namespace EnergyMission.Web.ViewModels.SystemAdministration
{
    public class CreateNewClientViewModel
    {
        /// <summary>
        ///     Class constructor
        /// </summary>
        public CreateNewClientViewModel()
        {
            FillLookups();
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter a password for the administrator.")]
        public string ClientAdminPassword { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Select an administrator.")]
        public long ClientAdminPersonId { get; set; }

        public long ClientId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Enter a client name.")]
        public string ClientName { get; set; }

        public List<SelectListItem> PersonsLookup { get; set; }

        /// <summary>
        ///     Lookups needed to support the view model
        /// </summary>
        private void FillLookups()
        {
            EmDbContext db = new EmDbContext();
            //IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

            List<Data.Person> sites = db.Persons.ToList();
            foreach (Data.Person site in sites)
            {
                PersonsLookup.Add(new SelectListItem
                {
                    Text = site.FirstName,
                    Value = Convert.ToString(site.Id)
                });
            }
            //Persons
            //PersonsLookup = listAppService.GetPersons()
            //                              .ToList();
        }
    }
}