﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Application.Services.Dto;
using Abp.Dependency;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Web.ViewModels
{
    /// <summary>
    ///     Viewmodel for adding a location mapping as we required some lookup information
    /// </summary>
    public class LocationMappingAddEditViewModel : IOutputDto
    {
        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        public LocationMappingAddEditViewModel()
        {
            //FillLookups();
        }

        /// <summary>
        ///     Lookups needed for the view model to support Add/Edit
        /// </summary>
        //public void FillLookups()
        //{
        //    IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

        //    //Countries
        //    CountriesLookup = listAppService.GetCountries()
        //                                    .ToList();

        //    //PostCodes
        //    PostCodesLookup = listAppService.GetPostCodes()
        //                                    .ToList();

        //    //States
        //    StatesLookup = listAppService.GetStates()
        //                                 .ToList();

        //    //Suburbs
        //    SuburbsLookup = listAppService.GetSuburbs()
        //                                  .ToList();
        //}

        public IEnumerable<SelectListItem> CountriesLookup { get; set; }
        public long OriginalCountryId { get; set; }

        public LocationMappingEntity LocationMapping { get; set; }

        public long OriginalPostCodeId { get; set; }

        public IEnumerable<SelectListItem> PostCodesLookup { get; set; }
        public long OriginalStateId { get; set; }
        public IEnumerable<SelectListItem> StatesLookup { get; set; }
        public long OriginalSuburbId { get; set; }
        public IEnumerable<SelectListItem> SuburbsLookup { get; set; }

        #endregion
    }
}