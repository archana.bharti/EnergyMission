﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using EnergyMission.Domain.Entities;


namespace EnergyMission.Web.ViewModels.ClientTeamMember
{
    public class ClientTeamMemberAddEditViewModel
    {
        #region Private Members

        /// <summary>
        ///     Lookups needed to support Add/Edit
        /// </summary>
        private void FillLookups()
        {
            //IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

            ////Clients
            //ClientTeamsLookup = listAppService.GetClientTeams()
            //                              .ToList();

            ////Persons
            //PersonsLookup = listAppService.GetPersons()
            //                              .ToList();
        }

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        public ClientTeamMemberAddEditViewModel()
        {
            FillLookups();
        }

        public  ClientTeamMemberEntity  ClientTeamMember { get; set; }
        public List<SelectListItem> ClientTeamsLookup { get; set; }
        public List<SelectListItem> PersonsLookup { get; set; }
        public long OriginalClientTeamId { get; set; }
        public long OriginalPersonId { get; set; }

        #endregion
    }
}