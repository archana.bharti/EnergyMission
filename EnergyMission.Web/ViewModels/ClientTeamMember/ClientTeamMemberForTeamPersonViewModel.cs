﻿using System.ComponentModel.DataAnnotations;

namespace EnergyMission.Web.ViewModels.ClientTeamMember
{
    public class ClientTeamMemberForTeamPersonViewModel
    {
        public long ClientId { get; set; }
        public long ClientTeamId { get; set; }

        [Required(ErrorMessage = "Enter Last Name")]
        public string PersonEmailAddress { get; set; }
        
        [Required(ErrorMessage = "Enter First Name")]
        public string PersonFirstName { get; set; }
    }
}