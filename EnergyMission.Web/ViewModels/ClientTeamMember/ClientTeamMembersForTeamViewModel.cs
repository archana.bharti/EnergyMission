﻿using System.Collections.Generic;
using Abp.Dependency;

using EnergyMission.Domain.Entities;

namespace EnergyMission.Web.ViewModels.ClientTeamMember
{
    public class ClientTeamMembersForTeamViewModel
    {
        private long _ClientTeamId;

        public ClientTeamMembersForTeamViewModel()
        {
            Members = new List<PersonEntity>();
        }

        public PersonEntity ClientTeam { get; set; }

        public long ClientTeamId
        {
            get { return _ClientTeamId; }
            set
            {
                _ClientTeamId = value;
                //InternalPopulateView(_ClientTeamId);
            }
        }

        public List<PersonEntity> Members { get; set; }

        //private void InternalPopulateView(long aClientTeamId)
        //{
        //    //Client Team
        //    IClientTeamAppService clientTeamAppService = IocManager.Instance.Resolve<IClientTeamAppService>();
        //    ClientTeam = clientTeamAppService.GetClientTeamWithInclude(new GetClientTeamInput {Id = aClientTeamId});

        //    //Team Members
        //    IClientTeamMemberAppService clientTeamMembersAppService = IocManager.Instance.Resolve<IClientTeamMemberAppService>();
        //    GetClientTeamMembersOutput teamMembers = clientTeamMembersAppService.GetClientTeamMembersWithInclude(new GetClientTeamMembersInput {ClientTeamId = aClientTeamId});
        //    foreach (ClientTeamMemberDto teamMember in teamMembers.ClientTeamMembers)
        //    {
        //        Members.Add(teamMember.Person);
        //    }
        //}
    }
}