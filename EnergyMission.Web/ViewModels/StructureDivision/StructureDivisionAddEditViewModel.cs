using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Application.Services;
using Abp.Dependency;
using EnergyMission.Domain.Entities;
using EnergyMission.Data;
namespace EnergyMission.Web.ViewModels.StructureDivision
{
    public class StructureDivisionAddEditViewModel 
    {
        private long _RestrictedClientId;

        /// <summary>
        ///     Class constructor
        /// </summary>
        public StructureDivisionAddEditViewModel()
        {
            _RestrictedClientId = -1;

            FillLookups();
        }

        /// <summary>
        ///     When TRUE we have been restricted by client
        /// </summary>
        public bool RestrictedByClient
        {
            get { return _RestrictedClientId != -1; }
        }

        public StructureDivisionEntity StructureDivision { get; set; }
        public List<SelectListItem> StructuresLookup { get; set; }

        /// <summary>
        ///     Restricts the structures displayed to those belonging to the client
        /// </summary>
        /// <param name="aClientId">Client to restrict by</param>
        public void RestrictToClient(long aClientId)
        {
            _RestrictedClientId = aClientId;
            EmDbContext db = new EmDbContext();
            //IStructureAppService structureAppService = IocManager.Instance.Resolve<IStructureAppService>();
            List<Data.Structure> structures = db.Structures.Where(tt => tt.SiteId == aClientId).ToList();

            StructuresLookup.Clear();
            foreach (Data.Structure structure in structures)
            {
                StructuresLookup.Add(new SelectListItem
                {
                    Text = structure.Name,
                    Value = Convert.ToString(structure.Id)
                });
            }
        }

        /// <summary>
        ///     Lookups needed to support Add/Edit
        /// </summary>
        private void FillLookups()
        {
            //IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

            //Structures


            EmDbContext db = new EmDbContext();
            //IStructureAppService structureAppService = IocManager.Instance.Resolve<IStructureAppService>();
            List<Data.Structure> structures = db.Structures.ToList();

            StructuresLookup.Clear();
            foreach (Data.Structure structure in structures)
            {
                StructuresLookup.Add(new SelectListItem
                {
                    Text = structure.Name,
                    Value = Convert.ToString(structure.Id)
                });
            }
            //StructuresLookup = listAppService.GetStructures()
            //                                 .ToList();
        }
    }
}