﻿using EnergyMission.Common.Enums;

namespace EnergyMission.Web.ViewModels.ClientTeam
{
    public class CreateNewClientTeamViewModel
    {
        public long ClientId { get; set; }
        public string PersonEmailAddress { get; set; }
        public string PersonFirstName { get; set; }
        public string TeamName { get; set; }
    }
}