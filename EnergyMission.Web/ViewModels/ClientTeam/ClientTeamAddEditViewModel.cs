﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using Abp.Dependency;
using EnergyMission.Domain.Entities;
using EnergyMission.Person;
using EnergyMission.Person.Dtos;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Data;
using EnergyMission.Web.Authentication;

namespace EnergyMission.Web.ViewModels.ClientTeam
{
    public class ClientTeamAddEditViewModel 
    {
        private long _RestrictedClientId;

        /// <summary>
        ///     Class constructor
        /// </summary>
        public ClientTeamAddEditViewModel()
        {
            //By default we are not restricted to a client
            _RestrictedClientId = -1;

            //FillLookups();
        }

        public List<SelectListItem> ClientsLookup { get; set; }
        public Data.ClientTeam ClientTeam { get; set; }
        public List<SelectListItem> PersonsLookup { get; set; }

        /// <summary>
        ///     Returns TRUE when the view model has been restricted to a particular client
        /// </summary>
        public bool RestrictedToClient
        {
            get { return _RestrictedClientId != -1; }
        }

        /// <summary>
        ///     Called to adjust the view model to be restricted to a particular client. This performs the following:
        ///     - Removes entries from drop down list
        ///     - Sets internal member to show we are restricted by client
        /// </summary>
        /// <param name="aClientId">Client to restrict by</param>
        public void RestrictToClient(long aClientId)
        {
            _RestrictedClientId = aClientId;

            //Replace the lookup with the filtered one
            ClientsLookup = ClientsLookup.Where(item => item.Value == Convert.ToString(aClientId))
                                         .ToList();

            //Replace the items in the lookup list for persons with what is applicable for the client
            List<string> personEmailAddresses = new List<string>();
            foreach (ApplicationUser appUser in RequestSecurityContextHelper.AppUserManager.Users)
            {
                if (appUser.UserProfileInformations != null && appUser.UserProfileInformations.ClientId == aClientId)
                {
                    if (!personEmailAddresses.Contains(appUser.UserName))
                    {
                        personEmailAddresses.Add(appUser.UserName);
                    }
                }
            }

            IPersonAppService personAppService=null ;  
          
            GetPersonsOutput personsForClient =  personAppService.GetPersonsForClient(new GetPersonsForClientInput
            {
                ClientId = aClientId,
                EmailAddresses = personEmailAddresses
            });

            PersonsLookup.Clear();
            foreach ( PersonEntity person in personsForClient.Persons.OrderBy(r=>r.LastName).ThenBy(r=>r.FirstName))
            {
                PersonsLookup.Add(new SelectListItem
                {
                    Text = string.Format("{0}, {1}", person.LastName, person.FirstName),
                    Value = Convert.ToString(person.Id)
                });
            }

            ClientTeam.ClientId = aClientId;
        }

        /// <summary>
        ///     Lookups needed to support Add/Edit
        /// </summary>
        //private void FillLookups()
        //{
        //    IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

        //    //Clients
        //    ClientsLookup = listAppService.GetClients()
        //                                  .ToList();

        //    //Persons
        //    PersonsLookup = listAppService.GetPersons()
        //                                  .ToList();
        //}
    }
}