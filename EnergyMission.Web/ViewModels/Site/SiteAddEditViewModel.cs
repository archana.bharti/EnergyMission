﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Application.Services;
using Abp.Dependency;
using EnergyMission.Domain.Entities;
using EnergyMission.Data;
namespace EnergyMission.Web.ViewModels.Site
{
    /// <summary>
    ///     Viewmodel when adding or editing a site
    /// </summary>
    public class SiteAddEditViewModel 
    {
        private long _RestrictedClientId;
        private SiteEntity _Site;

        /// <summary>
        ///     Class constructor
        /// </summary>
        public SiteAddEditViewModel()
        {
            FillLookups();
            _RestrictedClientId = -1;
        }

        public IList<SelectListItem> ClientsLookup { get; set; }
        public long Id { get; set; }

        /// <summary>
        ///     Returns TRUE when the view model has been restricted to a particular client
        /// </summary>
        public bool RestrictedToClient
        {
            get { return _RestrictedClientId != -1; }
        }

        public SiteEntity Site
        {
            get { return _Site; }
            set
            {
                _Site = value;
                if (_Site == null)
                {
                    Id = -1;
                }
                else
                {
                    Id = Convert.ToInt32(_Site.Id);
                }
            }
        }

        /// <summary>
        ///     Called to adjust the view model to be restricted to a particular client. This performs the following:
        ///     - Removes entries from drop down list
        ///     - Sets internal member to show we are restricted by client
        /// </summary>
        /// <param name="aClientId">Client to restrict by</param>
        public void RestrictToClient(long aClientId)
        {
            _RestrictedClientId = aClientId;
            EmDbContext db = new EmDbContext();
            //Replace the lookup with the filtered one
            List<Data.Client> clients = db.Clients.Where(tt=>tt.Id==aClientId).ToList();
            ClientsLookup.Clear();
            foreach (Data.Client Client in clients)
            {
                ClientsLookup.Add(new SelectListItem
                {
                    Text = Client.Name,
                    Value = Convert.ToString(Client.Id)
                });
            }
            //ClientsLookup = ClientsLookup.Where(item => item.Value == Convert.ToString(aClientId));

            //Site.ClientId = aClientId;
        }

        /// <summary>
        ///     Lookups needed to support the viewmodel
        /// </summary>
        private void FillLookups()
        {
            EmDbContext db = new EmDbContext();

            List<Data.Client> clients = db.Clients.ToList();
            foreach (Data.Client Client in clients)
            {
                ClientsLookup.Add(new SelectListItem
                {
                    Text = Client.Name,
                    Value = Convert.ToString(Client.Id)
                });
            }


            //Clients
            //ClientsLookup = listAppService.GetClients()
            //                              .ToList();
        }
    }
}