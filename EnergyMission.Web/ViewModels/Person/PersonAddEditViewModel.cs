﻿using Abp.Dependency;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Data;

namespace EnergyMission.Web.ViewModels.Person
{
    public class PersonAddEditViewModel 
    {
        /// <summary>
        ///     Lookups needed to support Add/Edit
        /// </summary>
        private void FillLookups()
        {
            //IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

            //Sites
            //SitesLookup = listAppService.GetSites()
            //                            .ToList();
        }


        /// <summary>
        ///     Class constructor
        /// </summary>
        public PersonAddEditViewModel()
        {
            FillLookups();
        }

        public Domain.Entities.PersonEntity Person { get; set; }

    }
}