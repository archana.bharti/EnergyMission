﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Application.Services;
using Abp.Dependency;
using EnergyMission.Domain.Entities;
using EnergyMission.Data;

namespace EnergyMission.Web.ViewModels.ClientAdministrator
{
    public class ClientAdministratorAddEditViewModel
    {
        /// <summary>
        ///     Class constructor
        /// </summary>
        public ClientAdministratorAddEditViewModel()
        {
            //FillLookups();
        }

        public ClientAdministratorEntity  ClientAdministrator { get; set; }
        public List<SelectListItem> ClientsLookup { get; set; }
        public long OriginalClientAdministratorId { get; set; }
        public long OriginalPersonId { get; set; }
        public List<SelectListItem> PersonsLookup { get; set; }

        /// <summary>
        ///     Lookups needed to support Add/Edit
        ///// </summary>
        //private void FillLookups()
        //{
        //    IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

        //    //Clients
        //    ClientsLookup = listAppService.GetClients()
        //                                  .ToList();

        //    //Persons
        //    PersonsLookup = listAppService.GetPersons()
        //                                  .ToList();
        //}
    }
}