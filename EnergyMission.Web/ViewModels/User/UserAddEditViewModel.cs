﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using System.Data;
using EnergyMission.Common.Security;
using EnergyMission.Data;
using System;

namespace EnergyMission.Web.ViewModels.User
{
    public class UserAddEditViewModel
    {
        /// <summary>
        ///     Class constructor
        /// </summary>
        public UserAddEditViewModel()
        {
            FillLookups();
        }

        public long? ClientId { get; set; }
        public List<SelectListItem> ClientsLookup { get; set; }

        public string EmailAddress { get; set; }
        public string Id { get; set; }
        public string Password { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Passwords don't match.")]
        public string PasswordConfirm { get; set; }

        public List<SelectListItem> RolesLookup { get; set; }
        public SystemRoleKindEnum SystemRole { get; set; }

        /// <summary>
        ///     Lookups needed to support the viewmodel
        /// </summary>
        private void FillLookups()
        {
            EmDbContext db = new EmDbContext();
            //IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

            //Clients


            List<Data.Client> sites = db.Clients.ToList();
            foreach (Data.Client Client in sites)
            {
                ClientsLookup.Add(new SelectListItem
                {
                    Text = Client.Name,
                    Value = Convert.ToString(Client.Id)
                });
            }

            List<Data.AspNetRole> Roles = db.AspNetRoles.ToList();
            foreach (Data.AspNetRole Client in Roles)
            {
                RolesLookup.Add(new SelectListItem
                {
                    Text = Client.Name,
                    Value = Convert.ToString(Client.Id)
                });
            }
            //ClientsLookup = listAppService.GetClients()
            //                              .ToList();

            //Roles
            //RolesLookup = listAppService.GetRoles()
            //                            .ToList();
        }
    }
}