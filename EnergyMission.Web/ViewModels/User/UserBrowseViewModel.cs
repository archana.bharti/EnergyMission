﻿using System.Collections.Generic;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.Web.ViewModels.User
{
    public class UserBrowseViewModel
    {
        public UserBrowseViewModel()
        {
            Users = new List<UserDto>();
        }

        public List<UserDto> Users { get; set; }
    }
}