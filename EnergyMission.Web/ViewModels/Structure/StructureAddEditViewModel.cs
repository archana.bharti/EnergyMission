﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using EnergyMission.Domain.Entities;
using EnergyMission.Data;
using EnergyMission.Site;

namespace EnergyMission.Web.ViewModels.Structure
{
    public class StructureAddEditViewModel 
    {
        private long _RestrictedClientId;

        /// <summary>
        ///     Class constructor
        /// </summary>
        public StructureAddEditViewModel()
        {
            _RestrictedClientId = -1;

            FillLookups();
        }

        /// <summary>
        ///     Returns TRUE should we have been restricted by client
        /// </summary>
        public bool RestrictedToClient
        {
            get { return _RestrictedClientId != -1; }
        }

        public List<SelectListItem> SitesLookup { get; set; }
        public StructureEntity Structure { get; set; }

        /// <summary>
        ///     Restricts the sites displayed to those belonging to the client.
        /// </summary>
        /// <param name="aClientId">Client to restrict by</param>
        public void RestrictToClient(long aClientId)
        {
            _RestrictedClientId = aClientId;

            EmDbContext db = new EmDbContext();
            //ISiteAppService siteAppService = IocManager.Instance.Resolve<ISiteAppService>();
            List<Data.Site> sites = db.Sites.Where(tt=>tt.ClientId==aClientId).ToList();

            SitesLookup.Clear();
            foreach (Data.Site site in sites)
            {
                SitesLookup.Add(new SelectListItem
                {
                    Text = site.Name,
                    Value = Convert.ToString(site.Id)
                });
            }
        }

        /// <summary>
        ///     Lookups needed to support Add/Edit
        /// </summary>
        private void FillLookups()
        {
            //IListAppService listAppService = IocManager.Instance.IocContainer.Resolve<IListAppService>();

            //Sites
            EmDbContext db = new EmDbContext();

            List<Data.Site> sites = db.Sites.ToList();
            foreach (Data.Site site in sites)
            {
                SitesLookup.Add(new SelectListItem
                {
                    Text = site.Name,
                    Value = Convert.ToString(site.Id)
                });
            }
        
                                     
        }
    }
}