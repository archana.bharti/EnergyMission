﻿using System.Linq;
using Abp.Dependency;

using EnergyMission.Person;
using EnergyMission.Site;
using EnergyMission.Web.Authentication;
using EnergyMission.Data;
namespace EnergyMission.Web.Helpers
{
    /// <summary>
    ///     Helper class to determine whether buttons should be enabled or not on
    ///     the client dashboard.
    /// </summary>
    public static class ClientDashboardEnabledHelper
    {

      
        /// <summary>
        ///     Determines whether any persons (with logins) exist for this client.
        /// </summary>
        /// <param name="aClientId">Client we are interested in.</param>
        /// <returns>Returns TRUE should any persons (with logins) exist for this client.</returns>
        public static bool StaffEnabled(long aClientId)
        {
           
            return RequestSecurityContextHelper.AppUserManager.Users
                                               .Any(r => r.UserProfileInformations.ClientId == aClientId);
        }

        /// <summary>
        ///     Determines whether the client can start adding structure divisions. They can only
        ///     start doing this once they have a structure defined.
        /// </summary>
        /// <param name="aClientId">Client identifier</param>
        /// <returns>Returns TRUE should the button be enabled, FALSE otherwise</returns>
        public static bool StructureDivisionEnabled(long aClientId)
        {

            EmDbContext db = new EmDbContext();
            bool Flag = false;
            int count = db.StructureDivisions.Where(tt => tt.StructureId == aClientId).Count();
            if (count >= 1)
                Flag = true;
            //IStructureAppService structureAppService = IocManager.Instance.Resolve<IStructureAppService>();
            return Flag;

        }

        /// <summary>
        ///     Determines when the client can start adding structure division sensors. They can only start doing
        ///     this once they have a structure division defined.
        /// </summary>
        /// <param name="aClientId">Client identifier</param>
        /// <returns>Returns TRUE should the button be enabled, FALSE otherwise</returns>
        public static bool StructureDivisionSensorEnabled(long aClientId)
        {

             EmDbContext db = new EmDbContext();
            bool Flag = false;
            int count = db.StructureDivisionSensors.Where(tt => tt.StructureDivisionId == aClientId).Count();
            if (count >= 1)
                Flag = true;
            //IStructureDivisionAppService structureDivisionDivisionAppService = IocManager.Instance.Resolve<IStructureDivisionAppService>();
            return Flag;
        }

        /// <summary>
        ///     Determines whether the client can start adding structures. They can only do this once they have a site defined.
        /// </summary>
        /// <param name="aClientId">Client identifier</param>
        /// <returns>Returns TRUE if the button should be enabled, FALSE otherwise</returns>
        public static bool StructureEnabled(long aClientId)
        {

            EmDbContext db = new EmDbContext();
            bool Flag = false;
            int count = db.Sites.Where(tt => tt.ClientId == aClientId).Count();
            if (count >= 1)
                Flag = true;
            //IStructureDivisionAppService structureDivisionDivisionAppService = IocManager.Instance.Resolve<IStructureDivisionAppService>();
            return Flag;
            //ISiteAppService siteAppService = IocManager.Instance.Resolve<ISiteAppService>();
            //return siteAppService.GetSitesForClient(aClientId)
            //                     .Any();
        }

        /// <summary>
        ///     Determines whether a client has any client teams configured in the system.
        /// </summary>
        /// <param name="aClientId">Client we are interested in.</param>
        /// <returns>Returns TRUE should the client already have a client team(s) in the system, FALSE otherwise.</returns>
        public static bool TeamsEnabled(long aClientId)
        {

            EmDbContext db = new EmDbContext();
            bool Flag = false;
            int count = db.ClientTeams.Where(tt => tt.ClientId == aClientId).Count();
            if (count >= 1)
                Flag = true;
            //IStructureDivisionAppService structureDivisionDivisionAppService = IocManager.Instance.Resolve<IStructureDivisionAppService>();
            return Flag;

            //IClientTeamAppService clientTeamAppService = IocManager.Instance.Resolve<IClientTeamAppService>();
            //return clientTeamAppService.GetClientTeams(new GetClientTeamsInput())
            //                           .ClientTeams
            //                           .Any(r => r.ClientId == aClientId);
        }
    }
}