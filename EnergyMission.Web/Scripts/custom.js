$(document).ready(function () {
    /* Sidebar */
    $(".sidebar ul li a.user").click(function () {
        $(".sidebar ul li ul.user_profile").slideToggle();
    });

    /* Verification Status */
    $(".examples .white_inner .client_verfied.people .inner .row_client span.edit a.check").click(function () {
        $(".examples .white_inner .client_verfied.people .inner .row_client span.edit .popup").slideToggle();
    });

    /* Header Menu */
    $(".toggle").click(function () {
        $("header nav").slideToggle();
    });

    $(".toggle").click(function () {
        $(".toggle").toggleClass("open");
    });

    $("header nav ul li.dropdown i").click(function () {
        $(this).siblings('ul').slideToggle();
    });

    //$('ul li a').on('click', function () {
    //    $(this).parent().find('ul li a').removeClass('menu');
    //    $(this).addClass('active');
    //});

    $("ul li a")
        .click(function (e) {
            var link = $(this);

            var item = link.parent("li a");

            if (item.hasClass("active")) {
                item.removeClass("menu").children("a").removeClass("active");
            } else {
                item.addClass("menu").children("a").addClass("active");
            }

            if (item.children("ul").length > 0) {
                var href = link.attr("href");
                link.attr("href", "#");
                setTimeout(function () {
                    link.attr("href", href);
                }, 300);
                e.preventDefault();
            }
        })
        .each(function () {
            var link = $(this);
            if (link.get(0).href === location.href) {
                link.addClass("active").parents("li").addClass("active");
                return false;
            }
        });


    /* Sidebar */
    $(".toggle2").click(function () {
        if ($("body").css('marginLeft') == '0px') {
            $("body").animate({ marginLeft: "-130" }, 500);
            $(".sidebar").animate({ margin: "0", right: '0' }, 500);
        }
        else {
            $("body").animate({ marginLeft: "0" }, 500);
            $(".sidebar").animate({ marginLeft: "-130px", right: "-130" }, 500);
        }
    });

    $(".toggle2").click(function () {
        $(this).toggleClass("open");
    });

    $('.examples .bottom label.name .tool').hover(function () {
        $(this).next().toggle()
    });

    $('.signin-form .form-row label .tool').hover(function () {
        $(this).children('.tooltip').toggle()
    });



    /* Sidebar */
    $(".red-label").click(function () {
        $("#chart2").slideUp();
        $("#chart9").slideUp();
        $("#chart8").slideDown();
    });

    $(".red-label.yellow-label").click(function () {
        $("#chart2").slideToggle();

        $("#chart8").slideUp();
    });

    $(".red-label.green-label").click(function () {
        $("#chart9").slideToggle();
        $("#chart8").slideUp();
    });


});

// Menu
$('.sidebar-v ul li a.togg').click(function (e) {
    e.stopPropagation();
    $('body').toggleClass("open");
});

/* Visualization */
//$(".sidebar-v ul li a.togg").click(function () {
//    $(".sidebar-v .popup").toggleClass('open');
//});

$("#locli").click(function () {
    $(".sidebar-v .popup").toggleClass('open');
});

$("#calli").click(function () {
    $(".sidebar-v .popup").toggleClass('open');
});


$(".sidebar-v ul li a").click(function () {
    $(this).toggleClass("active2");
    $(this).parent().siblings().find('a').removeClass('active2')
});

/* toggle - top */
$("a.toggle-top").click(function () {
    $(".sidebar-v").toggleClass('open');
    $(this).toggleClass('open');
    $(".right-sidebar").toggleClass('open');
});

/* sidebar.sidvis */
$(".sidebar").hover(function () {
    $(".right-sidebar").toggleClass('open');
});

