﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EnergyMission.Data;
using EnergyMission.Web.Authentication;
using EnergyMission.Domain.Entities;
using AutoMapper;
namespace EnergyMission.Web.Common
{
    public  static class UserDomainModule
    {


        public static void AutoMapperRegistrations()
        {
            //Entity --> DTO
            Mapper.CreateMap<AspNetUser,ApplicationUser>();
            Mapper.CreateMap<AspNetRole, ApplicationRole>();
       
          

            //DTO --> Entity
            Mapper.CreateMap<ApplicationUser, AspNetUser>();
            Mapper.CreateMap<ApplicationRole, AspNetRole>();
        }

    }
}