﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;
using System.Data;
using EnergyMission.Data;
using System.Web.Mvc;
using System.Threading.Tasks;
using EnergyMission.Web._Framework;
using EnergyMission.Web.Authentication;
using Microsoft.AspNet.Identity;
using DevExpress.XtraCharts;
using System.Collections;

namespace EnergyMission.Web.Common
{
    public class Common 
    {

       private EmDbContext db = null;
        public Common()
        {

         db=  new EmDbContext();
        
        }

        public List<Data.Site> GetSite()
        {
            var val = db.Database.SqlQuery<Data.Site>("select * from sites  where  IsDeleted=0 ").ToList();

            return val;



        }
        public List<Data.ClientTeam> GetAllTeams()
        {
            //var U = user;


            var val = db.Database.SqlQuery<Data.ClientTeam>("select * from ClientTeams  where IsDeleted=0 ").ToList();

            return val;



        }

        public Data.Site site { get; set; }

        public Data.ClientTeam Team { get; set; }

        public SelectList States
        {
            get
            {
                var value = db.States.Where(tt=>tt.IsDeleted==false).Select(tt => new {StateId=  tt.Id,Name= tt.Name }).ToList();


                return new SelectList(value, "StateId", "Name");
        }
            set { }
        }

        public SelectList Statesn
        {
            get
            {
                var value = db.States.Where(tt => tt.IsDeleted == false).Select(tt => new { StateId = tt.Id, Name1 = tt.Name }).ToList();


                return new SelectList(value, "Name1", "Name1");
            }
            set { }
        }
        public SelectList Countries
        {
            get
            {
                var value = db.Countries.Where(tt=>tt.IsDeleted==false).Select(tt => new {  tt.Id, Name1 = tt.Name }).ToList();


                return new SelectList(value, "Name1", "Name1");
            }
            set { }
        }
        public SelectList Countriess
        {
            get
            {
                var value = db.Countries.Where(tt => tt.IsDeleted == false).Select(tt => new {countryId= tt.Id, Name = tt.Name }).ToList();


                return new SelectList(value, "countryId", "Name");
            }
            set { }
        }
        public SelectList Persons
        {
            get
            {
                var value = db.Persons.Where(tt => tt.IsDeleted == false).Select(tt => new { tt.Id, Name = tt.FirstName }).ToList();


                return new SelectList(value, "Id", "Name");
            }
            set { }
        }


        public SelectList clientTeam
        {
            get
            {
                var value = db.ClientTeams.Where(tt => tt.IsDeleted == false).Select(tt => new { ClientTeamId=tt.Id, Name = tt.Name }).ToList();


                return new SelectList(value, "ClientTeamId", "Name");
            }
            set { }
        }
        //public SelectList getperson()
        //{
        
        //}
        public SelectList Postcodes
        {
            get
            {
                var value = db.PostCodes.Where(tt=>tt.IsDeleted==false).Select(tt=>new {tt.PostCodeNumber,PostCodeId=tt.Id}).ToList();


              return new SelectList(value, "PostCodeId", "PostCodeNumber");   
            }
            set { }
        }
        public SelectList Postcodess
        {
            get
            {
                var value = db.PostCodes.Where(tt => tt.IsDeleted == false).Select(tt => new {PostCodeNumber= tt.PostCodeNumber+" "+tt.PostName, PostCodeId = tt.Id }).ToList();


                return new SelectList(value, "PostCodeId", "PostCodeNumber");
            }
            set { }
        }
        public SelectList LocationList
        {
            get
            {
                var value = db.LocationMappings.Where(tt => tt.IsDeleted == false).Select(tt => new { LID = tt.Id, Name = tt.Locationname }).ToList();


                return new SelectList(value, "LID", "Name");
            }
            set { }
        }

        public SelectList SiteList
        {
            get
            {
                var value = db.Sites.Where(tt => tt.IsDeleted == false).Select(tt => new { SiteId = tt.Id, Name = tt.Name }).ToList();


                return new SelectList(value, "SiteId", "Name");
            }
            set { }
        }

        public SelectList Postcodename
        {
            get
            {
                var value = db.PostCodes.Where(tt => tt.IsDeleted == false).Select(tt => new {PostCodeNumber1= tt.PostCodeNumber, postname = tt.PostCodeNumber+" ("+tt.PostName+")" }).ToList();


                return new SelectList(value, "PostCodeNumber1", "postname");
            }
            set { }
        }
        public SelectList Suburbs
        {

            get
            {

                var value = db.Suburbs.Where(tt => tt.IsDeleted == false).Select(tt => new { tt.Name, Suburbid = tt.Id }).ToList();
                return new SelectList(value, "Suburbid", "Name");
            }
            set
            {

            }

        }


        ///rakesh
        ///






        // by Reena maan
        public SelectList SensorsList
        {
            get
            {
                var value = db.Sensors.Where(tt => tt.IsDeleted == false).Select(tt => new { SensorId = tt.Id, Name = tt.Name }).OrderBy(x => x.SensorId).ToList();


                return new SelectList(value, "SensorId", "Name");
            }
            set { }
        }

        //public SelectList getperson()
        //{

        //}


        public SelectList StructureDivisionSensor
        {

            get
            {

                var value = db.StructureDivisionSensors.Where(tt => tt.IsDeleted == false).Select(tt => new { tt.Id, sesorname = tt.Sensor.Name }).ToList();
                return new SelectList(value, "Id", "sesorname");
            }
            set
            {

            }

        }

        public SelectList StructureDivisionSensors(long struc)
        {

            var value = db.StructureDivisionSensors.Where(tt => tt.IsDeleted == false && tt.StructureDivisionId == struc).Select(tt => new { tt.Id, sesorname = tt.Sensor.Name }).ToList();
            return new SelectList(value, "ID", "sesorname");
        }
   


        public SelectList StructureList
        {
            get
            {
                var value = db.Structures.Where(tt => tt.IsDeleted == false).Select(tt => new { StructureId = tt.Id, Name = tt.Name }).OrderBy(x => x.Name).ToList();


                return new SelectList(value, "StructureId", "Name");
            }
            set { }
        }
        public SelectList StructureDivisionList
        {
            get
            {
                var value = db.StructureDivisions.Where(tt => tt.IsDeleted == false).Select(tt => new { StructureDivisId = tt.Id, StructureDivisName = tt.Name }).OrderBy(x => x.StructureDivisName).ToList();


                return new SelectList(value, "StructureDivisId", "StructureDivisName");
            }
            set { }
        }
        public List<Data.Person> GetPersons(long user)

        { 
            var U=user;

            var val = db.Database.SqlQuery<Data.Person>("select * from persons where CreatorUserId={0} and IsDeleted=0 ", U).ToList();

            return val ;

        
        
        }



        public List<Person> GetPerson(long user)
        {

            var U = user;

            var val = db.Database.SqlQuery<Person>("select Id as PersonId ,Firstname+' '+Lastname as Persons from persons where CreatorUserId={0} and IsDeleted=0 and id not in(select personId from ClientAdministrators where isdeleted =0  ) and id not in (select personid from  ClientTeamManager where isdeleted=0)", U).ToList();

            return val;



        }
        public List<Data.Organization> GetClients(long user)
        {
            var U = user;

            var val = db.Database.SqlQuery<Data.Organization>("select * from Organizations  where ClientId={0} and isdeleted=0", U).ToList();

            return val;



        }
        public List<Data.ClientTeam> GetTeams(long user)
        {
            var U = user;


            var val = db.Database.SqlQuery<Data.ClientTeam>("select * from ClientTeams  where CreatorUserId={0} and IsDeleted=0 ", U).ToList();

            return val;



        }

        public string GetTeamId(long team)
        {
            return db.ClientTeams.Where(tt => tt.Id == team).Select(tt => tt.Name).FirstOrDefault(); 



        }

    }
    public class mainchecked
    {

        public Boolean MainChecked { get; set; }


    }

    public class Person {

        public long PersonId { get; set; }
        public string Persons { get; set; }

    
    }
    public class ChartSeriesBindingDemoOptions
    {

        public const string DefaultCategory = "All";

        bool showLabels;
        bool isDay;
        bool isNight;
        string category = DefaultCategory;
        SeriesPointKey sortingKey = SeriesPointKey.Value_1;
        SortingMode sortingMode = SortingMode.Ascending;
        IEnumerable categories;

        public bool ShowLabels
        {
            get { return showLabels; }
            set { showLabels = value; }
        }

        public bool IsDay
        {
            get { return isDay; }
            set { isDay = value; }
        }

        public bool IsNight
        {
            get { return isNight; }
            set { isNight = value; }
        }
        public string Category
        {
            get { return category; }
            set { category = value; }
        }
        public SeriesPointKey SortingKey
        {
            get { return sortingKey; }
            set { sortingKey = value; }
        }
        public SortingMode SortingMode
        {
            get { return sortingMode; }
            set { sortingMode = value; }
        }
        public IEnumerable Categories { get { return categories; } }

        public ChartSeriesBindingDemoOptions()
        {
            categories = "Select";
        }


        public const string Month = "Month";
        public const string Year = "Year";


        string seriesDataMember = Year;
        public string SeriesDataMember
        {
            get { return seriesDataMember; }
            set { seriesDataMember = string.IsNullOrEmpty(value) ? Year : value; }
        }
        public string ArgumentDataMember { get { return seriesDataMember == Month ? Year : Month; } }




    }

}