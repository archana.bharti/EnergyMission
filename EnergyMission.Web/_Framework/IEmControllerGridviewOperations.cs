using System.Web.Mvc;

namespace EnergyMission.Web._Framework
{
    /// <summary>
    ///     Defines the various grid operations. This ensures that the controller meets
    ///     all the aspects of the Resharper code template. In essence these methods map to
    ///     the operations on the grid for display of individual records for "Edit" and "New"
    /// </summary>
    public interface IEmControllerGridviewOperations
    {
        #region Public Members

        [AcceptVerbs(HttpVerbs.Get)]
        ActionResult Edit(long Id);

        ActionResult GridViewPartial();

        [AcceptVerbs(HttpVerbs.Get)]
        ActionResult New();

        #endregion
    }
}