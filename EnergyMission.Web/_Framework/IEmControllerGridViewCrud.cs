using System.Web.Mvc;


namespace EnergyMission.Web._Framework
{
    /// <summary>
    ///     Interface that defines how the data (CRUD) operations are to be performed from the grid:
    ///     .
    ///     Operation - HTTP VERB - Database
    ///     ---------   ---------   --------
    ///     Create - POST - Insert
    ///     Delete - DELETE - Delete
    ///     Update - PUT - Update
    ///     .
    ///     This is based on  http://www.restapitutorial.com/ as well as satisfying the
    ///     Post Redirect Get pattern
    ///     http://www.stevefenton.co.uk/Content/Blog/Date/201104/Blog/ASP-NET-MVC-Post-Redirect-Get-Pattern/
    /// </summary>
    /// <typeparam name="TCreateDataFromViewDto">
    ///     Class to use for DTO containing the data from the view for ADDING the new
    ///     record.
    /// </typeparam>
    /// <typeparam name="TUpdateDataFromViewDto">
    ///     Class to use for DTO containing the data from the view for UPDATING the record
    ///     record.
    /// </typeparam>
    public interface IEmControllerGridViewCrud<TCreateDataFromViewDto, TUpdateDataFromViewDto> 
    {
        #region Public Members

        [AcceptVerbs(HttpVerbs.Post)]
        ActionResult Create(TCreateDataFromViewDto aDataFromView);

        [AcceptVerbs(HttpVerbs.Delete)]
        ActionResult Delete(long Id);

        [AcceptVerbs(HttpVerbs.Post)] //TODO: Need to change this to a PUT
        ActionResult Update(TUpdateDataFromViewDto aDataFromView);

        #endregion
    }
}