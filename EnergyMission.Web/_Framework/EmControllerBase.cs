﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Abp.Web.Mvc.Controllers;
using DevExpress.XtraPrinting.Native;
using EnergyMission.Web.Identity;
using Microsoft.AspNet.Identity.Owin;
using Natiki.Extensions;

namespace EnergyMission.Web._Framework
{
    [Authorize]
    public abstract class EmControllerBase : AbpController
    {
        /// <summary>
        ///     Class constructor
        /// </summary>
        //protected EmControllerBase()
        //{
        //    LocalizationSourceName = EnergyMissionConsts.LOCALIZATION_SOURCE_NAME;
        //}

        /// <summary>
        ///     Role manager being used in the application
        /// </summary>
        public ApplicationRoleManager AppRoleManager
        {
            get
            {
                return HttpContext.GetOwinContext()
                                  .Get<ApplicationRoleManager>();
            }
        }

        /// <summary>
        ///     User manager being used in the application
        /// </summary>
        public ApplicationUserManager AppUserManager
        {
            get
            {
                return HttpContext.GetOwinContext()
                                  .GetUserManager<ApplicationUserManager>();
            }
        }

        /// <summary>
        ///     Sets/Gets the page title to be displayed
        /// </summary>
        public string PageTitle
        {
            get { return ViewBag.PageTitle; }
            set { ViewBag.PageTitle = value; }
        }

        /// <summary>
        ///     Directs user back from where they came.
        /// </summary>
        /// <returns>Returns the user to the location from which they came</returns>
        public ActionResult RedirectToPrevious(string aDefaultAction = "Index", string aDefaultController = "Home")
        {
            if (Session == null || Session["PrevUrl"] == null)
            {
                return RedirectToAction(aDefaultAction, aDefaultController);
            }

            String url = ((Uri) Session["PrevUrl"]).PathAndQuery;

            if (Request.Url != null && Request.Url.PathAndQuery != url)
            {
                return Redirect(url);
            }

            return RedirectToAction(aDefaultAction, aDefaultController);
        }

        /// <summary>
        ///     Called each time an action is run. We use this opportunity to:
        ///     * Record the URL from which we were called for use with RedirectToPrevious()
        /// </summary>
        /// <param name="aFilterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext aFilterContext)
        {
            HttpContextBase httpContext = aFilterContext.HttpContext;

            if (httpContext.Request.RequestType == "GET"
                && !httpContext.Request.IsAjaxRequest()
                && aFilterContext.IsChildAction == false) // do no overwrite if we do child action.
            {
                //Stop overwriting previous page if we just reload the current page.
                if (Session["CurUrl"] != null
                    && ((Uri) Session["CurUrl"]).Equals(httpContext.Request.Url))
                    return;

                //Prevent overwrite if we are calling RedirectToPrevious
                if (Session["CurUrl"] != null
                    && (httpContext.Request != null)
                    && (httpContext.Request.Url != null)
                    && (httpContext.Request.Url.ToString()
                                   .Contains("RedirectToPrevious")))
                    return;

                Session["PrevUrl"] = Session["CurUrl"] ?? httpContext.Request.Url;
                Session["CurUrl"] = httpContext.Request.Url;
            }
        }

        /// <summary>
        ///     Allows for an error message to be shown in the layout
        /// </summary>
        /// <param name="aErrorMessage">Error message to display</param>
        protected void StatusMessageError(string aErrorMessage)
        {
            ViewData["StatusMessageError"] = aErrorMessage;
        }

        /// <summary>
        ///     Constructs a string to display all the model state errors.
        /// </summary>
        /// <param name="aModelStateDictionary">Dictionary containing all the model values and any associated errors.</param>
        protected void StatusMessageErrorsModelState(ModelStateDictionary aModelStateDictionary)
        {
            string errorMessage = string.Empty;
            List<string> errors = aModelStateDictionary.GetErrorListFromModelState();
            foreach (string error in errors)
            {
                errorMessage += "<li>" + error + "</li>";
            }

            if (!errorMessage.IsEmpty())
                errorMessage += "<ul>" + errorMessage + "</ul>";
            StatusMessageError(errorMessage);
        }

        /// <summary>
        ///     Allows for a success message to be shown in the layout
        /// </summary>
        /// <param name="aSuccessMessage">Success message</param>
        protected void StatusMessageSuccess(string aSuccessMessage)
        {
            ViewData["StatusMessageSuccess"] = aSuccessMessage;
        }
    }
}