﻿using Abp.Web.Mvc.Views;

namespace EnergyMission.Web.Views
{
    public abstract class EnergyMissionWebViewPageBase : EnergyMissionWebViewPageBase<dynamic>
    {

    }

    public abstract class EnergyMissionWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected EnergyMissionWebViewPageBase()
        {
            LocalizationSourceName = EnergyMissionConsts.LOCALIZATION_SOURCE_NAME;
        }
    }
}