﻿<script type="text/javascript">
    // ----------------------------------------------------------------------------
    // Called when a record has been successfully deleted from a grid view
    // ----------------------------------------------------------------------------
    function GridViewDeleteSuccess(response) {
        $("#gridContainer").html(response);
    }
</script>

