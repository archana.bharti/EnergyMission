﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EnergyMission.Common.Security;
using EnergyMission.Web.Authentication;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace EnergyMission.Web.Identity
{
    /// <summary>
    ///     Class managing all the user roles within the system using MS Identity 2.0
    /// </summary>
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        #region Private Members

        /// <summary>
        ///     Internal helper method that retrieves the role based on the role kind.
        /// </summary>
        /// <param name="aRoleKind">Role we are interested in.</param>
        /// <returns>Returns the role if found, null otherwise.</returns>
        private ApplicationRole GetApplicationRoleFromRoleKind(SystemRoleKindEnum aRoleKind)
        {
            KeyValuePair<SystemRoleKindEnum, ApplicationRole> roleInfo = SystemRoleList.SystemRoles.FirstOrDefault(r => r.Key == aRoleKind);
            try
            {
                return roleInfo.Value;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #region Public Members

        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> aRoleStore) : base(aRoleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> aOptions, IOwinContext aContext)
        {
            return new ApplicationRoleManager(new RoleStore<ApplicationRole>(aContext.Get<ApplicationDbContext>()));
        }

        /// <summary>
        ///     Determines the role kind for the currently logged in user.
        /// </summary>
        /// <param name="aUserManager">User manager providing access to the underlying users.</param>
        /// <returns>Retuns the role for the logged in user, returns SystemRoleKindEnum.None if user is not assigned to any role.</returns>
        public SystemRoleKindEnum GetLoggedInUserRoleKind(ApplicationUserManager aUserManager)
        {
            try
            {
                ApplicationUser loggedInUser = aUserManager.FindById(HttpContext.Current.User.Identity.GetUserId());
                if (loggedInUser != null && loggedInUser.Roles.Count > 0)
                {
                    IdentityUserRole role = loggedInUser.Roles.First();
                    return GetRoleKindByRoleId(role.RoleId);
                }
                return SystemRoleKindEnum.None;
            }
            catch (Exception)
            {
                //This will deal with where for instance the ASP.NET user information tables have not been created
                return SystemRoleKindEnum.None;
            }
        }

        /// <summary>
        ///     Retrieves the role id based on the role kind
        /// </summary>
        /// <param name="aRoleKind">Role we are interested in</param>
        /// <returns>Returns the role id if found, empty string if not found.</returns>
        public string GetRoleIdByRoleKind(SystemRoleKindEnum aRoleKind)
        {
            ApplicationRole role = GetApplicationRoleFromRoleKind(aRoleKind);
            return role == null ? string.Empty : role.Id;
        }

        /// <summary>
        ///     Translates a role id into its associated enumeration.
        /// </summary>
        /// <param name="aRoleId">Unique identifier for the role within the ASP.NET roles subsystem.</param>
        /// <returns>Returns the role kind, None if not found</returns>
        public SystemRoleKindEnum GetRoleKindByRoleId(string aRoleId)
        {
            ApplicationRole appRole = Roles.FirstOrDefault(r => r.Id == aRoleId);
            if (appRole == null)
                return SystemRoleKindEnum.None;

            KeyValuePair<SystemRoleKindEnum, ApplicationRole> systemRole = SystemRoleList.SystemRoles.FirstOrDefault(r => r.Value.Name == appRole.Name);
            return systemRole.Key;
        }

        /// <summary>
        ///     Retrieves the role name based on the role kind
        /// </summary>
        /// <param name="aRoleKind">Role we are interested in</param>
        /// <returns>Returns the role id if found, empty string if not found.</returns>
        public string GetRoleNameByRoleKind(SystemRoleKindEnum aRoleKind)
        {
            ApplicationRole role = GetApplicationRoleFromRoleKind(aRoleKind);
            return role == null ? string.Empty : role.Name;
        }

        #endregion
    }
}