using System;
using System.Linq;
using System.Web;
using EnergyMission.Web.Authentication;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using EnergyMission.Data;
using EnergyMission.Domain.Entities;
using AutoMapper;
namespace EnergyMission.Web.Identity
{
    /// <summary>
    ///     Application user manager based on MS Identity 2.0
    /// </summary>
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> aStore)
            : base(aStore)
        {
        }

        /// <summary>
        /// Generates a random password based on the parameters supplied. The defaults ensure that we 
        /// satisfy the PasswordValidator used in the constructor.
        /// </summary>
        /// <param name="aLowerCase">Number of lower case letters to include.</param>
        /// <param name="aUpperCase">Number of upper case letters to include.</param>
        /// <param name="aNumerics">Number of numerics to include.</param>
        /// <param name="aNonLetters">Number of not numerics and letters to include</param>
        /// <returns>Returns the new password.</returns>
        public static string GeneratePassword(int aLowerCase = 3, int aUpperCase = 3, int aNumerics = 1, int aNonLetters = 2)
        {
            string lowers = "abcdefghijklmnopqrstuvwxyz";
            string nonLetters = "!@#$^*()_-+=[{]};:<>|./";
            string uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string number = "0123456789";

            Random random = new Random();

            string result = string.Empty;
            
            //Lower Case
            for (int i = 1; i <= aLowerCase; i++)
                result = result.Insert(
                    random.Next(result.Length),
                    lowers[random.Next(lowers.Length - 1)].ToString()
                );

            //Non Letters
            for (int i = 1; i <= aNonLetters; i++)
                result = result.Insert(
                    random.Next(result.Length),
                    nonLetters[random.Next(nonLetters.Length - 1)].ToString()
                );

            //Upper Case
            for (int i = 1; i <= aUpperCase; i++)
                result = result.Insert(
                    random.Next(result.Length),
                    uppers[random.Next(uppers.Length - 1)].ToString()
                );

            //Numerics
            for (int i = 1; i <= aNumerics; i++)
                result = result.Insert(
                    random.Next(result.Length),
                    number[random.Next(number.Length - 1)].ToString()
                );

            //Now shuffle the string
            return new string(result.OrderBy(r => random.Next()).ToArray()); ;
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="aOptions"></param>
        /// <param name="aContext"></param>
        /// <returns></returns>
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> aOptions, IOwinContext aContext)
        {
            ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(aContext.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            IDataProtectionProvider dataProtectionProvider = aOptions.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }

        /// <summary>
        ///     Get the object representing the currently loggged in user
        /// </summary>
        /// <returns>Returns the currently logged in user, null otherwise</returns>
        public ApplicationUser GetLoggedInUser()
        {
            EmDbContext db = new EmDbContext();
            string userId = HttpContext.Current.User.Identity.GetUserId();
            var value = db.AspNetUsers.FirstOrDefault(r => r.Id == userId);

            UserProfileInformationEntity userentity = new UserProfileInformationEntity

            {
                ClientId=value.UserProfileInformation.ClientId,
                PersonId=value.UserProfileInformation.PersonId,
                //CreationTime=value.UserProfileInformation.CreationTime,
                //LastModifierUserId=value.UserProfileInformation.LastModifierUserId,
                //IsDeleted=value.UserProfileInformation.IsDeleted,
                //LastModificationTime=value.UserProfileInformation.LastModificationTime
             

            };
            ApplicationUser user = new ApplicationUser
                {
                    Id = value.Id,
                    UserName = value.UserName,
                    UserProfileInformations = userentity
                
                };
                
           
        
         
            //user.UserName = value.UserName;
            //user.Id = value.Id;
            //user.PasswordHash = value.PasswordHash;
            //user.PhoneNumber = value.PhoneNumber;
            //user.PhoneNumberConfirmed = value.PhoneNumberConfirmed;
            //user.UserProfileInformation.ClientId = value.UserProfileInformation.ClientId;
            //user.UserProfileInformation.PersonId = value.UserProfileInformation.PersonId;
          
         

              return user;
        }
    }
}