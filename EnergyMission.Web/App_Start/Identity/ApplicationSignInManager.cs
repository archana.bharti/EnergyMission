using System.Security.Claims;
using System.Threading.Tasks;
using EnergyMission.Web.Authentication;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace EnergyMission.Web.Identity
{
    /// <summary>
    ///     Application sign in manager based on MS Identity 2.0
    /// </summary>
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        #region Public Members

        public ApplicationSignInManager(ApplicationUserManager aUserManager, IAuthenticationManager aUthenticationManager)
            : base(aUserManager, aUthenticationManager)
        {
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> aOptions, IOwinContext aContext)
        {
            return new ApplicationSignInManager(aContext.GetUserManager<ApplicationUserManager>(), aContext.Authentication);
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser aUser)
        {
            return aUser.GenerateUserIdentityAsync((ApplicationUserManager) UserManager);
        }

        #endregion
    }
}