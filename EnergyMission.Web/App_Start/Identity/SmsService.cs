using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace EnergyMission.Web.Identity
{
    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage aMessage)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}