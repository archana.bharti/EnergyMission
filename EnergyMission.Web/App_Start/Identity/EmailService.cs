using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Configuration;

namespace EnergyMission.Web.Identity
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage aMessage)
        {
            // Plug in your email service here to send an email.
            var sentFrom = "testifiedemail@gmail.com";
            var pwd = "testifiedpassword@hackfree";

            // Configure the client:
            System.Net.Mail.SmtpClient client =
                new System.Net.Mail.SmtpClient("smtp.gmail.com");

            client.Port = 587;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            

            // Create the credentials:
            System.Net.NetworkCredential credentials =
                new System.Net.NetworkCredential(sentFrom, pwd);

            client.EnableSsl = true;
            client.Credentials = credentials;

            // Create the message:
            var mail =
                new System.Net.Mail.MailMessage(sentFrom, aMessage.Destination);

            mail.Subject = aMessage.Subject;
            mail.Body = aMessage.Body;

            // Send:
            //client.Send(mail);
            //return Task.FromResult(0);
            return client.SendMailAsync(mail);
            
         //return Task.FromResult(0);
        }
    }
}