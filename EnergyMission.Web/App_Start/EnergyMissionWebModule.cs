﻿using System.Reflection;
using System.Web;
using Abp.Localization;
using Abp.Localization.Sources.Xml;
using Abp.Modules;
using Castle.Core.Logging;
using EnergyMission.Country;
using EnergyMission.Country.Dtos;

namespace EnergyMission.Web
{
    [DependsOn( typeof(EnergyMissionWebApiModule))]
    public class EnergyMissionWebModule : AbpModule
    {
        #region Public Members

        public EnergyMissionWebModule()
        {
            Logger = NullLogger.Instance;
        }

        public override void Initialize()
        {
            //Logger.Debug("Initialize");
            //IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            //Logger.Debug("/RegisterAssemblyByConvention");

            Configuration.Localization.Sources.Add(
                new XmlLocalizationSource(
                    EnergyMissionConsts.LOCALIZATION_SOURCE_NAME,
                    HttpContext.Current.Server.MapPath("~/Localization/EnergyMission")
                    )
                );

            Logger.Debug("/LocalizationAdd");
            //**AreaRegistration.RegisterAllAreas();
            //**RouteConfig.RegisterRoutes(RouteTable.Routes);                `
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //public override void PostInitialize()
        //{
        //    GetCountriesOutput countries = IocManager.Resolve<CountryAppService>()
        //                                             .GetCountries(new GetCountriesInput());
        //    if (countries.Countries.Count > 0)
        //    {
        //    }
        //}

        public override void PreInitialize()
        {
            Logger.Debug("PreInitialize");
            //Add/remove languages for your application
            Configuration.Localization.Languages.Add(new LanguageInfo("en", "English", "famfamfam-flag-england", true));
            Configuration.Localization.Languages.Add(new LanguageInfo("tr", "Türkçe", "famfamfam-flag-tr"));

            //Configure navigation/menu
            //Configuration.Navigation.Providers.Add<EnergyMissionNavigationProvider>();
            Logger.Debug("/PreInitialize");
        }

        public ILogger Logger { get; set; }

        #endregion
    }
}