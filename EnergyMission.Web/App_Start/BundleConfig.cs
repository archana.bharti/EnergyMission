﻿using System.Web;
using System.Web.Optimization;

namespace EnergyMission.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.2.2.js", "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                   "~/Scripts/jquery-ui-1.11.4.js"));
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-hover-dropdown.js", "~/Scripts/gridmvc.js"));

            bundles.Add(new ScriptBundle("~/bundles/font").Include(
                        "~/content/font-awesome.min.css","~/content/fonts.css"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/Scripts/custom.js","~/scripts/jquery.slimscroll.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/gmap").Include(
                       "~/Scripts/jquery.gmap.js",
                       "~/Scripts/jquery.gmap_init.js"));

            bundles.Add(new ScriptBundle("~/bundles/modalform").Include(
                        "~/Scripts/modalform.js"));

            bundles.Add(new StyleBundle("~/Content/reset").Include(
                      "~/Content/reset.css"

                      
                  ));
            bundles.Add(new StyleBundle("~/Content/superadmin").Include(
                    "~/Content/super-admin.css"


                ));
            bundles.Add(new StyleBundle("~/Content/response").Include(
                 "~/Content/responsive.css", "~/Content/Custom.css"


             ));
        

        }
    }
}