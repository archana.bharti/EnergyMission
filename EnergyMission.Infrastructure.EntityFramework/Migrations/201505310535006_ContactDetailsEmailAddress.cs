namespace EnergyMission.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContactDetailsEmailAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContactDetails", "EmailAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ContactDetails", "EmailAddress");
        }
    }
}
