namespace EnergyMission.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestPropertyAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfileInformation", "TestPropertyAdd", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfileInformation", "TestPropertyAdd");
        }
    }
}
