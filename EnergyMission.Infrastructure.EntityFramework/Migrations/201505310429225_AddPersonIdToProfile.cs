namespace EnergyMission.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPersonIdToProfile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfileInformation", "PersonId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserProfileInformation", "PersonId");
        }
    }
}
