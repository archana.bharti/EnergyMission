namespace EnergyMission.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AssociateUserProfiles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserProfileInformation_Id", c => c.Long());
            CreateIndex("dbo.AspNetUsers", "UserProfileInformation_Id");
            AddForeignKey("dbo.AspNetUsers", "UserProfileInformation_Id", "dbo.UserProfileInformation", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "UserProfileInformation_Id", "dbo.UserProfileInformation");
            DropIndex("dbo.AspNetUsers", new[] { "UserProfileInformation_Id" });
            DropColumn("dbo.AspNetUsers", "UserProfileInformation_Id");
            DropTable("dbo.UserProfileInformation");
        }
    }
}
