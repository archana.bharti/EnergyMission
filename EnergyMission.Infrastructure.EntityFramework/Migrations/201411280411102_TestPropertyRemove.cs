namespace EnergyMission.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestPropertyRemove : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserProfileInformation", "TestPropertyAdd");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfileInformation", "TestPropertyAdd", c => c.String());
        }
    }
}
