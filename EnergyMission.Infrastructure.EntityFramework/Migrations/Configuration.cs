using System;
using System.Data.Entity.Migrations;
using EnergyMission.Domain.Entities;
using Natiki.Web.ChangeLog;

namespace EnergyMission.Migrations
{
    internal sealed class Configuration 
    {
        #region Private Members

        /// <summary>
        ///     Adds all the change log entries to the database
        /// </summary>
        /// <param name="aDataContext">Data context to use for the change log entries</param>
        private void ChangeLogEntriesUpdate(EmDbContext aDataContext)
        {
            //00.16.00
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2015-07-26"), 1, "00.16.00", "Client team members can now be managed by the designated team manager.", "BAAB0EBC-2F7D-4C2B-944F-5C73283C4757", "")));


            //00.15.00
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2015-07-13"), 1, "00.15.00", "Client can now add client teams.", "CDB56B77-4E22-41DB-9517-C08E8395DAFA", "")));

            //00.14.00
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2015-06-28"), 1, "00.14.00", "Client dashboard navigation now enables as needed. Dynamic navigation based on role. Currently for client only.", "3CB5B8EE-F5BE-4A2C-88D1-46247D59A43E", "")));

            //00.13.00
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2015-06-08"), 1, "00.13.00", "Added create new client as well as ability for a client administrator to create all the necessary 'things' with the exception of team members.", "962370D3-5BC7-4DDB-BEF7-67DC29C848FF", "")));

            //00.12.00
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2015-01-22"), 1, "00.12.00", "Exposed user login management.", "E0F8C1AA-A0E6-4C7D-A7E5-D5C56F55A1AE", "")));

            //00.11.01
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2015-01-22"), 1, "00.11.01", "Filtering now works for all grids except Client Team Members, Location Mappings.", "C0C60899-B7BD-4FEF-B125-1390E41C1D2F", "")));
            
            //00.11.00
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2014-11-28"), 1, "00.11.00", "Change log link now works.", "501B9389-BCAF-47C0-9A68-C13487CD9E21", "")));
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2014-11-28"), 2, "00.11.00", "Main desktop display is based on user role within the system.", "C15553FD-22D8-4E8C-8A92-F875672B3566", "")));
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2014-11-28"), 3, "00.11.00", "Menu composition can now be controlled bsed on user role in the system.", "21E999E1-A6C2-41F3-8927-BB59A846551C", "")));

            //00.10.01
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2014-11-21"), 1, "00.10.01", "Roles framework and other fixes now in place.", "EECCB766-210C-413D-B26A-2B06AE3FF1F5", "")));

            //00.10.00
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2014-11-07"), 1, "00.10.00", "Second public release.", "F3702F72-1E30-4B27-84B9-C8251AA2D2DD", "")));
            aDataContext.ChangeLogs.AddOrUpdate(entry => entry.UserKey, ChangeLogModelConvert(ChangeLogHelper.EntryCreate(DateTime.Parse("2014-11-07"), 2, "00.10.00", "Bread crumbs are now on the right hand side.", "799D389E-288A-4169-A7BB-B41070EFE0DC", "12")));

            //Save all the changes
            aDataContext.SaveChanges();
        }

        /// <summary>
        ///     Adapter to convert a change log model to the entity model we are using in this project.
        /// </summary>
        /// <param name="aModel">Model to convert</param>
        /// <returns>Returns the adapated entity</returns>
        private ChangeLogEntity ChangeLogModelConvert(ChangeLogModel aModel)
        {
            ChangeLogEntity result = new ChangeLogEntity
            {
                UserKey = aModel.UserKey,
                CreationTime = DateTime.Now,
                Date = aModel.Date,
                FogBugzReferenceNumber = aModel.FogBugzReferenceNumber,
                Explain = aModel.Explain,
                LastModificationTime = DateTime.Now,
                Ordering = aModel.Ordering,
                Version = aModel.Version
            };
            return result;
        }

        #endregion

        #region Protected Members

        protected override void Seed(EmDbContext aContext)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            ChangeLogEntriesUpdate(aContext);
        }

        #endregion

        #region Public Members

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "EnergyMission";
        }

        #endregion
    }
}