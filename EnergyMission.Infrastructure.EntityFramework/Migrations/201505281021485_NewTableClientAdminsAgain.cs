namespace EnergyMission.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class NewTableClientAdminsAgain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClientAdministrators",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ClientId = c.Long(nullable: false),
                        PersonId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClientAdministrastorEntity_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.PersonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClientAdministrators", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.ClientAdministrators", "ClientId", "dbo.Clients");
            DropIndex("dbo.ClientAdministrators", new[] { "PersonId" });
            DropIndex("dbo.ClientAdministrators", new[] { "ClientId" });
            DropTable("dbo.ClientAdministrators",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClientAdministrastorEntity_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
