namespace EnergyMission.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class InitialScaffold : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CountryId = c.Long(nullable: false),
                        Line1 = c.String(nullable: false, maxLength: 50),
                        Line2 = c.String(maxLength: 50),
                        Line3 = c.String(maxLength: 50),
                        PostCodeId = c.Long(nullable: false),
                        StateId = c.Long(nullable: false),
                        SuburbId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.PostCodes", t => t.PostCodeId, cascadeDelete: true)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .ForeignKey("dbo.Suburbs", t => t.SuburbId, cascadeDelete: true)
                .Index(t => t.CountryId)
                .Index(t => t.PostCodeId)
                .Index(t => t.StateId)
                .Index(t => t.SuburbId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AbbreviationCode = c.String(nullable: false, maxLength: 3),
                        InternationalCountryDialCode = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PostCodes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PostCodeNumber = c.String(nullable: false, maxLength: 10),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AbbreviationCode = c.String(nullable: false, maxLength: 3),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Suburbs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ChangeLog",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        Date = c.DateTime(nullable: false),
                        Explain = c.String(nullable: false),
                        FogBugzReferenceNumber = c.String(maxLength: 5),
                        Ordering = c.Int(nullable: false),
                        UserKey = c.String(),
                        Version = c.String(maxLength: 8),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentClientId = c.Long(),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        Status=c.Boolean(nullable: false),
                        City = c.String(),
                        Address=c.String(),
                        NOE=c.Int(),
                        StateId=c.Long(),
                        PostCode=c.Long(),
                        Geo_location_Latitude=c.String(),
                        Geo_location_Longditude=c.String()
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.States", t => t.StateId)
                .ForeignKey("dbo.PostCodes", t => t.StateId)
                .ForeignKey("dbo.Clients", t => t.Id)
                .Index(t => t.ParentClientId);

            
            CreateTable(
                "dbo.ClientTeams",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ClientId = c.Long(nullable: false),
                        Name = c.String(),
                        ParentClientTeamId = c.Long(),
                        PersonTeamManagerId = c.Long(nullable: false),
                        TeamContactDetailsId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.ClientTeams", t => t.ParentClientTeamId)
                .ForeignKey("dbo.ContactDetails", t => t.TeamContactDetailsId, cascadeDelete: true)
                .ForeignKey("dbo.Persons", t => t.PersonTeamManagerId, cascadeDelete: false)
                .Index(t => t.ClientId)
                .Index(t => t.ParentClientTeamId)
                .Index(t => t.PersonTeamManagerId)
                .Index(t => t.TeamContactDetailsId);

            CreateTable(
              "dbo.ClientTeamMembers",
              c => new
              {
                  ClientTeamId = c.Long(nullable: false),
                  PersonId = c.Long(nullable: false),
                  IsDeleted = c.Boolean(nullable: false),
                  LastModificationTime = c.DateTime(),
                  LastModifierUserId = c.Long(),
                  CreationTime = c.DateTime(nullable: false),
                  CreatorUserId = c.Long(),
              },
              annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
              .PrimaryKey(t => new { t.ClientTeamId, t.PersonId })
              .ForeignKey("dbo.ClientTeams", t => t.ClientTeamId, cascadeDelete: true)
              .ForeignKey("dbo.Persons", t => t.PersonId, cascadeDelete: true)
              .Index(t => t.ClientTeamId)
              .Index(t => t.PersonId);
            CreateTable(
                "dbo.ContactDetails",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Dummy = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContactDetailsAddresses",
                c => new
                    {
                        AddressId = c.Long(nullable: false),
                        ContactDetailsId = c.Long(nullable: false),
                        AddressKind = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => new { t.AddressId, t.ContactDetailsId })
                .ForeignKey("dbo.Addresses", t => t.AddressId, cascadeDelete: true)
                .ForeignKey("dbo.ContactDetails", t => t.ContactDetailsId, cascadeDelete: true)
                .Index(t => t.AddressId)
                .Index(t => t.ContactDetailsId);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ContactDetailsId = c.Long(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(),
                        MiddleName = c.String(),
                        NameSuffix = c.String(),
                        Title = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContactDetails", t => t.ContactDetailsId, cascadeDelete: false)
                .Index(t => t.ContactDetailsId);
            
            CreateTable(
                "dbo.DataPoints",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StructureDivisionSensorId = c.Long(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Value = c.Double(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StructureDivisionSensors", t => t.StructureDivisionSensorId, cascadeDelete: true)
                .Index(t => t.StructureDivisionSensorId);
            
            CreateTable(
                "dbo.StructureDivisionSensors",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SensorId = c.Long(nullable: false),
                        StructureDivisionId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sensors", t => t.SensorId, cascadeDelete: true)
                .ForeignKey("dbo.StructureDivisions", t => t.StructureDivisionId, cascadeDelete: true)
                .Index(t => t.SensorId)
                .Index(t => t.StructureDivisionId);
            
            CreateTable(
                "dbo.Sensors",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StructureDivisions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentStructureDivisionId = c.Long(),
                        StructureId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StructureDivisions", t => t.ParentStructureDivisionId)
                .ForeignKey("dbo.Structures", t => t.StructureId, cascadeDelete: true)
                .Index(t => t.ParentStructureDivisionId)
                .Index(t => t.StructureId);
            
            CreateTable(
                "dbo.Structures",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentStructureId = c.Long(),
                        SiteId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Structures", t => t.ParentStructureId)
                .ForeignKey("dbo.Sites", t => t.SiteId, cascadeDelete: true)
                .Index(t => t.ParentStructureId)
                .Index(t => t.SiteId);
            
            CreateTable(
                "dbo.Sites",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentSiteId = c.Long(),
                        ClientId = c.Long(nullable: false),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Sites", t => t.ParentSiteId)
                .Index(t => t.ParentSiteId)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.LocationMappings",
                c => new
                    {
                        CountryId = c.Long(nullable: false),
                        PostCodeId = c.Long(nullable: false),
                        StateId = c.Long(nullable: false),
                        SuburbId = c.Long(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                })
                .PrimaryKey(t => new { t.CountryId, t.PostCodeId, t.StateId, t.SuburbId })
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.PostCodes", t => t.PostCodeId, cascadeDelete: true)
                .ForeignKey("dbo.States", t => t.StateId, cascadeDelete: true)
                .ForeignKey("dbo.Suburbs", t => t.SuburbId, cascadeDelete: true)
                .Index(t => t.CountryId)
                .Index(t => t.PostCodeId)
                .Index(t => t.StateId)
                .Index(t => t.SuburbId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LocationMappings", "SuburbId", "dbo.Suburbs");
            DropForeignKey("dbo.LocationMappings", "StateId", "dbo.States");
            DropForeignKey("dbo.LocationMappings", "PostCodeId", "dbo.PostCodes");
            DropForeignKey("dbo.LocationMappings", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.DataPoints", "StructureDivisionSensorId", "dbo.StructureDivisionSensors");
            DropForeignKey("dbo.StructureDivisionSensors", "StructureDivisionId", "dbo.StructureDivisions");
            DropForeignKey("dbo.StructureDivisions", "StructureId", "dbo.Structures");
            DropForeignKey("dbo.Structures", "SiteId", "dbo.Sites");
            DropForeignKey("dbo.Sites", "ParentSiteId", "dbo.Sites");
            DropForeignKey("dbo.Sites", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Structures", "ParentStructureId", "dbo.Structures");
            DropForeignKey("dbo.StructureDivisions", "ParentStructureDivisionId", "dbo.StructureDivisions");
            DropForeignKey("dbo.StructureDivisionSensors", "SensorId", "dbo.Sensors");
            DropForeignKey("dbo.ClientTeamMembers", "PersonId", "dbo.Persons");
            DropForeignKey("dbo.ClientTeams", "PersonTeamManagerId", "dbo.Persons");
            DropForeignKey("dbo.Persons", "ContactDetailsId", "dbo.ContactDetails");
            DropForeignKey("dbo.ClientTeams", "TeamContactDetailsId", "dbo.ContactDetails");
            DropForeignKey("dbo.ContactDetailsAddresses", "ContactDetailsId", "dbo.ContactDetails");
            DropForeignKey("dbo.ContactDetailsAddresses", "AddressId", "dbo.Addresses");
            DropForeignKey("dbo.ClientTeams", "ParentClientTeamId", "dbo.ClientTeams");
            DropForeignKey("dbo.ClientTeamMembers", "ClientTeamId", "dbo.ClientTeams");
            DropForeignKey("dbo.ClientTeams", "ClientId", "dbo.Clients");
            DropForeignKey("dbo.Clients", "ParentClientId", "dbo.Clients");
            DropForeignKey("dbo.Addresses", "SuburbId", "dbo.Suburbs");
            DropForeignKey("dbo.Addresses", "StateId", "dbo.States");
            DropForeignKey("dbo.Addresses", "PostCodeId", "dbo.PostCodes");
            DropForeignKey("dbo.Addresses", "CountryId", "dbo.Countries");
            DropIndex("dbo.LocationMappings", new[] { "SuburbId" });
            DropIndex("dbo.LocationMappings", new[] { "StateId" });
            DropIndex("dbo.LocationMappings", new[] { "PostCodeId" });
            DropIndex("dbo.LocationMappings", new[] { "CountryId" });
            DropIndex("dbo.Sites", new[] { "ClientId" });
            DropIndex("dbo.Sites", new[] { "ParentSiteId" });
            DropIndex("dbo.Structures", new[] { "SiteId" });
            DropIndex("dbo.Structures", new[] { "ParentStructureId" });
            DropIndex("dbo.StructureDivisions", new[] { "StructureId" });
            DropIndex("dbo.StructureDivisions", new[] { "ParentStructureDivisionId" });
            DropIndex("dbo.StructureDivisionSensors", new[] { "StructureDivisionId" });
            DropIndex("dbo.StructureDivisionSensors", new[] { "SensorId" });
            DropIndex("dbo.DataPoints", new[] { "StructureDivisionSensorId" });
            DropIndex("dbo.Persons", new[] { "ContactDetailsId" });
            DropIndex("dbo.ContactDetailsAddresses", new[] { "ContactDetailsId" });
            DropIndex("dbo.ContactDetailsAddresses", new[] { "AddressId" });
            DropIndex("dbo.ClientTeams", new[] { "TeamContactDetailsId" });
            DropIndex("dbo.ClientTeams", new[] { "PersonTeamManagerId" });
            DropIndex("dbo.ClientTeams", new[] { "ParentClientTeamId" });
            DropIndex("dbo.ClientTeams", new[] { "ClientId" });
            DropIndex("dbo.ClientTeamMembers", new[] { "Id" });
            DropIndex("dbo.Clients", new[] { "ParentClientId" });
            DropIndex("dbo.Addresses", new[] { "SuburbId" });
            DropIndex("dbo.Addresses", new[] { "StateId" });
            DropIndex("dbo.Addresses", new[] { "PostCodeId" });
            DropIndex("dbo.Addresses", new[] { "CountryId" });
            DropTable("dbo.LocationMappings",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Sites",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Structures",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.StructureDivisions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Sensors",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.StructureDivisionSensors",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.DataPoints",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Persons",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.ContactDetailsAddresses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.ContactDetails",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.ClientTeams",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.ClientTeamMembers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Clients",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.ChangeLog",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Suburbs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.States",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.PostCodes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Countries",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
            DropTable("dbo.Addresses",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "Abp_SoftDelete", "True" },
                });
        }
    }
}
