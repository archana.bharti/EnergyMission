﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;

using EnergyMission.Migrations;

namespace EnergyMission
{
    [DependsOn(typeof (AbpEntityFrameworkModule), typeof (EnergyMissionCoreModule))]
    public class EnergyMissionInfrastructureEntityFrameworkModule : AbpModule
    {
        #region Public Members

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<EmDbContext>(new MigrateDatabaseToLatestVersion<EmDbContext, Configuration>());
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "DefaultConnection";
        }

        #endregion
    }
}