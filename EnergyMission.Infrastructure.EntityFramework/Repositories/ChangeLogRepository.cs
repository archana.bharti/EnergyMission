﻿using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    public class ChangeLogRepository : EmRepositoryBase<ChangeLogEntity>, IChangeLogRepository
    {
        public ChangeLogRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }
    }
}