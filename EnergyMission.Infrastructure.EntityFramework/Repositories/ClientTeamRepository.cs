﻿using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;
using System.Data.SqlClient;
using System;

namespace EnergyMission.Repositories
{
    public class ClientTeamRepository : EmRepositoryBase<ClientTeamEntity>, IClientTeamRepository
    {
        public ClientTeamRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<ClientTeamEntity,in long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<ClientTeamEntity> GetAllWithInclude()
        {
            return GetAll()
                .Include(e => e.Client.ParentClient)
                .Include(e => e.Members)
                .Include(e => e.TeamContactDetail.Addresses)
                .Include(e => e.TeamManager.ContactDetail.Addresses);
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        /// 
        public bool DeleteClientTeam(long Id)
        {
            bool flag=false;
            EmDbContext db = new EmDbContext();
            var id = Id;
   
            int value = db.Database.ExecuteSqlCommand("Delete from ClientTeamMembers where Id={0}", id);
            if (value >= 1)
                flag = true;
            return flag;
        
        
        }

        /// 
        public bool UpdateClientTeam (ClientTeamEntity t)
        {
            bool flag = false;
            EmDbContext db = new EmDbContext();

            if (t.ParentClientTeamId == null)
                t.ParentClientTeamId = 1;
            int value = db.Database.ExecuteSqlCommand("update ClientTeams  set  ClientId=@ClientId ,PersonTeamManagerId=@PersonTeamManagerId ,TeamContactDetailsId=@TeamContactDetailsId,LastModificationTime=@LastModificationTime,name =@name where id=@id ", 
                new SqlParameter("ClientId", t.ClientId),
                new SqlParameter("PersonTeamManagerId", t.PersonTeamManagerId),
                new SqlParameter("TeamContactDetailsId", t.TeamContactDetailsId),

                new SqlParameter("LastModificationTime", DateTime.Now ),
                                new SqlParameter("name", t.Name),
                                                                new SqlParameter("@id", t.Id)

                );
            if (value >= 1)
                flag = true;
            return flag;


        }
        public ClientTeamEntity GetWithInclude(long aKey)
        {
            return GetAllWithInclude()
                .First(r => r.Id == aKey);
        }

        #endregion
    }
}