﻿using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;
using System.Data.SqlClient;
using System;

namespace EnergyMission.Repositories
{
    public class StructureDivisionRepository : EmRepositoryBase<StructureDivisionEntity>, IStructureDivisionRepository
    {
        public StructureDivisionRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<StructureDivisionEntity,in long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<StructureDivisionEntity> GetAllWithInclude()
        {
            return GetAll()
                .Include(e => e.Structure)
                .Include(e => e.Structure.Site.Client);
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public StructureDivisionEntity GetWithInclude(long aKey)
        {
            return GetAllWithInclude()
                .First(r => r.Id == aKey);
        }

        public bool EditStructureDivision(StructureDivisionEntity t)
        { 
            bool flag=false;

            var SiteId = new SqlParameter("StructureId", t.StructureId);

    var name = new SqlParameter("Name", t.Name) ;
    
    var LastModificationTime = new SqlParameter("up_date", DateTime.Now); 
    //var up_By = new SqlParameter("User", "5ded88b5-6e69-4388-ba5b-e412151150c3"); 
     var id  = new SqlParameter("id", t.Id); 


      EmDbContext dbs =new EmDbContext();
       int value =dbs.Database.ExecuteSqlCommand("Update StructureDivisions set StructureId=@StructureId ,Name=@name,LastModificationTime=@up_date where id=@id",
          
           SiteId,name,LastModificationTime,id);

       if (value >= 1)
           flag = true;


       return flag;
        
        }
    

        #endregion

    }
}