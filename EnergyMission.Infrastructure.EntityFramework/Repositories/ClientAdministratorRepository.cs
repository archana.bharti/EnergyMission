﻿using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;
using EnergyMission.Domain.ClientAdministrator;
using System.Collections.Generic;
namespace EnergyMission.Repositories
{
    public class ClientAdministratorRepository : EmRepositoryBase<ClientAdministratorEntity>, IClientAdministratorRepository
    {
        public ClientAdministratorRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<ClientAdministratorEntity,in long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<ClientAdministratorEntity> GetAllWithInclude()
        {
            return GetAll()
                .Include(e => e.Client.ParentClient);
        }

        public List<ClientAdminDto> GetList()
        {
           
            EmDbContext db =new EmDbContext();
            var value = db.Database.SqlQuery<ClientAdminDto>("ClientAdmininstratorList").ToList();

         return value;
        }
        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public ClientAdministratorEntity GetWithInclude(long aKey)
        {
            return GetAllWithInclude()
                .First(r => r.Id == aKey);
        }

        #endregion
    }
}