﻿using System;
using System.Collections.Generic;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.Repositories
{
    /// <summary>
    ///     As users are actually a virtual construct made up of MS Identity and UserProfileInformation
    ///     this repository does not descend from the usual base interfaces
    /// </summary>
    public class UserRepository 
    {
        #region Implementation of IUserRepository

        public void Delete(string aId)
        {
            throw new NotImplementedException();
        }

        public List<UserDto> GetAllList()
        {
            throw new NotImplementedException();
        }

        public UserDto Get(string aId)
        {
            throw new NotImplementedException();
        }

        public void Update(UserDto aUser)
        {
            throw new NotImplementedException();
        }

        public void Insert(UserDto aUser)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}