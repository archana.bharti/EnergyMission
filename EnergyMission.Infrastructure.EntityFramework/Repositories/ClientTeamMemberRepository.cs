﻿using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    public class ClientTeamMemberRepository : EmRepositoryBase<ClientTeamMemberEntity>, IClientTeamMemberRepository
    {
        public ClientTeamMemberRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<ClientTeamMemberEntity,in long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<ClientTeamMemberEntity> GetAllWithInclude()
        {
            return GetAll()
                .Where(r => r.IsDeleted == false)
                .Include(e => e.ClientTeam)
                .Include(e => e.Person.ContactDetail.Addresses);
        }
        public bool DeleteClientTeamMember(ClientTeamMemberEntity ClientTeamMemberEntity)
        {
            bool flag = false;
            EmDbContext db = new EmDbContext();
            var Teamid = ClientTeamMemberEntity.ClientTeamId;
            var PersonId = ClientTeamMemberEntity.PersonId;

            int value = db.Database.ExecuteSqlCommand("Delete from ClientTeamMembers where ClientTeamId={0} and PersonId={1}", Teamid, PersonId);
            if (value >= 1)
                flag = true;
            return flag;


        }
        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public ClientTeamMemberEntity GetWithInclude(long aKey)
        {
            return GetAllWithInclude()
                .Where(r => r.IsDeleted == false)
                .First(r => r.Id == aKey);
        }

        #endregion
    }
}