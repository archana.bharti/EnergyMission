﻿using System.Data.Entity;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;

namespace EnergyMission.EntityFramework
{
    /// <summary>
    ///     Class representing the underlying database
    /// </summary>
    public class EmDbContext : AbpDbContext
    {
        public EmDbContext()
            : base("DefaultConnection")
        {

        }

        public virtual IDbSet<AddressEntity> Addresss { get; set; }
        public virtual IDbSet<ChangeLogEntity> ChangeLogs { get; set; }
        public virtual IDbSet<ClientAdministratorEntity> ClientAdministrators { get; set; }
        public virtual IDbSet<ClientEntity> Clients { get; set; }
        public virtual IDbSet<ClientTeamMemberEntity> ClientTeamMembers { get; set; }
        public virtual IDbSet<ClientTeamEntity> ClientTeams { get; set; }
        public virtual IDbSet<ContactDetailAddressEntity> ContactDetailAddresses { get; set; }
        public virtual IDbSet<ContactDetailEntity> ContactDetails { get; set; }
        public virtual IDbSet<CountryEntity> Countries { get; set; }
        public virtual IDbSet<DataPointEntity> DataPoints { get; set; }
        public virtual IDbSet<LocationMappingEntity> LocationMappings { get; set; }
        public virtual IDbSet<PersonEntity> Persons { get; set; }
        public virtual IDbSet<PostCodeEntity> PostCodes { get; set; }
        public virtual IDbSet<SensorEntity> Sensors { get; set; }
        public virtual IDbSet<SiteEntity> Sites { get; set; }
        public virtual IDbSet<StateEntity> States { get; set; }
        public virtual IDbSet<StructureDivisionEntity> StructureDivisions { get; set; }
        public virtual IDbSet<StructureDivisionSensorEntity> StructureDivisionSensors { get; set; }
        public virtual IDbSet<StructureEntity> Structures { get; set; }
        public virtual IDbSet<SuburbEntity> Suburbs { get; set; }
        public IDbSet<UserProfileInformationEntity> UserProfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //TODO: Make additional model setup...

            //modelBuilder.Entity<Person>().ToTable("TsPeople"); //Sample
        }
    }
}