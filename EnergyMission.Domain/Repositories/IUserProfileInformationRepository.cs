﻿using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Domain.Repositories
{
    public interface IUserProfileInformationRepository : IEmRepositoryBase<UserProfileInformationEntity, long>
    {
    }
}