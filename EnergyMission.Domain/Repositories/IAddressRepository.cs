﻿using System;
using System.Collections.Generic;

namespace EnergyMission.Domain.Repositories
{
    public interface IAddressRepository : IDisposable
    {
        IEnumerable<> GetAddresss();
        AddressEntity GetAddressID(int Id);
        void InsertStudent(AddressEntity AddressEntity);
        void DeleteStudent(int Id);
        void UpdateStudent(AddressEntity AddressEntity);
        void Save();

    }
}