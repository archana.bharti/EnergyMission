﻿using Abp.Domain.Repositories;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Domain.Repositories
{
    public interface ILocationMappingRepository : IEmRepositoryBase<LocationMappingEntity, long>
    {
        void Delete(long aCountryId, long aPostCodeId, long aStateId, long aSuburbId);
        LocationMappingEntity Get(long aCountryId, long aPostCodeId, long aStateId, long aSuburbId);
    }
}