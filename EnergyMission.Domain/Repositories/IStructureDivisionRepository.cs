﻿using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Domain.Repositories
{
    public interface IStructureDivisionRepository : IEmRepositoryBase<StructureDivisionEntity, long>
    {
        bool EditStructureDivision(StructureDivisionEntity t);
    }
}