﻿using System;
using System.Collections.Generic;
using Abp.Domain.Repositories;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.Domain.Repositories
{
    /// <summary>
    /// As users are actually a virtual construct made up of MS Identity and UserProfileInformation
    /// this repository does not descend from the usual base interfaces
    /// </summary>
    public interface IUserRepository : IRepository
    {
        void Delete(string aId);
        List<UserDto> GetAllList();
        UserDto Get(string aId);
        void Update(UserDto aUser);
        void Insert(UserDto aUser);
    }
}