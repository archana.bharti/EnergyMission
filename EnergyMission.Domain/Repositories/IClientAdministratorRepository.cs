﻿using EnergyMission.Domain.ClientAdministrator;
using System.Collections.Generic;
using System.Linq;
namespace EnergyMission.Domain.Repositories
{
    public interface IClientAdministratorRepository : 
    {
        List<ClientAdminDto> GetList();
      

    }
}