﻿using EnergyMission.Domain.Entities;

namespace EnergyMission.Domain.Repositories
{
    public interface IChangeLogRepository : IRepository<ChangeLogEntity, long>
    {
    }
}