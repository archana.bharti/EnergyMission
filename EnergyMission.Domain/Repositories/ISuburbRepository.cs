﻿using Abp.Domain.Repositories;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Domain.Repositories
{
    public interface ISuburbRepository : IRepository<SuburbEntity, long>
    {
    }
}