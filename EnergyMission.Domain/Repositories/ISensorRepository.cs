﻿using Abp.Domain.Repositories;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Domain.Repositories
{
    public interface ISensorRepository : IEmRepositoryBase<SensorEntity, long>
    {
    }
}