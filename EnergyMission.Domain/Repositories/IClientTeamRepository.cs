﻿using Abp.Domain.Repositories;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Domain.Repositories
{
    public interface IClientTeamRepository : IEmRepositoryBase<ClientTeamEntity, long>
    {
        bool DeleteClientTeam(long Id);
        bool UpdateClientTeam(ClientTeamEntity t);

    }
}
