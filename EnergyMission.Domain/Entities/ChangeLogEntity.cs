﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

using Natiki.Web.ChangeLog;

namespace EnergyMission.Domain.Entities
{
    public class ChangeLogEntity 
    {

        public long Id { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime Date { get; set; }
        public string Explain { get; set; }
        public string FogBugzReferenceNumber { get; set; }
        public int Ordering { get; set; }
        public string UserKey { get; set; }
        public string Version { get; set; }


        //#region Implementation of ISoftDelete

        ///// <summary>
        /////     Used to mark an Entity as 'Deleted'.
        ///// </summary>
        //public bool IsDeleted { get; set; }

        //#endregion

        //#region Implementation of IHasCreationTime

        ///// <summary>
        /////     Creation time of this entity.
        ///// </summary>
        //public DateTime CreationTime { get; set; }

        //#endregion

        //#region Implementation of ICreationAudited

        ///// <summary>
        /////     Creator of this entity.
        ///// </summary>
        //public long? CreatorUserId { get; set; }

        //#endregion

        //#region Implementation of IModificationAudited

        ///// <summary>
        /////     The last time of modification.
        ///// </summary>
        //public DateTime? LastModificationTime { get; set; }

        ///// <summary>
        /////     Last modifier user for this entity.
        ///// </summary>
        //public long? LastModifierUserId { get; set; }

        //#endregion

        //#region Implementation of IEntity<long>

        ///// <summary>
        /////     Checks if this entity is transient (not persisted to database and it has not an
        /////     <see cref="P:Abp.Domain.Entities.IEntity`1.Id" />).
        ///// </summary>
        ///// <returns>
        /////     True, if this entity is transient
        ///// </returns>
        //public bool IsTransient()
        //{
        //    return EqualityComparer<long>.Default.Equals(Id, default(long));
        //}

       // #endregion
    }
}