﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
  public  class VisulationSearchEntity
    {
      public long? SiteId { get; set; }
      
      public long? StructureId { get; set; }
      public long? StructureDivisionId { get; set; }
      public long? SensorId { get; set; }
      public long? TeamId { get; set; }
      public long? LocationId { get;set;}
      public Boolean Electricitys { get; set; }
      public Boolean Water { get; set; }
      public Boolean Gas { get; set; }
      public Boolean IsDay { get; set; }
      public Boolean Label { get; set; }

      public Boolean DD { get; set; }
      public Boolean MM { get; set; }
      public Boolean YY { get; set; }

      public DateTime? Start_Date { get; set; }

      public DateTime? End_Date { get; set; }
      
      public Boolean IsNight { get; set; }
      public List<Visulaization> ObjList { get; set; }
      
      public VisulationSearchEntity()
      {
          ObjList = new List<Visulaization>(); 
      }
    }
}
