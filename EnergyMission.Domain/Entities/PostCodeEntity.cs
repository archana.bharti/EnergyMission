﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace EnergyMission.Domain.Entities
{

    public class PostCodeEntity 
    {
        public PostCodeEntity()
        {
            this.Addresses = new HashSet<AddressEntity>();
            this.LocationMappings = new HashSet<LocationMappingEntity>();
            this.Clients = new HashSet<ClientEntity>();
        }
    
        public string NameConfirm { get; set; }
        [Required]
        [DisplayName("Post Name")]
        public string PostName { get; set; }
        public string postcodes { get { return PostCodeNumber + " " + PostName; } }

        public long Id { get; set; }
        [Required(ErrorMessage = "Postcode Required.")]
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(10, ErrorMessage = "Maximum of 10 characters.")]
        public string PostCodeNumber { get; set; }
        public string PostCodeNumber1 { get; set; }
              [DisplayName("State")]
        public long StateId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public virtual ICollection<AddressEntity> Addresses { get; set; }
        public virtual ICollection<LocationMappingEntity> LocationMappings { get; set; }
        public virtual ICollection<ClientEntity> Clients { get; set; }

    }
}