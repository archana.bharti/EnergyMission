﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System.Collections.Generic;
using System;

namespace EnergyMission.Domain.Entities
{

    public class SiteEntity
    {

        public SiteEntity()
        {
            this.Sites1 = new HashSet<SiteEntity>();
            this.Structures = new HashSet<StructureEntity>();
        }




        public long? Id { get; set; }
        public Nullable<long> ParentSiteId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public long ClientId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }
        public long LocationId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }

        public virtual ClientEntity Client { get; set; }

        public virtual ICollection<SiteEntity> Sites1 { get; set; }
        public virtual SiteEntity Site1 { get; set; }

        [ForeignKey("LocationId")]
        public virtual LocationMappingEntity LocationMapping { get; set; }


        public virtual ICollection<StructureEntity> Structures { get; set; }

    }
}