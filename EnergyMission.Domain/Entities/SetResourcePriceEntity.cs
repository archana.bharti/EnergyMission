﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace EnergyMission.Domain.Entities
{
    public partial class SetResourcePriceEntity
    {

        public long? Id { get; set; }

        [Required(ErrorMessage = "Required.")]
        public double Elec { get; set; }
        [Required(ErrorMessage = "Required")]
        public double ElecCO { get; set; }
        [Required(ErrorMessage = "Required")]
        public double Gas { get; set; }
        [Required(ErrorMessage = "Required")]
        public double GasCo { get; set; }
        [Required(ErrorMessage = "Required")]
        public double Water { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }

    }
}
