﻿using System;
using EnergyMission.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace EnergyMission.Domain.Entities
{
    
    public class ClientEntity
    {
        public ClientEntity()
        {
            this.ClientAdministrators = new HashSet<ClientAdministratorEntity>();
            this.ClientTeams = new HashSet<ClientTeamEntity>();
            this.Sites = new HashSet<SiteEntity>();

            this.Organizations = new HashSet<OrganizationEntity>();
        }
  
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required(ErrorMessage = "Email can't be empty")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [EmailAddress]
        public string EmailAddress { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        
        [RegularExpression ("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,15}$")]
        public string Password { get; set; }
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password doesn't match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }

        public int Status { get; set; }


        public virtual ICollection<OrganizationEntity> Organizations { get; set; }
        public virtual ICollection<ClientAdministratorEntity> ClientAdministrators { get; set; }
        public virtual ICollection<ClientTeamEntity> ClientTeams { get; set; }
        public virtual ICollection<SiteEntity> Sites { get; set; }

       
    }
}