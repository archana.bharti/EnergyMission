﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyMission.Domain.Entities
{

    public class AddressEntity 
    {
        #region Public Members
        [Key]
        public long Id { get; set; }
     
        public virtual CountryEntity Country { get; set; }

        public long CountryId { get; set; }


        [MaxLength(50, ErrorMessage = "Max length 50 characters.")]
        [Required(ErrorMessage = "Required.")]
        public string Line1 { get; set; }

        [MaxLength(50, ErrorMessage = "Max length 50 characters.")]
        public string Line2 { get; set; }

        [MaxLength(50, ErrorMessage = "Max length 50 characters.")]
        public string Line3 { get; set; }

       
        public virtual PostCodeEntity PostCode { get; set; }

        public long PostCodeId { get; set; }


        public virtual StateEntity State { get; set; }

        public long StateId { get; set; }


        public virtual SuburbEntity Suburb { get; set; }

        public long SuburbId { get; set; }

        #endregion
    }
}