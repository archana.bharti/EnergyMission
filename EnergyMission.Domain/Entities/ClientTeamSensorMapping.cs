﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
   public class ClientTeamSensorMapping
   {
       public ClientTeamSensorMapping()
       {
           SensorEntity = new List<SensorEntity>();
       }
      public List<SensorEntity> SensorEntity { get; set; }
       public long Id { get; set; }
       public long LocationId { get; set; }
       public long SiteId { get; set; }


       public long TeamId { get; set; }
       public long StructureId { get; set; }
       public long SensorId { get; set; }
       public long StructureDivisionId { get; set; }
       public string Name { get; set; }
       public bool IsDeleted { get; set; }
       public Nullable<System.DateTime> LastModificationTime { get; set; }
       public Nullable<long> LastModifierUserId { get; set; }
       public System.DateTime CreationTime { get; set; }
       public Nullable<long> CreatorUserId { get; set; }
       [Required]
       public String TeamName { get; set; }

   }
}
