﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;

namespace EnergyMission.Domain.Entities
{

    public class LocationMappingEntity
    {

        public long? Id { get; set; }
        [Required(ErrorMessage = "Location Name required.")]
        public string Locationname { get; set; }
        public long CountryId { get; set; }
        public long PostCodeId { get; set; }
        public long StateId { get; set; }
        public long SuburbId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        [ForeignKey("CountryId")]
        public virtual CountryEntity Country { get; set; }
        [ForeignKey("PostCodeId")]
        public virtual PostCodeEntity PostCode { get; set; }
        [ForeignKey("StateId")]
        public virtual StateEntity State { get; set; }
        [ForeignKey("SuburbId")]
        public virtual SuburbEntity Suburb { get; set; }
        /// <summary>
        /// Suppress unique identifier for this entity from base.
        /// </summary>
        //[NotMapped]
        //public override long Id { get; set; }


        //[ForeignKey("CountryId")]
        //public virtual CountryEntity Country { get; set; }

        //[Column(Order = 1)]
        //[Key]
        //public long CountryId { get; set; }

        //[ForeignKey("PostCodeId")]
        //public virtual PostCodeEntity PostCode { get; set; }

        //[Key]
        //[Column(Order = 2)]
        //public long PostCodeId { get; set; }

        //[ForeignKey("StateId")]
        //public virtual StateEntity State { get; set; }

        //[Key]
        //[Column(Order = 3)]
        //public long StateId { get; set; }

        //[ForeignKey("SuburbId")]
        //public virtual SuburbEntity Suburb { get; set; }

        //[Column(Order = 4)]
        //[Key]
        //public long SuburbId { get; set; }
    }
}