﻿using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace EnergyMission.Domain.Entities
{

    public class ClientAdministratorEntity 
    {
        public long Id { get; set; }
        public long? ClientId { get; set; }

        public string  confname { get; set; }
        public string personname { get; set; }
        public long PersonId { get; set; }

        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public virtual ClientEntity Client { get; set; }
        public virtual PersonEntity Person { get; set; }
    }
}