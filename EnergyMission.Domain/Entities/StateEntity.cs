﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyMission.Domain.Entities
{

    public class StateEntity 
    {
        #region Public Members

        public StateEntity()
        {
            this.Addresses = new HashSet<AddressEntity>();
            this.LocationMappings = new HashSet<LocationMappingEntity>();
            this.Clients = new HashSet<ClientEntity>();
        }


        public string NameConfirm { get; set; }
        public string DropComp { get; set; }
        [DisplayName("County")]

        public long countryId { get; set; }
        public string states { get { return AbbreviationCode + " " + Name; } set { } }
        
        public long Id { get; set; }
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        [Required(ErrorMessage = "Abbreviation Code   required.")]
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        public string AbbreviationCode { get; set; }
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        [Required(ErrorMessage = "State name required.")]
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        public string Name { get; set; }
        public string Name1 { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public virtual ICollection<AddressEntity> Addresses { get; set; }
        public virtual ICollection<LocationMappingEntity> LocationMappings { get; set; }
        public virtual ICollection<ClientEntity> Clients { get; set; }
        public virtual CountryEntity Country { get; set; }
   
    
        #endregion
    }
}