﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace EnergyMission.Domain.Entities
{
    public class SuburbEntity 
    {
        public SuburbEntity()
        {
            this.Addresses = new HashSet<AddressEntity>();
            this.LocationMappings = new HashSet<LocationMappingEntity>();
            this.LocationMappings = new HashSet<LocationMappingEntity>();
        }
        public long Id { get; set; }
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }
        public string Suburbs { get { return Name + " " + postcode.PostCodeNumber; } set { } }

        public string NameConfirm { get; set; }
        [DisplayName("Post Code")]
        public long PostcodeId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<long> StateId { get; set; }
        public virtual  PostCodeEntity  postcode { get; set; }
        public virtual ICollection<AddressEntity> Addresses { get; set; }
        public virtual ICollection<LocationMappingEntity> LocationMappings { get; set; }
  
    }
}