﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
namespace EnergyMission.Domain.Entities
{
  public  class OrganizationEntity
    {

        public long? Id { get; set; }
      
      [Required]
      [DisplayName("Organization")]
        public string Name { get; set; }
            [Required]
        public Nullable<int> NOE { get; set; }
        public Nullable<long> ClientId { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<long> StateId { get; set; }
        public Nullable<long> PostCodeId { get; set; }
        public string Geo_location_Latitude { get; set; }
        public string Geo_location_Longditude { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        [EmailAddress]
        public string SuperAdminEmail { get; set; }

        public virtual ClientEntity  Client { get; set; }
        public virtual PostCodeEntity PostCode { get; set; }
        public virtual  StateEntity State { get; set; }
    }
}
