﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Domain.Entities
{
 
    public class DataPointEntity 
    {

        public long Id { get; set; }
        public long StructureDivisionSensorId { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public double Value { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }


        public virtual StructureDivisionSensorEntity StructureDivisionSensor { get; set; }


        //#endregion
    }
}