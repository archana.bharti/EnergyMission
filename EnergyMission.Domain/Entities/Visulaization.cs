﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
 public class Visulaization
    {
        public int timespan { get; set; }
        public double value { get; set; }
        public double curval { get; set; }
        public double prvValue { get; set; }
        public double Target { get; set; }
        public string type { get; set; }
        public Double Day { get; set; }
        public Double Night { get; set; }

    }
}
