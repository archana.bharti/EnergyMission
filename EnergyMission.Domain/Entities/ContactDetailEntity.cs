﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyMission.Domain.Entities
{
    public class ContactDetailEntity 
    {
        public virtual ICollection<ContactDetailAddressEntity> Addresses { get; set; }
        [Key]
        public long Id { get; set; }


        public string Dummy { get; set; }

        [Required]
        public string EmailAddress { get; set; }
    }
}