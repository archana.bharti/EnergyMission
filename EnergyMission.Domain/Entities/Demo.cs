﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
   public class Demo
    {
        public double Percentage { get; set; }
        public double flow { get; set; }
        public Int32 Month { get; set; }
        public Int32 Year { get; set; }
        public Int32 Day { get; set; }
    }
}
