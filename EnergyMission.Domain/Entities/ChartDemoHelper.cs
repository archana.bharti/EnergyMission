﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EnergyMission.Domain.Entities
{
    public class ChartDemoHelper
    {
        public static string OptionsKey { get; set; }

        public  static SelectList GetSeriesDataMembers()
        {

            return ( new SelectList(new[]
                {
                    new {ID="Year", Name="Year"},
                    new {ID="Month", Name="Month"}
                 
                             },
              "ID", "Name", 1));

        }
    }
}