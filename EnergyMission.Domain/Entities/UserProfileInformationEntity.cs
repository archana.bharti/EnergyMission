﻿using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyMission.Domain.Entities
{
       [Table("UserProfileInformation", Schema = "dbo")]
    public class UserProfileInformationEntity  
    {
       
          [Key]

          public long? Id { get; set; }



        public Nullable<long> ClientId { get; set; }
          [ForeignKey("ClientId")]
        public virtual ClientEntity Client { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<long> PersonId { get; set; }
               [ForeignKey("PersonId")]
        public virtual PersonEntity Person { get; set; }

     
    }
}