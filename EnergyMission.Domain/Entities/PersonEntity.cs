﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Enums;
using EnergyMission.Common.Framework;
using System.Collections.Generic;
using System;

namespace EnergyMission.Domain.Entities
{

    public class PersonEntity
    {
        #region Public Members
        public PersonEntity()
        {
            this.ClientAdministrators = new HashSet<ClientAdministratorEntity>();
            this.ClientTeamMembers = new HashSet<ClientTeamMemberEntity>();
            this.ClientTeams = new HashSet<ClientTeamEntity>();
        }

        public long Id { get; set; }

        public string persons { get { return FirstName + " " + LastName; } set { } }
        [Required(ErrorMessage = "Required.")]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string EmailAddress { get; set; }
        public int? EmployeeID { get; set; }
        public string CVUrl { get; set; }
        public string NameSuffix { get; set; }
        public TitleEnum Title { get; set; }
        public StatusEnum STT { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,15}$")]
        public string Password { get; set; }
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password doesn't match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }
        public int Status { get; set; }

        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }



        public virtual ICollection<ClientAdministratorEntity> ClientAdministrators { get; set; }

        public virtual ICollection<ClientTeamMemberEntity> ClientTeamMembers { get; set; }

        public virtual ICollection<ClientTeamEntity> ClientTeams { get; set; }

        
 

        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string MiddleName { get; set; }
        //public string NameSuffix { get; set; }
      

        #endregion
    }
}