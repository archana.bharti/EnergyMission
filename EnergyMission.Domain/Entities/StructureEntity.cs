﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;

namespace EnergyMission.Domain.Entities
{

    public class StructureEntity
    {

        public StructureEntity()
        {
            this.StructureDivisions = new HashSet<StructureDivisionEntity>();
        }
        public long? Id { get; set; }

        [Required(ErrorMessage = "Required.")]
        public long SiteId { get; set; }
        public long LocationId { get; set; }
        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }

        public bool IsDeleted { get; set; }

        public Nullable<System.DateTime> LastModificationTime { get; set; }

        public Nullable<long> LastModifierUserId { get; set; }

        public System.DateTime CreationTime { get; set; }

        public Nullable<long> CreatorUserId { get; set; }

        public Nullable<long> ParentStructureId { get; set; }

        [ForeignKey("SiteId")]
        public virtual SiteEntity Site { get; set; }

        public virtual ICollection<StructureDivisionEntity> StructureDivisions { get; set; }


    }
}