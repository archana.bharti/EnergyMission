﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;

namespace EnergyMission.Domain.Entities
{

    public class SensorEntity 
    {



        public SensorEntity()
        {
            this.StructureDivisionSensors = new HashSet<StructureDivisionSensorEntity>();
        }
        public long Id { get; set; }
        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }

                [Required(ErrorMessage = "Required.")]
        public string DataFeed { get; set; }
                [Required(ErrorMessage = "Required.")]
        public string SensorType { get; set; }
        public Int64 TeamId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public virtual ICollection<StructureDivisionSensorEntity> StructureDivisionSensors { get; set; }

    }
}