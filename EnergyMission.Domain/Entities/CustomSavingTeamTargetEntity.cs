﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
    public class CustomSavingTeamTargetEntity
    {
        public long? Id { get; set; }

        [Required(ErrorMessage = "Required.")]
        public double RTElec { get; set; }
        [Required(ErrorMessage = "Required")]
        public double RTGas { get; set; }
        [Required(ErrorMessage = "Required")]
        public double RTWater { get; set; }



        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<long> ClientId { get; set; }
    }
}
