﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;

namespace EnergyMission.Domain.Entities
{

    public class CountryEntity
    {


        public CountryEntity()
        {
            this.Addresses = new HashSet<AddressEntity>();
            this.LocationMappings = new HashSet<LocationMappingEntity>();
        }


        public long Id { get; set; }
        //[MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        //[MaxLength(3, ErrorMessage = "Maximum of 3 characters.")]
        //[Required(ErrorMessage = "Required.")]
        //public string AbbreviationCode { get; set; }
        //   [Required(ErrorMessage = "Required.")]
        //public int InternationalCountryDialCode { get; set; }
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        [Required(ErrorMessage = "Country name required.")]
        public string Name { get; set; }
        public string Name1 { get; set; }
        public int? InternationalCountryDialCode { get; set; }  
         //[CompareAttribute("Name", ErrorMessage = "Country not found")]
        public string NameConfirm { get; set; }
        public string DropComp { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }

   
        public virtual ICollection<AddressEntity> Addresses { get; set; }
        public virtual ICollection<LocationMappingEntity> LocationMappings { get; set; }
        //[MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        //[MaxLength(3, ErrorMessage = "Maximum of 3 characters.")]
        //[Required(ErrorMessage = "Required.")]
        //public string AbbreviationCode { get; set; }

        //[Required(ErrorMessage = "Required.")]
        //public int InternationalCountryDialCode { get; set; }

        //[MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        //[MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        //[Required(ErrorMessage = "Required.")]
        //public string Name { get; set; }
    }
}
