﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;

namespace EnergyMission.Domain.Entities
{

    public class StructureDivisionEntity
    {
        public StructureDivisionEntity()
        {
            this.StructureDivisionSensors = new HashSet<StructureDivisionSensorEntity>();
        }

        //[ForeignKey("ParentStructureDivisionId")]
        //public virtual StructureDivisionEntity ParentStructureDivision { get; set; }
        public long? Id { get; set; }
        public long StructureId { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public long LocationId { get; set; }
        public long SiteId { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }


        public virtual StructureEntity Structure { get; set; }

        public virtual ICollection<StructureDivisionSensorEntity> StructureDivisionSensors { get; set; }

        //[ForeignKey("StructureId")]
        //public virtual StructureEntity Structure { get; set; }

        //public long StructureId { get; set; }

        //[Required(ErrorMessage = "Required.")]
        //public string Name { get; set; }


        //#endregion
    }
}