﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.Collections.Generic;

namespace EnergyMission.Domain.Entities
{
  
    public class StructureDivisionSensorEntity
    {
        public StructureDivisionSensorEntity()
        {
            this.DataPoints = new HashSet<DataPointEntity>();
        }

        [Key]
        public long Id { get; set; }
        public long SensorId { get; set; }
        public long StructureDivisionId { get; set; }
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public virtual ICollection<DataPointEntity> DataPoints { get; set; }
        [ForeignKey("SensorId")]
        public virtual SensorEntity Sensor { get; set; }
              

        public virtual StructureDivisionEntity StructureDivision { get; set; }
      
    }
}