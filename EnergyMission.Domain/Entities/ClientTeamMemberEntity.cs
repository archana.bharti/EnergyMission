﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace EnergyMission.Domain.Entities
{

    public class ClientTeamMemberEntity
    {
        public long Id { get; set; }
        public long ClientTeamId { get; set; }
        public long PersonId { get; set; }
        public bool IsDeleted { get; set; }
        public string personname { get; set; }
        public string teamname { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }

        public virtual ClientTeamEntity ClientTeam { get; set; }
        public virtual PersonEntity Person { get; set; }

        /// <summary>
        /// Suppress unique identifier for this entity from base.
        /// </summary>

    }
}