﻿using System;
using EnergyMission.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace EnergyMission.Domain.Entities
{
    public class TeamEntity
    {
        public TeamEntity()
        {
            this.ClientAdministrators = new HashSet<ClientAdministratorEntity>();
            this.ClientTeams = new HashSet<ClientTeamEntity>();
            this.Sites = new HashSet<SiteEntity>();

            this.Organizations = new HashSet<OrganizationEntity>();
        }

        public long Id { get; set; }
       
        public long ClientId { get; set; }
         [Required]
        public string Name { get; set; }
        public long PersonTeamManagerId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<long> ParentClientTeamId { get; set; }

        public int Status { get; set; }

        public virtual ICollection<OrganizationEntity> Organizations { get; set; }
        public virtual ICollection<ClientAdministratorEntity> ClientAdministrators { get; set; }
        public virtual ICollection<ClientTeamEntity> ClientTeams { get; set; }
        public virtual ICollection<SiteEntity> Sites { get; set; }
        
    }


}
