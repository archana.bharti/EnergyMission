﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
    public class ClientTeamSensorsEntity
    {


        public long Id { get; set; }
        public Nullable<long> ClientTeamSendorId { get; set; }
        public Nullable<long> ClientTeamId { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }

        public virtual ClientTeamEntity ClientTeam { get; set; }
        public virtual StructureDivisionSensorEntity StructureDivisionSensor { get; set; }
    }
}