﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyMission.Domain.Entities
{
    public class ContactDetailAddressEntity
    {
        [Key]
        [Column(Order = 1)]
        [ForeignKey("Address")]
        public long AddressId { get; set; }
       
        [ForeignKey("ContactDetailsId")]
        public virtual ContactDetailEntity ContactDetail { get; set; }
        [Key]
        [Column(Order = 2)]
        public long ContactDetailsId { get; set; }

        /// <summary>
        ///     Suppress unique identifier for this entity from base.
        /// </summary>
     

        public int AddressKind { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
                [ForeignKey("AddressId")]
        public virtual AddressEntity Address { get; set; }

    }
}