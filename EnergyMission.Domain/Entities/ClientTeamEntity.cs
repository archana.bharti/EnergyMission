﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace EnergyMission.Domain.Entities
{

    public class ClientTeamEntity 
    {

        public ClientTeamEntity()
        {
            this.ClientTeamMembers = new HashSet<ClientTeamMemberEntity>();
            this.ClientTeamsSensors = new HashSet<ClientTeamSensorsEntity>();
        }


        public long Id { get; set; }
        public long ClientId { get; set; }
        [Required(ErrorMessage = "Team Name Required..")]
        [DisplayName("Team Name")]
        public string Name { get; set; }
        public long PersonTeamManagerId { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<long> ParentClientTeamId { get; set; }

        public virtual ClientEntity Client { get; set; }
        public virtual ICollection<ClientTeamMemberEntity> ClientTeamMembers { get; set; }
        public virtual PersonEntity Person { get; set; }
        public virtual ICollection<ClientTeamSensorsEntity> ClientTeamsSensors { get; set; }
     
    }
}