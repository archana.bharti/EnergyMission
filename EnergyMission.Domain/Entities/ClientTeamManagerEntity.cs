﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
  public  class ClientTeamManagerEntity
    {

        public long Id { get; set; }
        public long? ClientId { get; set; }
        public long PersonId { get; set; }
        public long empid { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
        public System.DateTime CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }

      
        public virtual ClientEntity Client { get; set; }
        public virtual PersonEntity Person { get; set; }


    }
}
