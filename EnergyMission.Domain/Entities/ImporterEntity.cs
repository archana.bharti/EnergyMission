﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.Entities
{
    public class ImporterEntity
    {
        public long? ID { get; set; }
        public string Measurment { get; set; }
        [Required(ErrorMessage = "Required.")]
        public long LocationId { get; set; }
        [Required(ErrorMessage = "Required.")]
        public long SiteId { get; set; }
        [Required(ErrorMessage = "Required.")]
        public long StructureId { get; set; }
        public Nullable<System.DateTime> TimeStamp { get; set; }
        public Nullable<double> Value { get; set; }
        //StructureDivisionId
        [Required(ErrorMessage = "Required.")]
        public Nullable<long> StructureDivisionId { get; set; }
        [Required(ErrorMessage = "Required.")]
        public Nullable<long> StructureDivisionSensorId { get; set; }

   

    }
}
