﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Domain.ClientAdministrator
{
   public class ClientAdminDto
    {
        public long Id { get; set; }
        public long PersonId { get; set; }
        public long ClientId { get; set; }
        public string P_Name { get; set; }
        public string C_Name { get; set; }
           
    }
}
