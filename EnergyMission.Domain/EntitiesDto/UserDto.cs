﻿using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
namespace EnergyMission.Domain.EntitiesDto
{
    /// <summary>
    ///     This is not a physical entity within the database. It comprises:
    ///     - UserProfileInformationDto which is a physical table
    ///     - AspNetUsers which is a physical table managed my Microsoft Identity
    /// </summary>
    public class UserDto 
    {
        public new string Id { get; set; }
        public string UserName { get; set; }
        public UserProfileInformationEntity UserProfileInformation { get; set; }
    }
}