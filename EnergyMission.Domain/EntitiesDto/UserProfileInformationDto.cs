﻿using EnergyMission.Common.Framework;

namespace EnergyMission.Domain.EntitiesDto
{
    public class UserProfileInformationDto
    {
        #region Public Members

        public long? ClientId { get; set; }

        public bool IsClient
        {
            get { return ClientId.HasValue; }
        }

        #endregion
    }
}