namespace EnergyMission.ChangeLog
{
    public interface IChangeLogAppService
    {
        string ChangeLogTableAsHtml();
    }
}