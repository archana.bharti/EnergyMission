﻿using System.Linq;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using Natiki.Web.ChangeLog;

namespace EnergyMission.ChangeLog
{
    public class ChangeLogAppService : EmApplicationServiceBase, IChangeLogAppService
    {
        #region Private Members

        private readonly IChangeLogRepository _ChangeLogRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aChangeLogRepository">Allows access to the underlying data</param>
        public ChangeLogAppService(IChangeLogRepository aChangeLogRepository)
        {
            _ChangeLogRepository = aChangeLogRepository;
        }

        #endregion

        #region Implementation of IChangeLogAppService

        /// <summary>
        ///     Constructs a HTML table that can be used to display the change log for the application.
        /// </summary>
        /// <returns>Returns HTML that can be used to display the change log for the application.</returns>
        public string ChangeLogTableAsHtml()
        {
            IOrderedQueryable<ChangeLogEntity> changeLogEntries = _ChangeLogRepository.GetAll()
                                                                                      .OrderByDescending(r => r.Version)
                                                                                      .ThenBy(r => r.Date)
                                                                                      .ThenBy(r => r.Ordering);

            return ChangeLogHelper.BuildHtmlForBootstrap3(changeLogEntries, "https://apos.fogbugz.com/default.asp?");
        }

        #endregion
    }
}