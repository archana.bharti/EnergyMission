﻿using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using DevExpress.Web.Mvc;
using DevExpress.Web;

namespace EnergyMission
{
    public static class GridViewSettingsHelper
    {
        /// <summary>
        ///     Default settings for all grids.
        /// </summary>
        /// <param name="aSettingsName">Name to use for the settings for this grid</param>
        /// <param name="aControllerName">Name of the controller to invoke</param>
        /// <param name="aAjaxHelper"></param>
        /// <param name="aHtmlHelper"></param>
        /// <param name="aViewContext"></param>
        /// <returns>Returns the settings object to be used for the grid</returns>
        public static void GridViewSettingsDefaults(string aSettingsName, string aControllerName, AjaxHelper aAjaxHelper, HtmlHelper aHtmlHelper, ViewContext aViewContext, GridViewSettings aCurrentSettings)
        {


            aCurrentSettings.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
            aCurrentSettings.KeyFieldName = "Id";
            //aCurrentSettings.ClientSideEvents.SelectionChanged = "function(s,e){s.GetSelectedFieldValues('Id');}";
            aCurrentSettings.ClientSideEvents.BeginCallback = "function(s,e){if (selectedKeys != ''){e.customArgs['selectedKeys'] = selectedKeys;}}";
            aCurrentSettings.Name = aSettingsName;
          
            aCurrentSettings.CallbackRouteValues = new
            {
                Controller = aControllerName,
                Action = "GridViewPartial" //New link when the grid is empty
            };
            aCurrentSettings.Width = Unit.Percentage(100);

            //No command column as we create our own
            aCurrentSettings.CommandColumn.Visible = false;

            ////New, Edit, Delete column
            //aCurrentSettings.Columns.Add(column =>
            //{
            //    //column.Caption = "#";
            //    column.SetDataItemTemplateContent(c => aViewContext.Writer.Write(
                   
                  
            //        aAjaxHelper.ActionLink("Delete", "Delete", aControllerName,
            //            new
            //            {
            //                Id = DataBinder.Eval(c.DataItem, "Id"),
            //                @style = "color:#eb4d4e"
            //            },
            //            new AjaxOptions
            //            {
            //                Confirm = "Delete this record?",
            //                HttpMethod = "DELETE",
            //                OnSuccess = "GridViewDeleteSuccess"
            //            }) + "&nbsp;"
            //        ));
            //    //column.SetHeaderTemplateContent(c => aViewContext.Writer.Write(
            //    //    aHtmlHelper.ActionLink("New", "New", new {})
            //    //               .ToHtmlString()
            //    //    ));
            //    column.Settings.AllowDragDrop = DefaultBoolean.False;
            //    column.Settings.AllowSort = DefaultBoolean.False;
            //    column.Width = 70;
            //});

            //Set the key
            aCurrentSettings.KeyFieldName = "Id";

            aCurrentSettings.SettingsPager.Visible = false;
            aCurrentSettings.Settings.ShowGroupPanel = false;
            aCurrentSettings.Settings.ShowFilterRow = true;
            aCurrentSettings.SettingsBehavior.AllowSelectByRowClick = true;

            //Columns
            //Set filtering to contains by default
            //foreach (xxxMVCxGridViewColumn column in aCurrentSettings.Columns)
            //{
            //    if (column is MVCxGridViewColumn GridViewDataColumn)
            //    {
            //        ((GridViewDataColumn)column).Settings.AutoFilterCondition = AutoFilterCondition.Contains;
            //    }
            //}
        }

        /// <summary>
        ///     Default settings for all grids.
        /// </summary>
        /// <param name="aSettingsName">Name to use for the settings for this grid</param>
        /// <param name="aControllerName">Name of the controller to invoke</param>
        /// <param name="aAjaxHelper"></param>
        /// <param name="aHtmlHelper"></param>
        /// <param name="aViewContext"></param>
        /// <returns>Returns the settings object to be used for the grid</returns>
        /// <remarks>
        ///     The following is the original BROKEN implementation. It is broken because we should pass the settings object in as
        ///     otherwise things like filtering don't work etc.
        ///     Some views are still using this as they do complicated things. We need to get all usages of this removed
        /// </remarks>
        public static GridViewSettings GridViewSettingsDefaultsOldAndBroken(string aSettingsName, string aControllerName, AjaxHelper aAjaxHelper, HtmlHelper aHtmlHelper, ViewContext aViewContext)
        {
            GridViewSettings result = new GridViewSettings
            {
                Name = aSettingsName,
                CallbackRouteValues = new
                {
                    Controller = aControllerName,
                    Action = "GridViewNew" //New link when the grid is empty
                },
                Width = Unit.Percentage(100)
            };

            //No command column as we create our own
            result.CommandColumn.Visible = false;

            //New, Edit, Delete column
            result.Columns.Add(column =>
            {
                column.Caption = "#";
                column.SetDataItemTemplateContent(c => aViewContext.Writer.Write(
                    aHtmlHelper.ActionLink("Edit", "Edit",
                        new
                        {
                            Id = DataBinder.Eval(c.DataItem, "Id")
                        }) + "&nbsp;" +
                    aAjaxHelper.ActionLink("Delete", "Delete", aControllerName,
                        new
                        {
                            Id = DataBinder.Eval(c.DataItem, "Id")
                        },
                        new AjaxOptions
                        {
                            Confirm = "Delete this record?",
                            HttpMethod = "DELETE",
                            OnSuccess = "GridViewDeleteSuccess"
                        }) + "&nbsp;"
                    ));
                column.SetHeaderTemplateContent(c => aViewContext.Writer.Write(
                    aHtmlHelper.ActionLink("New", "New", new {})
                               .ToHtmlString()
                    ));
                column.Settings.AllowDragDrop = DefaultBoolean.False;
                column.Settings.AllowSort = DefaultBoolean.False;
                column.Width = 70;
            });

            result.KeyFieldName = "Id";
            result.SettingsPager.Mode = GridViewPagerMode.ShowAllRecords;
         
            result.SettingsPager.Visible =false;
            result.Settings.ShowGroupPanel = false;
            result.Settings.ShowFilterRow = true;
            result.SettingsBehavior.AllowSelectByRowClick = true;
      
            return result;
        }
    }
}