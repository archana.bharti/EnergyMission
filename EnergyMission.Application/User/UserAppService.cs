﻿using System;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Common.Security;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Data.IRepostitories;

using EnergyMission.User.Dtos;

namespace EnergyMission.User
{
    public class UserAppService : EmApplicationServiceBase, IUserAppService
    {
        private readonly IUserRepository _UserRepository;

        private bool ValidateNewUser(InsertUserInput aNewUser, out string aErrorMessage)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aUserRepository">Repository to access the underlying data layer</param>
        public UserAppService(IUserRepository aUserRepository)
        {
            _UserRepository = aUserRepository;
        }

        #region IUserAppService Members

        public event UserGetEventHandler OnUserGet;
        public event UsersGetEventHandler OnUsersGet;

        #endregion

        /// <summary>
        ///     Helper method to fire event of the same name
        /// </summary>
        /// <param name="aArgs"></param>
        protected virtual void DoOnUserGet(UserGetEventArgs aArgs)
        {
            UserGetEventHandler handler = OnUserGet;
            if (handler != null)
            {
                handler(this, aArgs);
            }
        }

        /// <summary>
        ///     Helper method to fire event of the same name
        /// </summary>
        /// <param name="aArgs"></param>
        protected virtual void DoOnUsersGet(UsersGetEventArgs aArgs)
        {
            UsersGetEventHandler handler = OnUsersGet;
            if (handler != null)
            {
                handler(this, aArgs);
            }
        }

        #region Implementation of IUserAppService

        /// <summary>
        ///     Delete the specified user
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteUser(DeleteUserInput aInput)
        {
            _UserRepository.Delete(aInput.Id);
        }

        public GetUserOutput GetUserWithInclude(GetUserInput aGetUserInput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Helper method to convert an UserIdentityInfoDto to an UserDto. Did not use
        ///     AutoMapper as it was overkill in this regard.
        /// </summary>
        /// <param name="aUserIdentityInfo">Information from MS Identity framework to transfer into the user instance.</param>
        /// <param name="aUser">User instance to populate</param>
        private void UserIdentiyInfoToUserDto(UserIdentityInfoDto aUserIdentityInfo, UserDto aUser)
        {
            aUser.UserName = aUserIdentityInfo.UserName;
            aUser.UserProfileInformation = aUserIdentityInfo.UserProfileInformation;
        }

        /// <summary>
        ///     Retrieves the users within the system
        /// </summary>
        /// <param name="aInput">Filter of which users to retrieve.</param>
        /// <returns>Returns the users within the system.</returns>
        public GetUsersOutput GetUsers(GetUsersInput aInput)
        {
            GetUsersOutput result = new GetUsersOutput();

            //Use callback to get the MS OWin Identity Information
            UsersGetEventArgs eventArgs = new UsersGetEventArgs();
            DoOnUsersGet(eventArgs);

            string dummyId = "1";
            foreach (UserIdentityInfoDto userIdentityInfo in eventArgs.UserIdentiyInfos)
            {
                UserDto user = new UserDto
                {
                    Id = dummyId,
                };
                UserIdentiyInfoToUserDto(userIdentityInfo, user);
                result.Users.Add(user);
                //dummyId++;
            }

            return result;
        }

        public GetUsersOutput GetUsersWithInclude(GetUsersInput aInput)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Retrieves a user based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what user to retrieve.</param>
        /// <returns>Returns the user if found, null otherwise.</returns>
        public GetUserOutput GetUser(GetUserInput aInput)
        {
            GetUserOutput result = new GetUserOutput();

            //Use callback to get the MS OWin Identity Information
            UserGetEventArgs eventArgs = new UserGetEventArgs {Id = aInput.Id};
            DoOnUserGet(eventArgs);

            //Populate what we are returning
            UserIdentiyInfoToUserDto(eventArgs.UserIdentityInfo, result.User);

            return result;
        }

        /// <summary>
        ///     Initialises a new user for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new user</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        public GetNewUserOutput GetNewUser(GetNewUserInput aInput)
        {
            return new GetNewUserOutput
            {
                SystemRole = SystemRoleKindEnum.None
            };
        }

        /// <summary>
        ///     Insert the new user
        /// </summary>
        /// <param name="aInput">User information to input.</param>
        /// <param name="aErrorMessage"></param>
        public void InsertUser(InsertUserInput aInput, string aErrorMessage)
        {
            if (!ValidateNewUser(aInput, out aErrorMessage))
            {
                throw new Exception(aErrorMessage);
            }

            //Raise the event to add the user via the identity subsystem

            UserDto user = new UserDto
            {
                //AbbreviationCode = aInput.AbbreviationCode,
            };
            throw new NotImplementedException();

            _UserRepository.Insert(user);
        }

        /// <summary>
        ///     Updates the specified users
        /// </summary>
        /// <param name="aInput">Information to update the user with</param>
        public void UpdateUser(UpdateUserInput aInput)
        {
            UserDto user = _UserRepository.Get(aInput.Id);
            Mapper.Map(aInput, user);
            _UserRepository.Update(user);
        }

        #endregion
    }
}