﻿using System;
using System.Collections.Generic;
using EnergyMission.Domain.Entities;
using EnergyMission.User.Dtos;

namespace EnergyMission.User
{
    public delegate void UserGetEventHandler(object aSender, UserGetEventArgs aArgs);

    public delegate void UsersGetEventHandler(object aSender, UsersGetEventArgs aArgs);

    /// <summary>
    ///     We need the application itself to interact with MS Owin Identity
    ///     so we use this DTO to transfer information from Identity into the application
    ///     service.
    /// </summary>
    public class UserIdentityInfoDto
    {
        public string UserName { get; set; }
        public UserProfileInformationEntity UserProfileInformation { get; set; }
    }

    /// <summary>
    ///     Arguments for event of the same name
    /// </summary>
    public class UserGetEventArgs : EventArgs
    {
        private readonly UserIdentityInfoDto _UserIdentityInfo;

        public UserGetEventArgs()
        {
            _UserIdentityInfo = new UserIdentityInfoDto();
        }

        public string Id { get; set; }

        public UserIdentityInfoDto UserIdentityInfo
        {
            get { return _UserIdentityInfo; }
        }
    }

    /// <summary>
    ///     Arguments for event of the same name
    /// </summary>
    public class UsersGetEventArgs : EventArgs
    {
        private readonly List<UserIdentityInfoDto> _UserIdentityInfos;

        public UsersGetEventArgs()
        {
            _UserIdentityInfos = new List<UserIdentityInfoDto>();
        }

        public List<UserIdentityInfoDto> UserIdentiyInfos
        {
            get { return _UserIdentityInfos; }
        }
    }

    public interface IUserAppService
    {
        event UserGetEventHandler OnUserGet;
        event UsersGetEventHandler OnUsersGet;
        void DeleteUser(DeleteUserInput aInput);
        GetNewUserOutput GetNewUser(GetNewUserInput aInput);
        GetUserOutput GetUser(GetUserInput aInput);
        GetUsersOutput GetUsers(GetUsersInput aInput);
        GetUsersOutput GetUsersWithInclude(GetUsersInput aInput);
        GetUserOutput GetUserWithInclude(GetUserInput aGetUserInput);
        void InsertUser(InsertUserInput aInput, string aErrorMessage);
        void UpdateUser(UpdateUserInput aInput);
    }
}