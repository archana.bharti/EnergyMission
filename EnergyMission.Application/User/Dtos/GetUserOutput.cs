﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.User.Dtos
{
    public class GetUserOutput : IOutputDto
    {
        public UserDto User { get; set; }
    }
}