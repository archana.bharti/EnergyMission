﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.User.Dtos
{
    public class GetUsersOutput : IOutputDto
    {
        private List<UserDto> _Users;

        #region Public Members

        public List<UserDto> Users { get { return _Users; } }

        #endregion

        public GetUsersOutput()
        {
            _Users = new List<UserDto>();
        }
    }
}