﻿using Abp.Application.Services.Dto;

namespace EnergyMission.User.Dtos
{
    public class UpdateUserInput : IInputDto
    {
        //Identifier into AspNetUsers
        public string Id { get; set; }
    }
}