﻿using Abp.Application.Services.Dto;
using EnergyMission.Common.Security;

namespace EnergyMission.User.Dtos
{
    public class GetNewUserInput : IInputDto
    {
        public long ClientId { get; set; }
        public SystemRoleKindEnum SystemRole { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }
}