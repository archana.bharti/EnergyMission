﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.User.Dtos
{
    public class GetUserInput : IInputDto
    {
        #region Public Members

        [Required]
        public string Id { get; set; }

        #endregion
    }
}