﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Site.Dtos
{
    public class GetNewSiteOutput : SiteEntity, IOutputDto
    {
    }
}