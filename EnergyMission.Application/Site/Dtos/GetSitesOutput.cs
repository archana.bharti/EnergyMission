﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Site.Dtos
{
    public class GetSitesOutput : IOutputDto
    {
        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        public GetSitesOutput()
        {
            Sites = new List<SiteEntity>();
        }

        public List<SiteEntity> Sites { get; set; }

        #endregion
    }
}