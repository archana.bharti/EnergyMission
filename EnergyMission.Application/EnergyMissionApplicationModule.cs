﻿using System.Reflection;
using Abp.Modules;
using Abp.WebApi.Controllers.Dynamic.Builders;
using AutoMapper;

using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission
{
    [DependsOn(typeof (EnergyMissionCoreModule))]
    public class EnergyMissionApplicationModule : AbpModule
    {
        #region Private Members

        /// <summary>
        ///     Registers all the various mappings between the various objects in use
        /// </summary>
        private void AutoMapperRegistrations()
        {
            //-- Client -------------------------------------------------------------
            //CRUD Inputs --> ClientTableModel
            //Mapper.CreateMap<InsertClientInput, ClientEntity>();
            //Mapper.CreateMap<UpdateClientInput, ClientEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetClientOutput, UpdateClientInput>();
            //Mapper.CreateMap<GetNewClientOutput, InsertClientInput>();
            //Mapper.CreateMap<ClientEntity, GetClientOutput>();
            ////-- /Client ------------------------------------------------------------

            ////-- ClientAdministrators ------------------------------------------------------
            ////CRUD Inputs --> ClientAdministratorTableModel
            //Mapper.CreateMap<InsertClientAdministratorInput, ClientAdministratorEntity>();
            //Mapper.CreateMap<UpdateClientAdministratorInput, ClientAdministratorEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetClientAdministratorOutput, UpdateClientAdministratorInput>();
            //Mapper.CreateMap<GetNewClientAdministratorOutput, InsertClientAdministratorInput>();
            //Mapper.CreateMap<ClientAdministratorDto, UpdateClientAdministratorInput>();
            //Mapper.CreateMap<ClientAdministratorDto, InsertClientAdministratorInput>();
            //Mapper.CreateMap<ClientAdministratorDto, GetClientAdministratorOutput>();
            //Mapper.CreateMap<ClientAdministratorEntity, GetClientAdministratorOutput>();
            //Mapper.CreateMap<ClientAdministratorEntity, ClientAdministratorDto>();
            ////-- /ClientAdministrators -----------------------------------------------------

            ////-- ClientTeams ------------------------------------------------------
            ////CRUD Inputs --> ClientTeamTableModel
            //Mapper.CreateMap<InsertClientTeamInput, ClientTeamEntity>();
            //Mapper.CreateMap<UpdateClientTeamInput, ClientTeamEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetClientTeamOutput, UpdateClientTeamInput>();
            //Mapper.CreateMap<GetNewClientTeamOutput, InsertClientTeamInput>();
            //Mapper.CreateMap<ClientTeamDto, UpdateClientTeamInput>();
            //Mapper.CreateMap<ClientTeamDto, InsertClientTeamInput>();
            //Mapper.CreateMap<ClientTeamDto, GetClientTeamOutput>();
            //Mapper.CreateMap<ClientTeamEntity, GetClientTeamOutput>();
            //Mapper.CreateMap<ClientTeamEntity, ClientTeamDto>();
            ////-- /ClientTeams -----------------------------------------------------

            ////-- ClientTeamMembers ------------------------------------------------------
            ////CRUD Inputs --> ClientTeamMemberTableModel
            //Mapper.CreateMap<InsertClientTeamMemberInput, ClientTeamMemberEntity>();
            //Mapper.CreateMap<UpdateClientTeamMemberInput, ClientTeamMemberEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetClientTeamMemberOutput, UpdateClientTeamMemberInput>();
            //Mapper.CreateMap<GetNewClientTeamMemberOutput, InsertClientTeamMemberInput>();
            //Mapper.CreateMap<ClientTeamMemberDto, UpdateClientTeamMemberInput>();
            //Mapper.CreateMap<ClientTeamMemberDto, InsertClientTeamMemberInput>();
            //Mapper.CreateMap<ClientTeamMemberDto, GetClientTeamMemberOutput>();
            //Mapper.CreateMap<ClientTeamMemberEntity, GetClientTeamMemberOutput>();
            ////-- /ClientTeamMembers -----------------------------------------------------

            ////-- Countries -------------------------------------------------------------
            ////CRUD Inputs --> CountryTableModel
            //Mapper.CreateMap<InsertCountryInput, CountryEntity>();
            //Mapper.CreateMap<UpdateCountryInput, CountryEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetCountryOutput, UpdateCountryInput>();
            //Mapper.CreateMap<GetNewCountryOutput, InsertCountryInput>();
            //Mapper.CreateMap<CountryEntity, GetCountryOutput>();
            ////-- /Countries ------------------------------------------------------------

            ////-- DataPoints ----------------------------------------------------------------
            ////CRUD Inputs --> DataPointTableModel
            //Mapper.CreateMap<InsertDataPointInput, DataPointEntity>();
            //Mapper.CreateMap<UpdateDataPointInput, DataPointEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetDataPointOutput, UpdateDataPointInput>();
            //Mapper.CreateMap<GetNewDataPointOutput, InsertDataPointInput>();
            //Mapper.CreateMap<DataPointDto, UpdateDataPointInput>();
            //Mapper.CreateMap<DataPointDto, InsertDataPointInput>();
            //Mapper.CreateMap<DataPointDto, GetDataPointOutput>();
            //Mapper.CreateMap<DataPointEntity, GetDataPointOutput>();
            ////-- /DataPoints ---------------------------------------------------------------

            ////-- LocationMappings ------------------------------------------------------
            ////CRUD Inputs --> LocationMappingTableModel
            //Mapper.CreateMap<InsertLocationMappingInput, LocationMappingEntity>();
            //Mapper.CreateMap<UpdateLocationMappingInput, LocationMappingEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetLocationMappingOutput, UpdateLocationMappingInput>();
            //Mapper.CreateMap<GetNewLocationMappingOutput, InsertLocationMappingInput>();
            //Mapper.CreateMap<LocationMappingDto, UpdateLocationMappingInput>();
            //Mapper.CreateMap<LocationMappingDto, InsertLocationMappingInput>();
            //Mapper.CreateMap<LocationMappingDto, GetLocationMappingOutput>();
            //Mapper.CreateMap<LocationMappingEntity, GetLocationMappingOutput>();
            ////-- /LocationMappings -----------------------------------------------------

            ////-- Persons ----------------------------------------------------------------
            ////CRUD Inputs --> PersonTableModel
            //Mapper.CreateMap<InsertPersonInput, PersonEntity>();
            //Mapper.CreateMap<UpdatePersonInput, PersonEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetPersonOutput, UpdatePersonInput>();
            //Mapper.CreateMap<GetNewPersonOutput, InsertPersonInput>();
            //Mapper.CreateMap<PersonDto, UpdatePersonInput>();
            //Mapper.CreateMap<PersonDto, InsertPersonInput>();
            //Mapper.CreateMap<PersonDto, GetPersonOutput>();
            //Mapper.CreateMap<PersonEntity, GetPersonOutput>();
            ////-- /Persons ---------------------------------------------------------------

            ////-- PostCodes -------------------------------------------------------------
            ////CRUD Inputs --> PostCodeTableModel
            //Mapper.CreateMap<InsertPostCodeInput, PostCodeEntity>();
            //Mapper.CreateMap<UpdatePostCodeInput, PostCodeEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetPostCodeOutput, UpdatePostCodeInput>();
            //Mapper.CreateMap<GetNewPostCodeOutput, InsertPostCodeInput>();
            //Mapper.CreateMap<PostCodeEntity, GetPostCodeOutput>();
            ////-- /PostCodes ------------------------------------------------------------

            //-- Sensor -------------------------------------------------------------
            //CRUD Inputs --> SensorTableModel
            //Mapper.CreateMap<InsertSensorInput, SensorEntity>();
            //Mapper.CreateMap<UpdateSensorInput, SensorEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetSensorOutput, UpdateSensorInput>();
            //Mapper.CreateMap<GetNewSensorOutput, InsertSensorInput>();
            //Mapper.CreateMap<SensorEntity, GetSensorOutput>();
            ////-- /Sensor ------------------------------------------------------------

            ////-- Sites ----------------------------------------------------------------
            ////CRUD Inputs --> SiteTableModel
            //Mapper.CreateMap<InsertSiteInput, SiteEntity>();
            //Mapper.CreateMap<UpdateSiteInput, SiteEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetSiteOutput, UpdateSiteInput>();
            //Mapper.CreateMap<GetNewSiteOutput, InsertSiteInput>();
            //Mapper.CreateMap<SiteDto, UpdateSiteInput>();
            //Mapper.CreateMap<SiteDto, InsertSiteInput>();
            //Mapper.CreateMap<SiteDto, GetSiteOutput>();
            //Mapper.CreateMap<SiteEntity, GetSiteOutput>();
            ////-- /Sites ---------------------------------------------------------------

            ////-- States ----------------------------------------------------------------
            ////CRUD Inputs --> StateTableModel
            //Mapper.CreateMap<InsertStateInput, StateEntity>();
            //Mapper.CreateMap<UpdateStateInput, StateEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetStateOutput, UpdateStateInput>();
            //Mapper.CreateMap<GetNewStateOutput, InsertStateInput>();
            //Mapper.CreateMap<StateEntity, GetStateOutput>();
            ////-- /States ---------------------------------------------------------------

            ////-- Structures -------------------------------------------------------------
            ////CRUD Inputs --> StructureTableModel
            //Mapper.CreateMap<InsertStructureInput, StructureEntity>();
            //Mapper.CreateMap<UpdateStructureInput, StructureEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<StructureDto, UpdateStructureInput>();
            //Mapper.CreateMap<StructureDto, InsertStructureInput>();
            //Mapper.CreateMap<StructureEntity, GetStructureOutput>();
            ////-- /Structures ------------------------------------------------------------

            ////-- StructureDivisions ----------------------------------------------------------------
            ////CRUD Inputs --> StructureDivisionTableModel
            //Mapper.CreateMap<InsertStructureDivisionInput, StructureDivisionEntity>();
            //Mapper.CreateMap<UpdateStructureDivisionInput, StructureDivisionEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetStructureDivisionOutput, UpdateStructureDivisionInput>();
            //Mapper.CreateMap<GetNewStructureDivisionOutput, InsertStructureDivisionInput>();
            //Mapper.CreateMap<StructureDivisionDto, UpdateStructureDivisionInput>();
            //Mapper.CreateMap<StructureDivisionDto, InsertStructureDivisionInput>();
            //Mapper.CreateMap<StructureDivisionDto, GetStructureDivisionOutput>();
            //Mapper.CreateMap<StructureDivisionEntity, GetStructureDivisionOutput>();
            ////-- /StructureDivisions ---------------------------------------------------------------

            ////-- StructureDivisionSensors ----------------------------------------------------------------
            ////CRUD Inputs --> StructureDivisionSensorTableModel
            //Mapper.CreateMap<InsertStructureDivisionSensorInput, StructureDivisionSensorEntity>();
            //Mapper.CreateMap<UpdateStructureDivisionSensorInput, StructureDivisionSensorEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetStructureDivisionSensorOutput, UpdateStructureDivisionSensorInput>();
            //Mapper.CreateMap<GetNewStructureDivisionSensorOutput, InsertStructureDivisionSensorInput>();
            //Mapper.CreateMap<StructureDivisionSensorDto, UpdateStructureDivisionSensorInput>();
            //Mapper.CreateMap<StructureDivisionSensorDto, InsertStructureDivisionSensorInput>();
            //Mapper.CreateMap<StructureDivisionSensorDto, GetStructureDivisionSensorOutput>();
            //Mapper.CreateMap<StructureDivisionSensorEntity, GetStructureDivisionSensorOutput>();
            ////-- /StructureDivisionSensors ---------------------------------------------------------------

            ////-- Suburbs ----------------------------------------------------------------
            ////CRUD Inputs --> SuburbTableModel
            //Mapper.CreateMap<InsertSuburbInput, SuburbEntity>();
            //Mapper.CreateMap<UpdateSuburbInput, SuburbEntity>();

            ////Misc other mappings
            //Mapper.CreateMap<GetSuburbOutput, UpdateSuburbInput>();
            //Mapper.CreateMap<GetNewSuburbOutput, InsertSuburbInput>();
            //Mapper.CreateMap<SuburbEntity, GetSuburbOutput>();
            //-- /Suburbs ---------------------------------------------------------------
        }

        /// <summary>
        ///     Will expose the listed application service classes to WebAPI as
        ///     /api/services/.....
        /// </summary>
        //private void ExposeApplicationServicesToWebApi()
        //{
        //    //"/api/services/application/list"
        //    DynamicApiControllerBuilder.For<IListAppService>("application/list")
        //                               .Build();
        //}

        #endregion

        #region Public Members

        public override void Initialize()
        {
            //IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            //Register the mappings
            AutoMapperRegistrations();

            //Expose application services via WebApi
            //ExposeApplicationServicesToWebApi();
        }

        #endregion
    }
}