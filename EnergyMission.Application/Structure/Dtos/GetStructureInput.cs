﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Structure.Dtos
{
    public class GetStructureInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}