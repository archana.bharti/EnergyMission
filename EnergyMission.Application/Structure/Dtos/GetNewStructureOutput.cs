﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Structure.Dtos
{
    public class GetNewStructureOutput : StructureEntity, IOutputDto
    {
    }
}