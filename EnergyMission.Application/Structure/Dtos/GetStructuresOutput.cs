﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Structure.Dtos
{
    public class GetStructuresOutput : IOutputDto
    {
        public List<StructureEntity> Structures { get; set; }
    }
}