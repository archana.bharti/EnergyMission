﻿using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using Abp.Dependency;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Site;
using EnergyMission.Data;
using EnergyMission.Data.IRepostitories;
using EnergyMission.Structure.Dtos;
using EnergyMission.StructureDivision;
using EnergyMission.StructureDivision.Dtos;

namespace EnergyMission.Structure
{
    public class StructureAppService :  IStructureAppService
    {



        private readonly IRepository<Structure> _StructureRepository;

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStructureRepository">Repository to access the underlying data layer</param>
        //public StructureAppService(IStructureRepository aStructureRepository)
        //{
        //    _StructureRepository = aStructureRepository;
        //}

        #region Implementation of IStructureAppService

        /// <summary>
        ///     Delete the specified structure
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteStructure(DeleteStructureInput aInput)
        {
            IStructureDivisionAppService structureDivisionAppService = IocManager.Instance.Resolve<IStructureDivisionAppService>();
            
            IEnumerable<StructureDivisionDto> structureDivisions = structureDivisionAppService.GetStructureDivisions(new GetStructureDivisionsInput()).StructureDivisions.Where(r=>r.StructureId == aInput.Id);
            foreach (StructureDivisionDto structureDivision in structureDivisions)
            {
                structureDivisionAppService.DeleteStructureDivision(new DeleteStructureDivisionInput
                {
                    Id = structureDivision.Id.Value
                });
            }

            _StructureRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Initialises a new structure for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new structure</param>
        /// <returns>Returns a view modl for use in the display tier.</returns>
        public GetNewStructureOutput GetNewStructure(GetNewStructureInput aInput)
        {
            return new GetNewStructureOutput();
        }

        /// <summary>
        ///     Retrieves a structure based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what structure to retrieve.</param>
        /// <returns>Returns the structure if found, null otherwise.</returns>
        public GetStructureOutput GetStructure(GetStructureInput aInput)
        {
            StructureEntity structure = _StructureRepository.Get(aInput.Id);
            return structure == null ? null : Mapper.Map<GetStructureOutput>(structure);
        }

        /// <summary>
        ///     Retrieves the structures within the system
        /// </summary>
        /// <param name="aInput">Filter of which structures to retrieve.</param>
        /// <returns>Returns the structures within the system.</returns>
        public GetStructuresOutput GetStructures(GetStructuresInput aInput)
        {
            List<Structure> structures = _StructureRepository.SelectAll();

            return new GetStructuresOutput {Structures = Mapper.Map<List<StructureEntity>>(structures)};
        }

        /// <summary>
        ///     Gets all the structures on a particular site
        /// </summary>
        /// <param name="aSiteId">Site to retrieve the structures for</param>
        /// <returns>Return list of structures for thge </returns>
        public List<StructureEntity> GetStructuresForSite(long aSiteId)
        {
            List<Structure> structures = _StructureRepository.GetAll()
                                                                   .Where(r => r.SiteId == aSiteId)
                                                                   .ToList();

            return Mapper.Map<List<StructureDto>>(structures);
        }

        /// <summary>
        ///     Gets all the structures associated with the client
        /// </summary>
        /// <param name="aClientId">Client identifier</param>
        /// <returns></returns>
        public List<StructureEntity> GetStructuresForClient(long aClientId)
        {
            List<StructureEntity> result = new List<StructureEntity>();

            ISiteAppService siteAppService = IocManager.Instance.Resolve<ISiteAppService>();
            List<SiteEntity> sites = siteAppService.GetSitesForClient(aClientId);
            foreach (SiteEntity site in sites)
            {
                List<StructureEntity> siteStructures = GetStructuresForSite(site.Id);
                foreach (StructureEntity siteStructure in siteStructures)
                {
                    result.Add(siteStructure);
                }
            }

            return result;
        }

        /// <summary>
        ///     Insert the new structure
        /// </summary>
        /// <param name="aInput">Structure information to input.</param>
        public void InsertStructure(InsertStructureInput aInput)
        {
            StructureEntity structure = new StructureEntity
            {
                Name = aInput.Name,
                SiteId = aInput.SiteId
            };

            _StructureRepository.Insert(structure);
        }

        /// <summary>
        ///     Updates the specified structures
        /// </summary>
        /// <param name="aInput">Information to update the structure with</param>
        public void UpdateStructure(UpdateStructureInput aInput)
        {
            StructureEntity structure = _StructureRepository.Get(aInput.Id);
            ////** This may fail because of object context 
            structure.Name = aInput.Name;
            structure.SiteId = aInput.SiteId;

            //Mapper.Map(aInput, structure);
            _StructureRepository.UpdateStructure(structure);
        }

        #endregion
    }
}