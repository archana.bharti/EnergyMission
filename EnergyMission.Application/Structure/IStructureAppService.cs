﻿using System.Collections.Generic;
using EnergyMission.Domain.Entities;
using EnergyMission.Structure.Dtos;

namespace EnergyMission.Structure
{
    public interface IStructureAppService
    {
        void DeleteStructure(DeleteStructureInput aInput);
        GetNewStructureOutput GetNewStructure(GetNewStructureInput aInput);
        GetStructureOutput GetStructure(GetStructureInput aInput);
        GetStructuresOutput GetStructures(GetStructuresInput aInput);
        List<StructureEntity> GetStructuresForClient(long aClientId);
        List<StructureEntity> GetStructuresForSite(long aSiteId);
        void InsertStructure(InsertStructureInput aInput);
        void UpdateStructure(UpdateStructureInput aInput);
    }
}