﻿using Abp.Application.Services.Dto;

namespace EnergyMission.DataPoint.Dtos
{
    public class GetNewDataPointInput : IInputDto
    {
        #region Public Members

        /// <summary>
        ///   Id is not required for inserts
        /// </summary>
        public long? Id { get; set; }

        #endregion
    }
}