﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.DataPoint.Dtos
{
    public class UpdateDataPointInput : DataPointEntity, IInputDto
    {
    }

}
