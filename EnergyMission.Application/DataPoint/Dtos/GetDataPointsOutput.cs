﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.DataPoint.Dtos
{
    public class GetDataPointsOutput : IOutputDto
    {
        public List<DataPointEntity> DataPoints { get; set; }
    }
}