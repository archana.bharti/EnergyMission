﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.DataPoint.Dtos
{
    public class GetNewDataPointOutput : DataPointEntity, IOutputDto
    {
        #region Public Members

        /// <summary>
        ///   Id is not required for inserts
        /// </summary>
        public new long? Id { get; set; }

        #endregion
    }
}