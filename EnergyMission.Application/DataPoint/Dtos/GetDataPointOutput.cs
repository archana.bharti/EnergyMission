using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.DataPoint.Dtos
{
    public class GetDataPointOutput : DataPointEntity, IOutputDto
    {
    }
}