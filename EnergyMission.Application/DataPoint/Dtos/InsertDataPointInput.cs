using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.DataPoint.Dtos
{
    public class InsertDataPointInput : DataPointEntity, IInputDto
    {
    }
}