﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.DataPoint.Dtos
{
    public class GetDataPointInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}