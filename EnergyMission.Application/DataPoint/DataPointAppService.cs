﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.DataPoint.Dtos;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;


namespace EnergyMission.DataPoint
{
    public class DataPointAppService : EmApplicationServiceBase, IDataPointAppService
    {
        #region Private Members

        private readonly IDataPointRepository _DataPointRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aDataPointRepository">Repository to access the underlying data layer</param>
        public DataPointAppService(IDataPointRepository aDataPointRepository)
        {
            _DataPointRepository = aDataPointRepository;
        }

        #endregion

        #region Implementation of IDataPointAppService

        /// <summary>
        ///     Delete the specified dataPoint
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteDataPoint(DeleteDataPointInput aInput)
        {
            _DataPointRepository.Delete(aInput.Id);
        }

        /// <summary>
        /// Inserts a collection of data points 
        /// </summary>
        /// <param name="aInput">Data point to input</param>
        public void InsertDataPoints(List<InsertDataPointInput> aInput)
        {
            foreach (InsertDataPointInput dataPointInput in aInput)
            {
                InsertDataPoint(dataPointInput);
            }
        }

        /// <summary>
        ///     Retrieves the dataPoints within the system
        /// </summary>
        /// <param name="aInput">Filter of which dataPoints to retrieve.</param>
        /// <returns>Returns the dataPoints within the system.</returns>
        public GetDataPointsOutput GetDataPoints(GetDataPointsInput aInput)
        {
            List<DataPointEntity> dataPoints = _DataPointRepository.GetAllList();

            return new GetDataPointsOutput {DataPoints = Mapper.Map<List<DataPointDto>>(dataPoints)};
        }

        /// <summary>
        ///     Retrieves a dataPoint based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what dataPoint to retrieve.</param>
        /// <returns>Returns the dataPoint if found, null otherwise.</returns>
        public GetDataPointOutput GetDataPoint(GetDataPointInput aInput)
        {
            DataPointEntity dataPoint = _DataPointRepository.Get(aInput.Id);
            return dataPoint == null ? null : Mapper.Map<GetDataPointOutput>(dataPoint);
        }

        /// <summary>
        ///     Initialises a new dataPoint for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new dataPoint</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        public GetNewDataPointOutput GetNewDataPoint(GetNewDataPointInput aInput)
        {
            return new GetNewDataPointOutput();
        }

        /// <summary>
        ///     Insert the new dataPoint
        /// </summary>
        /// <param name="aInput">DataPoint information to input.</param>
        public void InsertDataPoint(InsertDataPointInput aInput)
        {
            DataPointEntity dataPoint = new DataPointEntity();
            Mapper.Map(aInput, dataPoint);
            _DataPointRepository.Insert(dataPoint);
        }

        /// <summary>
        ///     Updates the specified dataPoints
        /// </summary>
        /// <param name="aInput">Information to update the dataPoint with</param>
        public void UpdateDataPoint(UpdateDataPointInput aInput)
        {
            DataPointEntity dataPoint = _DataPointRepository.Get(aInput.Id);
            Mapper.Map(aInput, dataPoint);
            _DataPointRepository.Update(dataPoint);
        }

        #endregion
    }
}
