﻿using EnergyMission.DataPoint.Dtos;
using System.Collections.Generic;

namespace EnergyMission.DataPoint
{
    public interface IDataPointAppService
    {
        #region Public Members

        void DeleteDataPoint(DeleteDataPointInput aInput);
        void InsertDataPoint(InsertDataPointInput aInput);
        void InsertDataPoints(List<InsertDataPointInput> aInput);

        GetDataPointsOutput GetDataPoints(GetDataPointsInput aInput);
        void UpdateDataPoint(UpdateDataPointInput aInput);

        GetDataPointOutput GetDataPoint(GetDataPointInput aInput);

        GetNewDataPointOutput GetNewDataPoint(GetNewDataPointInput aInput);

        #endregion
    }
}