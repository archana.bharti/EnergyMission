﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.LocationMapping.Dtos
{
    public class UpdateLocationMappingInput : LocationMappingEntity, IInputDto
    {
        public long OriginalCountryId { get; set; }
        public long OriginalPostCodeId { get; set; }
        public long OriginalStateId { get; set; }
        public long OriginalSuburbId { get; set; }
    }
}