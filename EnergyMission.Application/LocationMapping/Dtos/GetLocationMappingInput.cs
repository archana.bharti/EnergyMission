﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.LocationMapping.Dtos
{
    public class GetLocationMappingInput : IInputDto
    {
        [Required]
        public long CountryId { get; set; }
        [Required]
        public long PostCodeId { get; set; }
        [Required]
        public long StateId { get; set; }
        [Required]
        public long SuburbId { get; set; }

    }
}