using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.LocationMapping.Dtos
{
    public class GetLocationMappingOutput : LocationMappingEntity, IOutputDto
    {
    }
}