﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.LocationMapping.Dtos
{
    public class ExistsLocationMappingInput : IInputDto
    {
        #region Public Members

        [Required]
        public long CountryId { get; set; }

        [Required]
        public long PostCodeId { get; set; }

        [Required]
        public long StateId { get; set; }

        [Required]
        public long SuburbId { get; set; }

        #endregion
    }
}