﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.LocationMapping.Dtos
{
    public class GetLocationMappingsOutput : IOutputDto
    {
        /// <summary>
        /// Class constructor
        /// </summary>
        public GetLocationMappingsOutput()
        {
            LocationMappings = new List<LocationMappingEntity>();
        }

        public List<LocationMappingEntity> LocationMappings { get; set; }
    }
}