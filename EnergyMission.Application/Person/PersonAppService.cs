﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Data;
using EnergyMission.Person.Dtos;

namespace EnergyMission.Person
{
    public class PersonAppService: IPersonAppService
    {
        private readonly  EmDbContext _PersonRepository;

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aPersonRepository">Repository to access the underlying data layer</param>
        public PersonAppService()
        {
            _PersonRepository =  new EmDbContext();
        }

        #region Implementation of IPersonAppService

        /// <summary>
        ///     Delete the specified person
        /// </summary>
        /// <param name="aInput"></param>
        //public void DeletePerson(DeletePersonInput aInput)
        //{
        //    Data.Person ps = new Data.Person();
        //    ps=_PersonRepository.Persons.Where(tt=>tt.Id)
        //    _PersonRepository.Persons(aInput.Id);
        //}

        /// <summary>
        ///     Initialises a new person for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new person</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        //public GetNewPersonOutput GetNewPerson(GetNewPersonInput aInput)
        //{
        //    return new GetNewPersonOutput
        //    {
        //        ContactDetail = new  ContactDetailEntity()
        //    };
        //}

        /// <summary>
        ///     Retrieves a person based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what person to retrieve.</param>
        /// <returns>Returns the person if found, null otherwise.</returns>
        public GetPersonOutput GetPerson(GetPersonInput aInput)
        {
            Data.Person person = _PersonRepository.Persons.Find(aInput.Id);
            return person == null ? null : Mapper.Map<GetPersonOutput>(person);
        }

        /// <summary>
        ///     Retrieves a person based on the input. Also retrieves any related
        ///     entities.
        /// </summary>
        /// <param name="aInput">Used to determine what person to retrieve.</param>
        ///// <returns>Returns the person if found, null otherwise.</returns>
        //public GetPersonOutput GetPersonWithInclude(GetPersonInput aInput)
        //{
        //    PersonEntity person = _PersonRepository.Persons(aInput.Id);
        //    return person == null ? null : Mapper.Map<GetPersonOutput>(person);
        //}

        /// <summary>
        ///     Retrieves the persons within the system
        /// </summary>
        /// <param name="aInput">Filter of which persons to retrieve.</param>
        /// <returns>Returns the persons within the system.</returns>
        //public GetPersonsOutput GetPersons(GetPersonsInput aInput)
        //{
        //    List<PersonEntity> persons = _PersonRepository.GetAllList();

        //    return new GetPersonsOutput {Persons = Mapper.Map<List<PersonDto>>(persons)};
        //}

        /// <summary>
        ///     Gets the persons for a client based on the email addresses supplied which match up to the client.
        /// </summary>
        /// <param name="aInput">Information about the persons that relate to the client.</param>
        /// <returns>Returns the persons for a client based on the email addresses supplied which match up to the client.</returns>
        public GetPersonsOutput GetPersonsForClient(GetPersonsForClientInput aInput)
        {
            List<Data.Person> result = new List<Data.Person>();

            foreach (var person in _PersonRepository.Persons.ToList())
            {
                if (aInput.EmailAddresses.Contains(person.EmailAddress))
                {
                    result.Add(person);
                }
            }

            return new GetPersonsOutput {Persons = Mapper.Map<List<PersonEntity>>(result)};
        }

        /// <summary>
        ///     Retrieves the persons within the system. Also retrieves the associated entities.
        /// </summary>
        /// <param name="aInput">Filter of which persons to retrieve.</param>
        /// <returns>Returns the persons within the system.</returns>
        //public GetPersonsOutput GetPersonsWithInclude(GetPersonsInput aInput)
        //{
        //    List<PersonEntity> persons = _PersonRepository.GetAllWithInclude()
        //                                                  .ToList();

        //    return new GetPersonsOutput {Persons = Mapper.Map<List<PersonDto>>(persons)};
        //}

        /// <summary>
        ///     Insert the new person
        /// </summary>
        /// <param name="aInput">Person information to input.</param>
        public long InsertPerson(InsertPersonInput aInput)
        {
            PersonEntity person = new PersonEntity
            {
                EmailAddress = aInput.EmailAddress,
                Title = aInput.Title,
                FirstName = aInput.FirstName,
                MiddleName = aInput.MiddleName,
                LastName = aInput.LastName,
                NameSuffix = aInput.NameSuffix
            };

            _PersonRepository.Persons.Add(Mapper.Map<Data.Person>(person));
            return 1; 
        }

        /// <summary>
        ///     Updates the specified persons
        /// </summary>
        /// <param name="aInput">Information to update the person with</param>
        //public void UpdatePerson(UpdatePersonInput aInput)
        //{
        //    PersonEntity person = _PersonRepository.Get(aInput.Id);
        //    Mapper.Map(aInput, person);
        //    _PersonRepository.Update(person);
        //}

        #endregion
    }
}