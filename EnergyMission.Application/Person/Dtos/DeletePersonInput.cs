using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Person.Dtos
{
    public class DeletePersonInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}