﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Person.Dtos
{
    public class InsertPersonInput : PersonEntity, IInputDto
    {
    }
}