﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Person.Dtos
{
    public class GetPersonsOutput : IOutputDto
    {
        public List<PersonEntity> Persons { get; set; }
    }
}