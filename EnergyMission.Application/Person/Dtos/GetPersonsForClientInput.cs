using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace EnergyMission.Person.Dtos
{
    public class GetPersonsForClientInput : IInputDto
    {
        public long ClientId { get; set; }
        public List<string> EmailAddresses { get; set; }

        public GetPersonsForClientInput()
        {
            EmailAddresses = new List<string>();
        }
    }
}