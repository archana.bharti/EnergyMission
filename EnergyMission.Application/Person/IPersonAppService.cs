﻿using EnergyMission.Person.Dtos;

namespace EnergyMission.Person
{
    public interface IPersonAppService
    {
        //void DeletePerson(DeletePersonInput aInput);
        //GetNewPersonOutput GetNewPerson(GetNewPersonInput aInput);
        GetPersonOutput GetPerson(GetPersonInput aInput);
        //GetPersonsOutput GetPersons(GetPersonsInput aInput);
        GetPersonsOutput GetPersonsForClient(GetPersonsForClientInput aInput);
        //GetPersonsOutput GetPersonsWithInclude(GetPersonsInput aInput);
        //GetPersonOutput GetPersonWithInclude(GetPersonInput aGetPersonInput);
        long InsertPerson(InsertPersonInput aInput);
        //void UpdatePerson(UpdatePersonInput aInput);
    }
}