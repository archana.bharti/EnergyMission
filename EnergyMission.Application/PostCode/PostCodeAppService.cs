﻿using System.Collections.Generic;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;
using EnergyMission.PostCode.Dtos;

namespace EnergyMission.PostCode
{
    public class PostCodeAppService : EmApplicationServiceBase, IPostCodeAppService
    {
        #region Private Members

        private readonly IPostCodeRepository _PostCodeRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aPostCodeRepository">Repository to access the underlying data layer</param>
        public PostCodeAppService(IPostCodeRepository aPostCodeRepository)
        {
            _PostCodeRepository = aPostCodeRepository;
        }

        #endregion

        #region Implementation of IPostCodeAppService

        /// <summary>
        ///     Delete the specified postCode
        /// </summary>
        /// <param name="aInput"></param>
        public void DeletePostCode(DeletePostCodeInput aInput)
        {
            _PostCodeRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Retrieves the postCodes within the system
        /// </summary>
        /// <param name="aInput">Filter of which postCodes to retrieve.</param>
        /// <returns>Returns the postCodes within the system.</returns>
        public GetPostCodesOutput GetPostCodes(GetPostCodesInput aInput)
        {
            List<PostCodeEntity> postCodes = _PostCodeRepository.GetAllList();

            return new GetPostCodesOutput { PostCodes = Mapper.Map<List<PostCodeDto>>(postCodes) };
        }

        /// <summary>
        ///     Retrieves a postCode based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what postCode to retrieve.</param>
        /// <returns>Returns the postCode if found, null otherwise.</returns>
        public GetPostCodeOutput GetPostCode(GetPostCodeInput aInput)
        {
            PostCodeEntity postCode = _PostCodeRepository.Get(aInput.Id);
            return postCode == null ? null : Mapper.Map<GetPostCodeOutput>(postCode);
        }

        /// <summary>
        ///     Initialises a new postCode for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new postCode</param>
        /// <returns>Returns a view modl for use in the display tier.</returns>
        public GetNewPostCodeOutput GetNewPostCode(GetNewPostCodeInput aInput)
        {
            return new GetNewPostCodeOutput();
        }

        /// <summary>
        ///     Insert the new postCode
        /// </summary>
        /// <param name="aInput">PostCode information to input.</param>
        public void InsertPostCode(InsertPostCodeInput aInput)
        {
            PostCodeEntity postCode = new PostCodeEntity
            {
                PostCodeNumber = aInput.PostCodeNumber
            };

            _PostCodeRepository.Insert(postCode);
        }

        /// <summary>
        ///     Updates the specified postCodes
        /// </summary>
        /// <param name="aInput">Information to update the postCode with</param>
        public void UpdatePostCode(UpdatePostCodeInput aInput)
        {
            PostCodeEntity postCode = _PostCodeRepository.Get(aInput.Id);
            Mapper.Map(aInput, postCode);
            _PostCodeRepository.Update(postCode);
        }

        #endregion
    }
}