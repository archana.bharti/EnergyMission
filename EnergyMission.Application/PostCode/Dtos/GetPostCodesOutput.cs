﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.PostCode.Dtos
{
    public class GetPostCodesOutput : IOutputDto
    {
        public List<PostCodeEntity> PostCodes { get; set; }
    }
}