﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.PostCode.Dtos
{
    public class GetNewPostCodeOutput : PostCodeEntity, IOutputDto
    {
    }
}