﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.PostCode.Dtos
{
    public class DeletePostCodeInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}