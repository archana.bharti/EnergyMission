﻿using EnergyMission.PostCode.Dtos;

namespace EnergyMission.PostCode
{
    public interface IPostCodeAppService
    {
        #region Public Members

        void DeletePostCode(DeletePostCodeInput aInput);
        GetNewPostCodeOutput GetNewPostCode(GetNewPostCodeInput aInput);
        GetPostCodeOutput GetPostCode(GetPostCodeInput aInput);

        GetPostCodesOutput GetPostCodes(GetPostCodesInput aInput);
        void InsertPostCode(InsertPostCodeInput aInput);
        void UpdatePostCode(UpdatePostCodeInput aInput);

        #endregion
    }
}