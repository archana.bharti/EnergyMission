﻿using System.Collections.Generic;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Sensor.Dtos;

namespace EnergyMission.Sensor
{
    public class SensorAppService : EmApplicationServiceBase, ISensorAppService
    {
        #region Private Members

        private readonly ISensorRepository _SensorRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aSensorRepository">Repository to access the underlying data layer</param>
        public SensorAppService(ISensorRepository aSensorRepository)
        {
            _SensorRepository = aSensorRepository;
        }

        #endregion

        #region Implementation of ISensorAppService

        /// <summary>
        ///     Delete the specified sensor
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteSensor(DeleteSensorInput aInput)
        {
            _SensorRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Initialises a new sensor for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new sensor</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        public GetNewSensorOutput GetNewSensor(GetNewSensorInput aInput)
        {
            return new GetNewSensorOutput();
        }

        /// <summary>
        ///     Retrieves a sensor based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what sensor to retrieve.</param>
        /// <returns>Returns the sensor if found, null otherwise.</returns>
        public GetSensorOutput GetSensor(GetSensorInput aInput)
        {
            SensorEntity sensor = _SensorRepository.Get(aInput.Id);
            return sensor == null ? null : Mapper.Map<GetSensorOutput>(sensor);
        }

        /// <summary>
        ///     Retrieves the sensors within the system
        /// </summary>
        /// <param name="aInput">Filter of which sensors to retrieve.</param>
        /// <returns>Returns the sensors within the system.</returns>
        public GetSensorsOutput GetSensors(GetSensorsInput aInput)
        {
            List<SensorEntity> sensors = _SensorRepository.GetAllList();

            return new GetSensorsOutput {Sensors = Mapper.Map<List<SensorDto>>(sensors)};
        }

        /// <summary>
        ///     Insert the new sensor
        /// </summary>
        /// <param name="aInput">Sensor information to input.</param>
        public void InsertSensor(InsertSensorInput aInput)
        {
            SensorEntity sensor = new SensorEntity
            {
                Name = aInput.Name
            };

            _SensorRepository.Insert(sensor);
        }

        /// <summary>
        ///     Updates the specified sensors
        /// </summary>
        /// <param name="aInput">Information to update the sensor with</param>
        public void UpdateSensor(UpdateSensorInput aInput)
        {
            SensorEntity sensor = _SensorRepository.Get(aInput.Id);
            Mapper.Map(aInput, sensor);
            _SensorRepository.Update(sensor);
        }

        #endregion
    }
}