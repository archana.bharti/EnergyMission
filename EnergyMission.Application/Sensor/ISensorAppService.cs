﻿using EnergyMission.Sensor.Dtos;

namespace EnergyMission.Sensor
{
    public interface ISensorAppService
    {
        #region Public Members

        void DeleteSensor(DeleteSensorInput aInput);
        void InsertSensor(InsertSensorInput aInput);

        GetSensorsOutput GetSensors(GetSensorsInput aInput);
        void UpdateSensor(UpdateSensorInput aInput);

        GetSensorOutput GetSensor(GetSensorInput aInput);

        GetNewSensorOutput GetNewSensor(GetNewSensorInput aInput);

        #endregion
    }
}