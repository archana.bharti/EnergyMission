﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Sensor.Dtos
{
    public class InsertSensorInput : SensorEntity, IInputDto
    {
    }
}