﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Sensor.Dtos
{
    public class DeleteSensorInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}