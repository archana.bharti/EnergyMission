using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Sensor.Dtos
{
    public class GetSensorInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}