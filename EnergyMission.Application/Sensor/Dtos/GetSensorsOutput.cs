﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Sensor.Dtos
{
    public class GetSensorsOutput : IOutputDto
    {
        public List<SensorEntity> Sensors { get; set; }
    }
}