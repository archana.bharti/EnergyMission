﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.UserProfileInformation.Dtos
{
    public class GetUserProfileInformationsOutput : IOutputDto
    {
        #region Public Members

        public List<UserProfileInformationEntity> UserProfileInformations { get; set; }

        #endregion
    }
}