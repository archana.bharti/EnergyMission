﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.UserProfileInformation.Dtos
{
    public class InsertUserProfileInformationInput : UserProfileInformationEntity, IInputDto
    {
    }
}