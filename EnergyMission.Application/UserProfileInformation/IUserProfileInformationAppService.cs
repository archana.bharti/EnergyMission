﻿using EnergyMission.UserProfileInformation.Dtos;

namespace EnergyMission.UserProfileInformation
{
    public interface IUserProfileInformationAppService
    {
        #region Public Members

        void DeleteUserProfileInformation(DeleteUserProfileInformationInput aInput);
        GetNewUserProfileInformationOutput GetNewUserProfileInformation(GetNewUserProfileInformationInput aInput);
        GetUserProfileInformationOutput GetUserProfileInformation(GetUserProfileInformationInput aInput);

        GetUserProfileInformationsOutput GetUserProfileInformations(GetUserProfileInformationsInput aInput);
        void InsertUserProfileInformation(InsertUserProfileInformationInput aInput);
        void UpdateUserProfileInformation(UpdateUserProfileInformationInput aInput);

        #endregion
    }
}