﻿using System;
using System.Collections.Generic;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;
using EnergyMission.UserProfileInformation.Dtos;

namespace EnergyMission.UserProfileInformation
{
    public class UserProfileInformationAppService : EmApplicationServiceBase, IUserProfileInformationAppService
    {
        #region Private Members

        private readonly IUserProfileInformationRepository _UserProfileInformationRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aUserProfileInformationRepository">Repository to access the underlying data layer</param>
        public UserProfileInformationAppService(IUserProfileInformationRepository aUserProfileInformationRepository)
        {
            _UserProfileInformationRepository = aUserProfileInformationRepository;
        }

        #endregion

        #region Implementation of IUserProfileInformationAppService

        /// <summary>
        ///     Delete the specified userProfileInformation
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteUserProfileInformation(DeleteUserProfileInformationInput aInput)
        {
            _UserProfileInformationRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Initialises a new userProfileInformation for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new userProfileInformation</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        public GetNewUserProfileInformationOutput GetNewUserProfileInformation(GetNewUserProfileInformationInput aInput)
        {
            return new GetNewUserProfileInformationOutput();
        }

        /// <summary>
        ///     Retrieves a userProfileInformation based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what userProfileInformation to retrieve.</param>
        /// <returns>Returns the userProfileInformation if found, null otherwise.</returns>
        public GetUserProfileInformationOutput GetUserProfileInformation(GetUserProfileInformationInput aInput)
        {
            UserProfileInformationEntity userProfileInformation = _UserProfileInformationRepository.Get(aInput.Id);
            return userProfileInformation == null ? null : Mapper.Map<GetUserProfileInformationOutput>(userProfileInformation);
        }

        /// <summary>
        ///     Retrieves the userProfileInformations within the system
        /// </summary>
        /// <param name="aInput">Filter of which userProfileInformations to retrieve.</param>
        /// <returns>Returns the userProfileInformations within the system.</returns>
        public GetUserProfileInformationsOutput GetUserProfileInformations(GetUserProfileInformationsInput aInput)
        {
            List<UserProfileInformationEntity> userProfileInformations = _UserProfileInformationRepository.GetAllList();

            return new GetUserProfileInformationsOutput {UserProfileInformations = Mapper.Map<List<UserProfileInformationDto>>(userProfileInformations)};
        }

        /// <summary>
        ///     Insert the new userProfileInformation
        /// </summary>
        /// <param name="aInput">UserProfileInformation information to input.</param>
        public void InsertUserProfileInformation(InsertUserProfileInformationInput aInput)
        {
            UserProfileInformationEntity userProfileInformation = new UserProfileInformationEntity
            {
                //AbbreviationCode = aInput.AbbreviationCode,
            };
            throw new NotImplementedException();

            _UserProfileInformationRepository.Insert(userProfileInformation);
        }

        /// <summary>
        ///     Updates the specified userProfileInformations
        /// </summary>
        /// <param name="aInput">Information to update the userProfileInformation with</param>
        public void UpdateUserProfileInformation(UpdateUserProfileInformationInput aInput)
        {
            UserProfileInformationEntity userProfileInformation = _UserProfileInformationRepository.Get(aInput.Id);
            Mapper.Map(aInput, userProfileInformation);
            _UserProfileInformationRepository.Update(userProfileInformation);
        }

        #endregion
    }
}