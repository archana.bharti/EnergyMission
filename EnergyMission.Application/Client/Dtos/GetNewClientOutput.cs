﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Client.Dtos
{
    public class GetNewClientOutput : ClientEntity, IOutputDto
    {
    }
}