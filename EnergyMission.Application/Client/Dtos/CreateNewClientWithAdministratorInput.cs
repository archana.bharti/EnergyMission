﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Client.Dtos
{
    public class CreateNewClientWithAdministratorInput : IInputDto
    {
        [Required]
        public long ClientAdminPersonId { get; set; }
        [Required]
        public string ClientName { get; set; }
    }
}