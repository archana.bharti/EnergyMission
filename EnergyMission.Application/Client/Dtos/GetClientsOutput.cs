﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Client.Dtos
{
    public class GetClientsOutput : IOutputDto
    {
        public List<ClientEntity> Clients { get; set; }
    }
}