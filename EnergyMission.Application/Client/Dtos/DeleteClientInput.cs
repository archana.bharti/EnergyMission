﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Client.Dtos
{
    public class DeleteClientInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}


        
        