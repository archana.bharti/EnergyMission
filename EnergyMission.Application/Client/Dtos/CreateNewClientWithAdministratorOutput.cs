﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace EnergyMission.Client.Dtos
{
    public class CreateNewClientWithAdministratorOutput : IOutputDto
    {
        /// <summary>
        ///     Class constructor
        /// </summary>
        public CreateNewClientWithAdministratorOutput()
        {
            Errors = new List<string>();
        }

        public List<string> Errors { get; set; }
        public long NewClientId { get; set; }
        public bool Success { get; set; }
    }
}