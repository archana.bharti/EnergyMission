﻿using System.Collections.Generic;
using Abp.Dependency;
using AutoMapper;
using EnergyMission.Client.Dtos;
using EnergyMission.ClientAdministrator;
using EnergyMission.ClientAdministrator.Dtos;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;
using EnergyMission.Site;
using EnergyMission.Site.Dtos;

namespace EnergyMission.Client
{
    public class ClientAppService : EmApplicationServiceBase, IClientAppService
    {
        private readonly IClientRepository _ClientRepository;

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientRepository">Repository to access the underlying data layer</param>
        public ClientAppService(IClientRepository aClientRepository)
        {
            _ClientRepository = aClientRepository;
        }

        #region Implementation of IClientAppService

        /// <summary>
        ///     Creates a new client and associates the supplied person as the administrator for the client
        /// </summary>
        /// <param name="aInput"></param>
        /// <returns></returns>
        public CreateNewClientWithAdministratorOutput CreateNewClientWithAdminstrator(CreateNewClientWithAdministratorInput aInput)
        {
            CreateNewClientWithAdministratorOutput result = new CreateNewClientWithAdministratorOutput();
            
            //Add client record
            InsertClientInput newClient = new InsertClientInput
            {
                Name = aInput.ClientName
            };
            long newClientId = InsertClient(newClient);

            //Add client admin record
            InsertClientAdministratorInput clientAdministrator = new InsertClientAdministratorInput
            {
                ClientId = newClientId,
                PersonId = aInput.ClientAdminPersonId
            };
            IClientAdministratorAppService clientAdminAppService = IocManager.Instance.Resolve<IClientAdministratorAppService>();
            clientAdminAppService.InsertClientAdministrator(clientAdministrator);

            result.NewClientId = newClientId;
            result.Success = true;
            return result;
        }

        /// <summary>
        ///     Delete the specified client
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteClient(DeleteClientInput aInput)
        {
            ISiteAppService siteAppService = IocManager.Instance.Resolve<ISiteAppService>();

            List<SiteDto> sites = siteAppService.GetSitesForClient(aInput.Id);
            foreach (SiteDto site in sites)
            {
                siteAppService.DeleteSite(new DeleteSiteInput
                {
                    Id = site.Id.Value
                });
            }

            _ClientRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Retrieves a client based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what client to retrieve.</param>
        /// <returns>Returns the client if found, null otherwise.</returns>
        public GetClientOutput GetClient(GetClientInput aInput)
        {
            ClientEntity client = _ClientRepository.Get(aInput.Id);
            return client == null ? null : Mapper.Map<GetClientOutput>(client);
        }

        /// <summary>
        ///     Retrieves the clients within the system
        /// </summary>
        /// <param name="aInput">Filter of which clients to retrieve.</param>
        /// <returns>Returns the clients within the system.</returns>
        public GetClientsOutput GetClients(GetClientsInput aInput)
        {
            List<ClientEntity> clients = _ClientRepository.GetAllList();

            return new GetClientsOutput {Clients = Mapper.Map<List<ClientDto>>(clients)};
        }

        /// <summary>
        ///     Initialises a new client for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new client</param>
        /// <returns>Returns a view modl for use in the display tier.</returns>
        public GetNewClientOutput GetNewClient(GetNewClientInput aInput)
        {
            return new GetNewClientOutput();
        }

        /// <summary>
        ///     Insert the new client
        /// </summary>
        /// <param name="aInput">Client information to input.</param>
        public long InsertClient(InsertClientInput aInput)
        {
            ClientEntity client = new ClientEntity
            {
                Name = aInput.Name
            };

            return _ClientRepository.InsertAndGetId(client);
        }

        /// <summary>
        ///     Updates the specified clients
        /// </summary>
        /// <param name="aInput">Information to update the client with</param>
        public void UpdateClient(UpdateClientInput aInput)
        {
            ClientEntity client = _ClientRepository.Get(aInput.Id);
            Mapper.Map(aInput, client);
            _ClientRepository.Update(client);
        }

        #endregion
    }
}