﻿using EnergyMission.Client.Dtos;

namespace EnergyMission.Client
{
    public interface IClientAppService
    {
        CreateNewClientWithAdministratorOutput CreateNewClientWithAdminstrator(CreateNewClientWithAdministratorInput aInput);
        void DeleteClient(DeleteClientInput aInput);
        GetClientOutput GetClient(GetClientInput aInput);
        GetClientsOutput GetClients(GetClientsInput aInput);
        GetNewClientOutput GetNewClient(GetNewClientInput aInput);
        long InsertClient(InsertClientInput aInput);
        void UpdateClient(UpdateClientInput aInput);
    }
}