﻿using EnergyMission.Address.Dtos;

namespace EnergyMission.Address
{
    public interface IAddressAppService
    {
        #region Public Members

        void DeleteAddress(DeleteAddressInput aInput);

        GetAddressOutput GetAddress(GetAddressInput aInput);
        GetAddressesOutput GetAddresses(GetAddressesInput aInput);

        GetNewAddressOutput GetNewAddress(GetNewAddressInput aInput);
        void InsertAddress(InsertAddressInput aInput);
        void UpdateAddress(UpdateAddressInput aInput);

        #endregion
    }
}