﻿using System.Collections.Generic;
using AutoMapper;
using EnergyMission.Address.Dtos;
using EnergyMission.Common.Framework;
using EnergyMission.Country;
using EnergyMission.Country.Dtos;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;


namespace EnergyMission.Address
{
    public class AddressAppService : EmApplicationServiceBase, IAddressAppService
    {
        #region Private Members

        private readonly IAddressRepository _AddressRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aAddressRepository">Repository to access the underlying data layer</param>
        public AddressAppService(IAddressRepository aAddressRepository)
        {
            _AddressRepository = aAddressRepository;
        }

        #endregion

        #region Implementation of IAddressAppService

        /// <summary>
        ///     Delete the specified address
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteAddress(DeleteAddressInput aInput)
        {
            _AddressRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Retrieves a address based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what address to retrieve.</param>
        /// <returns>Returns the address if found, null otherwise.</returns>
        public GetAddressOutput GetAddress(GetAddressInput aInput)
        {
            GetAddressOutput result = null;
            AddressEntity address = _AddressRepository.Get(aInput.Id);

            ////Get the list of countries
            //ICountryAppService countryAppService = new CountryAppService(new CountryRepository());
            //GetCountriesOutput countriesOutput = countryAppService.GetCountries(new GetCountriesInput());
            

            if (address != null)
            {
                result = Mapper.Map<GetAddressOutput>(address);
                //result.Countries = countriesOutput.Countries;    
                result.Countries = null;    
            }

            return result;
        }

        /// <summary>
        ///     Retrieves the addresses within the system
        /// </summary>
        /// <param name="aInput">Filter of which addresses to retrieve.</param>
        /// <returns>Returns the addresses within the system.</returns>
        public GetAddressesOutput GetAddresses(GetAddressesInput aInput)
        {
            List<AddressEntity> addresses = _AddressRepository.GetAllList();

            return new GetAddressesOutput {Addresses = Mapper.Map<List<AddressDto>>(addresses)};
        }

        /// <summary>
        ///     Initialises a new address for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new address</param>
        /// <returns>Returns a view modl for use in the display tier.</returns>
        public GetNewAddressOutput GetNewAddress(GetNewAddressInput aInput)
        {
            return new GetNewAddressOutput();
        }

        /// <summary>
        ///     Insert the new address
        /// </summary>
        /// <param name="aInput">Address information to input.</param>
        public void InsertAddress(InsertAddressInput aInput)
        {
            AddressEntity address = new AddressEntity
            {
                //AbbreviationCode = aInput.AbbreviationCode,
                //InternationalAddressDialCode = aInput.InternationalAddressDialCode,
                //Name = aInput.Name
            };

            _AddressRepository.Insert(address);
        }

        /// <summary>
        ///     Updates the specified addresses
        /// </summary>
        /// <param name="aInput">Information to update the address with</param>
        public void UpdateAddress(UpdateAddressInput aInput)
        {
            AddressEntity address = _AddressRepository.Get(aInput.Id);
            Mapper.Map(aInput, address);
            _AddressRepository.Update(address);
        }

        #endregion
    }
}