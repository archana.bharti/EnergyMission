﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Address.Dtos
{
    public class GetAddressInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}