﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Address.Dtos
{
    public class DeleteAddressInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}