﻿using System.Collections;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.Address.Dtos
{
    public class GetAddressOutput : AddressDto, IOutputDto
    {
        public IEnumerable<CountryDto> Countries { get; set; }
    }
}