using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.Address.Dtos
{
    public class GetAddressesOutput : IOutputDto
    {
        public List<AddressDto> Addresses { get; set; }
    }
}