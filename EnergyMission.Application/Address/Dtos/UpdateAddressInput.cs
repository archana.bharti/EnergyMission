﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Address.Dtos
{
    public class UpdateAddressInput : AddressEntity, IInputDto
    {
    }
}