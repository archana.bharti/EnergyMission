﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.Address.Dtos
{
    public class GetNewAddressOutput : AddressDto, IOutputDto
    {
    }
}