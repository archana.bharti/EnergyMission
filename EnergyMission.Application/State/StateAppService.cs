﻿using System.Collections.Generic;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.State.Dtos;

namespace EnergyMission.State
{
    public class StateAppService : EmApplicationServiceBase, IStateAppService
    {
        #region Private Members

        private readonly IStateRepository _StateRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStateRepository">Repository to access the underlying data layer</param>
        public StateAppService(IStateRepository aStateRepository)
        {
            _StateRepository = aStateRepository;
        }

        #endregion

        #region Implementation of IStateAppService

        /// <summary>
        ///     Delete the specified state
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteState(DeleteStateInput aInput)
        {
            _StateRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Initialises a new state for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new state</param>
        /// <returns>Returns a view modl for use in the display tier.</returns>
        public GetNewStateOutput GetNewState(GetNewStateInput aInput)
        {
            return new GetNewStateOutput();
        }

        /// <summary>
        ///     Retrieves a state based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what state to retrieve.</param>
        /// <returns>Returns the state if found, null otherwise.</returns>
        public GetStateOutput GetState(GetStateInput aInput)
        {
            StateEntity state = _StateRepository.Get(aInput.Id);
            return state == null ? null : Mapper.Map<GetStateOutput>(state);
        }

        /// <summary>
        ///     Retrieves the states within the system
        /// </summary>
        /// <param name="aInput">Filter of which states to retrieve.</param>
        /// <returns>Returns the states within the system.</returns>
        public GetStatesOutput GetStates(GetStatesInput aInput)
        {
            List<StateEntity> states = _StateRepository.GetAllList();

            return new GetStatesOutput {States = Mapper.Map<List<StateDto>>(states)};
        }

        /// <summary>
        ///     Insert the new state
        /// </summary>
        /// <param name="aInput">State information to input.</param>
        public void InsertState(InsertStateInput aInput)
        {
            StateEntity state = new StateEntity
            {
                AbbreviationCode = aInput.AbbreviationCode,
                Name = aInput.Name
            };

            _StateRepository.Insert(state);
        }

        /// <summary>
        ///     Updates the specified states
        /// </summary>
        /// <param name="aInput">Information to update the state with</param>
        public void UpdateState(UpdateStateInput aInput)
        {
            StateEntity state = _StateRepository.Get(aInput.Id);
            Mapper.Map(aInput, state);
            _StateRepository.Update(state);
        }

        #endregion
    }
}