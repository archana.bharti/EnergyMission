using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.State.Dtos
{
    public class GetStatesOutput : IOutputDto
    {
        public List<StateEntity> States { get; set; } 
    }
}