﻿using Abp.Application.Services.Dto;

namespace EnergyMission.State.Dtos
{
    public class GetStateInput : IInputDto
    {
        #region Public Members

        public long Id { get; set; }

        #endregion
    }
}