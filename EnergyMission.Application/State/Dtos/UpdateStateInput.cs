﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.State.Dtos
{
    public class UpdateStateInput : StateEntity, IInputDto
    {
    }
}