﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.State.Dtos
{
    public class DeleteStateInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}