using EnergyMission.State.Dtos;

namespace EnergyMission.State
{
    public interface IStateAppService
    {
        void DeleteState(DeleteStateInput aInput);
        void InsertState(InsertStateInput aInput);

        GetStatesOutput GetStates(GetStatesInput aInput);
        void UpdateState(UpdateStateInput aInput);

        GetStateOutput GetState(GetStateInput aInput);

        GetNewStateOutput GetNewState(GetNewStateInput aInput);

    }
}