﻿using EnergyMission.Suburb.Dtos;

namespace EnergyMission.Suburb
{
    public interface ISuburbAppService
    {
        #region Public Members

        void DeleteSuburb(DeleteSuburbInput aInput);
        void InsertSuburb(InsertSuburbInput aInput);

        GetSuburbsOutput GetSuburbs(GetSuburbsInput aInput);
        void UpdateSuburb(UpdateSuburbInput aInput);

        GetSuburbOutput GetSuburb(GetSuburbInput aInput);

        GetNewSuburbOutput GetNewSuburb(GetNewSuburbInput aInput);

        #endregion
    }
}