﻿using System.Collections.Generic;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;
using EnergyMission.Suburb.Dtos;

namespace EnergyMission.Suburb
{
    public class SuburbAppService : EmApplicationServiceBase, ISuburbAppService
    {
        #region Private Members

        private readonly ISuburbRepository _SuburbRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aSuburbRepository">Repository to access the underlying data layer</param>
        public SuburbAppService(ISuburbRepository aSuburbRepository)
        {
            _SuburbRepository = aSuburbRepository;
        }

        #endregion

        #region Implementation of ISuburbAppService

        /// <summary>
        ///     Delete the specified Suburb
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteSuburb(DeleteSuburbInput aInput)
        {
            _SuburbRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Retrieves the Suburbs within the system
        /// </summary>
        /// <param name="aInput">Filter of which Suburbs to retrieve.</param>
        /// <returns>Returns the Suburbs within the system.</returns>
        public GetSuburbsOutput GetSuburbs(GetSuburbsInput aInput)
        {
            List<SuburbEntity> suburbs = _SuburbRepository.GetAllList();

            return new GetSuburbsOutput { Suburbs = Mapper.Map<List<SuburbDto>>(suburbs) };
        }

        /// <summary>
        ///     Retrieves a Suburb based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what Suburb to retrieve.</param>
        /// <returns>Returns the Suburb if found, null otherwise.</returns>
        public GetSuburbOutput GetSuburb(GetSuburbInput aInput)
        {
            SuburbEntity suburb = _SuburbRepository.Get(aInput.Id);
            return suburb == null ? null : Mapper.Map<GetSuburbOutput>(suburb);
        }

        /// <summary>
        ///     Initialises a new Suburb for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new Suburb</param>
        /// <returns>Returns a view modl for use in the display tier.</returns>
        public GetNewSuburbOutput GetNewSuburb(GetNewSuburbInput aInput)
        {
            return new GetNewSuburbOutput();
        }

        /// <summary>
        ///     Insert the new Suburb
        /// </summary>
        /// <param name="aInput">Suburb information to input.</param>
        public void InsertSuburb(InsertSuburbInput aInput)
        {
            SuburbEntity suburb = new SuburbEntity
            {
                Name = aInput.Name
            };

            _SuburbRepository.Insert(suburb);
        }

        /// <summary>
        ///     Updates the specified Suburbs
        /// </summary>
        /// <param name="aInput">Information to update the Suburb with</param>
        public void UpdateSuburb(UpdateSuburbInput aInput)
        {
            SuburbEntity suburb = _SuburbRepository.Get(aInput.Id);
            Mapper.Map(aInput, suburb);
            _SuburbRepository.Update(suburb);
        }

        #endregion
    }
}
