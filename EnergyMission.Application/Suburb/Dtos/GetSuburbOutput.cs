﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Suburb.Dtos
{
    public class GetSuburbOutput : SuburbEntity, IOutputDto
    {
    }
}