﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Suburb.Dtos
{
    public class GetSuburbInput : IInputDto
    {
        #region Public Members

        [Required]
        public long Id { get; set; }

        #endregion
    }
}