﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Suburb.Dtos
{
    public class GetSuburbsOutput : IOutputDto
    {
        #region Public Members

        public List<SuburbEntity> Suburbs { get; set; }

        #endregion
    }
}