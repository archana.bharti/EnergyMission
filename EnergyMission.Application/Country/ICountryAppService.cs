﻿using Abp.Application.Services.Dto;
using EnergyMission.Country.Dtos;

namespace EnergyMission.Country
{
    public interface ICountryAppService
    {
        #region Public Members

        void DeleteCountry(DeleteCountryInput aInput);
        void InsertCountry(InsertCountryInput aInput);

        GetCountriesOutput GetCountries(GetCountriesInput aInput);
        void UpdateCountry(UpdateCountryInput aInput);

        GetCountryOutput GetCountry(GetCountryInput aInput);

        GetNewCountryOutput GetNewCountry(GetNewCountryInput aInput);

        #endregion
    }
}