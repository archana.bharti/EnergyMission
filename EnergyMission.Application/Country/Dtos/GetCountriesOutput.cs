using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Country.Dtos
{
    public class GetCountriesOutput : IOutputDto
    {
        public List<CountryEntity> Countries { get; set; } 
    }
}