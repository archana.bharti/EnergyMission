﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.Country.Dtos
{
    public class DeleteCountryInput : IInputDto
    {
        [Required]
        public long Id { get; set; }
    }
}