﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Country.Dtos
{
    public class UpdateCountryInput : CountryEntity, IInputDto
    {
    }
}