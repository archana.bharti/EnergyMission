﻿using Abp.Application.Services.Dto;

namespace EnergyMission.Country.Dtos
{
    public class GetCountryInput : IInputDto
    {
        #region Public Members

        public long Id { get; set; }

        #endregion
    }
}