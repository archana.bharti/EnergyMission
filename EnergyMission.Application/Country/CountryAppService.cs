﻿using System.Collections.Generic;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Country.Dtos;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;

namespace EnergyMission.Country
{
    public class CountryAppService : EmApplicationServiceBase, ICountryAppService
    {
        #region Private Members

        private readonly ICountryRepository _CountryRepository;

        #endregion


        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aCountryRepository">Repository to access the underlying data layer</param>
        public CountryAppService(ICountryRepository aCountryRepository)
        {
            _CountryRepository = aCountryRepository;
        }

        #endregion

        #region Implementation of ICountryAppService

        /// <summary>
        ///     Delete the specified country
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteCountry(DeleteCountryInput aInput)
        {
            _CountryRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Retrieves the countries within the system
        /// </summary>
        /// <param name="aInput">Filter of which countries to retrieve.</param>
        /// <returns>Returns the countries within the system.</returns>
        public GetCountriesOutput GetCountries(GetCountriesInput aInput)
        {
            List<CountryEntity> countries = _CountryRepository.GetAllList();

            return new GetCountriesOutput {Countries = Mapper.Map<List<CountryDto>>(countries)};
        }

        /// <summary>
        ///     Retrieves a country based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what country to retrieve.</param>
        /// <returns>Returns the country if found, null otherwise.</returns>
        public GetCountryOutput GetCountry(GetCountryInput aInput)
        {
            CountryEntity country = _CountryRepository.Get(aInput.Id);
            return country == null ? null : Mapper.Map<GetCountryOutput>(country);
        }

        /// <summary>
        ///     Initialises a new country for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new country</param>
        /// <returns>Returns a view modl for use in the display tier.</returns>
        public GetNewCountryOutput GetNewCountry(GetNewCountryInput aInput)
        {
            return new GetNewCountryOutput{Id = -1};
        }

        /// <summary>
        ///     Insert the new country
        /// </summary>
        /// <param name="aInput">Country information to input.</param>
        public void InsertCountry(InsertCountryInput aInput)
        {
            CountryEntity country = new CountryEntity
            {
                AbbreviationCode = aInput.AbbreviationCode,
                InternationalCountryDialCode = aInput.InternationalCountryDialCode,
                Name = aInput.Name
            };

            _CountryRepository.Insert(country);
        }

        /// <summary>
        ///     Updates the specified countries
        /// </summary>
        /// <param name="aInput">Information to update the country with</param>
        public void UpdateCountry(UpdateCountryInput aInput)
        {
            CountryEntity country = _CountryRepository.Get(aInput.Id);
            Mapper.Map(aInput, country);
            _CountryRepository.Update(country);
        }

        #endregion
    }
}