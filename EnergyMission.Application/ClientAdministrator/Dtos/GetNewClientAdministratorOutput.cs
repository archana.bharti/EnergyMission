﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.ClientAdministrator.Dtos
{
    public class GetNewClientAdministratorOutput : ClientAdministratorEntity, IOutputDto
    {
    }
}