using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.ClientAdministrator.Dtos
{
    public class DeleteClientAdministratorInput : IInputDto
    {
        #region Public Members

        [Required]
        public long Id { get; set; }

        #endregion
    }
}