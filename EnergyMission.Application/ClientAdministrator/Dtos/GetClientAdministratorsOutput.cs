﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.ClientAdministrator;

namespace EnergyMission.ClientAdministrator.Dtos
{
    public class GetClientAdministratorsOutput : IOutputDto
    {
        public List<ClientAdminDto> ClientAdministrators { get; set; }
    }
}