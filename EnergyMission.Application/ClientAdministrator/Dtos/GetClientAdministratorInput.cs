﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.ClientAdministrator.Dtos
{
    public class GetClientAdministratorInput : IInputDto
    {
        #region Public Members

        [Required]
        public long Id { get; set; }

        #endregion
    }
}