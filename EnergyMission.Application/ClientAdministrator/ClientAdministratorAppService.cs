﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EnergyMission.ClientAdministrator.Dtos;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.ClientAdministrator;
using EnergyMission.Domain.Repositories;

namespace EnergyMission.ClientAdministrator
{
    public class ClientAdministratorAppService : EmApplicationServiceBase, IClientAdministratorAppService
    {
        private readonly IClientAdministratorRepository _ClientAdministratorRepository;

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aClientAdministratorRepository">Repository to access the underlying data layer</param>
        public ClientAdministratorAppService(IClientAdministratorRepository aClientAdministratorRepository)
        {
            _ClientAdministratorRepository = aClientAdministratorRepository;
        }

        #region Implementation of IClientAdministratorAppService

        /// <summary>
        ///     Delete the specified ClientAdministrator
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteClientAdministrator(DeleteClientAdministratorInput aInput)
        {
            _ClientAdministratorRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Retrieves a ClientAdministrator based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what ClientAdministrator to retrieve.</param>
        /// <returns>Returns the ClientAdministrator if found, null otherwise.</returns>
        public GetClientAdministratorOutput GetClientAdministrator(GetClientAdministratorInput aInput)
        {
            ClientAdministratorEntity clientAdministrator = _ClientAdministratorRepository.Get(aInput.Id);
            return clientAdministrator == null ? null : Mapper.Map<GetClientAdministratorOutput>(clientAdministrator);
        }

        /// <summary>
        ///     Retrieves a ClientAdministrator based on the input. Also retrieves associated entities
        /// </summary>
        /// <param name="aInput">Used to determine what ClientAdministrator to retrieve.</param>
        /// <returns>Returns the ClientAdministrator if found, null otherwise.</returns>
        public GetClientAdministratorOutput GetClientAdministratorWithInclude(GetClientAdministratorInput aInput)
        {
            ClientAdministratorEntity clientAdministrator = _ClientAdministratorRepository.GetWithInclude(aInput.Id);
            return clientAdministrator == null ? null : Mapper.Map<GetClientAdministratorOutput>(clientAdministrator);
        }

        /// <summary>
        ///     Retrieves the ClientAdministrators within the system
        /// </summary>
        /// <param name="aInput">Filter of which ClientAdministrators to retrieve.</param>
        /// <returns>Returns the ClientAdministrators within the system.</returns>
        public GetClientAdministratorsOutput GetClientAdministrators(GetClientAdministratorsInput aInput)
        {
            List<ClientAdminDto> clientAdministrators = _ClientAdministratorRepository.GetList();

            return new GetClientAdministratorsOutput {ClientAdministrators = Mapper.Map<List<ClientAdminDto>>(clientAdministrators)};
        }

        /// <summary>
        ///     Retrieves the ClientAdministrators within the system. Also retrieves referenced entities.
        /// </summary>
        /// <param name="aInput">Filter of which ClientAdministrators to retrieve.</param>
        /// <returns>Returns the ClientAdministrators within the system.</returns>
        public GetClientAdministratorsOutput GetClientAdministratorsWithInclude(GetClientAdministratorsInput aInput)
        {
            List<ClientAdministratorEntity> ClientAdministrators = _ClientAdministratorRepository.GetAllList()
                                                                                                 .ToList();

            return new GetClientAdministratorsOutput { ClientAdministrators = Mapper.Map<List<ClientAdminDto>>(ClientAdministrators) };
        }

        /// <summary>
        ///     Initialises a new ClientAdministrator for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new ClientAdministrator</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        public GetNewClientAdministratorOutput GetNewClientAdministrator(GetNewClientAdministratorInput aInput)
        {
            GetNewClientAdministratorOutput newClientAdministrator = new GetNewClientAdministratorOutput();

            return newClientAdministrator;
        }

        /// <summary>
        ///     Insert the new ClientAdministrator
        /// </summary>
        /// <param name="aInput">ClientAdministrator information to input.</param>
        public void InsertClientAdministrator(InsertClientAdministratorInput aInput)
        {
            ClientAdministratorEntity clientAdministrator = new ClientAdministratorEntity
            {
                ClientId = aInput.ClientId,
                Client = aInput.Client,
                PersonId = aInput.PersonId,
                Person = aInput.Person
            };

            _ClientAdministratorRepository.Insert(clientAdministrator);
        }

        /// <summary>
        ///     Updates the specified ClientAdministrators
        /// </summary>
        /// <param name="aInput">Information to update the ClientAdministrator with</param>
        public void UpdateClientAdministrator(UpdateClientAdministratorInput aInput)
        {
            ClientAdministratorEntity clientAdministrator = _ClientAdministratorRepository.Get(aInput.Id);

            clientAdministrator.ClientId = aInput.ClientId;
            clientAdministrator.Client = aInput.Client;
            clientAdministrator.PersonId = aInput.PersonId;
            clientAdministrator.Person = aInput.Person;

            _ClientAdministratorRepository.Update(clientAdministrator);
        }

        #endregion
    }
}