﻿using EnergyMission.ClientAdministrator.Dtos;

namespace EnergyMission.ClientAdministrator
{
    public interface IClientAdministratorAppService
    {
        void DeleteClientAdministrator(DeleteClientAdministratorInput aInput);
        GetClientAdministratorOutput GetClientAdministrator(GetClientAdministratorInput aInput);
        GetClientAdministratorsOutput GetClientAdministrators(GetClientAdministratorsInput aInput);
        GetClientAdministratorsOutput GetClientAdministratorsWithInclude(GetClientAdministratorsInput aInput);
        GetClientAdministratorOutput GetClientAdministratorWithInclude(GetClientAdministratorInput aGetClientAdministratorInput);
        GetNewClientAdministratorOutput GetNewClientAdministrator(GetNewClientAdministratorInput aInput);
        void InsertClientAdministrator(InsertClientAdministratorInput aInput);
        void UpdateClientAdministrator(UpdateClientAdministratorInput aInput);
    }
}