﻿using System.Collections.Generic;
using System.Linq;
using Abp.Dependency;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;
using EnergyMission.Structure;
using EnergyMission.StructureDivision.Dtos;
using EnergyMission.StructureDivisionSensor;
using EnergyMission.StructureDivisionSensor.Dtos;

namespace EnergyMission.StructureDivision
{
    public class StructureDivisionAppService : EmApplicationServiceBase, IStructureDivisionAppService
    {
        private readonly IStructureDivisionRepository _StructureDivisionRepository;

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStructureDivisionRepository">Repository to access the underlying data layer</param>
        public StructureDivisionAppService(IStructureDivisionRepository aStructureDivisionRepository)
        {
            _StructureDivisionRepository = aStructureDivisionRepository;
        }

        #region Implementation of IStructureDivisionAppService

        /// <summary>
        ///     Delete the specified structureDivision
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteStructureDivision(DeleteStructureDivisionInput aInput)
        {
            IStructureDivisionSensorAppService structureDivisionSensorAppService = IocManager.Instance.Resolve<IStructureDivisionSensorAppService>();

            List<StructureDivisionSensorDto> structureDivisionSensors = structureDivisionSensorAppService.GetStructureDivisionSensors(new GetStructureDivisionSensorsInput())
                                                                                                         .StructureDivisionSensors.Where(r => r.StructureDivisionId == aInput.Id)
                                                                                                         .ToList();
            foreach (StructureDivisionSensorDto divisionSensor in structureDivisionSensors)
            {
                structureDivisionSensorAppService.DeleteStructureDivisionSensor(new DeleteStructureDivisionSensorInput
                {
                    Id = divisionSensor.Id.Value
                });
            }

            _StructureDivisionRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Initialises a new structureDivision for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new structureDivision</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        public GetNewStructureDivisionOutput GetNewStructureDivision(GetNewStructureDivisionInput aInput)
        {
            return new GetNewStructureDivisionOutput();
        }

        /// <summary>
        ///     Retrieves a structureDivision based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what structureDivision to retrieve.</param>
        /// <returns>Returns the structureDivision if found, null otherwise.</returns>
        public GetStructureDivisionOutput GetStructureDivision(GetStructureDivisionInput aInput)
        {
            StructureDivisionEntity structureDivision = _StructureDivisionRepository.Get(aInput.Id);
            return structureDivision == null ? null : Mapper.Map<GetStructureDivisionOutput>(structureDivision);
        }

        /// <summary>
        ///     Retrieves a structureDivision based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what structureDivision to retrieve.</param>
        /// <returns>Returns the structureDivision if found, null otherwise.</returns>
        public GetStructureDivisionOutput GetStructureDivisionWithStructure(GetStructureDivisionInput aInput)
        {
            StructureDivisionEntity structureDivision = _StructureDivisionRepository.GetWithInclude(aInput.Id);
            return structureDivision == null ? null : Mapper.Map<GetStructureDivisionOutput>(structureDivision);
        }

        /// <summary>
        ///     Retrieves the structureDivisions within the system
        /// </summary>
        /// <param name="aInput">Filter of which structureDivisions to retrieve.</param>
        /// <returns>Returns the structureDivisions within the system.</returns>
        public GetStructureDivisionsOutput GetStructureDivisions(GetStructureDivisionsInput aInput)
        {
            List<StructureDivisionEntity> structureDivisions = _StructureDivisionRepository.GetAllList();

            return new GetStructureDivisionsOutput {StructureDivisions = Mapper.Map<List<StructureDivisionDto>>(structureDivisions)};
        }

        /// <summary>
        ///     Retrieves the structureDivisions within the system
        /// </summary>
        /// <param name="aInput">Filter of which structureDivisions to retrieve.</param>
        /// <returns>Returns the structureDivisions within the system.</returns>
        public GetStructureDivisionsOutput GetStructureDivisionsWithStructures(GetStructureDivisionsInput aInput)
        {
            List<StructureDivisionEntity> structureDivisions = _StructureDivisionRepository.GetAllWithInclude()
                                                                                           .ToList();

            return new GetStructureDivisionsOutput {StructureDivisions = Mapper.Map<List<StructureDivisionDto>>(structureDivisions)};
        }

        /// <summary>
        ///     Insert the new structureDivision
        /// </summary>
        /// <param name="aInput">StructureDivision information to input.</param>
        public void InsertStructureDivision(InsertStructureDivisionInput aInput)
        {
            StructureDivisionEntity structureDivision = new StructureDivisionEntity
            {
                Name = aInput.Name,
                StructureId = aInput.StructureId
            };

            _StructureDivisionRepository.Insert(structureDivision);
        }

        /// <summary>
        ///     Updates the specified structureDivisions
        /// </summary>
        /// <param name="aInput">Information to update the structureDivision with</param>
        public void UpdateStructureDivision(UpdateStructureDivisionInput aInput)
        {
            StructureDivisionEntity structureDivision = _StructureDivisionRepository.GetWithInclude(aInput.Id);
            structureDivision.StructureId = aInput.StructureId;
            structureDivision.Name = aInput.Name;
            //structureDivision.ParentStructureDivisionId = 1;
            //Mapper.Map(aInput, structureDivision);
        _StructureDivisionRepository.EditStructureDivision(structureDivision);
        }

        /// <summary>
        ///     Retrieves a list of structure divisions for the client
        /// </summary>
        /// <param name="aClientId">Client to retrieve the structure divisions for</param>
        /// <returns>Returns a list of structure divisions for the client</returns>
        public List<StructureDivisionEntity> GetStructureDivisionsForClient(long aClientId)
        {
            List<StructureDivisionEntity> result = new List<StructureDivisionEntity>();

            IStructureAppService structureAppService = IocManager.Instance.Resolve<IStructureAppService>();
            List<StructureEntity> structuresForClient = structureAppService.GetStructuresForClient(aClientId);

            foreach (StructureEntity structure in structuresForClient)
            {
                List<StructureDivisionEntity> structureDivisions = _StructureDivisionRepository.GetAll()
                                                                                               .Where(r => r.StructureId == structure.Id)
                                                                                               .ToList();

                foreach (StructureDivisionEntity structureDivision in structureDivisions)
                {
                    result.Add(Mapper.Map<Structure>(structureDivision));
                }
            }

            return result;
        }

        #endregion
    }
}