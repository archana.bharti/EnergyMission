﻿using System.Collections.Generic;
using EnergyMission.Domain.Entities;
using EnergyMission.StructureDivision.Dtos;

namespace EnergyMission.StructureDivision
{
    public interface IStructureDivisionAppService
    {
        void DeleteStructureDivision(DeleteStructureDivisionInput aInput);
        GetNewStructureDivisionOutput GetNewStructureDivision(GetNewStructureDivisionInput aInput);
        GetStructureDivisionOutput GetStructureDivision(GetStructureDivisionInput aInput);
        GetStructureDivisionsOutput GetStructureDivisions(GetStructureDivisionsInput aInput);
        List<StructureDivisionEntity> GetStructureDivisionsForClient(long aClientId);
        GetStructureDivisionsOutput GetStructureDivisionsWithStructures(GetStructureDivisionsInput aInput);
        GetStructureDivisionOutput GetStructureDivisionWithStructure(GetStructureDivisionInput aInput);
        void InsertStructureDivision(InsertStructureDivisionInput aInput);
        void UpdateStructureDivision(UpdateStructureDivisionInput aInput);
    }
}