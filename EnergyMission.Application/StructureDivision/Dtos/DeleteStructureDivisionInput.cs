﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.StructureDivision.Dtos
{
    public class DeleteStructureDivisionInput : IInputDto
    {
        #region Public Members

        [Required]
        public long Id { get; set; }

        #endregion
    }
}