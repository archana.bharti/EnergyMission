﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.StructureDivision.Dtos
{
    public class UpdateStructureDivisionInput : StructureDivisionEntity, IInputDto
    {
    }
}