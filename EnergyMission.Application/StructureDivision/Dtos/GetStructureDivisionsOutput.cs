using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.StructureDivision.Dtos
{
    public class GetStructureDivisionsOutput : IOutputDto
    {
        #region Public Members

        public List<StructureDivisionDto> StructureDivisions { get; set; }

        #endregion
    }
}