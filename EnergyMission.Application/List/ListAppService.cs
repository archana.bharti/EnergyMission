using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Abp.Dependency;
using EnergyMission.Client;
using EnergyMission.Client.Dtos;
using EnergyMission.ClientTeam;
using EnergyMission.ClientTeam.Dtos;
using EnergyMission.Common.Framework;
using EnergyMission.Common.Security;
using EnergyMission.Country;
using EnergyMission.Country.Dtos;
using EnergyMission.Person;
using EnergyMission.Person.Dtos;
using EnergyMission.PostCode;
using EnergyMission.PostCode.Dtos;
using EnergyMission.Sensor;
using EnergyMission.Sensor.Dtos;
using EnergyMission.Site;
using EnergyMission.Site.Dtos;
using EnergyMission.State;
using EnergyMission.State.Dtos;
using EnergyMission.Structure;
using EnergyMission.Structure.Dtos;
using EnergyMission.StructureDivision;
using EnergyMission.StructureDivision.Dtos;
using EnergyMission.Suburb;
using EnergyMission.Suburb.Dtos;

namespace EnergyMission.List
{
    public class ListAppService : EmApplicationServiceBase, IListAppService
    {
        #region Implementation of IListAppService

        /// <summary>
        ///     Retrieves the list of clients in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of clients in the system in alphabetical order.</returns>
        public List<SelectListItem> GetClients()
        {
            IClientAppService clientAppService = IocManager.Instance.IocContainer.Resolve<IClientAppService>();
            GetClientsOutput clients = clientAppService.GetClients(new GetClientsInput());

            return clients.Clients.Select(client => new SelectListItem
            {
                Text = client.Name,
                Value = Convert.ToString(client.Id)
            })
                          .OrderBy(r => r.Text)
                          .ToList();
        }

        /// <summary>
        ///     Retrieves the list of client teams in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of client teams in the system in alphabetical order.</returns>
        public List<SelectListItem> GetClientTeams()
        {
            IClientTeamAppService clientTeamAppService = IocManager.Instance.IocContainer.Resolve<IClientTeamAppService>();
            GetClientTeamsOutput clientTeams = clientTeamAppService.GetClientTeams(new GetClientTeamsInput());

            return clientTeams.ClientTeams.Select(clientTeam => new SelectListItem
            {
                Text = clientTeam.Name,
                Value = Convert.ToString(clientTeam.Id)
            })
                              .OrderBy(r => r.Text)
                              .ToList();
        }

        /// <summary>
        ///     Retrieves the list of countries in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of countries in the system in alphabetical order.</returns>
        public List<SelectListItem> GetCountries()
        {
            ICountryAppService countryAppService = IocManager.Instance.IocContainer.Resolve<ICountryAppService>();
            GetCountriesOutput countries = countryAppService.GetCountries(new GetCountriesInput());

            return countries.Countries.Select(country => new SelectListItem
            {
                Text = country.Name,
                Value = Convert.ToString(country.Id)
            })
                            .OrderBy(r => r.Text)
                            .ToList();
        }

        /// <summary>
        ///     Retrieves the list of persons in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of persons in the system in alphabetical order.</returns>
        public List<SelectListItem> GetPersons()
        {
            IPersonAppService personAppService = IocManager.Instance.IocContainer.Resolve<IPersonAppService>();
            GetPersonsOutput persons = personAppService.GetPersons(new GetPersonsInput());

            return persons.Persons.Select(person => new SelectListItem
            {
                Text = string.Format("{0}, {1}", person.LastName, person.FirstName),
                Value = Convert.ToString(person.Id)
            })
                          .OrderBy(r => r.Text)
                          .ToList();
        }

        /// <summary>
        ///     Retrieves the list of postcodes in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of postcodes in the system in alphabetical order.</returns>
        public List<SelectListItem> GetPostCodes()
        {
            IPostCodeAppService postCodeAppService = IocManager.Instance.IocContainer.Resolve<IPostCodeAppService>();
            GetPostCodesOutput postCodes = postCodeAppService.GetPostCodes(new GetPostCodesInput());

            return postCodes.PostCodes.Select(postCode => new SelectListItem
            {
                Text = postCode.PostCodeNumber,
                Value = Convert.ToString(postCode.Id)
            })
                            .OrderBy(r => r.Text)
                            .ToList();
        }

        /// <summary>
        ///     Retrieves a list of system roles in alphabetical order.
        /// </summary>
        /// <returns>Returns a list of system roles in alphabetical order.</returns>
        public List<SelectListItem> GetRoles()
        {
            List<SelectListItem> result = new List<SelectListItem>();
            foreach (SystemRoleKindEnum role in Enum.GetValues(typeof (SystemRoleKindEnum)))
            {
                result.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof (SystemRoleKindEnum), role),
                    Value = role.ToString()
                });
            }
            return result.OrderBy(r => r.Text)
                         .ToList();
        }

        /// <summary>
        ///     Retrieves the list of sensors in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of sensors in the system in alphabetical order.</returns>
        public List<SelectListItem> GetSensors()
        {
            ISensorAppService sensorsAppService = IocManager.Instance.IocContainer.Resolve<ISensorAppService>();
            GetSensorsOutput sensors = sensorsAppService.GetSensors(new GetSensorsInput());

            return sensors.Sensors.Select(sensor => new SelectListItem
            {
                Text = sensor.Name,
                Value = Convert.ToString(sensor.Id)
            })
                          .OrderBy(r => r.Text)
                          .ToList();
        }

        /// <summary>
        ///     Retrieves the list of sites in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of sites in the system in alphabetical order.</returns>
        public List<SelectListItem> GetSites()
        {
            ISiteAppService sitesAppService = IocManager.Instance.IocContainer.Resolve<ISiteAppService>();
            GetSitesOutput sites = sitesAppService.GetSites(new GetSitesInput());

            return sites.Sites.Select(site => new SelectListItem
            {
                Text = site.Name,
                Value = Convert.ToString(site.Id)
            })
                        .OrderBy(r => r.Text)
                        .ToList();
        }

        /// <summary>
        ///     Retrieves the list of states in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of states in the system in alphabetical order.</returns>
        public List<SelectListItem> GetStates()
        {
            IStateAppService stateAppService = IocManager.Instance.IocContainer.Resolve<IStateAppService>();
            GetStatesOutput countries = stateAppService.GetStates(new GetStatesInput());

            return countries.States.Select(state => new SelectListItem
            {
                Text = state.Name,
                Value = Convert.ToString(state.Id)
            })
                            .OrderBy(r => r.Text)
                            .ToList();
        }

        /// <summary>
        ///     Retrieves the list of suburbs in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of suburbs in the system in alphabetical order.</returns>
        public List<SelectListItem> GetStructures()
        {
            IStructureAppService structureAppService = IocManager.Instance.IocContainer.Resolve<IStructureAppService>();
            GetStructuresOutput structures = structureAppService.GetStructures(new GetStructuresInput());

            return structures.Structures.Select(structure => new SelectListItem
            {
                Text = structure.Name,
                Value = Convert.ToString(structure.Id)
            })
                             .OrderBy(r => r.Text)
                             .ToList();
        }

        /// <summary>
        ///     Retrieves the list of structure divisions in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of structure divisions in the system in alphabetical order.</returns>
        public List<SelectListItem> GetStructureDivisions()
        {
            IStructureDivisionAppService structureDivisionAppService = IocManager.Instance.IocContainer.Resolve<IStructureDivisionAppService>();
            GetStructureDivisionsOutput countries = structureDivisionAppService.GetStructureDivisions(new GetStructureDivisionsInput());

            return countries.StructureDivisions.Select(structureDivision => new SelectListItem
            {
                Text = structureDivision.Name,
                Value = Convert.ToString(structureDivision.Id)
            })
                            .OrderBy(r => r.Text)
                            .ToList();
        }

        /// <summary>
        ///     Retrieves the list of suburbs in the system in alphabetical order.
        /// </summary>
        /// <returns>Returns the list of suburbs in the system in alphabetical order.</returns>
        public List<SelectListItem> GetSuburbs()
        {
            ISuburbAppService suburbAppService = IocManager.Instance.IocContainer.Resolve<ISuburbAppService>();
            GetSuburbsOutput countries = suburbAppService.GetSuburbs(new GetSuburbsInput());

            return countries.Suburbs.Select(suburb => new SelectListItem
            {
                Text = suburb.Name,
                Value = Convert.ToString(suburb.Id)
            })
                            .OrderBy(r => r.Text)
                            .ToList();
        }

        #endregion
    }
}