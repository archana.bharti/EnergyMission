﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace EnergyMission.List
{
    public interface IListAppService
    {
        #region Public Members

        List<SelectListItem> GetClients();
        List<SelectListItem> GetClientTeams();
        List<SelectListItem> GetCountries();
        List<SelectListItem> GetPersons();
        List<SelectListItem> GetPostCodes();
        List<SelectListItem> GetRoles();
        List<SelectListItem> GetSensors();
        List<SelectListItem> GetSites();
        List<SelectListItem> GetStates();
        List<SelectListItem> GetStructureDivisions();
        List<SelectListItem> GetStructures();
        List<SelectListItem> GetSuburbs();

        #endregion


    }
}