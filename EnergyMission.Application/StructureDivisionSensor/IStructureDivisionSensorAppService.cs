﻿using EnergyMission.StructureDivisionSensor.Dtos;

namespace EnergyMission.StructureDivisionSensor
{
    public interface IStructureDivisionSensorAppService
    {
        #region Public Members

        void DeleteStructureDivisionSensor(DeleteStructureDivisionSensorInput aInput);
        void InsertStructureDivisionSensor(InsertStructureDivisionSensorInput aInput);
        GetStructureDivisionSensorsOutput GetStructureDivisionSensors(GetStructureDivisionSensorsInput aInput);
        GetStructureDivisionSensorsOutput GetStructureDivisionSensorsWithInclude(GetStructureDivisionSensorsInput aInput);
        void UpdateStructureDivisionSensor(UpdateStructureDivisionSensorInput aInput);
        GetStructureDivisionSensorOutput GetStructureDivisionSensor(GetStructureDivisionSensorInput aInput);
        GetStructureDivisionSensorOutput GetStructureDivisionSensorWithInclude(GetStructureDivisionSensorInput aInput);
        GetNewStructureDivisionSensorOutput GetNewStructureDivisionSensor(GetNewStructureDivisionSensorInput aInput);

        #endregion
    }
}