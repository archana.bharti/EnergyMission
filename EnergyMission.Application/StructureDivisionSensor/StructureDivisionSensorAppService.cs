﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EnergyMission.Common.Framework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.EntitiesDto;
using EnergyMission.Domain.Repositories;
using EnergyMission.StructureDivisionSensor.Dtos;

namespace EnergyMission.StructureDivisionSensor
{
    public class StructureDivisionSensorAppService : EmApplicationServiceBase, IStructureDivisionSensorAppService
    {
        #region Private Members

        private readonly IStructureDivisionSensorRepository _StructureDivisionSensorRepository;

        #endregion

        #region Public Members

        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aStructureDivisionSensorRepository">Repository to access the underlying data layer</param>
        public StructureDivisionSensorAppService(IStructureDivisionSensorRepository aStructureDivisionSensorRepository)
        {
            _StructureDivisionSensorRepository = aStructureDivisionSensorRepository;
        }

        #endregion

        #region Implementation of IStructureDivisionSensorAppService

        /// <summary>
        ///     Delete the specified structureDivisionSensor
        /// </summary>
        /// <param name="aInput"></param>
        public void DeleteStructureDivisionSensor(DeleteStructureDivisionSensorInput aInput)
        {
            _StructureDivisionSensorRepository.Delete(aInput.Id);
        }

        /// <summary>
        ///     Initialises a new structureDivisionSensor for future insertion into the database
        /// </summary>
        /// <param name="aInput">Additional parameters to use to initialise the new structureDivisionSensor</param>
        /// <returns>Returns a view modle for use in the display tier.</returns>
        public GetNewStructureDivisionSensorOutput GetNewStructureDivisionSensor(GetNewStructureDivisionSensorInput aInput)
        {
            return new GetNewStructureDivisionSensorOutput();
        }

        /// <summary>
        ///     Retrieves a structureDivisionSensor based on the input.
        /// </summary>
        /// <param name="aInput">Used to determine what structureDivisionSensor to retrieve.</param>
        /// <returns>Returns the structureDivisionSensor if found, null otherwise.</returns>
        public GetStructureDivisionSensorOutput GetStructureDivisionSensor(GetStructureDivisionSensorInput aInput)
        {
            StructureDivisionSensorEntity structureDivisionSensor = _StructureDivisionSensorRepository.Get(aInput.Id);
            return structureDivisionSensor == null ? null : Mapper.Map<GetStructureDivisionSensorOutput>(structureDivisionSensor);
        }

        /// <summary>
        ///     Retrieves a structureDivisionSensor based on the input. Also retrieves any referenced entities
        /// </summary>
        /// <param name="aInput">Used to determine what structureDivisionSensor to retrieve.</param>
        /// <returns>Returns the structureDivisionSensor if found, null otherwise.</returns>
        public GetStructureDivisionSensorOutput GetStructureDivisionSensorWithInclude(GetStructureDivisionSensorInput aInput)
        {
            StructureDivisionSensorEntity structureDivisionSensor = _StructureDivisionSensorRepository.GetWithInclude(aInput.Id);
            return structureDivisionSensor == null ? null : Mapper.Map<GetStructureDivisionSensorOutput>(structureDivisionSensor);
        }

        /// <summary>
        ///     Retrieves the structureDivisionSensors within the system
        /// </summary>
        /// <param name="aInput">Filter of which structureDivisionSensors to retrieve.</param>
        /// <returns>Returns the structureDivisionSensors within the system.</returns>
        public GetStructureDivisionSensorsOutput GetStructureDivisionSensors(GetStructureDivisionSensorsInput aInput)
        {
            List<StructureDivisionSensorEntity> structureDivisionSensors = _StructureDivisionSensorRepository.GetAllList();

            return new GetStructureDivisionSensorsOutput {StructureDivisionSensors = Mapper.Map<List<StructureDivisionSensorDto>>(structureDivisionSensors)};
        }

        /// <summary>
        ///     Retrieves the structureDivisionSensors within the system and also retrieves the
        ///     referenced entities
        /// </summary>
        /// <param name="aInput">Filter of which structureDivisionSensors to retrieve.</param>
        /// <returns>Returns the structureDivisionSensors within the system.</returns>
        public GetStructureDivisionSensorsOutput GetStructureDivisionSensorsWithInclude(GetStructureDivisionSensorsInput aInput)
        {
            List<StructureDivisionSensorEntity> structureDivisionSensors = _StructureDivisionSensorRepository.GetAllWithInclude()
                                                                                                             .ToList();

            return new GetStructureDivisionSensorsOutput {StructureDivisionSensors = Mapper.Map<List<StructureDivisionSensorDto>>(structureDivisionSensors)};
        }

        /// <summary>
        ///     Insert the new structureDivisionSensor
        /// </summary>
        /// <param name="aInput">StructureDivisionSensor information to input.</param>
        public void InsertStructureDivisionSensor(InsertStructureDivisionSensorInput aInput)
        {
            StructureDivisionSensorEntity structureDivisionSensor = new StructureDivisionSensorEntity
            {
                Name = aInput.Name,
                SensorId = aInput.SensorId,
                StructureDivisionId = aInput.StructureDivisionId
            };

            _StructureDivisionSensorRepository.Insert(structureDivisionSensor);
        }

        /// <summary>
        ///     Updates the specified structureDivisionSensors
        /// </summary>
        /// <param name="aInput">Information to update the structureDivisionSensor with</param>
        public void UpdateStructureDivisionSensor(UpdateStructureDivisionSensorInput aInput)
        {
            StructureDivisionSensorEntity structureDivisionSensor = _StructureDivisionSensorRepository.GetWithInclude(aInput.Id);
            
            structureDivisionSensor.Name = aInput.Name;
            structureDivisionSensor.SensorId = aInput.SensorId;
            structureDivisionSensor.StructureDivisionId = aInput.StructureDivisionId;
            _StructureDivisionSensorRepository.Update(structureDivisionSensor);
        }

        #endregion
    }
}