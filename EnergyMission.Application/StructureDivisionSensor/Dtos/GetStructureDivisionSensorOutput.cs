﻿using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.StructureDivisionSensor.Dtos
{
    public class GetStructureDivisionSensorOutput : StructureDivisionEntity, IOutputDto
    {
    }
}