﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EnergyMission.Domain.Entities;

namespace EnergyMission.StructureDivisionSensor.Dtos
{
    public class GetStructureDivisionSensorsOutput : IOutputDto
    {
        #region Public Members

        public List<StructureDivisionSensorEntity> StructureDivisionSensors { get; set; }

        #endregion
    }
}