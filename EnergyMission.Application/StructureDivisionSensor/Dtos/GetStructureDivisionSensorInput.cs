﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EnergyMission.StructureDivisionSensor.Dtos
{
    public class GetStructureDivisionSensorInput : IInputDto
    {
        #region Public Members

        [Required]
        public long Id { get; set; }

        #endregion
    }
}