﻿using System.Reflection;
using Abp.Modules;


namespace EnergyMission
{
    public class EnergyMissionCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
