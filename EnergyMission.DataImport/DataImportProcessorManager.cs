﻿using System;
using System.Collections.Generic;
using EnergyMission.DataPoint;
using EnergyMission.DataPoint.Dtos;

namespace EnergyMission.DataImport
{
    /// <summary>
    ///     Allows for some data to be imported into the data points table
    /// </summary>
    public class DataImportProcessorManager
    {
        /// <summary>
        ///     Processes some data points to import into the database
        /// </summary>
        /// <param name="aStructureDivisionSensorId">Division sensor the data points belong to</param>
        /// <param name="aDataPointImportProcessor">Data processor to use</param>
        /// <param name="aDataPointAppService">Application service to use to update the database.</param>
        /// <param name="aErrorMessage">Contains the error message should any error occur</param>
        /// <returns>Returns TRUE on success, FALSE should there be any error.</returns>
        /// 
       
        public bool Process(long aStructureDivisionSensorId, IDataPointImportProcessor aDataPointImportProcessor, IDataPointAppService aDataPointAppService, out string aErrorMessage)
        {
            //Default behavior
            aErrorMessage = string.Empty;

            //Process the import source
            string errorMessage;
            List<IDataPointMeasurementItem> importItems = new List<IDataPointMeasurementItem>();
            bool result = aDataPointImportProcessor.Import(out importItems, out errorMessage);

            //Should we have processed the import source then go through and update the database
            if (result)
            {
                List<InsertDataPointInput> dataPoints = new List<InsertDataPointInput>();

                foreach (IDataPointMeasurementItem dataPointMeasurementItem in importItems)
                {
                    dataPoints.Add(new InsertDataPointInput
                    {
                        StructureDivisionSensorId = aStructureDivisionSensorId,
                        TimeStamp = dataPointMeasurementItem.TimeStamp,
                        Value = dataPointMeasurementItem.Value
                    });
                }

                //Update the database should we have some data points
                if (dataPoints.Count > 0)
                {
                    try
                    {
                        aDataPointAppService.InsertDataPoints(dataPoints);
                    }
                    catch (Exception ex)
                    {
                        aErrorMessage = ex.Message;
                        result = false;
                    }
                }
            }
            else
            {
                aErrorMessage = errorMessage;
            }

            return result;
        }
    }
}