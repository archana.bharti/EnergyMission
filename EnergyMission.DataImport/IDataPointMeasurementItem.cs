using System;
using EnergyMission.Common.Enums;

namespace EnergyMission.DataImport
{
    /// <summary>
    /// Interface describing the information that needs to be recorded for each data point
    /// for a sensor when it is imported into the system
    /// </summary>
    public interface IDataPointMeasurementItem
    {
        DateTime TimeStamp { get; set; }
        float Value { get; set; }

        DataPointMeasurementEnum Measurement { get; set; }

    }
}