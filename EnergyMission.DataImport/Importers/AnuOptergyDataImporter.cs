﻿using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using EnergyMission.Common.Enums;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using EnergyMission.DataImport;
using EnergyMission.DataImport.Importers;
using EnergyMission.Client;
using EnergyMission.Client.Dtos;
using EnergyMission.Domain.Entities;
using Newtonsoft.Json;
using System.Xml.Serialization;
using System.Text;
using EnergyMission.Data;

namespace EnergyMission.DataImport.Importers
{
    /// <summary>
    ///     Reads a CSV export file from the ANU Opetergy Systm. The file is a CSV file with the following format:
    ///     Yesterday (03/03/2015) Timestamp	BLD 13 - Beryl Rawson Main Incoming Power SATEC - Electricity Meter	BLD 13 - Beryl
    ///     Rawson Main Incoming
    ///     Power SATEC - Electricity Meter Unit
    ///     03-03-15 00:15	4	kWh
    ///     03-03-15 00:30	6	kWh
    /// </summary>
    public class AnuOptergyDataImporter : IDataPointImportProcessor
    {
        private EmDbContext context;
        private readonly string _CsvFileName;
        List<DataPointMeasurementItem> list = new List<DataPointMeasurementItem>();

        public AnuOptergyDataImporter()
        {
            this.context = new EmDbContext();
        }
        /// <summary>
        ///     Class constructor
        /// </summary>
        /// <param name="aCsvFileName">Full name and path to the CSV file containing the data.</param>
        public AnuOptergyDataImporter(string aCsvFileName)
        {
            //Save parameters
            _CsvFileName = aCsvFileName;
        }

        #region Implementation of IDataPointImportProcessor

        /// <summary>
        ///     Internal helper method to construct a sensible and descriptive error message.
        /// </summary>
        /// <param name="aLineNumber">Line number in the file the error occurred on.</param>
        /// <param name="aLineContents">Contents for the line</param>
        /// <param name="aSpecificError">Specific error encountered.</param>
        /// <returns>Returns the well constructed error message</returns>
        private string ConstructErrorMessage(long aLineNumber, string[] aLineContents, string aSpecificError)
        {
            string lineContents = string.Empty;
            foreach (string lineContent in aLineContents)
            {
                if (lineContents != string.Empty)
                {
                    lineContents = "," + lineContents;
                    lineContents += lineContent;
                }
            }

            return string.Format("Line {0}: {1}\n\n{2}", aLineNumber, lineContents, aSpecificError);
        }

        public bool Import(out List<DataPointMeasurementItem> aImportItems, out string aErrorMessage)
        {
            //Default behaviour
            aErrorMessage = string.Empty;
            aImportItems = new List<DataPointMeasurementItem>();

            using (TextReader textReader = File.OpenText(_CsvFileName))
            {
                //textReader.ReadToEnd();

                using (CsvReader csvReader = new CsvReader(textReader))
                {
                    //We have no header record
                    csvReader.Configuration.HasHeaderRecord = false;

                    const int NUMBER_LINES_IGNORE = 2;
                    while (csvReader.Read())
                    {
                        //Skip the first 2 lines as they contain heading data
                        if (csvReader.Row > NUMBER_LINES_IGNORE)
                        {
                            //Field 0: DateTime eg. 04/03/2015 12:15 AM
                            //Field 1: Value eg. 4.0
                            //Field 2: Measurement eg. kWh
                            DateTime columnTimeStampValue;
                            if (!csvReader.TryGetField<DateTime>(0, out columnTimeStampValue))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the timestamp.");
                                return false;
                            }

                            float columnValueValue;
                            if (!csvReader.TryGetField<float>(1, out columnValueValue))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the value.");
                                return false;
                            }

                            string columnValueMeasurement;
                            if (!csvReader.TryGetField<string>(2, out columnValueMeasurement))
                            {
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not convert the measurement.");
                                return false;
                            }

                            //Got this far after reading all the contents out of the record
                            DataPointMeasurementEnum measurementForDataPoint;
                            if (columnValueMeasurement.Trim()
                                                      .ToLower() == "kwh")
                            {
                                measurementForDataPoint = DataPointMeasurementEnum.KiloWattHours;
                            }
                            else
                            {
                                measurementForDataPoint = DataPointMeasurementEnum.Unknown;
                                aErrorMessage = ConstructErrorMessage(csvReader.Row + 1, csvReader.CurrentRecord, "Could not parse the measurement from the string supplied.");
                                return false;
                            }

                            //Got this far so add a new data point item to what we are returning
                            aImportItems.Add(new DataPointMeasurementItem
                            {
                                Measurement = measurementForDataPoint,
                                TimeStamp = columnTimeStampValue,
                                Value = columnValueValue
                            });
                        }
                    }
                }
            }

            return true;
        }
        public void Save(ImporterEntity IImporterEntity)
        {
            List<DataPointMeasurementItem>.Enumerator e = list.GetEnumerator();

            while (e.MoveNext())
            {
                // string rrr = e.Current.Measurement .ToString();

                context.Database.ExecuteSqlCommand("Add_CSVAPIImport @ID=@ID,@Measurment=@Measurment,@TimeStamp=@TimeStamp,@Value=@Value,@StructureDivisionSensorId=@StructureDivisionSensorId",
                 new SqlParameter("@StructureDivisionSensorId", IImporterEntity.StructureDivisionSensorId),
                new SqlParameter("@Measurment", e.Current.Measurement.ToString()),
                new SqlParameter("@TimeStamp", e.Current.TimeStamp),
                new SqlParameter("@Value", e.Current.Value), new SqlParameter("@ID", e.Current.Equals(0)));


            }
        }
        string error = "File Not Right Format";
        public bool ReadCSV(string filename)
        {
            AnuOptergyDataImporter _IDataPointImportProcessor = new AnuOptergyDataImporter(filename);
            return _IDataPointImportProcessor.Import(out list, out error);

        }
        public void CreateXML(Object list)
        {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(list.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, list);
                xmlStream.Position = 0;

                xmlDoc.Load(xmlStream);
                // Save();
                //context.Database.ExecuteSqlCommand("Add_CSVAPIImport", new SqlParameter(list));

            }
        }

    }

        #endregion
}