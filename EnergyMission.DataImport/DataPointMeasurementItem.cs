using System;
using EnergyMission.Common.Enums;

namespace EnergyMission.DataImport
{
    /// <summary>
    ///     Concrete implementation that can be used by the data importers unless they need something custom
    /// </summary>
    public class DataPointMeasurementItem : IDataPointMeasurementItem
    {
        #region Implementation of IDataPointMeasurementItem

        public DateTime TimeStamp { get; set; }

        public float Value { get; set; }
        public long structuredivisionSencorId { get; set; }
        public DataPointMeasurementEnum Measurement { get; set; }

        #endregion
    }
}