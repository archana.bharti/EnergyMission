﻿using EnergyMission.Domain.Entities;
using System;
using System.Collections.Generic;

namespace EnergyMission.DataImport
{
    /// <summary>
    ///     Interface describing the capabilities a data import processor has to support
    /// </summary>
    public interface IDataPointImportProcessor
    {
        bool Import(out List<DataPointMeasurementItem> aImportItems, out string aErrorMessage);
        void Save(ImporterEntity IImporterEntity);
        bool ReadCSV(string filename);
        void CreateXML(Object list);
    }
}