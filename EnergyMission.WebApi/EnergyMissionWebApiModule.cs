﻿using System.Reflection;
using Abp.Application.Services;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;

namespace EnergyMission
{
    [DependsOn(typeof (AbpWebApiModule), typeof (EnergyMissionApplicationModule))]
    public class EnergyMissionWebApiModule : AbpModule
    {
        #region Public Members

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof (EnergyMissionApplicationModule).Assembly, "app")
                .Build();
        }

        #endregion
    }
}