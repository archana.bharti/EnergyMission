﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using AutoMapper;
using EnergyMission.Domain.Entities;
namespace EnergyMission.Data
{
    public static class EnergyMissionDomainModule
    {

       public static  void AutoMapperRegistrations()
       {
           //Entity --> DTO
           Mapper.CreateMap<Address, AddressEntity>();
           Mapper.CreateMap<Organization, OrganizationEntity>();
           Mapper.CreateMap<Address, AddressEntity>();
           Mapper.CreateMap<Country, CountryEntity>();
           Mapper.CreateMap<Client, ClientEntity>();
           Mapper.CreateMap<ClientTeam, TeamEntity>();
           Mapper.CreateMap<ClientTeam, ClientTeamEntity>();
           Mapper.CreateMap<ClientAdministrator, ClientAdministratorEntity>();
           Mapper.CreateMap<Country, CountryEntity>();
           Mapper.CreateMap<ContactDetailsAddress, ContactDetailAddressEntity>();
           Mapper.CreateMap<ContactDetail, ContactDetailEntity>();
           Mapper.CreateMap<DataPoint, DataPointEntity>();
           Mapper.CreateMap<LocationMapping, LocationMappingEntity>();
           Mapper.CreateMap<Person, PersonEntity>();
           Mapper.CreateMap<PostCode, PostCodeEntity>();
           Mapper.CreateMap<Sensor, SensorEntity>();
           Mapper.CreateMap<Site, SiteEntity>();
           Mapper.CreateMap<State, StateEntity>();
           Mapper.CreateMap<Structure, StructureEntity>();
           Mapper.CreateMap<StructureDivisionSensor, StructureDivisionSensorEntity>();
           Mapper.CreateMap<StructureDivision, StructureDivisionEntity>();
           Mapper.CreateMap<Suburb, SuburbEntity>();
           Mapper.CreateMap<ClientTeamManager, ClientTeamManagerEntity>();

           //DTO --> Entity
           Mapper.CreateMap<OrganizationEntity, Organization>();
           Mapper.CreateMap<AddressEntity, Address>();
           Mapper.CreateMap<CountryEntity, Country>();
           Mapper.CreateMap<ClientEntity, Client>();
           Mapper.CreateMap<ClientAdministratorEntity, ClientAdministrator>();
           Mapper.CreateMap<ClientTeamMemberEntity,ClientTeamMember>();
           Mapper.CreateMap<ContactDetailAddressEntity, ContactDetailsAddress>();
           Mapper.CreateMap<ContactDetailEntity, ContactDetail>();
           Mapper.CreateMap<DataPointEntity, DataPoint>();
           Mapper.CreateMap<LocationMappingEntity, LocationMapping>();
           Mapper.CreateMap<PersonEntity, Person>();
           Mapper.CreateMap<PostCodeEntity, PostCode>();
           Mapper.CreateMap<SensorEntity, Sensor>();
           Mapper.CreateMap<SiteEntity, Site>();
           Mapper.CreateMap<StateEntity, State>();
           Mapper.CreateMap<StructureEntity, Structure>();
           Mapper.CreateMap<StructureDivisionSensorEntity, StructureDivisionSensor>();
           Mapper.CreateMap<StructureDivisionEntity, StructureDivision>();
           Mapper.CreateMap<SuburbEntity, Suburb>();
           Mapper.CreateMap<ClientTeamManagerEntity, ClientTeamManager>();
           Mapper.CreateMap<TeamEntity, ClientTeam>();
           Mapper.CreateMap<ClientTeamEntity, ClientTeam>();
           Mapper.CreateMap<ClientTeamMember, ClientTeamMemberEntity>().ReverseMap();
           Mapper.CreateMap<ClientTeamsSensor, ClientTeamSensorsEntity>().ReverseMap();
           Mapper.CreateMap<UserProfileInformation, UserProfileInformationEntity>().ReverseMap();

       }

    }
}
