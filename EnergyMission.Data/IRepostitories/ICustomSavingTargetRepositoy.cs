﻿using EnergyMission.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.IRepostitories
{
    public interface ICustomSavingTargetRepositoy
    {
        CustomSavingTargetEntity getByID(CustomSavingTargetEntity CustomSavingTarget);
        //set GetClientById(long Id); 
        void UpdateClient(CustomSavingTargetEntity CustomSavingTarget);
        void InsertClient(CustomSavingTargetEntity CustomSavingTarget);
        void Save();
    }
}
