﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.IRepostitories
{
   public interface IRepository<T> where T : class
    {
        IEnumerable<T> SelectAll();
        IQueryable<T> SelectAll1();
        T SelectByID(object id);
        void Insert(T obj);
        T Inserts(T obj);
        IQueryable<T> Where(Expression<Func<T, bool>> predicate);
        T Wheres(Expression<Func<T, bool>> predicate);
        void Update(T obj);
        void Delete(object id);
        void Save();
    }
}
