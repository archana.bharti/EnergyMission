﻿using EnergyMission.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.IRepostitories
{
   public interface IStructureDivisionSensorRepository
    {
       Int32 StructureDivisionSensors(StructureDivisionSensorEntity objStructureDivisionSensorEntity, int TeamId);
    }
}
