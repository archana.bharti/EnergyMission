﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.IRepostitories
{
    public interface IChangeLogRepository : IDisposable
    {

        IEnumerable<Address> GetAddresss();
        Address GetAddressID(int Id);
        void InsertStudent(Address AddressEntity);
        void DeleteStudent(int Id);
        void UpdateStudent(Address AddressEntity);
        void Save();

    }
}
