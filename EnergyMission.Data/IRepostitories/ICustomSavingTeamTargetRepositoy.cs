﻿using EnergyMission.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EnergyMission.Data.IRepostitories
{
    public interface ICustomSavingTeamTargetRepositoy
    {
        CustomSavingTeamTargetEntity getByID(CustomSavingTeamTargetEntity CustomSavingTeamTarget);
        void UpdateClient(CustomSavingTeamTargetEntity CustomSavingTeamTarget);
        void InsertClient(CustomSavingTeamTargetEntity CustomSavingTeamTarget);
        void Save();
    }
}
