﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using EnergyMission.Domain.Entities;
namespace EnergyMission.Data.IRepostitories
{
    public interface IClientRepositoy:IDisposable 
    {
        IEnumerable<Client> GetClients(long? Id);
        Client GetClientById(long Id);
        string GetUserID(long Id);
        bool GetClientbyname(string name);
        object GetClientByIds(long Id);
         long InsertClient(Client Cleint);
         string RoleKey(string Role);
        void DeleteClient(long Id);
        int UpdateClient(Organization Cleint);
        void Save();
        IEnumerable<SensorEntity> GetSensorByTeam(long teamId);
       
    }
}
