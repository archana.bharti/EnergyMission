﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.IRepostitories
{
    public interface IAddressRepository : IDisposable
    {

        IEnumerable<ChangeLog> GetAddresss();
        ChangeLog GetAddressID(int Id);
        void InsertStudent(ChangeLog ChangeLog);
        void DeleteStudent(int Id);
        void UpdateStudent(ChangeLog ChangeLog);
        void Save();

    }
}
