﻿using System;
using System.Collections.Generic;
using Abp.Domain.Repositories;
using EnergyMission.Domain.EntitiesDto;

namespace EnergyMission.Data.IRepostitories
{
    public interface IUserRepository : IRepository
    {

        void Delete(string aId);
        List<UserDto> GetAllList();
        UserDto Get(string aId);
        void Update(UserDto aUser);
        void Insert(UserDto aUser);

    }
}