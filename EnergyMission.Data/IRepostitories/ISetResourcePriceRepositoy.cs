﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EnergyMission.Domain.Entities;


namespace EnergyMission.Data.IRepostitories
{
   public interface ISetResourcePriceRepositoy:IDisposable
    {

       //object GetvalueByID(long Id);
       SetResourcePriceEntity getByID(SetResourcePriceEntity SetResourcePrice);
       //set GetClientById(long Id); 
       void UpdateClient(SetResourcePriceEntity SetResourcePrice);
       void InsertClient(SetResourcePriceEntity SetResourcePrice);
       void Save();

    }
}
