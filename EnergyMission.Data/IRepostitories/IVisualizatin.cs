﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnergyMission.Domain.Entities;
namespace EnergyMission.Data.IRepostitories
{
    public interface IVisualizatin : IDisposable
    {

        //List<Visulaization> Getvaisualizaton(long Site, string constpe, int year, int month, int days, bool night, bool day, DateTime? start_date, DateTime? EndDate);

        List<Visulaization> Getvaisualizaton(long Site, long Strct, long StrctDiv, long Team, Boolean elec, Boolean Gas, Boolean Water, int year, int month, int days, bool night, bool day, DateTime? start_date, DateTime? EndDate,long LocationId,long SensorId);
        
        void Save();
        List<SiteEntity> SiteAutoComplete(string text);

    }
}
