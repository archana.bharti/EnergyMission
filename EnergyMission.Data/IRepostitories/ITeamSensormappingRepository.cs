﻿using EnergyMission.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.IRepostitories
{
    public interface ITeamSensormappingRepository
    {
        void MapTeamSensor(long LogginUserId, long TeamId);
        void AddMapTeamSensor(long LogginUserId, long TeamId, long? ClientTeamSendorId);
        List<SensorEntity> getSensorByTeamID(long TeamId);
        List<SensorEntity> getNotMappedSensorListOnTeamID(long TeamId,long CreatedUserId);
        void RemoveTeamSensorMapping(long Id);
        List<SensorEntity> GetSensorListByStructureDivisionId(long StructureDivisionId);
    }
}
