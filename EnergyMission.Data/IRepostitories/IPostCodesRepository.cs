﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.IRepostitories
{
    public interface IPostCodesRepository : IDisposable
    {
        IEnumerable<PostCode> GetPostCodes();

        object GetPostcodeByIds(long Id);
        PostCode GetPostcodeById(long Id);
        PostCode GetPostcodeBypostnumber(string postnumber);
        void InsertPostcode( PostCode postcode);
        void DeletePostcode(long Id);
        void UpdatePostcode(PostCode postcode);
        void Save();

    }
}
