//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EnergyMission.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClientTeamsSensor
    {
        public long Id { get; set; }
        public Nullable<long> ClientTeamSendorId { get; set; }
        public Nullable<long> ClientTeamId { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<long> CreatorUserId { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<System.DateTime> LastModificationTime { get; set; }
        public Nullable<long> LastModifierUserId { get; set; }
    
        public virtual ClientTeam ClientTeam { get; set; }
        public virtual StructureDivisionSensor StructureDivisionSensor { get; set; }
    }
}
