﻿using EnergyMission.Data.IRepostitories;
using EnergyMission.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.Repositories
{
    public class StructureDivisionSensorRepositoryMain : IStructureDivisionSensorRepository
    {
         private EmDbContext context;
         public StructureDivisionSensorRepositoryMain()
        {

            this.context = new EmDbContext();
        }

         public Int32 StructureDivisionSensors(StructureDivisionSensorEntity objStructureDivisionSensorEntity, int TeamId)
        {


            var param = new SqlParameter("@parll", SqlDbType.VarChar)
            {
                Direction = ParameterDirection.Output
            };
            param.Size = 15;


            //var param = new SqlParameter
            //{
            //    ParameterName = "@parll",
            //    DbType = SqlDbType.VarChar,
            //    Direction = System.Data.ParameterDirection.Output
            //};
            param.Size = 40;

            var s = context.Database.ExecuteSqlCommand("Curd_StructureDivisionSensors @SensorId=@SensorId,@StructureDivisionId=@StructureDivisionId,@CreateorUserId=@CreateorUserId,@TeamId=@TeamId,@parll=@parll out",


                               // new SqlParameter("@id", 0),
                                new SqlParameter("SensorId", objStructureDivisionSensorEntity.SensorId),
                                new SqlParameter("StructureDivisionId", objStructureDivisionSensorEntity.StructureDivisionId),
                                new SqlParameter("CreateorUserId", objStructureDivisionSensorEntity.CreatorUserId),
                                 new SqlParameter("TeamId", TeamId),
                                 param
                                );

            string ys = param.Value.ToString();
            
            context.SaveChanges();
            return Convert.ToInt32(ys);
        }
    }
}
