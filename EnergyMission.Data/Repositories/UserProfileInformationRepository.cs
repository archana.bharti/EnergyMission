﻿using System;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    public class UserProfileInformationRepository : EmRepositoryBase<UserProfileInformationEntity>, IUserProfileInformationRepository
    {
        public UserProfileInformationRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<UserProfileInformationEntity,long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<UserProfileInformationEntity> GetAllWithInclude()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public UserProfileInformationEntity GetWithInclude(long aKey)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}