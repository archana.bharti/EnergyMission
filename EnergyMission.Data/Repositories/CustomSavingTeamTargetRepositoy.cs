﻿using EnergyMission.Data.IRepostitories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.SqlClient;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Data.Repositories
{
    public class CustomSavingTeamTargetRepositoy : ICustomSavingTeamTargetRepositoy
    {
        private EmDbContext context;

    public CustomSavingTeamTargetRepositoy()
        {

            this.context = new EmDbContext();
        }
   public void InsertClient(CustomSavingTeamTargetEntity CustomSavingTeamTarget)
        {

            if (CustomSavingTeamTarget.Id == 0 || CustomSavingTeamTarget.Id == null)
            {
                int value = context.Database.ExecuteSqlCommand("Curd_CustomSavingTeamTarget @id=0,@RTElec=@RTElec,@RTGas=@RTGas,@RTWater=@RTWater,@CreatorUserId=@CreatorUserId,@ClientId=@ClientId,@Action='I' ",

                    new SqlParameter("RTElec", CustomSavingTeamTarget.RTElec),
                    new SqlParameter("RTGas", CustomSavingTeamTarget.RTGas),
                    new SqlParameter("RTWater", CustomSavingTeamTarget.RTWater),
                    new SqlParameter("CreatorUserId", CustomSavingTeamTarget.CreatorUserId),
                    new SqlParameter("ClientId", CustomSavingTeamTarget.ClientId));
                context.SaveChanges();

            }
            else
            {
                  int value = context.Database.ExecuteSqlCommand("Curd_CustomSavingTeamTarget @id=@id,@RTElec=@RTElec,@RTGas=@RTGas,@RTWater=@RTWater,@CreatorUserId=@CreatorUserId,@ClientId=@ClientId,@Action='U' ",
                  new SqlParameter("@id", CustomSavingTeamTarget.Id),
                  new SqlParameter("RTElec", CustomSavingTeamTarget.RTElec),
                  new SqlParameter("RTGas", CustomSavingTeamTarget.RTGas),
                  new SqlParameter("RTWater", CustomSavingTeamTarget.RTWater),
                  new SqlParameter("CreatorUserId", CustomSavingTeamTarget.CreatorUserId),
                  new SqlParameter("ClientId", CustomSavingTeamTarget.ClientId));
                context.SaveChanges();


            }


        }

        public void UpdateClient(CustomSavingTeamTargetEntity CustomSavingTeamTarget)
        {
        int value = context.Database.ExecuteSqlCommand("Exchange_CustomSavingTeamTarget @id=@id",
        new SqlParameter("@id", CustomSavingTeamTarget.CreatorUserId));
        context.SaveChanges();
        }

        public CustomSavingTeamTargetEntity getByID(CustomSavingTeamTargetEntity CustomSavingTeamTarget)
        {
        var obj = context.Database.SqlQuery<CustomSavingTeamTargetEntity>("GetValueFromCustomSavingTeamTarget @id=  @id", new SqlParameter("@id", CustomSavingTeamTarget.CreatorUserId)).FirstOrDefault();     
        return obj;
        }

        public void Save()
        {
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}