﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    /// <summary>
    ///     Base class for all repositories in the system. We add any other common functionality here.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TPrimaryKey"></typeparam>
    public class EmRepositoryCommon<TEntity, TPrimaryKey> : EfRepositoryBase<EmDbContext, TEntity, TPrimaryKey> where TEntity : class, IEntity<TPrimaryKey>
    {
        public EmRepositoryCommon(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }
    }

    /// <summary>
    ///     Base class for all repositories in the system. This is implemented as a long for the primary
    ///     key tying in with EmEntityBase
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class EmRepositoryBase<TEntity> : EmRepositoryCommon<TEntity, long> where TEntity : class, IEntity<long>
    {
        public EmRepositoryBase(IDbContextProvider<EmDbContext> aDbContextProvider) : base(aDbContextProvider)
        {
        }
    }
}