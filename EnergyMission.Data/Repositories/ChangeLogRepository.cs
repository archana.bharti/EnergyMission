﻿
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;


namespace EnergyMission.Repositories
{
    public class ChangeLogRepository : EmRepositoryBase<ChangeLogEntity>, IChangeLogRepository
    {
        public ChangeLogRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }
    }
}