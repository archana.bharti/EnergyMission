﻿using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    public class PersonRepository : EmRepositoryBase<PersonEntity>, IPersonRepository
    {
        public PersonRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<PersonEntity,in long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<PersonEntity> GetAllWithInclude()
        {
            return GetAll()
                .Include(e => e.ContactDetail.Addresses);
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public PersonEntity GetWithInclude(long aKey)
        {
            return GetAllWithInclude()
                .First(r => r.Id == aKey);
        }

        #endregion
    }
}