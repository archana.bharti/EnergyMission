﻿using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    public class StructureDivisionSensorRepository : EmRepositoryBase<StructureDivisionSensorEntity>, IStructureDivisionSensorRepository
    {
        public StructureDivisionSensorRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<StructureDivisionSensorEntity,in long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<StructureDivisionSensorEntity> GetAllWithInclude()
        {
            return GetAll()
                .Include(e => e.Sensor)
                .Include(e => e.StructureDivision.Structure.Site.Client);
                //.Include(e => e.StructureDivision.ParentStructureDivision.Structure.Site.Client);
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public StructureDivisionSensorEntity GetWithInclude(long aKey)
        {
            return GetAllWithInclude()
                .Include(e => e.Sensor)
                .Include(e => e.StructureDivision.Structure.Site.Client)
                //.Include(e => e.StructureDivision.ParentStructureDivision.Structure.Site.Client)
                .First(r => r.Id == aKey);
        }

        #endregion
    }
}