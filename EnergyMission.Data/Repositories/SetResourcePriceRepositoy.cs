﻿using EnergyMission.Data.IRepostitories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.SqlClient;
using EnergyMission.Domain.Entities;



namespace EnergyMission.Data.Repositories
{
    public class SetResourcePriceRepositoy : ISetResourcePriceRepositoy
    {

        private EmDbContext context;
        public SetResourcePriceRepositoy()
        {

            this.context = new EmDbContext();
        }


        public void InsertClient(SetResourcePriceEntity SetResourcePrice)
        {

            if (SetResourcePrice.Id == 0 || SetResourcePrice.Id==null)
            {
                int value = context.Database.ExecuteSqlCommand("Curd_SetResourcePrice @id=0,@Elec=@Elec,@ElecCO=@ElecCO,@Gas=@Gas,@GasCo=@GasCo,@Water=@Water,@CreatorUserId=@CreatorUserId,@Action='I' ",


                   // new SqlParameter("@id", 0),
                    new SqlParameter("Elec", SetResourcePrice.Elec),
                    new SqlParameter("ElecCO", SetResourcePrice.ElecCO),
                    new SqlParameter("Gas", SetResourcePrice.Gas),
                    new SqlParameter("GasCo", SetResourcePrice.GasCo),
                    new SqlParameter("Water", SetResourcePrice.Water),
                    new SqlParameter("CreatorUserId", SetResourcePrice.CreatorUserId));
                    context.SaveChanges();

            }
            else
            {
                int value = context.Database.ExecuteSqlCommand("Curd_SetResourcePrice @id=@id,@Elec=@Elec,@ElecCO=@ElecCO,@Gas=@Gas,@GasCo=@GasCo,@Water=@Water,@CreatorUserId=@CreatorUserId,@Action='U' ",


                  new SqlParameter("@id", SetResourcePrice.Id),
                  new SqlParameter("Elec", SetResourcePrice.Elec),
                  new SqlParameter("ElecCO", SetResourcePrice.ElecCO),
                  new SqlParameter("Gas", SetResourcePrice.Gas),
                  new SqlParameter("GasCo", SetResourcePrice.GasCo),
                  new SqlParameter("Water", SetResourcePrice.Water),
                               new SqlParameter("CreatorUserId", SetResourcePrice.CreatorUserId));
                  context.SaveChanges();


            }
       
            
        }

        public void UpdateClient(SetResourcePriceEntity SetResourcePrice)
        {
            int value = context.Database.ExecuteSqlCommand("CopydatafromResetToSet @id=@id",

                new SqlParameter("@id", SetResourcePrice.CreatorUserId));

                      


            context.SaveChanges();

        }

        public SetResourcePriceEntity getByID(SetResourcePriceEntity SetResourcePrice)
        {

            var obj = context.Database.SqlQuery<SetResourcePriceEntity>("GetValueFromSetResourcePrice @id=  @id", new SqlParameter("@id", SetResourcePrice.CreatorUserId)).FirstOrDefault();
           // return context.Clients.Select(tt => new { tt.Id, tt.NOE, tt.Name, tt.PostCodeId, tt.StateId, tt.SuperAdminEmail, tt.ClientAdmin, tt.Address, tt.City, tt.Geo_location_Latitude, tt.Geo_location_Longditude }).Where(tt => tt.Id == Id).FirstOrDefault();

            return obj;
        }
        public void Save()
        {
           

        }



        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
