﻿using EnergyMission.Data.IRepostitories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.SqlClient;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Data.Repositories
{
    public class CustomSavingTargetRepositoy : ICustomSavingTargetRepositoy
    {
        private EmDbContext context;

        public CustomSavingTargetRepositoy()
        {

            this.context = new EmDbContext();
        }

        public void InsertClient(CustomSavingTargetEntity CustomSavingTarget)
        {

            if (CustomSavingTarget.Id == 0 || CustomSavingTarget.Id == null)
            {
                int value = context.Database.ExecuteSqlCommand("Curd_CustomSavingTarget @id=0,@RTElec=@RTElec,@RTGas=@RTGas,@RTWater=@RTWater,@CreatorUserId=@CreatorUserId,@Action='I' ",


                   // new SqlParameter("@id", 0),
                    new SqlParameter("RTElec", CustomSavingTarget.RTElec),
                    new SqlParameter("RTGas", CustomSavingTarget.RTGas),
                    new SqlParameter("RTWater", CustomSavingTarget.RTWater),
                    new SqlParameter("CreatorUserId", CustomSavingTarget.CreatorUserId));
                context.SaveChanges();

            }
            else
            {
                int value = context.Database.ExecuteSqlCommand("Curd_CustomSavingTarget @id=@id,@RTElec=@RTElec,@RTGas=@RTGas,@RTWater=@RTWater,@CreatorUserId=@CreatorUserId,@Action='U' ",


                  new SqlParameter("@id", CustomSavingTarget.Id),
                  new SqlParameter("RTElec", CustomSavingTarget.RTElec),
                  new SqlParameter("RTGas", CustomSavingTarget.RTGas),
                  new SqlParameter("RTWater", CustomSavingTarget.RTWater),

                new SqlParameter("CreatorUserId", CustomSavingTarget.CreatorUserId));
                context.SaveChanges();


            }


        }

        public void UpdateClient(CustomSavingTargetEntity CustomSavingTarget)
        {
            int value = context.Database.ExecuteSqlCommand("Exchange_CustomSavingTarget @id=@id",

                new SqlParameter("@id", CustomSavingTarget.CreatorUserId));




            context.SaveChanges();

        }

        public CustomSavingTargetEntity getByID(CustomSavingTargetEntity CustomSavingTarget)
        {

            var obj = context.Database.SqlQuery<CustomSavingTargetEntity>("GetValueFromCustomSavingTarget @id=  @id", new SqlParameter("@id", CustomSavingTarget.CreatorUserId)).FirstOrDefault();
            // return context.Clients.Select(tt => new { tt.Id, tt.NOE, tt.Name, tt.PostCodeId, tt.StateId, tt.SuperAdminEmail, tt.ClientAdmin, tt.Address, tt.City, tt.Geo_location_Latitude, tt.Geo_location_Longditude }).Where(tt => tt.Id == Id).FirstOrDefault();

            return obj;
        }
        public void Save()
        {


        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
