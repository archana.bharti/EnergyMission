﻿using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    public class LocationMappingRepository : EmRepositoryBase<LocationMappingEntity>, ILocationMappingRepository
    {
        public LocationMappingRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of ILocationMappingRepository

        /// <summary>
        ///     Overloaded delete method using all the primary keys
        /// </summary>
        /// <param name="aCountryId">Unique identifier for the country</param>
        /// <param name="aPostCodeId">Unique identifier for the post code</param>
        /// <param name="aStateId">Unique identifier for the state</param>
        /// <param name="aSuburbId">Unique identifier for the suburb</param>
        public void Delete(long aCountryId, long aPostCodeId, long aStateId, long aSuburbId)
        {
            LocationMappingEntity locationMapping = Get(aCountryId, aPostCodeId, aStateId, aSuburbId);
            if (locationMapping != null)
                Delete(locationMapping);
        }

        /// <summary>
        ///     Retrieves the record identified by the primary key parameters passed
        /// </summary>
        /// <param name="aCountryId">Country identifier</param>
        /// <param name="aPostCodeId">Postcode identifier</param>
        /// <param name="aStateId">State identifier</param>
        /// <param name="aSuburbId">Suburb identifier</param>
        /// <returns>Returns the record if found, NULL otherwise</returns>
        public LocationMappingEntity Get(long aCountryId, long aPostCodeId, long aStateId, long aSuburbId)
        {
            return Query(q => q.Where(p => p.CountryId == aCountryId && p.PostCodeId == aPostCodeId && p.StateId == aStateId && p.SuburbId == aSuburbId))
                .FirstOrDefault();
        }

        #endregion

        #region Implementation of IEmRepositoryBase<LocationMappingEntity,long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<LocationMappingEntity> GetAllWithInclude()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public LocationMappingEntity GetWithInclude(long aKey)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}