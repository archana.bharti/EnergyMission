﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnergyMission.Data.IRepostitories;
using System.Data.Entity;

using System.Data.SqlClient;
using EnergyMission.Domain.Entities;

namespace EnergyMission.Data.Repositories
{
  public  class ClientRepositoy: IClientRepositoy
    {


         private EmDbContext context;

         public ClientRepositoy()
        {
            this.context = new EmDbContext();
        }

        public IEnumerable<Client> GetClients(long? Id)
        {
           if(Id==0)
            return context.Clients.Where(tt=>tt.IsDeleted==false).ToList();
           else
            return context.Clients.Where(tt => tt.IsDeleted == false && tt.Id==Id).ToList();


        }


        public Client GetClientById(long Id)
        {
            return context.Clients.Find(Id);
        }
        public IEnumerable<SensorEntity> GetSensorByTeam(long Id)
        {
            var TeamID=Id;
            return context.Database.SqlQuery<SensorEntity>("[GetSensorByTeamId] @teamID={0}", TeamID).ToList();
        }
        
        public object GetClientByIds(long Id)
        {
            return context.Organizations.Select(tt => new { tt.Id, tt.NOE, tt.Name,postcode= tt.PostCode.PostCodeNumber,statename= tt.State.Name, tt.SuperAdminEmail, tt.Address, tt.City, tt.Geo_location_Latitude, tt.Geo_location_Longditude, tt.ClientId }).Where(tt => tt.ClientId == Id).FirstOrDefault();
        }
        public bool GetClientbyname(string  Name)
        {
            bool flag = true;
            int value= context.Clients.Where(tt=>tt.Name==Name && tt.IsDeleted==false).Count();
            if (value >= 1)
                flag = false;
            return flag;
        }

        public string GetUserID(long Id)
        {

        return   context.AspNetUsers.Where(tt => tt.UserProfileInformation.ClientId == Id).Select(tt=>tt.Id).FirstOrDefault();
        
        }
        public long  InsertClient(Client Client)
        {
            Client.CreationTime = DateTime.Now;
            //int value = context.Database.ExecuteSqlCommand("addEditClient @id=0,@parentCleintId=@parentCleintId,@name=@name,@status=@status,@userid=@userid,@address=@address,@City=@City,@NOE=@NOE,@Stateid=@Stateid,@PostCodeId=@PostCodeId,@Geo_location_Latitude=@Geo_location_Latitude,@Geo_location_Longditude=@Geo_location_Longditude,@SuperAdminEmail=@SuperAdminEmail,@ClientAdmin=@ClientAdmin,@Action='I'",
            //                      new SqlParameter("parentCleintId", Client.ParentClientId),
            //                     new SqlParameter("name", Client.Name),
            //      new SqlParameter("status", Client.Status),
            //       new SqlParameter("userid", Client.CreatorUserId),
            //      new SqlParameter("address", Client.Address),
            //                  new SqlParameter("City", Client.City),
            //                        new SqlParameter("NOE", Client.NOE),
            //                           new SqlParameter("Stateid", Client.StateId),
            //                                  new SqlParameter("PostCodeId", Client.PostCodeId),
            //                           new SqlParameter("Geo_location_Latitude", Client.Geo_location_Latitude),
            //                           new SqlParameter("Geo_location_Longditude", Client.Geo_location_Longditude),
            //                          new SqlParameter("SuperAdminEmail", Client.SuperAdminEmail),
            //                         new SqlParameter("ClientAdmin", Client.ClientAdmin));
            context.Clients.Add(Client);
            context.SaveChanges();
            return Client.Id;
            //return value;
        }

        public string RoleKey(string rolename)
        {
            return context.AspNetRoles.Where(tt => tt.Name == rolename).Select(tt => tt.Id).FirstOrDefault();
        
        
        }
        public void DeleteClient(long Id)
        {
            Client Client = context.Clients.Find(Id);
            Client.IsDeleted = true;
            Client.LastModificationTime = DateTime.Now;
            context.Entry(Client).State = EntityState.Modified;
        }

        public int  UpdateClient(Organization Client)
        {


            Client.CreatorUserId = 0;
            if (Client.Address == null)
                Client.Address = "";

           

            if (Client.City == null)
                Client.City = "";

            if (Client.Geo_location_Latitude == null)
                Client.Geo_location_Latitude = "";
            if(Client.SuperAdminEmail==null)
                Client.SuperAdminEmail="";
            if (Client.Geo_location_Longditude == null)
                Client.Geo_location_Longditude = "";
           
            int value =0;

        

            if (Client.Id == 0)
            {
                if (Client.StateId == null && Client.PostCodeId == null)
                    value = context.Database.ExecuteSqlCommand("addEditClient @id=@id, @parentCleintId=@parentCleintId, @name=@name,@userid=@userid,@address=@address,@City=@City,@NOE=@NOE,@Geo_location_Latitude=@Geo_location_Latitude,@Geo_location_Longditude=@Geo_location_Longditude,@SuperAdminEmail=@SuperAdminEmail,@Action='I'",

                                          new SqlParameter("@id", Client.Id),
                                               new SqlParameter("@parentCleintId", Client.ClientId),
                                   new SqlParameter("name", Client.Name),
                     new SqlParameter("userid", Client.CreatorUserId),
                    new SqlParameter("address", Client.Address),
                                new SqlParameter("City", Client.City),
                                      new SqlParameter("NOE", Client.NOE),
                                         new SqlParameter("Geo_location_Latitude", Client.Geo_location_Latitude),
                                         new SqlParameter("Geo_location_Longditude", Client.Geo_location_Longditude),
                                        new SqlParameter("SuperAdminEmail", Client.SuperAdminEmail));


                else if (Client.StateId != null && Client.PostCodeId == null)


                    value = context.Database.ExecuteSqlCommand("addEditClient @id=@id,@name=@name,@userid=@userid,@address=@address,@City=@City,@NOE=@NOE,@Geo_location_Latitude=@Geo_location_Latitude,@Geo_location_Longditude=@Geo_location_Longditude,@SuperAdminEmail=@SuperAdminEmail,@Action='I',StateId=@StateId",
                      new SqlParameter("@id", Client.Id),
     new SqlParameter("StateId", Client.StateId),
               new SqlParameter("name", Client.Name),

    new SqlParameter("userid", Client.CreatorUserId),
    new SqlParameter("address", Client.Address),
            new SqlParameter("City", Client.City),
                  new SqlParameter("NOE", Client.NOE),
                     new SqlParameter("Geo_location_Latitude", Client.Geo_location_Latitude),
                     new SqlParameter("Geo_location_Longditude", Client.Geo_location_Longditude),
                    new SqlParameter("SuperAdminEmail", Client.SuperAdminEmail));


                else

                    value = context.Database.ExecuteSqlCommand("addEditClient @id=@id,@name=@name,@userid=@userid,@address=@address,@City=@City,@NOE=@NOE,@Geo_location_Latitude=@Geo_location_Latitude,@Geo_location_Longditude=@Geo_location_Longditude,@SuperAdminEmail=@SuperAdminEmail,@Action='I',@StateId=@StateId,@PostCodeId=@PostCodeId",
                        new SqlParameter("@id", Client.Id),

                 new SqlParameter("name", Client.Name),

    new SqlParameter("userid", Client.CreatorUserId),
    new SqlParameter("address", Client.Address),
              new SqlParameter("City", Client.City),
                    new SqlParameter("NOE", Client.NOE),
                       new SqlParameter("Geo_location_Latitude", Client.Geo_location_Latitude),
                       new SqlParameter("Geo_location_Longditude", Client.Geo_location_Longditude),
                      new SqlParameter("SuperAdminEmail", Client.SuperAdminEmail),
                     new SqlParameter("ClientAdmin", ""),
                      new SqlParameter("StateId", Client.StateId),
            new SqlParameter("PostCodeId", Client.PostCodeId));

            }

            else

            {

                if (Client.StateId == null && Client.PostCodeId == null)
                    value = context.Database.ExecuteSqlCommand("addEditClient @id=@id, @parentCleintId=@parentCleintId, @name=@name,@userid=@userid,@address=@address,@City=@City,@NOE=@NOE,@Geo_location_Latitude=@Geo_location_Latitude,@Geo_location_Longditude=@Geo_location_Longditude,@SuperAdminEmail=@SuperAdminEmail,@Action='U'",

                                          new SqlParameter("@id", Client.Id),
                                               new SqlParameter("@parentCleintId", Client.ClientId),
                                   new SqlParameter("name", Client.Name),
                     new SqlParameter("userid", Client.CreatorUserId),
                    new SqlParameter("address", Client.Address),
                                new SqlParameter("City", Client.City),
                                      new SqlParameter("NOE", Client.NOE),
                                         new SqlParameter("Geo_location_Latitude", Client.Geo_location_Latitude),
                                         new SqlParameter("Geo_location_Longditude", Client.Geo_location_Longditude),
                                        new SqlParameter("SuperAdminEmail", Client.SuperAdminEmail));


                else if (Client.StateId != null && Client.PostCodeId == null)


                    value = context.Database.ExecuteSqlCommand("addEditClient @id=@id,@name=@name,@userid=@userid,@address=@address,@City=@City,@NOE=@NOE,@Geo_location_Latitude=@Geo_location_Latitude,@Geo_location_Longditude=@Geo_location_Longditude,@SuperAdminEmail=@SuperAdminEmail,@Action='U',StateId=@StateId",
                      new SqlParameter("@id", Client.Id),
    new SqlParameter("StateId", Client.StateId),
               new SqlParameter("name", Client.Name),

    new SqlParameter("userid", Client.CreatorUserId),
    new SqlParameter("address", Client.Address),
            new SqlParameter("City", Client.City),
                  new SqlParameter("NOE", Client.NOE),
                     new SqlParameter("Geo_location_Latitude", Client.Geo_location_Latitude),
                     new SqlParameter("Geo_location_Longditude", Client.Geo_location_Longditude),
                    new SqlParameter("SuperAdminEmail", Client.SuperAdminEmail));


                else

                    value = context.Database.ExecuteSqlCommand("addEditClient @id=@id,@name=@name,@userid=@userid,@address=@address,@City=@City,@NOE=@NOE,@Geo_location_Latitude=@Geo_location_Latitude,@Geo_location_Longditude=@Geo_location_Longditude,@SuperAdminEmail=@SuperAdminEmail,@Action='U',@StateId=@StateId,@PostCodeId=@PostCodeId",
                        new SqlParameter("@id", Client.Id),

                 new SqlParameter("name", Client.Name),

    new SqlParameter("userid", Client.CreatorUserId),
    new SqlParameter("address", Client.Address),
              new SqlParameter("City", Client.City),
                    new SqlParameter("NOE", Client.NOE),
                       new SqlParameter("Geo_location_Latitude", Client.Geo_location_Latitude),
                       new SqlParameter("Geo_location_Longditude", Client.Geo_location_Longditude),
                      new SqlParameter("SuperAdminEmail", Client.SuperAdminEmail),
                     new SqlParameter("ClientAdmin", ""),
                      new SqlParameter("StateId", Client.StateId),
            new SqlParameter("PostCodeId", Client.PostCodeId));

            
            }
                return value;
            //Client.LastModificationTime = DateTime.Now;
            //context.Entry(Client).State = EntityState.Modified;
           
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}
