﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnergyMission.Data.IRepostitories;
using System.Data.Entity;
namespace EnergyMission.Data.Repositories
{
    public class PostCodesRepository : IPostCodesRepository
    {
    private EmDbContext context;

    public PostCodesRepository()
        {
            this.context = new EmDbContext();
        }

    public IEnumerable<PostCode> GetPostCodes()
        {
            return context.PostCodes.Where(tt=>tt.IsDeleted==false).ToList();
        }


     public PostCode GetPostcodeById(long Id)
        {
            return context.PostCodes.Find(Id);
        }
     public Object GetPostcodeByIds(long Id)
     {
         return context.PostCodes.Where(tt=>tt.Id==Id).Select(tt=>new {tt.PostCodeNumber,tt.Id}).FirstOrDefault();
     }
     public PostCode GetPostcodeBypostnumber(string postnumber)
         {
             return context.PostCodes.Where(tt=>tt.PostCodeNumber==postnumber && tt.IsDeleted==false).FirstOrDefault();
         }
        //public object GetClientByIds(long Id)
        //{
        //    return context.Clients.Select(tt=>new {tt.Id,tt.NOE,tt.Name,tt.PostCodeId,tt.StateId,tt.SuperAdminEmail,tt.ClientAdmin,tt.Address,tt.City,tt.Geo_location_Latitude,tt.Geo_location_Longditude}).Where(tt=>tt.Id==Id).FirstOrDefault();
        //}
     public void InsertPostcode(PostCode postcode)
        {
            postcode.IsDeleted = false;
            postcode.CreationTime = DateTime.Now;
        
            context.PostCodes.Add(postcode);
            Save();
            //return value;
        }

        public void DeletePostcode(long Id)
        {
            PostCode postcode = context.PostCodes.Find(Id);
            postcode.IsDeleted = true;
            postcode.LastModificationTime = DateTime.Now;
            context.Entry(postcode).State = EntityState.Modified;
            Save();
        }
        public void UpdatePostcode(PostCode postcode)
        {
            //PostCode postcode = context.PostCodes.Find(Id);
            //postcode.IsDeleted = true;
            postcode.LastModificationTime = DateTime.Now;
            context.Entry(postcode).State = EntityState.Modified;
            Save();
        }

      
        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
