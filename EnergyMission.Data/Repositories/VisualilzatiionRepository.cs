﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnergyMission.Domain.Entities;
using System.Data.SqlClient;
using EnergyMission.Data.IRepostitories;
namespace EnergyMission.Data.Repositories
{
public    class VisualilzatiionRepository :IVisualizatin,IDisposable
    {
    private EmDbContext db = null;

    public VisualilzatiionRepository()
        {
            this.db = new EmDbContext();
          
        }


    //public List<Visulaization> Getvaisualizaton (long Site, string constpe,int year,int month,int days,bool night,bool day, DateTime? start_date , DateTime? EndDate )
    //{
    

    //    var value = db.Database.SqlQuery<Visulaization>("GetVisualization @site=@site,@constpe=@constpe,@year=@year,@month=@month,@day=@day,@night=@night,@dd=@dd,@startDate=@startDate,@endate=@endate",
    //                                                                        new SqlParameter("site", Site),
    //                                                                        new SqlParameter("constpe", constpe),
    //                                                                        new SqlParameter("year", year),
    //                                                                        new SqlParameter("month", month),
    //                                                                        new SqlParameter("day", days),
    //                                                                        new SqlParameter("night", night),
    //                                                                          new SqlParameter("dd", day),

    //                                                                        new SqlParameter("startDate", start_date ?? System.Data.SqlTypes.SqlDateTime.Null),
    //                                                                         new SqlParameter("endate", EndDate ?? System.Data.SqlTypes.SqlDateTime.Null)).ToList();
    // return value;


    //}

    public List<Visulaization> Getvaisualizaton(long Site, long Strct, long StrctDiv, long Team, Boolean elec, Boolean Gas, Boolean Water, int year, int month, int days, bool night, bool day, DateTime? start_date, DateTime? EndDate,long LocationId,long SensorId)
    {
        try
        {
            var value = db.Database.SqlQuery<Visulaization>("GetVisualization @location=@location,@Sensor=@Sensor, @site=@site,@Strct=@Strct,@StrctDiv=@StrctDiv,@Team=@Team,@elec=@elec,@Gas=@Gas,@Water=@Water,@year=@year,@month=@month,@day=@day,@night=@night,@dd=@dd,@startDate=@startDate,@endate=@endate",
                                                                            new SqlParameter("location", LocationId),
                                                                            new SqlParameter("Sensor", SensorId),
                                                                            new SqlParameter("site", Site),
                                                                            new SqlParameter("Strct", Strct),
                                                                            new SqlParameter("StrctDiv", StrctDiv),
                                                                            new SqlParameter("Team", Team),
                                                                            new SqlParameter("elec", elec),
                                                                            new SqlParameter("Gas", Gas),
                                                                            new SqlParameter("Water", Water),
                                                                            new SqlParameter("year", year),
                                                                            new SqlParameter("month", month),
                                                                            new SqlParameter("day", days),
                                                                            new SqlParameter("night", night),
                                                                            new SqlParameter("dd", day),
                                                                            new SqlParameter("startDate", start_date ?? System.Data.SqlTypes.SqlDateTime.Null),
                                                                            new SqlParameter("endate", EndDate ?? System.Data.SqlTypes.SqlDateTime.Null)).ToList();

        return value;
        } 
        catch
        {
            return new List<Visulaization>();
        }                                         
        
    }

    public List<SiteEntity> SiteAutoComplete(string text)
    {
        List<SiteEntity> objList = db.Database.SqlQuery<SiteEntity>("GetsiteAutoComplete @text=@text",
                                                                                   new SqlParameter("text", text)).ToList();

       return objList;
    }

    public void Save()
    {
        db.SaveChanges();
    }

    private bool disposed = false;

    protected virtual void Dispose(bool disposing)
    {
        if (!this.disposed)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
        this.disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    }
}
