﻿using EnergyMission.Data.IRepostitories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.Repositories
{
   public  class Repository<T> : IRepository<T> where T : class
    {
        private EmDbContext db =null;


        private DbSet<T> table = null;

        public Repository()
        {
            this.db = new EmDbContext();
            table = db.Set<T>();
        }

        public Repository(EmDbContext db)
        {
            this.db = db;
            table = db.Set<T>();
        }

        public virtual IEnumerable<T> Get(
           Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "")
        {



            IQueryable<T> query = table;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }
        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return table.Where(predicate).AsQueryable();
        }
        public T Wheres(Expression<Func<T, bool>> predicate)
        {
            return table.Where(predicate).FirstOrDefault();
        }
        public IEnumerable<T> SelectAll()
        {
            return table.ToList();
        }

        public IQueryable<T> SelectAll1()
        {
            return table;
        }
        public T SelectByID(object id)
        {
            return table.Find(id);
        }
        public void Insert(T obj)
        {
           
            table.Add(obj);
            db.SaveChanges();
        }
        public T Inserts(T obj)
        {

            table.Add(obj);
            db.SaveChanges();
           return obj;
        }
        public void Update(T obj)
        {
            table.Attach(obj);
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
       
        public void Delete(object id)
        {
            T existing = table.Find(id);
     
            db.Entry(existing).State = System.Data.Entity.EntityState.Modified;
            table.Remove(existing);
            db.SaveChanges();
        }
        public void Save()
        {
            db.SaveChanges();
        }


    }
}
