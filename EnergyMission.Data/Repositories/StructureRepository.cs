﻿using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;
using System.Data.SqlClient;
using System;

namespace EnergyMission.Repositories
{
    public class StructureRepository : EmRepositoryBase<StructureEntity>, IStructureRepository
    {
        public StructureRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<StructureEntity,long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<StructureEntity> GetAllWithInclude()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        /// 
        public bool UpdateStructure(StructureEntity t)
        { 
            bool flag=false;

            var SiteId = new SqlParameter("SiteID", t.SiteId);

    var name = new SqlParameter("Name", t.Name) ;
    
    var LastModificationTime = new SqlParameter("up_date", DateTime.Now); 
    //var up_By = new SqlParameter("User", "5ded88b5-6e69-4388-ba5b-e412151150c3"); 
     var id  = new SqlParameter("id", t.Id); 


      EmDbContext dbs =new EmDbContext();
       int value =dbs.Database.ExecuteSqlCommand("Update Structures set SiteId=@SiteID ,Name=@name,LastModificationTime=@up_date where id=@id",
          
           SiteId,name,LastModificationTime,id);

       if (value >= 1)
           flag = true;


       return flag;
        
        }
        public StructureEntity GetWithInclude(long aKey)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}