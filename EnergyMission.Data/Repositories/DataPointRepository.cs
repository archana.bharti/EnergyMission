﻿using System.Linq;
using Abp.EntityFramework;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using EnergyMission.EntityFramework;

namespace EnergyMission.Repositories
{
    public class DataPointRepository : EmRepositoryBase<DataPointEntity>, IDataPointRepository
    {
        public DataPointRepository(IDbContextProvider<EmDbContext> aDbContextProvider)
            : base(aDbContextProvider)
        {
        }

        #region Implementation of IEmRepositoryBase<DataPointEntity,long>

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        public IQueryable<DataPointEntity> GetAllWithInclude()
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        public DataPointEntity GetWithInclude(long aKey)
        {
            throw new System.NotImplementedException();
        }

        #endregion
    }
}