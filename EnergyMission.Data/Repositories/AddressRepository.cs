﻿using System.Linq;
using EnergyMission.Domain.Entities;
using EnergyMission.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace EnergyMission.Data
{
    public class AddressRepository :IAddressRepository,IDisposable

    {


        private EmDbContext context;
        public AddressRepository(EmDbContext aDbContextProvider)
          
        {

            this.context = aDbContextProvider;
        }

          public IEnumerable<Address> GetStudents()
        {
            return context.Addresses.ToList();
        }

        public Address GetStudentByID(int id)
        {
            return context.Addresses.Find(id);
        }

        public void InsertAddress(Address Address)
        {
            context.Addresses.Add(Address);
        }

        public void DeleteAddress(int Id)
        {
           Address Address = context.Addresses.Find(Id);
           Address.IsDeleted=true;
           Address.LastModificationTime=DateTime.Now;
           context.Entry(Address).State = EntityState.Modified;
            //context.Addresses.Remove(student);
        }

        public void Update(Address Address)
        {
            context.Entry(Address).State = EntityState.Modified;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }  
}