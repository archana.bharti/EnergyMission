﻿using EnergyMission.Data.IRepostitories;
using EnergyMission.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyMission.Data.Repositories
{
   public class TeamSensorMappingRepostory:ITeamSensormappingRepository
    {
        private EmDbContext context;
        public TeamSensorMappingRepostory()
        {

            this.context = new EmDbContext();
        }
        public void RemoveTeamSensorMapping(long Id)
        {
            int effected = context.Database.ExecuteSqlCommand("DeleteTeamSensorMapping @Id=@Id",
                                                  new SqlParameter("Id", Id));

        }
       public void  MapTeamSensor(long LoginUserId,long TeamId)
        {
            List<ClientTeamSensorsEntity> value = context.Database.SqlQuery<ClientTeamSensorsEntity>("MapTeamSensor @LoginUserId=@LoginUserId,@TeamId=@TeamId",
                                                  new SqlParameter("LoginUserId", LoginUserId),
                                                   new SqlParameter("TeamId", TeamId)).ToList();



            foreach (var list in value)
            {
                AddMapTeamSensor(LoginUserId, TeamId, list.ClientTeamSendorId);
            }
                                      

            context.SaveChanges();
        }
        public void AddMapTeamSensor(long LogginUserId, long TeamId, long? ClientTeamSendorId)
        {
            int effected = context.Database.ExecuteSqlCommand("AddMapTeamSensor @LogginUserId=@LogginUserId,@TeamId=@TeamId,@ClientTeamSendorId=@ClientTeamSendorId",
                                                            new SqlParameter("LogginUserId", LogginUserId),
                                                               new SqlParameter("TeamId", TeamId),
                                                                   new SqlParameter("ClientTeamSendorId", ClientTeamSendorId));

        }
        public List<SensorEntity> GetSensorListByStructureDivisionId(long StructureDivisionId)
        {
            List<SensorEntity> value = context.Database.SqlQuery<SensorEntity>("GetSensorListByStructureDivisionId @StructureDivisionId=@StructureDivisionId",
                                                  new SqlParameter("StructureDivisionId", StructureDivisionId)).ToList();

            return value;
        }
        public List<SensorEntity> getSensorByTeamID(long TeamId)
           {
               List<SensorEntity> value = context.Database.SqlQuery<SensorEntity>("GetListOfSensorByTeamId @Id=@Id",
                                                     new SqlParameter("Id", TeamId)).ToList();
               return value;
                                                   
           }

        public List<SensorEntity> getNotMappedSensorListOnTeamID(long TeamId, long CreatedUserId)
        {
            List<SensorEntity> value = context.Database.SqlQuery<SensorEntity>("GetNotMappedListOfSensorByTeamId @Id=@Id,@LoginUserId=@LoginUserId",
                                                  new SqlParameter("Id", TeamId),
                                                      new SqlParameter("LoginUserId", CreatedUserId)).ToList();
            return value;

        }
    }
}
