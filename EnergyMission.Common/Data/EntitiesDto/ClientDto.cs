﻿using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class ClientDto : EmEntityDtoBase
    {
        public new long? Id { get; set; }

        public long? ParentClientId { get; set; }

        [ForeignKey("ParentClientId")]
        public virtual ClientDto ParentClient { get; set; }

        public string Name { get; set; }
    }
}