﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class ClientTeamMemberDto : EmEntityDtoBase
    {
        [ForeignKey("ClientId")]
        public virtual ClientDto Client { get; set; }

        [Column(Order = 1)]
        [Key]
        [Required(ErrorMessage = "Required.")]
        public long ClientId { get; set; }

        [ForeignKey("PersonId")]
        public virtual PersonDto Person { get; set; }

        [Column(Order = 2)]
        [Key]
        [Required(ErrorMessage = "Required.")]
        public long PersonId { get; set; }
    }
}