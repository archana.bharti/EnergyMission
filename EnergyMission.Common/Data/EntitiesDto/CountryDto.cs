﻿using System.ComponentModel.DataAnnotations;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class CountryDto : EmEntityDtoBase
    {
        public new long? Id { get; set; }

        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(3, ErrorMessage = "Maximum of 3 characters.")]
        [Required(ErrorMessage = "Required.")]
        public string AbbreviationCode { get; set; }

        [Required]
        public int InternationalCountryDialCode { get; set; }

        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }
    }
}