﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Enums;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class ContactDetailAddressDto : EmEntityDtoBase
    {
        #region Public Members

        public virtual AddressDto Address { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("Address")]
        public long AddressId { get; set; }

        public AddressKindEnum AddressKind { get; set; }

        [ForeignKey("ContactDetailsId")]
        public virtual ContactDetailDto ContactDetail { get; set; }

        [Key]
        [Column(Order = 2)]
        public long ContactDetailsId { get; set; }

        #endregion
    }
}