using System;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class DataPointDto : EmEntityDtoBase
    {
        [ForeignKey("StructureDivisionSensorId")]
        public virtual StructureDivisionSensorDto StructureDivisionSensor { get; set; }

        public long StructureDivisionSensorId { get; set; }

        public DateTime TimeStamp { get; set; }
        public double Value { get; set; }
    }
}