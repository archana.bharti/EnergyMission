﻿using System.ComponentModel.DataAnnotations;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class SensorDto : EmEntityDtoBase
    {
        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }

    }
}