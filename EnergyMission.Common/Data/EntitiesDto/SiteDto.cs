﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class SiteDto : EmEntityDtoBase
    {
        [ForeignKey("ParentSiteId")]
        public virtual SiteDto ParentSite { get; set; }

        public long? ParentSiteId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public long ClientId { get; set; }

        [ForeignKey("ClientId")]
        public ClientDto Client { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }
    }
}