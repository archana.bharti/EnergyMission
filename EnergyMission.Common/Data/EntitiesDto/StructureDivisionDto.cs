﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class StructureDivisionDto : EmEntityDtoBase
    {
        [ForeignKey("ParentStructureDivisionId")]
        public virtual StructureDivisionDto ParentStructureDivision { get; set; }

        public long? ParentStructureDivisionId { get; set; }

        [ForeignKey("StructureId")]
        public virtual StructureDto Structure { get; set; }

        public long StructureId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }
    }
}