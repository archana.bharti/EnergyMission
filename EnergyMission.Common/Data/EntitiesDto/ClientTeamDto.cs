using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class ClientTeamDto : EmEntityDtoBase
    {
        [ForeignKey("ClientId")]
        public virtual ClientDto Client { get; set; }

        public long ClientId { get; set; }
        public virtual ICollection<ClientTeamMemberDto> Members { get; set; }

        public string Name { get; set; }

        [ForeignKey("ParentClientTeamId")]
        public virtual ClientTeamDto ParentClientTeam { get; set; }

        public long? ParentClientTeamId { get; set; }

        public long PersonTeamManagerId { get; set; }

        [ForeignKey("TeamContactDetailsId")]
        public virtual ContactDetailDto TeamContactDetail { get; set; }

        public long TeamContactDetailsId { get; set; }

        [ForeignKey("PersonTeamManagerId")]
        public virtual PersonDto TeamManager { get; set; }

    }
}