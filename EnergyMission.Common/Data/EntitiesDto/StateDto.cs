﻿using System.ComponentModel.DataAnnotations;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class StateDto : EmEntityDtoBase
    {
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(3, ErrorMessage = "Maximum of 3 characters.")]
        [Required(ErrorMessage = "Required.")]
        public string AbbreviationCode { get; set; }

        [MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        [Required(ErrorMessage = "Required.")]
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        public string Name { get; set; }
    }
}