﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class StructureDto : EmEntityDtoBase
    {
        #region Public Members

        [ForeignKey("ParentStructureId")]
        public virtual StructureDto ParentStructure { get; set; }

        public long? ParentStructureId { get; set; }

        [ForeignKey("SiteId")]
        public virtual SiteDto Site { get; set; }

        [Required(ErrorMessage = "Required.")]
        public long SiteId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }


        #endregion
    }
}