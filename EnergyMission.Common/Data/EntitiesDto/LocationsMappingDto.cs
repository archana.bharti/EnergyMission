﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class LocationMappingDto : EmEntityDtoBase
    {
        public new long? Id { get; set; }


        [ForeignKey("CountryId")]
        public virtual CountryDto Country { get; set; }

        [Column(Order = 1)]
        [Key]
        public long CountryId { get; set; }

        [ForeignKey("PostCodeId")]
        public virtual PostCodeDto PostCode { get; set; }

        [Key]
        [Column(Order = 2)]
        public long PostCodeId { get; set; }

        [ForeignKey("StateId")]
        public virtual StateDto State { get; set; }

        [Key]
        [Column(Order = 3)]
        public long StateId { get; set; }

        [ForeignKey("SuburbId")]
        public virtual SuburbDto Suburb { get; set; }

        [Column(Order = 4)]
        [Key]
        public long SuburbId { get; set; }
    }
}