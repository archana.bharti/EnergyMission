﻿using System.ComponentModel.DataAnnotations;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class PostCodeDto : EmEntityDtoBase
    {
        [Required(ErrorMessage = "Required.")]
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(10, ErrorMessage = "Maximum of 10 characters.")]
        public string PostCodeNumber { get; set; }
    }
}