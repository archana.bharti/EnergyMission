﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class StructureDivisionSensorDto : EmEntityDtoBase
    {
        #region Public Members


        [ForeignKey("SensorId")]
        public virtual SensorDto Sensor { get; set; }

        public long SensorId { get; set; }

        [ForeignKey("StructureDivisionId")]
        public virtual StructureDivisionDto StructureDivision { get; set; }

        public long StructureDivisionId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }

        #endregion
    }
}