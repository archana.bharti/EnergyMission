using System.Collections.Generic;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.EntitiesDto
{
    public class ContactDetailDto : EmEntityDtoBase
    {
        #region Public Members

        public virtual ICollection<ContactDetailAddressDto> Addresses { get; set; }

        public string Dummy { get; set; }

        #endregion
    }
}