﻿using Abp.Domain.Repositories;
using EnergyMission.Common.Data.Entities;

namespace EnergyMission.Common.Data.Repositories
{
    public interface IStructureDivisionRepository : IRepository<StructureDivisionTableModel, long>
    {
    }
}