﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("ClientTeams", Schema = "dbo")]
    public class ClientTeamTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("ClientId")]
        public virtual ClientTableModel Client { get; set; }

        public long ClientId { get; set; }
        public virtual ICollection<ClientTeamMemberTableModel> Members { get; set; }

        public string Name { get; set; }

        [ForeignKey("ParentClientTeamId")]
        public virtual ClientTeamTableModel ParentClientTeam { get; set; }

        public long? ParentClientTeamId { get; set; }

        public long PersonTeamManagerId { get; set; }

        [ForeignKey("TeamContactDetailsId")]
        public virtual ContactDetailTableModel TeamContactDetail { get; set; }

        public long TeamContactDetailsId { get; set; }

        [ForeignKey("PersonTeamManagerId")]
        public virtual PersonTableModel TeamManager { get; set; }

        #endregion
    }
}