﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("DataPoints", Schema = "dbo")]
    public class DataPointTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("StructureDivisionSensorId")]
        public virtual StructureDivisionSensorTableModel StructureDivisionSensor { get; set; }

        public long StructureDivisionSensorId { get; set; }

        public DateTime TimeStamp { get; set; }
        public double Value { get; set; }

        #endregion
    }
}