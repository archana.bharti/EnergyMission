﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("Sites", Schema = "dbo")]
    public class SiteTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("ParentSiteId")]
        public virtual SiteTableModel ParentSite { get; set; }

        public long? ParentSiteId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public long ClientId { get; set; }

        [ForeignKey("ClientId")]
        public ClientTableModel Client { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }


        #endregion
    }
}