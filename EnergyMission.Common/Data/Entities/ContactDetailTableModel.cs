﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("ContactDetails", Schema = "dbo")]
    public class ContactDetailTableModel : EmEntityBase
    {
        public virtual ICollection<ContactDetailAddressTableModel> Addresses { get; set; }

        public string Dummy { get; set; }

    }
}