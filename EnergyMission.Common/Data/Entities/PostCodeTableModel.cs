﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("PostCodes", Schema = "dbo")]
    public class PostCodeTableModel : EmEntityBase
    {
        [Required(ErrorMessage = "Required.")]
        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(10, ErrorMessage = "Maximum of 10 characters.")]
        public string PostCodeNumber { get; set; }
    }
}