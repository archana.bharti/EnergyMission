﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Enums;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("ContactDetailsAddresses", Schema = "dbo")]
    public class ContactDetailAddressTableModel : EmEntityBase
    {
        #region Public Members

        public virtual AddressTableModel Address { get; set; }

        [Key]
        [Column(Order = 1)]
        [ForeignKey("Address")]
        public long AddressId { get; set; }

        public AddressKindEnum AddressKind { get; set; }

        [ForeignKey("ContactDetailsId")]
        public virtual ContactDetailTableModel ContactDetail { get; set; }

        [Key]
        [Column(Order = 2)]
        public long ContactDetailsId { get; set; }

        /// <summary>
        ///     Suppress unique identifier for this entity from base.
        /// </summary>
        [NotMapped]
        public override long Id { get; set; }

        #endregion
    }
}