﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("StructureDivisions", Schema = "dbo")]
    public class StructureDivisionTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("ParentStructureDivisionId")]
        public virtual StructureDivisionTableModel ParentStructureDivision { get; set; }

        public long? ParentStructureDivisionId { get; set; }

        [ForeignKey("StructureId")]
        public virtual StructureTableModel Structure { get; set; }

        public long StructureId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }


        #endregion
    }
}