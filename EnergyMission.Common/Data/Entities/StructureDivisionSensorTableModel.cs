﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("StructureDivisionSensors", Schema = "dbo")]
    public class StructureDivisionSensorTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("SensorId")]
        public virtual SensorTableModel Sensor { get; set; }

        public long SensorId { get; set; }

        [ForeignKey("StructureDivisionId")]
        public virtual StructureDivisionTableModel StructureDivision { get; set; }

        public long StructureDivisionId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }

        #endregion
    }
}