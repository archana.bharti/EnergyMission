﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("Addresses", Schema = "dbo")]
    public class AddressTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("CountryId")]
        public virtual CountryTableModel Country { get; set; }

        public long CountryId { get; set; }


        [MaxLength(50, ErrorMessage = "Max length 50 characters.")]
        [Required(ErrorMessage = "Required.")]
        public string Line1 { get; set; }

        [MaxLength(50, ErrorMessage = "Max length 50 characters.")]
        public string Line2 { get; set; }

        [MaxLength(50, ErrorMessage = "Max length 50 characters.")]
        public string Line3 { get; set; }

        [ForeignKey("PostCodeId")]
        public virtual PostCodeTableModel PostCode { get; set; }

        public long PostCodeId { get; set; }

        [ForeignKey("StateId")]
        public virtual StateTableModel State { get; set; }

        public long StateId { get; set; }

        public virtual SuburbTableModel Suburb { get; set; }

        public long SuburbId { get; set; }

        #endregion
    }
}