﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("Structures", Schema = "dbo")]
    public class StructureTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("ParentStructureId")]
        public virtual StructureTableModel ParentStructure { get; set; }

        public long? ParentStructureId { get; set; }

        [ForeignKey("SiteId")]
        public virtual SiteTableModel Site { get; set; }

        [Required(ErrorMessage = "Required.")]
        public long SiteId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }


        #endregion
    }
}