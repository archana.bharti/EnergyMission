﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Enums;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("Persons", Schema = "dbo")]
    public class PersonTableModel : EmEntityBase
    {
        #region Public Members

        [ForeignKey("ContactDetailsId")]
        public virtual ContactDetailTableModel ContactDetail { get; set; }

        public long ContactDetailsId { get; set; }

        [Required(ErrorMessage = "Required.")]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string NameSuffix { get; set; }
        public TitleEnum Title { get; set; }

        #endregion
    }
}