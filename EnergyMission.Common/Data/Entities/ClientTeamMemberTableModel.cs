﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("ClientTeamMembers", Schema = "dbo")]
    public class ClientTeamMemberTableModel : EmEntityBase
    {
        #region Public Members

        /// <summary>
        /// Suppress unique identifier for this entity from base.
        /// </summary>
        [NotMapped]
        public override long Id { get; set; }

        [ForeignKey("ClientId")]
        public virtual ClientTableModel Client { get; set; }

        [Column(Order = 1)]
        [Key]
        [Required(ErrorMessage = "Required.")]
        public long ClientId { get; set; }

        [ForeignKey("PersonId")]
        public virtual PersonTableModel Person { get; set; }

        [Column(Order = 2)]
        [Key]
        [Required(ErrorMessage = "Required.")]
        public long PersonId { get; set; }

        #endregion
    }
}