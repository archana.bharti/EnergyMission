﻿using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("Clients", Schema = "dbo")]
    public class ClientTableModel : EmEntityBase
    {
        public long? ParentClientId { get; set; }

        [ForeignKey("ParentClientId")]
        public virtual ClientTableModel ParentClient { get; set; }

        public string Name { get; set; }
    }
}