﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("Suburbs", Schema = "dbo")]
    public class SuburbTableModel : EmEntityBase
    {
        #region Public Members

        [MinLength(2, ErrorMessage = "Minimum of 2 characters.")]
        [MaxLength(50, ErrorMessage = "Maximum of 50 characters.")]
        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }

        #endregion
    }
}