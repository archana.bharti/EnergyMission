﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("Sensors", Schema = "dbo")]
    public class SensorTableModel : EmEntityBase
    {
        [Required(ErrorMessage = "Required.")]
        public string Name { get; set; }

    }
}