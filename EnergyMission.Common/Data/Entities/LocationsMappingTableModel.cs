﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EnergyMission.Common.Framework;

namespace EnergyMission.Common.Data.Entities
{
    [Table("LocationMappings", Schema = "dbo")]
    public class LocationMappingTableModel : EmEntityBase
    {
        /// <summary>
        /// Suppress unique identifier for this entity from base.
        /// </summary>
        [NotMapped]
        public override long Id { get; set; }

        
        [ForeignKey("CountryId")]
        public virtual CountryTableModel Country { get; set; }

        [Column(Order = 1)]
        [Key]
        public long CountryId { get; set; }

        [ForeignKey("PostCodeId")]
        public virtual PostCodeTableModel PostCode { get; set; }

        [Key]
        [Column(Order = 2)]
        public long PostCodeId { get; set; }

        [ForeignKey("StateId")]
        public virtual StateTableModel State { get; set; }

        [Key]
        [Column(Order = 3)]
        public long StateId { get; set; }

        [ForeignKey("SuburbId")]
        public virtual SuburbTableModel Suburb { get; set; }

        [Column(Order = 4)]
        [Key]
        public long SuburbId { get; set; }
    }
}