﻿using System.ComponentModel;

namespace EnergyMission.Common.Enums
{
    public enum StatusEnum
    {
        [Description("Request varification")] Request_varification = 0,
        [Description("Awaiting Response")] Awaiting_response = 1,
        [Description("Varified")] Varified = 2,
        [Description("Not Varified")] Not_varified = 3,
        [Description("Unknown")] Unkown = 4,
    }
}