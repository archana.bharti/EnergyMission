using System.ComponentModel;

namespace EnergyMission.Common.Enums
{
    public enum AddressKindEnum
    {
        [Description("Physical")]
        Physical = 1,
        [Description("Postal")]
        Postal = 2   
    }
}