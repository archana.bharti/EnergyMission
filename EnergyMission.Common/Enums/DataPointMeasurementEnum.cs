﻿namespace EnergyMission.Common.Enums
{
    /// <summary>
    ///     Describes the measurement of values within the DataPoint table
    /// </summary>
    public enum DataPointMeasurementEnum
    {
        Unknown = 1,
        KiloWattHours = 2 //KWh
    }
}