﻿using System.ComponentModel;

namespace EnergyMission.Common.Enums
{
    public enum TitleEnum
    {
        [Description("Unknown")] Unknown = -1,
        [Description("Dr")] Dr = 1,
        [Description("Mr")] Mr = 2,
        [Description("Miss")] Miss = 3,
        [Description("Ms")] Ms = 5,
        [Description("Mrs")] Mrs = 6,
        [Description("Prof")] Prof = 7,
    }
}