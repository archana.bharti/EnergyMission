﻿using Abp.Application.Services.Dto;

namespace EnergyMission.Common.Framework
{
    /// <summary>
    ///     Base class for all DTO objects that represent an entity. It
    ///     automatically includes a public property Id of type long
    ///     which matches EmEntityBase
    /// </summary>
    public class EmEntityDtoBase : EntityDto<long>
    {
    }
}