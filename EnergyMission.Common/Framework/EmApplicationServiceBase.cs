﻿using Abp.Application.Services;

namespace EnergyMission.Common.Framework
{
    /// <summary>
    ///     Base class for all application services for the application. Implements IApplicationService
    ///     via descending from ApplicationService.
    /// </summary>
    public class EmApplicationServiceBase : ApplicationService
    {
    }
}