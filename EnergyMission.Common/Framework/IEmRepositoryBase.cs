﻿using System.Linq;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;

namespace EnergyMission.Common.Framework
{
    public interface IEmRepositoryBase<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey> where TEntity : class, IEntity<TPrimaryKey>
    {
        #region Public Members

        /// <summary>
        ///     Same behaviour as GetAll() but also eagerly loads any included entities
        /// </summary>
        /// <returns>Records if found, empty list otherwise</returns>
        IQueryable<TEntity> GetAllWithInclude();

        /// <summary>
        ///     Same behaviour as Get() but also eagerly loads any included entities
        /// </summary>
        /// <param name="aKey">Primary key</param>
        /// <returns>Record if found, NullException otherwise</returns>
        TEntity GetWithInclude(TPrimaryKey aKey);

        #endregion
    }
}