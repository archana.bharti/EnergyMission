﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace EnergyMission.Common.Framework
{
    /// <summary>
    ///     Base class for all entities defined in the system. This allows for the following
    ///     COMMON features:
    ///     - Primary key "Id" of type long
    ///     - Auditing which adds Date & User information for creation and update
    /// </summary>
    public abstract class EmEntityCommon : AuditedEntity<long>
    {
    }

    /// <summary>
    ///     Extends the common functionality by adding the ability to mark a record as deleted rather than
    ///     deleting it from the database. Unless there is a reason not to mark an entity as a soft delete
    ///     then all entities should descend from this class.
    /// </summary>
    public class EmEntityBase : EmEntityCommon, ISoftDelete
    {
        #region Implementation of ISoftDelete

        /// <summary>
        ///     Used to mark an Entity as 'Deleted'.
        /// </summary>
        public bool IsDeleted { get; set; }

        #endregion
    }
}