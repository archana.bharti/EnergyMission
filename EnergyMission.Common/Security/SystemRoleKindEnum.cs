﻿namespace EnergyMission.Common.Security
{
    /// <summary>
    ///     Various roles recognised by the system
    /// </summary>
    public enum SystemRoleKindEnum
    {
        Administrator,
        Client,
        TeamManager,
        TeamMember,
        SuperAdministrator,
        None
    }
}